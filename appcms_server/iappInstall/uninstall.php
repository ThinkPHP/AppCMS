<?php
	@set_time_limit(1000);
	if(phpversion() < '5.3.0') set_magic_quotes_runtime(0);
	if(phpversion() < '5.2.0') exit('您的php版本过低，不能安装本软件，请升级到5.2.0或更高版本再安装，谢谢！');
	include '../phpcms/base.php';
	defined('IN_PHPCMS') or exit('No permission resources.');
	
	if(file_exists(CACHE_PATH.'iappInstall.lock')) exit('欢迎使用APPCMS卸载模块，卸载会删除appcms所有数据，并且不能恢复,如果要卸载，请先删除 ./caches/iappInstall.lock 文件！');
	
	pc_base::load_sys_class('param','','','0');
	pc_base::load_sys_func('global');
	pc_base::load_sys_func('dir');

	$_module = array(
					array('m'=>'iapp','n'=>'APPCMS主程序'),
					array('m'=>'model','n'=>'模型管理'),
					array('m'=>'category','n'=>'栏目管理'),
					array('m'=>'iappActivity','n'=>'活动'),
					array('m'=>'iappApps','n'=>'应用'),
					array('m'=>'iappBus','n'=>'汽车时刻'),
					array('m'=>'iappCoupons','n'=>'优惠券'),
					array('m'=>'iappJifen','n'=>'积分商城'),
					array('m'=>'iappPush','n'=>'推送'),
					array('m'=>'iappShare','n'=>'分享'),
					array('m'=>'iappSpecial','n'=>'手机专题'),
					array('m'=>'iappTalk','n'=>'说说'),
					array('m'=>'iappTel','n'=>'采用电话'),
					array('m'=>'iappToday','n'=>'今日'),
					array('m'=>'iappTrain','n'=>'火车时刻'),
					array('m'=>'iappVideo','n'=>'视频'),
					array('m'=>'iappYp','n'=>'商家'),
					array('m'=>'iappMenu','n'=>'菜单')
				);
	function delete_install($dir) {
		$dir = dir_path($dir);
		if (!is_dir($dir)) return FALSE;
		$list = glob($dir.'*');
		foreach($list as $v) {
			is_dir($v) ? delete_install($v) : @unlink($v);
		}
		return @rmdir($dir);
	}
	
	$step = trim($_REQUEST['step']) ? trim($_REQUEST['step']) : 1;
	$page = trim($_GET['page'])? intval($_GET['page']):0;
	
	switch($step)
	{
		case '1': 
			$html = '<h1>APPCMS卸载</h1>
					<div class="content">
						<h3  style="color:#F00;">警告：</h3>
						<div class="txt">本操作会删除模块所有数据，并且不能恢复。</div>
					</div>
					<div class="f"><div class="btn">
						<a href="'.APP_PATH.'iappInstall/uninstall.php?step=2">确认卸载</a>
					</div></div>';
		break;
		case '2':
				
			$api_module = pc_base::load_app_class('module_api','admin');
	
			foreach($_module as $k=>$r){
				if(module_exists($r[m])){
					$api_module->uninstall($r[m]);
				}
			}
			
			//删除安装目录
			file_put_contents(CACHE_PATH.'iappInstall.lock','');
			delete_install(PHPCMS_PATH.'iappInstall/');
			
			$cache = pc_base::load_app_class('cache_api','admin');
			
			$cache->cache('category');
			$cache->cache('cache_site');		 
			$cache->cache('downservers');
			$cache->cache('badword');
			$cache->cache('ipbanned');
			$cache->cache('keylink');
			$cache->cache('linkage');
			$cache->cache('position');
			$cache->cache('admin_role');
			$cache->cache('urlrule');
			$cache->cache('module');
			$cache->cache('sitemodel');
			$cache->cache('workflow');
			$cache->cache('dbsource');
			$cache->cache('member_group');
			$cache->cache('membermodel');
			$cache->cache('type','search');
			$cache->cache('special');
			$cache->cache('setting');
			$cache->cache('database');
			$cache->cache('member_setting');
			$cache->cache('member_model_field');
			$cache->cache('search_setting');
					 
			$html = 'APPCMS已经卸载成功, <a href="'. APP_PATH.'index.php?m=admin">登陆后台</a>！';
			
		break;
	}
?>	


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>APPCMS安装</title>
<link rel="stylesheet" href="css.css">
</head>
<body>
<div class="w">
	<div class="box" style="c">
		<?php echo $html;?>
	</div>
</div>
</body>
</html>