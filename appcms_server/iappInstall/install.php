<?php
	@set_time_limit(1000);
	if(phpversion() < '5.3.0') set_magic_quotes_runtime(0);
	if(phpversion() < '5.2.0') exit('您的php版本过低，不能安装本软件，请升级到5.2.0或更高版本再安装，谢谢！');
	include '../phpcms/base.php';
	defined('IN_PHPCMS') or exit('No permission resources.');
	if(file_exists(CACHE_PATH.'iappInstall.lock')) exit('您已经安装过APPCMS,如果需要重新安装，请删除 ./caches/iappInstall.lock 文件！');
	
	pc_base::load_sys_class('param','','','0');
	pc_base::load_sys_func('global');
	pc_base::load_sys_func('dir');

	$_module = array(
					array('m'=>'iapp','n'=>'APPCMS主程序'),
					array('m'=>'model','n'=>'模型管理'),
					array('m'=>'category','n'=>'栏目管理'),
					array('m'=>'iappActivity','n'=>'活动'),
					array('m'=>'iappApps','n'=>'应用'),
					array('m'=>'iappBus','n'=>'汽车时刻'),
					array('m'=>'iappCoupons','n'=>'优惠券'),
					array('m'=>'iappJifen','n'=>'积分商城'),
					array('m'=>'iappPush','n'=>'推送'),
					array('m'=>'iappShare','n'=>'分享'),
					array('m'=>'iappSpecial','n'=>'手机专题'),
					array('m'=>'iappTalk','n'=>'说说'),
					array('m'=>'iappTel','n'=>'采用电话'),
					array('m'=>'iappToday','n'=>'今日'),
					array('m'=>'iappTrain','n'=>'火车时刻'),
					array('m'=>'iappVideo','n'=>'视频'),
					array('m'=>'iappYp','n'=>'商家'),
					array('m'=>'iappMenu','n'=>'菜单')
				);

		
	function delete_install($dir) {
		$dir = dir_path($dir);
		if (!is_dir($dir)) return FALSE;
		$list = glob($dir.'*');
		foreach($list as $v) {
			is_dir($v) ? delete_install($v) : @unlink($v);
		}
		return @rmdir($dir);
	}

	function stats_install() {
		$http = pc_base::load_sys_class('http');
		$url = 'http://www.appcms.org/index.php?m=license&c=index&a=stats_add';
		$data = array(
				'url'=>$_SERVER['SERVER_NAME'],
				'ip'=>ip(),
				'content'=>array2string($_SERVER)
				);
		$http->post($url,$data);
		if($http->is_ok()) {
			
		}else{
			$http->post($url,$data);
		}
	}
	
	function format_textarea($string) {
		$chars = 'utf-8';
		if(CHARSET=='gbk') $chars = 'gb2312';
		return nl2br(str_replace(' ', '&nbsp;', htmlspecialchars($string,ENT_COMPAT,$chars)));
	}
	
	$step = trim($_REQUEST['step']) ? trim($_REQUEST['step']) : 1;
	$page = trim($_GET['page'])? intval($_GET['page']):0;
	
	switch($step)
	{
		case '1': 
			$license = file_get_contents(PHPCMS_PATH."iappInstall/license.txt");
			$html = '<h1>APPCMS安装</h1>
					<div class="content">
						<h3>APPCMS软件许可协议：</h3>
						<div class="txt">'.format_textarea($license).'</div>
					</div>
					<div class="f"><div class="btn">
						<a href="'.APP_PATH.'iappInstall/install.php?step=2">开始安装</a>
					</div></div>';
		break;
		case '2':
			$li = '';
			foreach($_module as $k=>$r){
				$li .= '<li>·'.$r[n].'</li>';
			}
								
			$html = '<h1>APPCMS安装</h1>
					<div class="content">
						<h3>感谢你选择APPCMS,本软件包含以下模块：</h3>
						<ul>'.$li.'</ul>
					</div>
					<div class="f"><div class="btn">
						<a href="'.APP_PATH.'iappInstall/install.php?page=0&step=3">安装</a>
					</div></div>';
		break;
		case '3':
			if(module_exists('admin')){
				$pagemax = count($_module)-1;

				
				if($page <$pagemax){
					$m = $_module[$page][m];
					$n = $_module[$page][n];
					
					$api_module = pc_base::load_app_class('module_api','admin');
					
					if(module_exists($m)){
						$api_module->uninstall($m);
					}
					
					if($api_module->check($m)){
						$s = $api_module->install($m);
						if($s){
							$s = '安装成功！';
						}else{
							$s = '安装失败！';
						}
					}else{
						$s = '不存在，跳过安装！';
					}
					
					$page++;
					$url = APP_PATH.'iappInstall/install.php?page='.$page.'&step=3';

					$js = '<SCRIPT LANGUAGE="javascript">
								location.href="'.$url.'";
							</SCRIPT>';

					$html ='<h1>APPCMS安装</h1>
							<div class="content">
								<h3>正在安装模块，稍后自动跳转安装下一模块：</h3>
								<p>'.$n.'模块'.$s.'['.$page.'/'.$pagemax.']'. $js.'</p>
							</div>
							<div class="f"><div class="btn">
								<a href="'.$url.'">没有跳转，点这里手动安装</a>
							</div></div>';
					
				}else{
					
					//删除安装目录
					file_put_contents(CACHE_PATH.'iappInstall.lock','');
					delete_install(PHPCMS_PATH.'iappInstall/');
					
					$cache = pc_base::load_app_class('cache_api','admin');
					
					$cache->cache('category');
					$cache->cache('cache_site');		 
					$cache->cache('downservers');
					$cache->cache('badword');
					$cache->cache('ipbanned');
					$cache->cache('keylink');
					$cache->cache('linkage');
					$cache->cache('position');
					$cache->cache('admin_role');
					$cache->cache('urlrule');
					$cache->cache('module');
					$cache->cache('sitemodel');
					$cache->cache('workflow');
					$cache->cache('dbsource');
					$cache->cache('member_group');
					$cache->cache('membermodel');
					$cache->cache('type','search');
					$cache->cache('special');
					$cache->cache('setting');
					$cache->cache('database');
					$cache->cache('member_setting');
					$cache->cache('member_model_field');
					$cache->cache('search_setting');
					
					$html = '<h1>APPCMS安装</h1>
							 <div class="content">
								<h3 style="color:#090">感谢你选择APPCMS！</h3>
								<p>恭喜您，APPCMS安装成功!</p>
							 </div>
							 <div class="f"><div class="btn">
							 <a href="'. APP_PATH.'index.php?m=admin">登陆后台</a>
							 </div></div>';
				}
			
			}else{	
				$html ='<h1>APPCMS安装</h1>
						<div class="content">
							<h3 style="color:#F00;">错误！</h3>
							<p>本软件为PHPCMS模块，依赖于PHPCMS基础框架，请先安装PHPCMS才能安装本软件!</p>
						</div>';
			}
			
		break;
	}
?>	


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>APPCMS安装</title>
<link rel="stylesheet" href="css.css">
</head>
<body>
<div class="w">
	<div class="box" style="c">
		<?php echo $html;?>
	</div>
</div>
</body>
</html>