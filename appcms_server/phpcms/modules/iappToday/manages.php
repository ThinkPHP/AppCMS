<?php 
defined('IN_PHPCMS') or exit('No permission resources.'); 
pc_base::load_app_class('admin','admin',0);
pc_base::load_sys_class('form', '', 0);
class manages extends admin {
	function __construct() {
		parent::__construct();
		//$this->sites = pc_base::load_app_class('sites','admin');
		$this->db = pc_base::load_model('iappToday_model');
		$this->iappconfig = getcache('iapp', 'commons');
		$this->siteid = $this->get_siteid();
	}
	
	function init() {
		$page = max(intval($_GET['page']), 1);
		$data = $this->db->listinfo(array(), '`id` DESC', $page);
		include $this->admin_tpl('list');
	}
	
	function add() {
		if($_POST['dosubmit']) {
			$info = array();
			$topinfo = $_POST['top'];
			$topinfo[thumb] = thumb($topinfo[thumb],615,300);
			
			$info[name] = $topinfo[title];
			
			$listinfo = $_POST['list'];
			foreach($listinfo as $k=>$v){
				$listinfo[$k][thumb] = thumb($v[thumb],100,100);
			}
			
			$data = array('top'=>$topinfo,'list'=>$listinfo);
			
			$info[datas] = array2string($data);
			$info[setting] = array2string($info[setting]);
			$info[addtime] = time();
		
			$return_id = $this->db->insert($info,'1');
			showmessage(L('add_success'), '?m=iappToday&c=manages&a=init&menuid='.$_GET['menuid']);
		} else {
			include $this->admin_tpl('add');			
		}		
	}
	
	function edit() {
		$id = intval($_GET['id']) ? intval($_GET['id']) : showmessage('参数错误',HTTP_REFERER);
		if($_POST['dosubmit']) {
			$info = array();
			$topinfo = $_POST['top'];
			$topinfo[thumb] = thumb($topinfo[thumb],615,300);
			$info[name] = $topinfo[title];
			
			$listinfo = $_POST['list'];
			foreach($listinfo as $k=>$v){
				$listinfo[$k][thumb] = thumb($v[thumb],100,100);
			}
			
			$data = array('top'=>$topinfo,'list'=>$listinfo);
			
			$info[datas] = array2string($data);
			$info[setting] = array2string($info[setting]);
			
			$this->db->update($info, array('id'=>$id));
			
			showmessage(L('operation_success'), '?m=iappToday&c=manages&a=edit&id='.$id.'&menuid='.$_GET['menuid']);
			
		}else{
			$info = $this->db->get_one(array('id'=>$id));
			$info[datas] = string2array($info[datas]);
			$info[setting] = string2array($info[setting]);
			
			if($info) {
				include $this->admin_tpl('edit');
			}else{
				showmessage(L('add_success'), '?m=iappToday&c=manages&a=init');
			}
		}		
		
	}
	
	public function delete() {
		if (isset($_GET['id']) && !empty($_GET['id'])) {
			$id = intval($_GET['id']);
			$this->db->delete(array('id'=>$id));
			showmessage(L('operation_success'), HTTP_REFERER);
		} elseif (isset($_POST['ids']) && !empty($_POST['ids'])) {
			if (is_array($_POST['ids'])) {
				foreach ($_POST['ids'] as $fid) {
					$this->db->delete(array('id'=>$fid));
				}
			}
			showmessage(L('operation_success'), HTTP_REFERER);
		} else {
			showmessage(L('illegal_operation'), HTTP_REFERER);
		}
	}
	
	
	function getlist() {
		$show_header = '';
		$page = max(intval($_GET['page']), 1);
		$catid = intval($_GET['catid'])? intval($_GET['catid']):$this->iappconfig[setting][defaultcatid];
		$cdb = pc_base::load_model('content_model');
		$CATEGORYS = getcache('category_content_'.$this->siteid,'commons');
		$modelid = $CATEGORYS[$catid]['modelid'];
		$cdb->set_model($modelid);
		
		$infos = $cdb->listinfo(array('catid'=>$catid), '`id` DESC', $page,7);
		
		include $this->admin_tpl('getlist');
	}
	
}
?>