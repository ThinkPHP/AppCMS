<?php
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');

$menus = $menu_db->select(array('name'=>'iappPlug', 'm'=>'iapp', 'c'=>'manages', 'a'=>'init'));

$parentid = $menu_db->insert(array('name'=>'iappToday_add ', 'parentid'=>$menus[0][id], 'm'=>'iappToday', 'c'=>'manages', 'a'=>'add', 'data'=>'', 'listorder'=>1, 'display'=>'1'), true);

$menu_db->insert(array('name'=>'iappToday', 'parentid'=>$parentid, 'm'=>'iappToday', 'c'=>'manages', 'a'=>'init', 'data'=>'', 'listorder'=>2, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappToday_edit', 'parentid'=>$parentid, 'm'=>'iappToday', 'c'=>'manages', 'a'=>'edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappToday_delete', 'parentid'=>$parentid, 'm'=>'iappToday', 'c'=>'manages', 'a'=>'delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));

$language = array('iappToday'=>'推荐历史',
				  'iappToday_add'=>'今日推荐',
				  'iappToday_edit'=>'修改',
				  'iappToday_delete'=>'删除'				  
                  );

?>