DROP TABLE IF EXISTS `phpcms_iappVideo_category`;
CREATE TABLE IF NOT EXISTS `phpcms_iappVideo_category` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `listorder` smallint(5) DEFAULT '0',
  `setting` mediumtext,
  PRIMARY KEY (`id`)
) TYPE=MyISAM;
