<?php
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');

$menus = $menu_db->select(array('name'=>'iappPlug', 'm'=>'iapp', 'c'=>'manages', 'a'=>'init'));
if($menus){
	$menuid=$menus[0][id];
}else{
	$menuid=821;
}

$parentid = $menu_db->insert(array('name'=>'iappVideo_manage', 'parentid'=>$menuid, 'm'=>'iappVideo', 'c'=>'manages', 'a'=>'init', 'data'=>'', 'listorder'=>0, 'display'=>'1'), true);
$menu_db->insert(array('name'=>'iappVideo_add', 'parentid'=>$parentid, 'm'=>'iappVideo', 'c'=>'manages', 'a'=>'add', 'data'=>'', 'listorder'=>0, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappVideo_import', 'parentid'=>$parentid, 'm'=>'iappVideo', 'c'=>'manages', 'a'=>'import_56video', 'data'=>'', 'listorder'=>1, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappVideo_setting', 'parentid'=>$parentid, 'm'=>'iappVideo', 'c'=>'manages', 'a'=>'setting', 'data'=>'', 'listorder'=>2, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappVideo_edit', 'parentid'=>$parentid, 'm'=>'iappVideo', 'c'=>'manages', 'a'=>'edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappVideo_delete', 'parentid'=>$parentid, 'm'=>'iappVideo', 'c'=>'manages', 'a'=>'delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappVideo_view', 'parentid'=>$parentid, 'm'=>'iappVideo', 'c'=>'manages', 'a'=>'public_view_video', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappVideo_preview_56video', 'parentid'=>$parentid, 'm'=>'iappVideo', 'c'=>'manages', 'a'=>'preview_56video', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappVideo_doimport', 'parentid'=>$parentid, 'm'=>'iappVideo', 'c'=>'manages', 'a'=>'doimport', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappVideo_insert', 'parentid'=>$parentid, 'm'=>'iappVideo', 'c'=>'video_for_ck', 'a'=>'init', 'data'=>'', 'listorder'=>0, 'display'=>'0'));


$menu_db->insert(array('name'=>'iappVideo_category_list', 'parentid'=>$parentid, 'm'=>'iappVideo', 'c'=>'manages', 'a'=>'category_list', 'data'=>'', 'listorder'=>6, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappVideo_category_add', 'parentid'=>$parentid, 'm'=>'iappVideo', 'c'=>'manages', 'a'=>'category_add', 'data'=>'', 'listorder'=>5, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappVideo_category_edit', 'parentid'=>$parentid, 'm'=>'iappVideo', 'c'=>'manages', 'a'=>'category_edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappVideo_category_delete', 'parentid'=>$parentid, 'm'=>'iappVideo', 'c'=>'manages', 'a'=>'category_delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappVideo_category_listorder', 'parentid'=>$parentid, 'm'=>'iappVideo', 'c'=>'manages', 'a'=>'category_listorder', 'data'=>'', 'listorder'=>0, 'display'=>'0'));






$language = array('iappVideo_manage'=>'IAPP视频', 
				    'iappVideo_add'=>'添加视频' ,
				    'iappVideo_import'=>'56视频导入',
					'iappVideo_setting'=>'视频设置',
				    'iappVideo_edit'=>'视频编辑',
					'iappVideo_delete'=>'视频删除',
					'iappVideo_view'=>'视频预览', 
					'iappVideo_preview_56video'=>'视频预览',
					'iappVideo_doimport'=>'导视频入库',
					'iappVideo_insert'=>'56视频插入',
					
					'iappVideo_category_add'=>'添加分类',
					'iappVideo_category_edit'=>'修改分类',
					'iappVideo_category_list'=>'分类列表',
					'iappVideo_category_delete'=>'删除分类',
					'iappVideo_category_listorder'=>'分类排序'  
					);
 
?>