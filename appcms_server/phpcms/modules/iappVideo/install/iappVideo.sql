DROP TABLE IF EXISTS `phpcms_iappVideo`;
CREATE TABLE `phpcms_iappVideo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '本地视频ID',
  `catid` smallint(5) NOT NULL COMMENT '栏目',
  `userid` int(10) unsigned default '0' COMMENT '用户ID',
  `username` varchar(20) default NULL COMMENT '用户名',
  
  `vid` int(11) unsigned NOT NULL COMMENT '56vid',
  `title` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '视频名称',
  `content` text CHARACTER SET utf8 NOT NULL COMMENT '视频简介',
  `tags` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '视频标签',
  `url_56` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '56视频地址',
  `player` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '56swf播放地址',
  `totaltime` int(11) unsigned NOT NULL COMMENT '视频时长',
  `thumb` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '封面图',
  
  `video_url` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '视频地址',
  
  `url` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT 'url地址',
  `collection` smallint(5) DEFAULT '0' COMMENT '收藏量',
  `hits` smallint(5) DEFAULT '0' COMMENT '查看量',
  `praise` smallint(5) DEFAULT '0' COMMENT '赞',
  `comment` smallint(5) DEFAULT '0' COMMENT '评论量',

  `forbid` char(1) CHARACTER SET utf8 NOT NULL COMMENT 'y-被屏蔽了，n-正常',
  `chk` char(1) CHARACTER SET utf8 NOT NULL COMMENT 'y-通过审核，n-待审',
  `coopid` varchar(15) CHARACTER SET utf8 DEFAULT NULL COMMENT 'coopid',
  `vtypes` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否原创视频',
  
  `addtime` int(11) unsigned NOT NULL COMMENT '添加时间',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM;