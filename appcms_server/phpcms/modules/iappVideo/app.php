<?php 
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_sys_class('format', '', 0);

class app {
	function __construct() {		
		$this->db = pc_base::load_model('iappVideo_model');
	}
	
	public function init() {
		echo "not init";
	}
	
	public function lists() {
		$page = max(intval($_GET['page']), 1);
		$catid = intval($_GET['catid']);
		$order = trim($_GET['order']) ? trim($_GET['order']) : null;
		
		if($catid>0){
			$_sql = "catid= $catid ";
		}else{
			$_sql='';
		}
		
		$_order = 'id DESC';
		if($order =='hot'){
			$_order = 'hits DESC';
		}
		$data = $this->db->listinfo($_sql, $_order, $page,5);

		$list = array();
		foreach ($data as $k=>$v) {
			$list[$k][id] = $v[id];
			$list[$k][catid] = $v[catid];
			$list[$k][title] = $v[title];
			$list[$k][thumb] = thumb($v[thumb],600,226);
			$list[$k][collection] = $v[collection];
			$list[$k][hits] = $v[hits];
			$list[$k][praise] = $v[praise];
			$list[$k][comment] = $v[comment];
		}
		$return_id = $this->db->update(array('hits'=>'+=1'),'1=1');
		echo json_encode(array("stime"=>time(),"list"=>$list));
	}
	
	public function show() {
		$re = array();
		$id = intval($_GET['id']) ? intval($_GET['id']) : null;
		if($id){
			$this->db->update(array('hits'=>'+=1'),array('id'=>$id));
			$data = $this->db->get_one(array('id'=>$id));
			$data[url] = str_replace("&c=index", "&c=touch", $data[url]);
			
			//print_r($data);
			
			if($data[vid]>0){
				if($data[video_url]==''){
					pc_base::load_app_class('api56', 'iappVideo', 0);
					$video56info = Open::Video_Mobile(array('vid'=>$data[vid]));
					$data[video_url] = $video56info[info][rfiles][0][url];
				}
				$data[thumb] = $video56info[info][bimg];
				$data[videodata] = $video56info[info][rfiles];
				
			}
			
			if($data){
				$re[status]="OK";
				$re[data]=$data;
				$re[msg]="数据加载成功~";
			}else{
				$re[status]="NODATA";
				$re[msg]="视频不存在~";
			}
		}else{
			$re[status]="ERR";
			$re[msg]="参数错误~";
		}
		
		//print_r($re);
		
		echo json_encode($re);
	}
	
}
?>