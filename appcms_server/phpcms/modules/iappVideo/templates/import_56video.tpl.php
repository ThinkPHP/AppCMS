<?php 
	defined('IN_ADMIN') or exit('No permission resources.');
	include $this->admin_tpl('header','admin');
?>
<div class="pad_10">
<div class="table-list">
<form name="searchform" action="" method="get" >
<input type="hidden" value="iappVideo" name="m">
<input type="hidden" value="manages" name="c">
<input type="hidden" value="import_56video" name="a">
<input type="hidden" value="<?php echo $_GET['menuid']?>" name="menuid">
<div class="explain-col search-form">
导入56视频：
频道
<select name="c56">
<option value="0" <?php if($_GET['c56']=='0' || empty($_GET['c56'])) echo "selected";?> >全部</option>
<option value="1" <?php if($_GET['c56']=='1') echo "selected";?> >娱乐</option>
<option value="2" <?php if($_GET['c56']=='2') echo "selected";?> >资讯</option>
<option value="3" <?php if($_GET['c56']=='3') echo "selected";?> >原创</option>
<option value="4" <?php if($_GET['c56']=='4') echo "selected";?> >搞笑</option>
<option value="8" <?php if($_GET['c56']=='8') echo "selected";?> >动漫</option>
<option value="10" <?php if($_GET['c56']=='10') echo "selected";?> >科教</option>
<option value="11" <?php if($_GET['c56']=='11') echo "selected";?> >女性</option>
<option value="14" <?php if($_GET['c56']=='14') echo "selected";?> >体育</option>
<option value="26" <?php if($_GET['c56']=='26') echo "selected";?> >游戏</option>
<option value="27" <?php if($_GET['c56']=='27') echo "selected";?> >旅游</option>
<option value="28" <?php if($_GET['c56']=='28') echo "selected";?> >汽车</option>
<option value="34" <?php if($_GET['c56']=='34') echo "selected";?> >亲子</option>
<option value="39" <?php if($_GET['c56']=='39') echo "selected";?> >财富</option>
<option value="40" <?php if($_GET['c56']=='40') echo "selected";?> >科技</option>
<option value="41" <?php if($_GET['c56']=='41') echo "selected";?> >音乐</option>
</select>
时间排序：
<select name="type">
<option value="hot" <?php if($_GET['type']=='hot' || empty($_GET['type'])) echo "selected";?> >最多观看</option>
<option value="ding" <?php if($_GET['type']=='ding') echo "selected";?> >最多推荐</option>
<option value="new" <?php if($_GET['type']=='new') echo "selected";?> >最新发布</option>
<option value="share" <?php if($_GET['type']=='share') echo "selected";?> >最多引用</option>
<option value="comment" <?php if($_GET['type']=='comment') echo "selected";?> >最多评论</option>
</select>

时间排序：
<select name="t">
<option value="" <?php if($_GET['t']=='') echo "selected";?> >全部</option>
<option value="today" <?php if($_GET['t']=='today') echo "selected";?> >今日</option>
<option value="week" <?php if($_GET['t']=='week') echo "selected";?> >本周</option>
<option value="month" <?php if($_GET['t']=='month') echo "selected";?> >本月</option>
</select>
关键词：<input type="text" value="<?php echo $_GET['keyword'];?>" class="input-text" name="keyword"> 
<input type="submit" value="<?php echo L('search')?>" class="button" name="dosubmit">
</div>
</form>
 
<form name="myform" id="myform" action="index.php?m=iappVideo&c=manages&a=doimport&pc_hash=<?php echo $_GET['pc_hash'];?>" method="post" >
<div class="table-list">
    <table width="100%">
        <thead>
            <tr>
			 <th width="16"><input type="checkbox" value="" id="check_box" onclick="selectall('ids[]');"></th>
 			<th width="100">缩略图</th>
			<th>标题</th>
            <th width="130">VID</th>
            <th width="90">上传者</th>
            </tr>
        </thead>
<tbody>
    <?php
	if(is_array($list)) {
		$k = 0;
		foreach ($list as $r) {
	?>
        <tr>
		<td align="center">
		<input class="inputcheckbox " name="ids[]" value="<?php echo $r['id'];?>" type="checkbox">
		<input type="hidden" name="importdata[<?php echo $k;?>][vid]" value="<?php echo $r['id'];?>">
		<input type="hidden" name="importdata[<?php echo $k;?>][tags]" value="<?php echo safe_replace($r['tag']);?>">
		<input type="hidden" name="importdata[<?php echo $k;?>][cover]" value="<?php echo $r['img'];?>">
		</td>
 		<td><a href="javascript:void(0)" onclick="preview('<?php echo $r['id'];?>','<?php echo str_cut($r['Subject'],80);?>')" title="<?php echo $r['Subject'];?>"><img src="<?php echo $r['img'];?>" style="margin-right: 6px;" width="132" height="99"></a></td>
		<td>
		<ul class="title_ul">
		<li><input type="text" size="30" title="" value="<?php echo $r['Subject'];?>" class="input-text" name="importdata[<?php echo $k;?>][subject]">  
		</li>
		</ul>
		</td>
		<td align='center'><?php echo $r['id'];?></td>
		<td align='center'><?php echo $r['user_id']?></td>
		</tr>
     <?php $k++;} }
 	 ?>
</tbody>
     </table>
    <div class="btn"><label for="check_box"><?php echo L('selected_all');?>/<?php echo L('cancel');?></label>
		<input name="dosubmit" type="button" value="导入选择视频" class="button" onclick="check_sbumit();">  
		
	</div>
   <div id="pages"><?php echo pages($totals,$page,$pagesize);?></div>
</div>
</form>

 
 </div>
</form>
</body>
</html>
<script type="text/javascript">
<!--
//检查选中
function check_sbumit() {
	var str = 0;
 	$("input[name='ids[]']").each(function() {
		if($(this).attr('checked')=='checked') {
			str = 1; 
		}
	});
	if(str==0) {
		alert('您没有勾选信息');
		return false;
	}

 	document.myform.submit(); 
}


//预览视频
function preview(vid, name) {
	window.top.art.dialog({id:'preview'}).close();
	window.top.art.dialog({title:'预览 '+name+' ',id:'preview',iframe:'?m=iappVideo&c=manages&a=preview_56video&vid='+vid,width:'530',height:'350'}, '', function(){window.top.art.dialog({id:'preview'}).close()});
}

//-->
</script>