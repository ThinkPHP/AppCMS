<?php 
	defined('IN_ADMIN') or exit('No permission resources.');
	include $this->admin_tpl('header', 'admin');
?>
<div class="pad-lr-10">
<div class="col-tab">
    <ul class="tabBut cu-li">
      <li id="tab_setting_1" class="on" onclick="SwapTab('setting','on','',2,1);">56上传</li>
      <li id="tab_setting_2" onclick="SwapTab('setting','on','',2,2);">本地上传</li>
    </ul>
    <div id="div_setting_1" class="contentList pad-10">
		<iframe scrolling="no" frameborder="0" src="<?php echo Open::GetPluginApi('Video/Diyupload',$params);?>" id="theatre-panel" style="width: 600px; height: 450px; "></iframe>
	</div>
    <div id="div_setting_2" class="contentList pad-10 hidden">
		<form name="myform" action="?m=iappVideo&c=manages&a=add" method="post" id="myform">
			<table width="100%" class="table_form">
			<tr>
			  <th width="100"><?php echo L('分类名称')?>：</th>
			  <td><select name="info[catid]" id="">
				  <option value="0">默认分类</option>
				  <?php
			  $i=0;
			  foreach($iappVideo_category as $catid=>$type){
			  $i++;
			?>
				  <option value="<?php echo $type['id'];?>"><?php echo $type['title'];?></option>
				  <?php }?>
				</select></td>
			</tr>
		
			<tr>
			<tr>
			<th >视频名称</th> 
			<td><input type="text" name="data[title]" size="40" value="<?php echo $info['title'];?>" id="title"></td>
			</tr>
			<tr>
			<tr>
			<th >上传视频</th> 
			<td>
			<?php echo form::upfiles('data[video_url]', 'video_url', $info[video_url], 'iappVideo','','','','','MP4')?>
			</td>
			</tr>
			<tr>
			<th>视频图片</th> 
			<td>
			<?php echo form::images('data[thumb]', 'thumb', $info[thumb], 'iappVideo')?>
			</td>
			</tr>
			<tr>
			<th>视频标签</th> 
			<td><input type="text" id="tags" name="data[tags]"  size="30" value="<?php echo $info['tags']?>"> 用, 号隔开</td>
			</tr>
			
			<th>视频简介</th> 
			<td><textarea id="content" name="data[content]" rows="5" cols="50"><?php echo $info['content']?></textarea></td>
			</tr>
			
			
			</table>
			<div class="bk15"></div>
			<input type="hidden" name="vid" id="vid" value="<?php echo $info['vid'];?>">
			<input name="dosubmit" type="submit" value="<?php echo L('submit')?>" class="button" id="dosubmit">
		</form>
	</div>
  </div>
</body>
<script type="text/javascript">
function SwapTab(name,cls_show,cls_hide,cnt,cur){
	for(i=1;i<=cnt;i++){
		if(i==cur){
			 $('#div_'+name+'_'+i).show();
			 $('#tab_'+name+'_'+i).attr('class',cls_show);
		}else{
			 $('#div_'+name+'_'+i).hide();
			 $('#tab_'+name+'_'+i).attr('class',cls_hide);
		}
	}
}
</script>
</html>