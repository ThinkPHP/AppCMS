<?php 
	defined('IN_ADMIN') or exit('No permission resources.');
	include $this->admin_tpl('header', 'admin');
?>
</script>
<div class="pad-10">
<div class="common-form">
<form name="myform" action="?m=iappVideo&c=manages&a=edit&id=<?php echo $info['id'];?>" method="post" id="myform">
<table width="100%" class="table_form">

<tr>
          <th width="100"><?php echo L('分类名称')?>：</th>
          <td>
			<select name="info[catid]" id="">
				<option value="0">默认分类</option>
				<?php
				  foreach($iappVideo_category as $catid=>$type){
				?>
				<option value="<?php echo $type['id'];?>" <?php if($type['id']==$info[catid]){echo 'selected=""';}?> ><?php echo $type['title'];?></option>
				<?php }?>
			</select>
			</td>
        </tr>
		
			<tr>
			<th >视频名称</th> 
			<td><input type="text" name="data[title]" size="40" value="<?php echo $info['title'];?>" id="title"></td>
			</tr>
			<tr>
			<tr>
			<th >上传视频</th> 
			<td>
			<?php echo form::upfiles('data[video_url]', 'video_url', $info[video_url], 'iappVideo','','','','','MP4')?>
			</td>
			</tr>
			<tr>
			<th >视频图片</th> 
			<td>
			<?php echo form::images('data[thumb]', 'thumb', $info[thumb], 'iappVideo')?>
			</td>
			</tr>
			<tr>
			<th >视频标签</th> 
			<td><input type="text" id="tags" name="data[tags]"  size="30" value="<?php echo $info['tags']?>"> 用, 号隔开</td>
			</tr>
			
			<th >视频简介</th> 
			<td><textarea id="content" name="data[content]" rows="5" cols="50"><?php echo $info['content']?></textarea></td>
			</tr>


</table>
<div class="bk15"></div>
<input name="dosubmit" type="submit" value="<?php echo L('submit')?>" class="button" id="dosubmit">
</form>
</div>
</body>
</html>