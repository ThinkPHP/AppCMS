<?php 
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_func('global');
pc_base::load_sys_class('format', '', 0);
class touch {
	function __construct() {		
		$this->db = pc_base::load_model('iappVideo_model');
		$this->hitsdb = pc_base::load_model('hits_model');
		
	}
	
	public function init() {
		$re = '';
		$config = $this->config;
		$SEO = seo($this->siteid);
		include template('iapp', 'index','touch');
	}
	
	/*
	* 获取文章内容数据接口
	*/
	public function show() {
		$id = intval($_GET['id']) ? intval($_GET['id']) : null;
		$this->db->update(array('hits'=>'+=1'),array('id'=>$id));
		$data = $this->db->get_one(array('id'=>$id));
		//print_r($data);
		include template('iapp', 'show_video','touch');

	}
	
	/**
	 * 获取点击数量
	 * @param $id
	 * @param $modelid
	 */
	public function hits($id,$modelid) {
		$hitsid = 'c-'.$modelid.'-'.intval($id);
		if($modelid && $id) {
			$r = $this->hitsdb->get_one(array('hitsid'=>$hitsid));
			if(!$r) return false;
			$views = $r['views'] + 1;
			$yesterdayviews = (date('Ymd', $r['updatetime']) == date('Ymd', strtotime('-1 day'))) ? $r['dayviews'] : $r['yesterdayviews'];
			$dayviews = (date('Ymd', $r['updatetime']) == date('Ymd', SYS_TIME)) ? ($r['dayviews'] + 1) : 1;
			$weekviews = (date('YW', $r['updatetime']) == date('YW', SYS_TIME)) ? ($r['weekviews'] + 1) : 1;
			$monthviews = (date('Ym', $r['updatetime']) == date('Ym', SYS_TIME)) ? ($r['monthviews'] + 1) : 1;
			$sql = array('views'=>$views,'yesterdayviews'=>$yesterdayviews,'dayviews'=>$dayviews,'weekviews'=>$weekviews,'monthviews'=>$monthviews,'updatetime'=>SYS_TIME);
			$this->hitsdb->update($sql, array('hitsid'=>$hitsid));
		}
		return $views;	
	}
}
?>