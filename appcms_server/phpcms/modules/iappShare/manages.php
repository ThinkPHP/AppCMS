<?php 
defined('IN_PHPCMS') or exit('No permission resources.'); 
pc_base::load_app_class('admin','admin',0);
pc_base::load_sys_class('form', '', 0);
class manages extends admin {
	function __construct() {
		parent::__construct();
		$this->sites = pc_base::load_app_class('sites','admin');
		$this->db = pc_base::load_model('iappShare_model');
	}
	
	function init() {
		$page = max(intval($_GET['page']), 1);
		$data = $this->db->listinfo(array(), '`id` DESC', $page);
		
		include $this->admin_tpl('list');
	}
	
	function add() {
		if($_POST['dosubmit']) {
			$info = $_POST[info];
			$info[userid] = trim($info[userid]) ? intval($info[userid]) : showmessage(L('userid不能为空'),HTTP_REFERER);
			$info[connectid] = trim($info[connectid]) ? intval($info[connectid]) : showmessage(L('connectid不能为空'),HTTP_REFERER);
			$info[name] = trim($info[name]) ? trim($info[name]) : showmessage(L('用户名不能为空'),HTTP_REFERER);
			
			$info[setting] = array2string($info[setting]);
			$info[addtime] = time();
			$return_id = $this->db->insert($info,'1');
			showmessage(L('add_success'), '?m=iappShare&c=manages&a=init');
		} else {
			include $this->admin_tpl('add');			
		}		
	}
	
	function edit() {
		if($_POST['dosubmit']) {
			$id = intval($_POST['id']) ? intval($_POST['id']) : showmessage(L('parameter_error'),HTTP_REFERER);
			$info = $_POST[info];
			$info[userid] = trim($info[userid]) ? intval($info[userid]) : showmessage(L('userid不能为空'),HTTP_REFERER);
			$info[connectid] = trim($info[connectid]) ? intval($info[connectid]) : showmessage(L('connectid不能为空'),HTTP_REFERER);
			$info[name] = trim($info[name]) ? trim($info[name]) : showmessage(L('用户名不能为空'),HTTP_REFERER);

			$info[setting] = array2string($info[setting]);
			$info[addtime] = time();
			$this->db->update($info, array('id'=>$id));
			showmessage(L('operation_success'), '?m=iappShare&c=manages&a=edit&id='.$id.'&menuid='.$_GET['menuid']);
		}else{
			include $this->admin_tpl('edit');
		}		
		
	}
	
	public function delete() {
		if (isset($_GET['id']) && !empty($_GET['id'])) {
			$id = intval($_GET['id']);
			$this->db->delete(array('id'=>$id));
			showmessage(L('operation_success'), HTTP_REFERER);
		} elseif (isset($_POST['ids']) && !empty($_POST['ids'])) {
			if (is_array($_POST['ids'])) {
				foreach ($_POST['ids'] as $fid) {
					$this->db->delete(array('id'=>$fid));
				}
			}
			showmessage(L('operation_success'), HTTP_REFERER);
		} else {
			showmessage(L('illegal_operation'), HTTP_REFERER);
		}
	}
	
	
}
?>