<?php
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');

$menus = $menu_db->select(array('name'=>'iappPlug', 'm'=>'iapp', 'c'=>'manages', 'a'=>'init'));

$parentid = $menu_db->insert(array('name'=>'iappShare', 'parentid'=>$menus[0][id], 'm'=>'iappShare', 'c'=>'manages', 'a'=>'init', 'data'=>'', 'listorder'=>1, 'display'=>'1'), true);


$menu_db->insert(array('name'=>'iappShare_add', 'parentid'=>$parentid, 'm'=>'iappShare', 'c'=>'manages', 'a'=>'add', 'data'=>'', 'listorder'=>2, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappShare_edit', 'parentid'=>$parentid, 'm'=>'iappShare', 'c'=>'manages', 'a'=>'edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappShare_delete', 'parentid'=>$parentid, 'm'=>'iappShare', 'c'=>'manages', 'a'=>'delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));

$language = array('iappShare'=>'分享模块',
				  'iappShare_add'=>'添加账号',
				  'iappShare_edit'=>'修改账号',
				  'iappShare_delete'=>'删除账号'				  
                  );

?>