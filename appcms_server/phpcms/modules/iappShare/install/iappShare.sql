DROP TABLE IF EXISTS `phpcms_iappShare`;
CREATE TABLE IF NOT EXISTS `phpcms_iappShare` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned default '0' COMMENT '用户ID',
  `connectid` varchar(200) NOT NULL DEFAULT '',
  `access_token` varchar(200) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `setting` mediumtext,
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) TYPE=MyISAM;

