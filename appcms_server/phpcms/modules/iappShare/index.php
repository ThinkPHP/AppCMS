<?php 
defined('IN_PHPCMS') or exit('No permission resources.');
class index {
	function __construct() {		
		$this->db = pc_base::load_model('iappShare_model');
		$this->member = pc_base::load_model('member_model');
	}
	
	public function init() {
		echo "not init";
	}
	
	public function add() {
		$params = $_POST;
		$re = array();
		$content = $params[text];

		if($content && $params[userid]){
			$userdata = $this->member->get_one(array('userid'=>$params[userid],'password'=>$params[acctoken]));
			if($userdata){
				$t=false;
				if($params[qqweibo]){
					$t=true;
					$share = $this->db->get_one(array('userid'=>$params[userid],'name'=>'qqweibo'));
					if($share){
						pc_base::load_app_class('Tencent', 'iapp', 0);
						$client_id = pc_base::load_config('system', 'qq_akey');
						$client_secret = pc_base::load_config('system', 'qq_skey');
						OAuth::init($client_id, $client_secret);
						$info = array(
							'access_token'=>$share[access_token],
							'openid'=>$share[connectid],
							'content' => $content
						);
						$r = Tencent::api('t/add_pic_url', $info, 'POST');
						$re['success'] = 'OK';
						$re['msg'] = '分享成功~';
					}else{
						$re['success'] = 'ERR';
						$re['msg'] = '分享失败,未授权~';
					}
				}
				
				if($params[sina]){
					$t=true;
					$share = $this->db->get_one(array('userid'=>$params[userid],'name'=>'sina'));
					if($share){
						pc_base::load_app_class('sinaweibo', 'iapp', 0);
						$WB_AKEY = pc_base::load_config('system', 'sina_akey');
						$WB_SKEY = pc_base::load_config('system', 'sina_skey');
						$SaeTOAuthV2 = new SaeTOAuthV2(WB_AKEY , WB_SKEY);
						$SaeTClientV2 = new SaeTClientV2(WB_AKEY , WB_SKEY , $share[connectid]);
						$r = $SaeTClientV2->update($content);
						$re['success'] = 'OK';
						$re['msg'] = '分享成功~';
					}else{
						$re['success'] = 'ERR';
						$re['msg'] = '分享失败,未授权~';
					}
				}
				
				if($params[qzone]){
					$t=true;
					$re['success'] = 'OK';
					$re['msg'] = '分享失败，接口未开放~';
				}
				
				if(!$t){
					$re['success'] = 'ERR';
					$re['msg'] = '分享失败，type错误~';
				}
			}else{
				$re['success'] = 'NOTLOGIN';
				$re['msg'] = '分享失败,没有登录~';
			}
		}else{
			$re['success'] = 'ERR';
			$re['msg'] = '分享失败,参数不完整~';
		}
		
		echo json_encode($re);
	}
	
	public function bind() {
		$params = $_GET;
		$type = $params['type'];
		$re = array();
		switch ($type)
		{
			case "qqweibo":
				if($params['access_token'] && $params['refresh_token'] && $params['expires_in'] && $params['openid'] && $params['userid'])
				{
					pc_base::load_app_class('Tencent', 'iapp', 0);
					$client_id = pc_base::load_config('system', 'qq_akey');
					$client_secret = pc_base::load_config('system', 'qq_skey');
					OAuth::init($client_id, $client_secret);
					
					//存储授权数据
					$_userdata = Tencent::api('user/info',array('access_token'=>$params['access_token'],'openid'=>$params['openid']));
					$_userdata = json_decode($_userdata, true);
					
					
					if ($_userdata['data']['name']) {
						$ud = $this->db->get_one(array('connectid'=>$_userdata[data][openid],'userid'=>$params[userid]));
						if($ud){
							$re[success]="OK";
							$re[msg]="绑定成功，已经绑定";
							$re[data]=array('connectid'=>$_userdata[data][openid]);
						}else{
							$info = array();	
							$info[userid] = $params[userid];
							$info[connectid] = $params['openid'];
							$info[access_token] = $params['access_token'];
							$info[name] = 'qqweibo';
							$info[addtime] = time();
							$insert_id = $this->db->insert($info,true);
							if($insert_id){
								$re[success]="OK";
								$re[msg]="绑定成功";
								$re[data]=array('connectid'=>$_userdata[data][openid]);
							}else{
								$re[success]="ERR";
								$re[msg]="绑定失败";
							}
						}
					}else{
						$re['success'] = 'ERR';
						$re['msg'] = '绑定失败,授权失败~';
					}
				}else{
					$re['success'] = 'ERR';
					$re['msg'] = '绑定失败,参数错误~';
				}
				break; 
			case "qzone":
				$re['success'] = 'ERR';
				$re['msg'] = '绑定失败,qzone类型分享接口未开放~';
				break;
			case "sina":
				if($params[access_token] && $params[remind_in] && $params[expires_in] && $params[uid] && $params[userid])
				{
			
					pc_base::load_app_class('sinaweibo', 'iapp', 0);
					$WB_AKEY = pc_base::load_config('system', 'sina_akey');
					$WB_SKEY = pc_base::load_config('system', 'sina_skey');
					$SaeTOAuthV2 = new SaeTOAuthV2(WB_AKEY , WB_SKEY);

					$token = array('access_token' => $params[access_token],
								   'remind_in' => $params[remind_in],
								   'expires_in' => $params[expires_in],
								   'uid' => $params[uid]
								  );
				
					$_SESSION['token'] = $token;
					setcookie('weibojs_'.$SaeTOAuthV2->client_id, http_build_query($token));
					
					$SaeTClientV2 = new SaeTClientV2(WB_AKEY , WB_SKEY , $token['access_token']);
					$sinauseruid = $SaeTClientV2->get_uid();
					$sinauserdata = $SaeTClientV2->show_user_by_id($sinauseruid['uid']);
					
					if($sinauserdata[id]>0){
						
						$ud = $this->db->get_one(array('connectid'=>$token[access_token],'userid'=>$params[userid]));
						if($ud){
							$re[success]="OK";
							$re[msg]="绑定成功，已经绑定";
							$re[data]=array('connectid'=>$token[access_token]);
						}else{
							$info = array();
							$info[userid] = $params[userid];
							$info[connectid] = $token[access_token];
							$info[name] = 'sina';
							$info[addtime] = time();
							$insert_id = $this->db->insert($info,true);
							if($insert_id){
								$re[success]="OK";
								$re[msg]="绑定成功";
								$re[data]=array('connectid'=>$token[access_token]);
							}else{
								$re[success]="ERR";
								$re[msg]="绑定失败";
							}
						}
					}else{
						$re['success'] = 'ERR';
						$re['msg'] = '绑定失败,微博授权失败~';
					}
				}else{
					$re['success'] = 'ERR';
					$re['msg'] = '绑定失败,授权数据不完整~';
				}
				break;
			default:
				$re['success'] = 'ERR';
				$re['msg'] = '绑定失败,type错误~';
		}
		echo json_encode($re);
	}
}
?>