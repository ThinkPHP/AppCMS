<?php 
defined('IN_PHPCMS') or exit('No permission resources.'); 
pc_base::load_app_class('admin','admin',0);
pc_base::load_sys_class('form', '', 0);
pc_base::load_sys_class('format', '', 0);
pc_base::load_app_func('global', 'iappSpecial');
class manages extends admin {
	private $db,$db_iappSpecial_type,$db_iappSpecial_reply;
	function __construct() {
		parent::__construct();
		$this->sites = pc_base::load_app_class('sites','admin');
		$this->db = pc_base::load_model('iappSpecial_model');
		$this->db_iappSpecial_data = pc_base::load_model('iappSpecial_data_model');
		$this->db_iappSpecial_type = pc_base::load_model('iappSpecial_type_model');
		$this->siteid = $this->get_siteid();
	}
	
	function init() {
		$page = max(intval($_GET['page']), 1);
		$data = $this->db->listinfo("", '`id` DESC', $page);
		include $this->admin_tpl('special_list');
	}
		
	
	function add() {
		
		if($_POST['dosubmit']) {
			$info = $_POST['info'];
			
			$userid = $_SESSION['userid'];
			$admin_username = param::get_cookie('admin_username');
			
			$info[userid] = $userid;
			$info[username] = $admin_username;
			$info[addtime] = time();
			$info[edittime] = time();
			$info[htis] = 1;
			$info[praise] = 1;
			$info[status] = 99;
			
			$_type =$info[setting][type];
			$_type = array_sort($_type,'listorder');
			$info[setting][type] = $_type;
			$info[setting] = array2string($info[setting]);
			
			$return_id = $this->db->insert($info,'1');
			
			pc_base::load_app_class('phpqrcode', 'iappSpecial', 0);
			$url = APP_PATH.'index.php?m=iappSpecial&c=index&a=init&id='.$return_id;
			QRcode::png($url, "./uploadfile/qrcode/".$return_id.".png",'L',4,0);
			$upinfo[qrcode] = APP_PATH.'uploadfile/qrcode/'.$return_id.'.png';
			$upinfo[url] = $url;
			$up_id = $this->db->update($upinfo,array('id'=>$return_id));
			
			showmessage(L('add_success'), '?m=iappSpecial&c=manages&a=init&menuid='.$_GET['menuid']);
		} else {
			include $this->admin_tpl('special_add');
		}		
	}
	

	
	
	function edit() {
		
		if($_POST['dosubmit']) {
			$id = intval($_POST['id']) ? intval($_POST['id']) : showmessage(L('parameter_error'),HTTP_REFERER);
			$info = $_POST['info'];

			
			
			$_type =$info[setting][type];
			$_type = array_sort($_type,'listorder');
			$info[setting][type] = $_type;
			$info[setting] = array2string($info[setting]);
		
			$info[edittime] = time();
			$return_id = $this->db->update($info,array('id'=>$id));
			
			pc_base::load_app_class('phpqrcode', 'iappSpecial', 0);
			$url = APP_PATH.'index.php?m=iappSpecial&c=index&a=init&id='.$id;
			QRcode::png($url, "./uploadfile/qrcode/".$id.".png",'L',4,0);
			$upinfo[qrcode] = APP_PATH.'uploadfile/qrcode/'.$id.'.png';
			
			showmessage(L('operation_success'), '?m=iappSpecial&c=manages&a=edit&id='.$id.'&menuid='.$_GET['menuid']);
		} else {
			$id = intval($_GET['id']) ? intval($_GET['id']) : showmessage(L('parameter_error'),HTTP_REFERER);		
			$info = $this->db->get_one(array('id'=>$id));
			$info[setting] = string2array($info[setting]);
			include $this->admin_tpl('special_edit');			
		}
	}
	
	/**
	 * 删除
	 */
	public function delete() {
		$siteid = $this->siteid;
		
		if (isset($_GET['id']) && !empty($_GET['id'])) {
			$id = intval($_GET['id']);
			$this->db->delete(array('id'=>$id));
			$this->db_iappSpecial_data->delete(array('specialid'=>$id));
			showmessage(L('operation_success'), HTTP_REFERER);
		} elseif (isset($_POST['ids']) && !empty($_POST['ids'])) {
			if (is_array($_POST['ids'])) {
				foreach ($_POST['ids'] as $fid) {
					$this->db->delete(array('id'=>$fid));
					$this->db_iappSpecial_data->delete(array('specialid'=>$fid));
				}
			}
			showmessage(L('operation_success'), HTTP_REFERER);
		} else {
			showmessage(L('illegal_operation'), HTTP_REFERER);
		}
	}
	/**
	 * 排序
	 */
	public function listorder() {
		if (isset($_POST['listorders']) && is_array($_POST['listorders'])) {
			$listorder = $_POST['listorders'];
			foreach ($listorder as $k => $v) {
				$this->db->update(array('listorder'=>$v), array('id'=>$k));
			}
		}
		showmessage(L('operation_success'), HTTP_REFERER);
	}
	
	function data_list() {
		$specialid = intval($_GET['specialid']) ? intval($_GET['specialid']) : showmessage(L('请先选择专题！'),HTTP_REFERER);
		$special = $this->db->get_one(array('id'=>$specialid));
		$special[setting] = string2array($special[setting]);
		$types = $special[setting][type];
		$typeid = intval($_GET['typeid']);
		$w = array();
		$w[specialid] = $specialid;
		if($typeid){
			$w[typeid] =$typeid;
		}
		
		$page = max(intval($_GET['page']), 1);
		$data = $this->db_iappSpecial_data->listinfo($w, '`id` DESC', $page);
		include $this->admin_tpl('data_list');
	}
		
	
	function data_add() {
		$specialid = intval($_REQUEST['specialid']) ? intval($_REQUEST['specialid']) : showmessage(L('请先选择专题后添加文章！'),HTTP_REFERER);
		
		$special = $this->db->get_one(array('id'=>$specialid));
		$special[setting] = string2array($special[setting]);
		$types = $special[setting][type];
		//echo $_POST['dosubmit'];
		
		if($_POST['dosubmit']) {
			$info = $_POST['info'];
			
			$userid = $_SESSION['userid'];
			$admin_username = param::get_cookie('admin_username');
			$info[userid] = $userid;
			$info[username] = $admin_username;
			$info[addtime] = time();
			$info[edittime] = time();
			
			$return_id = $this->db_iappSpecial_data->insert($info,'1');
			$upinfo[url] = APP_PATH.'index.php?m=iappSpecial&c=index&a=show&id='.$return_id;
			$up_id = $this->db_iappSpecial_data->update($upinfo,array('id'=>$return_id));
			
			if($_POST['dosubmit']=='提交'){
				showmessage(L('add_success'), '?m=iappSpecial&c=manages&a=data_list&specialid='.$specialid.'&menuid='.$_GET['menuid']);
			}else{
				showmessage(L('add_success'), '?m=iappSpecial&c=manages&a=data_add&specialid='.$specialid.'&menuid='.$_GET['menuid']);
			}
		} else {
			$show_validator = $show_scroll = true;
			include $this->admin_tpl('data_add');
		}		
	}
	
	function data_edit() {
	
		$id = intval($_REQUEST['id']) ? intval($_REQUEST['id']) : showmessage(L('parameter_error'),HTTP_REFERER);		
		$info = $this->db_iappSpecial_data->get_one(array('id'=>$id));
	
		$specialid = $info[specialid];
		$special = $this->db->get_one(array('id'=>$specialid));
		$special[setting] = string2array($special[setting]);
		$types = $special[setting][type];
		
		if($_POST['dosubmit']) {
			$info = $_POST['info'];
			
			$info[edittime] = time();
			$return_id = $this->db_iappSpecial_data->update($info,array('id'=>$id));
			showmessage(L('operation_success'), '?m=iappSpecial&c=manages&a=data_list&specialid='.$specialid.'&menuid='.$_GET['menuid']);
		} else {
			$show_validator = $show_scroll = true;
			include $this->admin_tpl('data_edit');			
		}
	}
	
	/**
	 * 删除
	 */
	public function data_delete() {
		if (isset($_GET['id']) && !empty($_GET['id'])) {
			$id = intval($_GET['id']);
			$this->db_iappSpecial_data->delete(array('id'=>$id));
			showmessage(L('operation_success'), HTTP_REFERER);
		} elseif (isset($_POST['ids']) && !empty($_POST['ids'])) {
			if (is_array($_POST['ids'])) {
				foreach ($_POST['ids'] as $fid) {
					$this->db_iappSpecial_data->delete(array('id'=>$fid));
				}
			}
			showmessage(L('operation_success'), HTTP_REFERER);
		} else {
			showmessage(L('illegal_operation'), HTTP_REFERER);
		}
	}
	/**
	 * 排序
	 */
	public function data_listorder() {
		if (isset($_POST['listorders']) && is_array($_POST['listorders'])) {
			$listorder = $_POST['listorders'];
			foreach ($listorder as $k => $v) {
				$this->db_iappSpecial_data->update(array('listorder'=>$v), array('id'=>$k));
			}
		}
		showmessage(L('operation_success'), HTTP_REFERER);
	}
	
	//$types = $this->db_iappSpecial_type->select('','*','','`listorder` DESC');
	function type_list() {
		$page = max(intval($_GET['page']), 1);
		$data = $this->db_iappSpecial_type->listinfo('', '`listorder` DESC', $page);
		include $this->admin_tpl('type_list');
	}
	function type_add() {
		if($_POST['dosubmit']) {
			$info = $_POST['info'];
			$info[title] = trim($_POST['info'][title]) ? trim($_POST['info'][title]) : showmessage(L('主题不能为空'),HTTP_REFERER);
			$info[siteid] = $this->siteid;
			$info[listorder] = 0;
			$return_id = $this->db_iappSpecial_type->insert($info,'1');
			showmessage(L('add_success'), '?m=iappSpecial&c=manages&a=type_list&menuid='.$_GET['menuid']);
		} else {
			include $this->admin_tpl('type_add');
		}		
	}
	
	function type_edit() {
		if($_POST['dosubmit']) {
			$id = intval($_POST['id']) ? intval($_POST['id']) : showmessage(L('parameter_error'),HTTP_REFERER);
			$info = $_POST['info'];
			$info[title] = trim($_POST['info'][title]) ? trim($_POST['info'][title]) : showmessage(L('主题不能为空'),HTTP_REFERER);
			$return_id = $this->db_iappSpecial_type->update($info,array('id'=>$id));
			showmessage(L('operation_success'), '?m=iappSpecial&c=manages&a=type_edit&id='.$id.'&menuid='.$_GET['menuid']);
		} else {
			$id = intval($_GET['id']) ? intval($_GET['id']) : showmessage(L('parameter_error'),HTTP_REFERER);		
			$info = $this->db_iappSpecial_type->get_one(array('id'=>$id));
			include $this->admin_tpl('type_edit');			
		}
	}
	
	/**
	 * 删除
	 */
	public function type_delete() {
		$siteid = $this->siteid;
		
		if (isset($_GET['id']) && !empty($_GET['id'])) {
			$id = intval($_GET['id']);
			$this->db_iappSpecial_type->delete(array('id'=>$id, 'siteid'=>$siteid));
			showmessage(L('operation_success'), HTTP_REFERER);
		} elseif (isset($_POST['ids']) && !empty($_POST['ids'])) {
			if (is_array($_POST['ids'])) {
				foreach ($_POST['ids'] as $fid) {
					$this->db_iappSpecial_type->delete(array('id'=>$fid, 'siteid'=>$siteid));
				}
			}
			showmessage(L('operation_success'), HTTP_REFERER);
		} else {
			showmessage(L('illegal_operation'), HTTP_REFERER);
		}
	}
	
	
	/**
	 * 排序
	 */
	public function type_listorder() {
		if (isset($_POST['listorders']) && is_array($_POST['listorders'])) {
			$listorder = $_POST['listorders'];
			foreach ($listorder as $k => $v) {
				$this->db_iappSpecial_type->update(array('listorder'=>$v), array('id'=>$k));
			}
		}
		showmessage(L('operation_success'), HTTP_REFERER);
	}
	
	


}
?>