<?php
defined('IN_ADMIN') or exit('No permission resources.'); 
include $this->admin_tpl('header', 'admin');?>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>colorpicker.js"></script>

<form method="post" action="?m=iappSpecial&c=manages&a=edit&menuid=<?php echo $_GET['menuid']?>">
<table width="100%" cellpadding="0" cellspacing="1" class="table_form">
	
	
	
	
	<tr>
		<th width="100">专题名称：</th>
		<td>
		<input type="text" name="info[name]" id="name"
			size="50" value="<?php echo $info[name]?>" class="input-text">
			专题背景颜色：<input id="style_color" type="text" size="10" style="color:<?php echo $info[setting][hbg];?>;" value="<?php echo $info[setting][hbg]?>" name="info[setting][hbg]">
			<img src="<?php echo IMG_PATH?>icon/colour.png" width="15" height="16" onclick="colorpicker('title_colorpanel','set_title_color');" style="cursor:hand"/> <span id="title_colorpanel" style="position:absolute;" class="colorpanel"></span>
			
		</td>
	</tr>
	<tr>
		<th width="100">专题副标题：</th>
		<td>
		<input type="text" name="info[lname]" id="lname"
			size="50" value="<?php echo $info[lname]?>" class="input-text">
		</td>
	</tr>
	
	
	
	
	<tr>
		<th width="100">专题横幅：</th>
		<td><?php echo form::images('info[banner]', 'banner', $info[banner], 'iappSpecial')?></td>
	</tr>
	<tr>
		<th width="100">专题缩略图：</th>
		<td><?php echo form::images('info[thumb]', 'thumb', $info[thumb], 'iappSpecial')?></td>
	</tr>
	
	<tr>
		<th width="100">关键字：</th>
		<td>
		<input type="text" name="info[keywords]" id="keywords"
			size="60" value="<?php echo $info[keywords]?>" class="input-text">
		</td>
	</tr>
	<tr>
		<th width="100">描述：</th>
		<td>
		<textarea name="info[description]" id="description" cols="100"
			rows="3"><?php echo $info[description]?></textarea>
		</td>
	</tr>
	
	<tr>
		<th width="100">导语：</th>
		<td>
		<textarea name="info[content]" id="content" cols="100"
			rows="6"><?php echo $info[content]?></textarea>
		</td>
	</tr>
	
	<tr>
	    	<th>专题栏目：<a href="javascript:addItem()" title="添加"><span style="color:red;" >+</span></a></th>
	        <td valign="top"><div id="option_list">
			 <?php 
				foreach($info[setting][type] as $k=>$r){
			 ?> 
	        	<div class="mb6"><input type="hidden" name="info[setting][type][<?php echo $r[typeid]?>][typeid]" value="<?php echo $r[typeid]?>"><span>名称：<input type="text" id="type_name" name="info[setting][type][<?php echo $r[typeid]?>][name]" value="<?php echo $r[name]?>" class="input-text" size="15">&nbsp;&nbsp;英文名称：<input type="text" name="info[setting][type][<?php echo $r[typeid]?>][enname]" value="<?php echo $r[enname]?>" id="type_path" class="input-text" size="15">&nbsp;&nbsp;排序：<input type="text" name="info[setting][type][<?php echo $r[typeid]?>][listorder]" value="<?php echo $r[listorder]?>" size="6" class="input-text" ></span>&nbsp;<span id="typeTip"></span>&nbsp;
				<?php if($k>0){?>
				<a href="javascript:;" onclick="descItem(this, '<?php echo $r[typeid]?>');">移除</a>
				<?php }?>
				</div>
			<?
				}
			?>
	        </div>
	        </td>
	</tr>

	


	<tr>
		<td>&nbsp;</td>
		<td>
		
		
		<input type="hidden" name="id" value="<?php echo $info[id];?>">
		<input type="submit" name="dosubmit" id="dosubmit" value=" <?php echo L('提交')?>" class="button">&nbsp;<input type="reset" value=" <?php echo L('重写')?> " class="button"></td>
	</tr>
</table>
</form>
<script type="text/javascript">
function addItem() {
	var n = parseInt($('#option_list input:last').val())+1;
	var newOption =  '<div class="mb6"><input type="hidden" name="info[setting][type]['+n+'][id]" value="'+n+'"><span>名称：<input type="text" name="info[setting][type]['+n+'][name]" class="input-text" size="15">&nbsp;&nbsp;英文名称：<input type="text" name="info[setting][type]['+n+'][enname]" value="t'+n+'" class="input-text" size="15">&nbsp;&nbsp;排序：<input type="text" name="info[setting][type]['+n+'][listorder]" value="'+n+'" size="6" class="input-text" ></span>&nbsp;<a href="javascript:;" onclick="descItem(this, '+n+');">移除</a></div>';
	$('#option_list').append(newOption);
}

function descItem(a, id) {
	$(a).parent().remove();
}
function set_title_color(color) {
	$('#style_color').css('color',color);
	$('#style_color').val(color);
}
</script>
</body>
</html>