<?php 
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');
?>

<div class="pad-lr-10">
<form name="myform" id="myform" action="" method="post">

	
	
 <table width="100%" cellspacing="0" class="table-list nHover">
        <thead>
            <tr>
            <th width="35"><input type="checkbox" value="" id="check_box" onclick="selectall('ids[]');"></th>
			<th width="30" align="center">ID</th>
			<th width="50" align="center">排序</th>
			<th >专题信息</th>
			<th width="160">管理操作</th>
            </tr>
        </thead>
        <tbody>
 <?php 
if(is_array($data)){
	foreach($data as $r){
?>   
	<tr>
	<td align="center"><input class="inputcheckbox" name="ids[]" value="<?php echo $r['id'];?>" type="checkbox"></td>
	<td  align="center"><?php echo $r['id']?></td>
	<td  align="center"><input type='text' name='listorder[<?php echo $r['id']?>]' value="<?php echo $r['listorder']?>" class="input-text-c" size="4"></td>
	<td>
    <div class="col-left mr10" style="width:146px; height:112px"><?php if ($r['thumb']) {?>
<a href="<?php echo $r['url']?>" target="_blank"><img src="<?php echo $r['thumb']?>" width="146" height="112" style="border:1px solid #eee" align="left"></a><?php }?>
</div>

<div class="col-left mr10" style="width:112px; height:112px">
<a href="<?php echo $r['qrcode']?>" target="_blank"><img src="<?php echo $r['qrcode']?>" width="112" height="112" style="border:1px solid #eee" align="left"></a>
</div>

<div class="col-auto">  
    <h2 class="title-1 f14 lh28 mb6 blue"><a href="<?php echo $r['url']?>" target="_blank"><?php echo $r['name']?></a></h2>
    <div class="lh22"><?php echo $r['description']?></div>
<p class="gray4">创建者：<a href="#" class="blue"><?php echo $r['username']?></a>， 创建时间：<?php echo format::date($r['addtime'], 1)?>&nbsp;&nbsp;扫描：<?php echo $r['htis']?></p>
</div>
	</td>
	<td align="center">
	<a href='?m=iappSpecial&c=manages&a=data_add&specialid=<?php echo $r['id']?>&menuid=<?php echo $_GET['menuid']?>' >添加文章</a> | 
<a href='?m=iappSpecial&c=manages&a=data_list&specialid=<?php echo $r['id']?>&menuid=<?php echo $_GET['menuid']?>'>管理文章</a><br/>

<a href="?m=iappSpecial&c=manages&a=edit&id=<?php echo $r['id']?>&menuid=<?php echo $_GET['menuid']?>"><?php echo L('修改专题')?></a> | <a href="?m=iappSpecial&c=manages&a=delete&id=<?php echo $r['id']?>" onClick="return confirm('确认要删除『<?php echo $r[name];?>』吗？')">删除专题</a>
<?php 
	}
}
?>
</tbody>
    </table>
	
	
	
  
    <div class="btn"><label for="check_box"><?php echo L('selected_all')?>/<?php echo L('cancel')?></label>
		<input type="hidden" value="YVt4Dk" name="pc_hash">
    	<input type="button" class="button" value="排序" onclick="myform.action='?m=iappSpecial&c=manages&a=listorder&dosubmit=1';myform.submit();"/>
		
		<input type="button" class="button" value="<?php echo L('delete');?>" onclick="myform.action='?m=iappSpecial&c=manages&a=delete&dosubmit=1';return confirm_delete()"/>
		
		</div>  

</div>










 <div id="pages"><?php echo $this->db->pages;?></div>
</form>
</div>
<script type="text/javascript"> 
window.top.$("#display_center_id").css("display","none");
function confirm_delete(){
	if(confirm('<?php echo L('确认要删除『选中』吗？');?>')) $('#myform').submit();
}
</script>
</body>
</html>