<?php 
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');?>


<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>colorpicker.js"></script>
<div class="pad-lr-10">



<form method="post" action="?m=iappSpecial&c=manages&a=add">
<table width="100%" cellpadding="0" cellspacing="1" class="table_form">


	
	<tr>
		<th width="100">专题名称：</th>
		<td>
		<input type="text" name="info[name]" id="name"
			size="50" class="input-text">
			专题背景颜色：<input id="style_color" type="text" size="10" value="" name="info[setting][hbg]">
			<img src="<?php echo IMG_PATH?>icon/colour.png" width="15" height="16" onclick="colorpicker('title_colorpanel','set_title_color');" style="cursor:hand"/> <span id="title_colorpanel" style="position:absolute;" class="colorpanel"></span>
			
		</td>
	</tr>
	<tr>
		<th width="100">专题副标题：</th>
		<td>
		<input type="text" name="info[lname]" id="lname"
			size="50" class="input-text">
		</td>
	</tr>
	
	
	<tr>
		<th width="100">专题横幅：</th>
		<td><?php echo form::images('info[banner]', 'banner', '', 'iappSpecial')?></td>
	</tr>
	<tr>
		<th width="100">专题缩略图：</th>
		<td><?php echo form::images('info[thumb]', 'thumb', '', 'iappSpecial')?></td>
	</tr>
	
	<tr>
		<th width="100">关键字：</th>
		<td>
		<input type="text" name="info[keywords]" id="keywords"
			size="60" class="input-text">
		</td>
	</tr>
	<tr>
		<th width="100">描述：</th>
		<td>
		<textarea name="info[description]" id="description" cols="100"
			rows="3"><?php echo $info[description]?></textarea>
		</td>
	</tr>
	
	<tr>
		<th width="100">导语：</th>
		<td>
		<textarea name="info[content]" id="content" cols="100"
			rows="6"><?php echo $info[content]?></textarea>
		</td>
	</tr>
	
	<tr>
	    	<th>专题栏目：<a href="javascript:addItem()" title="添加"><span style="color:red;" >+</span></a></th>
	        <td valign="top"><div id="option_list">
	        	<div class="mb6"><input type="hidden" name="info[setting][type][1][typeid]" value="1"><span>名称：<input type="text" id="type_name" name="info[setting][type][1][name]" class="input-text" size="15">&nbsp;&nbsp;英文名称：<input type="text" name="info[setting][type][1][enname]" value="t1" id="type_path" class="input-text" size="15">&nbsp;&nbsp;排序：<input type="text" name="info[setting][type][1][listorder]" value="1" size="6" class="input-text" ></span>&nbsp;<span id="typeTip"></span></div>
	        </div>
	        </td>
	</tr>


	<tr>
		<td>&nbsp;</td>
		<td>
		<input type="submit" name="dosubmit" id="dosubmit" value="<?php echo L('提交')?>" class="button">&nbsp;<input type="reset" value=" <?php echo L('重写')?> " class="button"></td>
	</tr>
	
</table>
</form>


</div>

<script type="text/javascript">
function addItem() {
	var n = parseInt($('#option_list input:last').val())+1;
	var newOption =  '<div class="mb6"><input type="hidden" name="info[setting][type]['+n+'][typeid]" value="'+n+'"><span>名称：<input type="text" name="info[setting][type]['+n+'][name]" class="input-text" size="15">&nbsp;&nbsp;英文名称：<input type="text" name="info[setting][type]['+n+'][enname]" value="t'+n+'" class="input-text" size="15">&nbsp;&nbsp;排序：<input type="text" name="info[setting][type]['+n+'][listorder]" value="'+n+'" size="6" class="input-text" ></span>&nbsp;<a href="javascript:;" onclick="descItem(this, '+n+');">移除</a></div>';
	$('#option_list').append(newOption);
}

function descItem(a, id) {
	$(a).parent().remove();
}

function set_title_color(color) {
	$('#style_color').css('color',color);
	$('#style_color').val(color);
} 
</script>
</body>
</html>