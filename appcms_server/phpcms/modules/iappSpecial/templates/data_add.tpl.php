<?php 
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');?>
<script type="text/javascript">
<!--
	$(function(){
	$.formValidator.initConfig({formid:"myform",autotip:true,onerror:function(msg,obj){window.top.art.dialog({content:msg,lock:true,width:'200',height:'50'}, function(){this.close();$(obj).focus();})}});
	$("#name").formValidator({onshow:"标题不能为空~",onfocus:"标题不能为空~"}).inputValidator({min:1,onerror:"标题不能为空~"});
	$("#copyfrom").formValidator({onshow:"文章来源不能为空~",onfocus:"文章来源不能为空"}).inputValidator({min:1,onerror:"文章来源不能为空"});
	})
//-->
</script>
<div class="pad-lr-10">
<form method="post" name="myform" id="myform" action="?m=iappSpecial&c=manages&a=data_add&specialid=<?php echo $specialid?>&&menuid=<?php echo $_GET['menuid']; ?>">
<table width="100%" cellpadding="0" cellspacing="1" class="table_form">


	<tr>
		<th width="100">当前专题：</th>
		<td>
		<?php echo $special[name]?>
		<input type="hidden" name="info[specialid]" value="<?php echo $special[id]?>">
		</td>
	</tr>


	<tr>
		<th width="100"><?php echo L('分类名称')?>：</th>
		<td>
		<select name="info[typeid]" id="typeid">
		<?php
		  $i=0;
		  foreach($types as $k=>$r){
		  $i++;
		?>
		<option value="<?php echo $r['typeid'];?>"><?php echo $r['name'];?></option>
		<?php }?>
		</select>
		
		</td>
	</tr>
	
	
	
	<tr>
		<th width="100">标题：</th>
		<td>
		<input type="text" name="info[title]" id="name"
			size="50" class="input-text">
		</td>
	</tr>
	<tr>
		<th width="100">副标题：</th>
		<td>
		<input type="text" name="info[ltitle]" id="ltitle"
			size="60" class="input-text">
		</td>
	</tr>
	
	<tr id="image_id">
		<th width="100">图片：</th>
		<td><?php echo form::images('info[thumb]', 'thumb', '', 'iappSpecial')?></td>
	</tr>
	<tr id="icon_id">
		<th width="100">ICON：</th>
		<td><?php echo form::images('info[icon]', 'icon', '', 'iappSpecial')?>
        <img src="<?php echo IMG_PATH?>iappspecial/1.jpg"  height="26" alt="黑" onclick="icon(this)" />
		<img src="<?php echo IMG_PATH?>iappspecial/2.jpg"  height="26" alt="红" onclick="icon(this)" />
        <img src="<?php echo IMG_PATH?>iappspecial/3.jpg"  height="26" alt="白" onclick="icon(this)" />
		</td>
	</tr>
	
	<tr>
		<th width="100">关键字：</th>
		<td>
		<input type="text" name="info[keywords]" id="keywords"
			size="60" class="input-text">
		</td>
	</tr>
	<tr>
		<th width="100">摘要：</th>
		<td>
		<textarea name="info[description]" id="description" cols="100"
			rows="3"><?php echo $info[description]?></textarea><br/>
            <p>
              是否显示摘要字样：
              <label><input type="radio" name="info[isdes]" value="1" id="isdes_0" checked="checked" />是</label>&nbsp;&nbsp;
              <label><input name="info[isdes]" type="radio" id="isdes_1" value="0"  />否</label>
        </p>
		</td>
	</tr>
	<tr>
		<th width="100">来源：</th>
		<td>
		<input type="text" name="info[copyfrom]" id="copyfrom"
			size="30" class="input-text">
		</td>
	</tr>
	
	
	
	
	<tr>
		<th width="100">内容：</th>
		<td>
		<textarea name="info[content]" id="content"></textarea>
		
		
		<?php echo form::editor('content','full','iappSpecial', '','',1,1,'',300,1)?>
		</td>
		
	</tr>
	
	<tr>
		<th width="100">视频：</th>
		<td>
		<?php echo form::upfiles('info[video]', 'video', '', 'iappSpecial','','','','','mp4')?>
		</td>
	</tr>
	



	<tr>
		<td>&nbsp;</td>
		<td>
		<input type="submit" name="dosubmit" id="dosubmit" value="<?php echo L('提交')?>" class="button">&nbsp;
		<input type="submit" name="dosubmit" id="dosubmit" value="<?php echo L('保存并继续添加')?>" class="button">&nbsp;
		<input type="reset" value=" <?php echo L('重写')?> " class="button">
		
		</td>
	</tr>
	
</table>
</form>




</div>
<script type="text/javascript">
function icon(obj)
{
	$('#icon').val($(obj).attr('src'));
}
</script>

</body>
</html>