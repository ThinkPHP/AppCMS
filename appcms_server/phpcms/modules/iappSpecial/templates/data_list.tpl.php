<?php 
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');
?>

<div class="pad-lr-10">

<div class="explain-col">
栏目筛选：
<?php 
	foreach($types as $r){
?>  

<a href="index.php?m=iappSpecial&c=manages&a=data_list&specialid=<?php echo$specialid ?>&typeid=<?php echo $r['typeid']?>&menuid=<?php echo $_GET['menuid']?>&pc_hash=<?php echo $pc_hash?>&page=<?php echo $page?>">
	<span <?php if($typeid==$r[typeid]){ echo 'style="color:#C00;"';}?> ><?php echo $r['name']?><span>&nbsp;&nbsp;
	</a>
<?php
	}
?>

<span style="float:right;"><a href="index.php?m=iappSpecial&c=manages&a=data_add&specialid=<?php echo$specialid ?>&menuid=<?php echo $_GET['menuid']?>">添加文章</a></span>
</div>
<div class="bk10"></div>
<form name="myform" id="myform" action="" method="post">
<div class="table-list">
    <table width="100%" cellspacing="0">
        <thead>
            <tr>
            <th width="35" align="center">
			<input type="checkbox" value="" id="check_box" onclick="selectall('ids[]');"></th>
			<th width="50" align="center"><?php echo L('listorder');?></th>
			<th width="30" align="center">ID</th>
			<th width="30" align="center">栏目</th>
			<th align="center">标题</th>
			<th width="50" align="center">点击量</th>
			<th width="50" align="center">发布者</th>
			<th width="50" align="center">更新时间</th>
			
			<th width="220" align="center">管理操作</th>
            </tr>
        </thead>
    <tbody>
 <?php 
if(is_array($data)){
	foreach($data as $r){
?>   
	<tr>
	<td align="center"><input class="inputcheckbox " name="ids[]" value="<?php echo $r['id'];?>" type="checkbox"></td>
    <td align='center'><input name='listorders[<?php echo $r['id'];?>]' type='text' size='3' value='<?php echo $r['listorder'];?>' class='input-text-c'></td>
	
	<td align='center' ><?php echo $r['id'];?></td>
	<td align='center' ><?php echo $r['typeid'];?></td>
	
	<td>
	<a href="?m=iappSpecial&c=index&a=show&id=<?php echo $r['id']?>" target="_blank">
	<?php echo $r['title']?>
	</a>
	<?php 
	if($r['thumb']!=''){
		echo '<img src="'.IMG_PATH.'icon/small_img.gif" title="'.L('thumb').'">'; 
	} 
	if($r['video']) {
		echo '<img src="'.IMG_PATH.'icon/small_video.gif" title="'.L('elite').'">';
	}
	?>
	
	</td>
	
	
	<td align='center' ><?php echo $r['htis'];?></td>
	<td align='center' ><?php echo $r['username'];?></td>
	<td align='center' ><?php echo date('Y-m-d', $r['edittime']);?></td>
	
	
	<td align="center">
	<a href="?m=iappSpecial&c=manages&a=data_edit&id=<?php echo $r['id']?>&menuid=<?php echo $_GET['menuid']?>"><?php echo L('修改')?></a> | <a href="?m=iappSpecial&c=manages&a=data_delete&id=<?php echo $r['id']?>" onClick="return confirm('<?php echo L('确认要删除『'.$r[title].'』吗？');?>')"><?php echo L('删除')?></a>
	</td>
	</tr>
<?php 
	}
}
?>
</tbody>
    </table>
  
    <div class="btn"><label for="check_box"><?php echo L('selected_all')?>/<?php echo L('cancel')?></label>
		<input type="hidden" value="<?php echo $pc_hash?>" name="pc_hash">
    	<input type="button" class="button" value="排序" onclick="myform.action='?m=iappSpecial&c=manages&a=data_listorder&dosubmit=1';myform.submit();"/>
		
		<input type="button" class="button" value="<?php echo L('delete');?>" onclick="myform.action='?m=iappSpecial&c=manages&a=delete&dosubmit=1';return confirm_delete()"/>
		
		</div>  

</div>
 <div id="pages"><?php echo $this->db->pages;?></div>
</form>
</div>
<script type="text/javascript"> 
window.top.$("#display_center_id").css("display","none");
function confirm_delete(){
	if(confirm('<?php echo L('确认要删除『选中』吗？');?>')) $('#myform').submit();
}
</script>
</body>
</html>