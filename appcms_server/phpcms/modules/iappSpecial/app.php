<?php 
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_sys_class('format', '', 0);
class app {
	function __construct() {		
		$this->db = pc_base::load_model('iappSpecial_model');
		$this->db_iappSpecial_data = pc_base::load_model('iappSpecial_data_model');
		$this->siteid = get_siteid();
	}

	function init() {
		$specialid = intval($_GET['id']) ? intval($_GET['id']) : showmessage('参数错误',HTTP_REFERER);		
		$special = $this->db->get_one(array('id'=>$specialid));
		$special[setting] = string2array($special[setting]);
		$types = $special[setting]['type'];
		$special_data = array();
		foreach ($types as $k=>$v) {
			$_typeid = $v[typeid];
			$special_data[$_typeid] =$v;
			$special_data[$_typeid][lists] = $this->db_iappSpecial_data->select(array("specialid"=>$specialid,'typeid'=>$_typeid),'title,ltitle,thumb,icon,video,copyfrom,addtime,htis,url','','listorder DESC');
		}
		$return_id = $this->db->update(array('htis'=>'+=1'),array('id'=>$specialid));
		
		include template('iappSpecial', 'index');
	}
	
	public function lists() {
		$page = max(intval($_GET['page']), 1);
		$data = $this->db->listinfo("", 'id DESC', $page,5);
		
		$list = array();
		foreach ($data as $k=>$v) {
			$list[$k][id] = $v[id];
			$list[$k][typeid] = $v[typeid];
			$list[$k][userid] = $v[userid];
			$list[$k][username] = $v[username];
			$list[$k][title] = $v[title];
			$list[$k][ltitle] = $v[ltitle];
			$list[$k][image] = thumb($v[image],600,226);
			$list[$k][start_time] = $v[start_time];
			$list[$k][end_time] = $v[end_time];
			$list[$k][activity_time] = $v[activity_time];	
			$list[$k][num] = $v[num];	
			$list[$k][bmnum] = $v[bmnum];	
			$list[$k][collection] = $v[collection];	
			$list[$k][htis] = $v[htis];
			$list[$k][praise] = $v[praise];
			
		}
		$return_id = $this->db->update(array('htis'=>'+=1'),'1=1');
		echo json_encode(array("stime"=>time(),"list"=>$list));
		
	}
	
	public function show() {
		
		$id = intval($_GET['id']);
		$data = $this->db_iappSpecial_data->get_one(array('id'=>$id));
		$return_id = $this->db_iappSpecial_data->update(array('htis'=>'+=1'),array('id'=>$id));
		include template('iappSpecial', 'show');
	}
	
	
}
?>