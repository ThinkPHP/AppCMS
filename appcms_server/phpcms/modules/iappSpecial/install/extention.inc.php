<?php
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');

$menus = $menu_db->select(array('name'=>'iappPlug', 'm'=>'iapp', 'c'=>'manages', 'a'=>'init'));
if($menus){
	$menuid=$menus[0][id];
}else{
	$menuid=821;
}
$parentid = $menu_db->insert(array('name'=>'iappSpecial', 'parentid'=>$menuid, 'm'=>'iappSpecial', 'c'=>'manages', 'a'=>'init', 'data'=>'', 'listorder'=>1, 'display'=>'1'), true);

$menu_db->insert(array('name'=>'iappSpecial_add', 'parentid'=>$parentid, 'm'=>'iappSpecial', 'c'=>'manages', 'a'=>'add', 'data'=>'', 'listorder'=>2, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappSpecial_edit', 'parentid'=>$parentid, 'm'=>'iappSpecial', 'c'=>'manages', 'a'=>'edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappSpecial_delete', 'parentid'=>$parentid, 'm'=>'iappSpecial', 'c'=>'manages', 'a'=>'delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));

$menu_db->insert(array('name'=>'iappSpecial_data_list', 'parentid'=>$parentid, 'm'=>'iappSpecial', 'c'=>'manages', 'a'=>'data_list', 'data'=>'', 'listorder'=>4, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappSpecial_data_add', 'parentid'=>$parentid, 'm'=>'iappSpecial', 'c'=>'manages', 'a'=>'data_add', 'data'=>'', 'listorder'=>3, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappSpecial_data_edit', 'parentid'=>$parentid, 'm'=>'iappSpecial', 'c'=>'manages', 'a'=>'data_edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappSpecial_data_delete', 'parentid'=>$parentid, 'm'=>'iappSpecial', 'c'=>'manages', 'a'=>'data_delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappSpecial_data_listorder', 'parentid'=>$parentid, 'm'=>'iappSpecial', 'c'=>'manages', 'a'=>'data_listorder', 'data'=>'', 'listorder'=>0, 'display'=>'0'));


$menu_db->insert(array('name'=>'iappSpecial_type_list', 'parentid'=>$parentid, 'm'=>'iappSpecial', 'c'=>'manages', 'a'=>'type_list', 'data'=>'', 'listorder'=>6, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappSpecial_type_add', 'parentid'=>$parentid, 'm'=>'iappSpecial', 'c'=>'manages', 'a'=>'type_add', 'data'=>'', 'listorder'=>5, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappSpecial_type_edit', 'parentid'=>$parentid, 'm'=>'iappSpecial', 'c'=>'manages', 'a'=>'type_edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappSpecial_type_delete', 'parentid'=>$parentid, 'm'=>'iappSpecial', 'c'=>'manages', 'a'=>'type_delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappSpecial_type_listorder', 'parentid'=>$parentid, 'm'=>'iappSpecial', 'c'=>'manages', 'a'=>'type_listorder', 'data'=>'', 'listorder'=>0, 'display'=>'0'));






$language = array('iappSpecial'=>'手机专题',
				  'iappSpecial_add'=>'添加专题',
				  'iappSpecial_edit'=>'修改专题',
				  'iappSpecial_delete'=>'删除专题',
				  
				  'iappSpecial_data_add'=>'添加文章',
				  'iappSpecial_data_edit'=>'修改文章',
				  'iappSpecial_data_list'=>'文章列表',
				  'iappSpecial_data_delete'=>'删除文章',
				  'iappSpecial_data_listorder'=>'文章排序', 
				 
				  'iappSpecial_type_add'=>'添加分类',
				  'iappSpecial_type_edit'=>'修改分类',
				  'iappSpecial_type_list'=>'分类列表',
				  'iappSpecial_type_delete'=>'删除分类',
				  'iappSpecial_type_listorder'=>'分类排序'  
                  );

?>