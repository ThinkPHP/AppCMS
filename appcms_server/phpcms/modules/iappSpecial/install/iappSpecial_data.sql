DROP TABLE IF EXISTS `phpcms_iappSpecial_data`;
CREATE TABLE IF NOT EXISTS `phpcms_iappSpecial_data` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `specialid` smallint(5) NOT NULL COMMENT '专题ID',
  `typeid` smallint(5) NOT NULL COMMENT '栏目ID',
  `userid` int(10) unsigned default '0' COMMENT '用户ID',
  `username` varchar(20) default NULL COMMENT '用户名',
  
  `title` varchar(50) NOT NULL COMMENT '标题',
  `ltitle` varchar(80) NOT NULL COMMENT '副标题',
  `thumb` varchar(200) NOT NULL COMMENT '缩略图',
  `icon` varchar(200) NOT NULL COMMENT 'ICON',
  `video` varchar(200) NOT NULL COMMENT '视频',
  `keywords` varchar(200) NOT NULL COMMENT '关键字',
  `copyfrom` varchar(50) NOT NULL COMMENT '来源',
  
  `description` text NOT NULL COMMENT '导读',
  
  `isdes` int(10) unsigned default '0' COMMENT '是否显示摘要字样',
  
  `content` text NOT NULL COMMENT '内容',
  `url` varchar(200) NOT NULL,
  
  `htis` smallint(5) DEFAULT '0' COMMENT '查看量',
  `praise` smallint(5) DEFAULT '0' COMMENT '赞',
  `status` smallint(5) DEFAULT '0' COMMENT '状态',
  `posids` smallint(5) DEFAULT '0' COMMENT '推荐位',
  
  `listorder` smallint(5) DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) TYPE=MyISAM;
