DROP TABLE IF EXISTS `phpcms_iappSpecial_type`;
CREATE TABLE IF NOT EXISTS `phpcms_iappSpecial_type` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `specialid` smallint(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `listorder` smallint(5) DEFAULT '0',
  `setting` mediumtext,
  PRIMARY KEY (`id`)
) TYPE=MyISAM;
