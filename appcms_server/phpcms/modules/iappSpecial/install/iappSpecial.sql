DROP TABLE IF EXISTS `phpcms_iappSpecial`;
CREATE TABLE IF NOT EXISTS `phpcms_iappSpecial` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned default '0' COMMENT '用户ID',
  `username` varchar(20) default NULL COMMENT '用户名',
  `name` varchar(50) NOT NULL COMMENT '专题标题',
  `lname` varchar(50) NOT NULL COMMENT '专题副标题',
  `banner` varchar(80) NOT NULL COMMENT '专题横幅',
  `thumb` varchar(200) NOT NULL COMMENT '专题缩略图',
  `qrcode` varchar(200)  COMMENT '专题二维码',
  `keywords` varchar(200) NOT NULL COMMENT '关键字',
  `description` text NOT NULL COMMENT '描述',
  `content` text NOT NULL COMMENT '专题导语',
  `url` varchar(200) NOT NULL,
  `htis` smallint(5) DEFAULT '0' COMMENT '查看量',
  `praise` smallint(5) DEFAULT '0' COMMENT '赞',
  `status` smallint(5) DEFAULT '0' COMMENT '状态',
  `posids` smallint(5) DEFAULT '0' COMMENT '推荐位',
  `setting` mediumtext,
  `listorder` smallint(5) DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) TYPE=MyISAM;
