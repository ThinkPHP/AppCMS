<?php
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');
$parentid = $menu_db->insert(array('name'=>'56video_manage', 'parentid'=>821, 'm'=>'video56', 'c'=>'video56', 'a'=>'init', 'data'=>'', 'listorder'=>0, 'display'=>'1'), true);
$menu_db->insert(array('name'=>'video_add', 'parentid'=>$parentid, 'm'=>'video56', 'c'=>'video56', 'a'=>'add', 'data'=>'', 'listorder'=>0, 'display'=>'1'));
$menu_db->insert(array('name'=>'import_56_video', 'parentid'=>$parentid, 'm'=>'video56', 'c'=>'video56', 'a'=>'import_56video', 'data'=>'', 'listorder'=>1, 'display'=>'1'));
$menu_db->insert(array('name'=>'video_setting', 'parentid'=>$parentid, 'm'=>'video56', 'c'=>'video56', 'a'=>'setting', 'data'=>'', 'listorder'=>2, 'display'=>'1'));
$menu_db->insert(array('name'=>'video_edit', 'parentid'=>$parentid, 'm'=>'video56', 'c'=>'video56', 'a'=>'edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'video_delete', 'parentid'=>$parentid, 'm'=>'video56', 'c'=>'video56', 'a'=>'delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'video_view', 'parentid'=>$parentid, 'm'=>'video56', 'c'=>'video56', 'a'=>'public_view_video', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'preview_56video', 'parentid'=>$parentid, 'm'=>'video56', 'c'=>'video56', 'a'=>'preview_56video', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'doimport', 'parentid'=>$parentid, 'm'=>'video56', 'c'=>'video56', 'a'=>'doimport', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'video_insert', 'parentid'=>$parentid, 'm'=>'video56', 'c'=>'video_for_ck', 'a'=>'init', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$language = array('56video_manage'=>'56视频管理', 'video_add'=>'添加视频' ,'video_setting'=>'视频设置', 'import_56_video'=>'56视频导入','video_edit'=>'视频编辑','video_delete'=>'视频删除','video_view'=>'视频预览', 'video_insert'=>'56视频插入','preview_56video'=>'导入视频预览','doimport'=>'导视频入库');
?>