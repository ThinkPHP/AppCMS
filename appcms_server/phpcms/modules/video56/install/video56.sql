DROP TABLE IF EXISTS `phpcms_video56`;

CREATE TABLE `phpcms_video56` (
  `videoid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '本地视频ID',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '上传者ID',
  `vid` int(11) unsigned NOT NULL COMMENT '56vid',
  `subject` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '56title',
  `content` text CHARACTER SET utf8 NOT NULL COMMENT '56content',
  `tags` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '视频标签',
  `player` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '56swf播放地址',
  `cover` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '封面图',
  `forbid` char(1) CHARACTER SET utf8 NOT NULL COMMENT 'y-被屏蔽了，n-正常',
  `chk` char(1) CHARACTER SET utf8 NOT NULL COMMENT 'y-通过审核，n-待审',
  `coopid` varchar(15) CHARACTER SET utf8 DEFAULT NULL COMMENT 'coopid',
  `userupload` tinyint(1) NOT NULL DEFAULT '0' COMMENT '原创视频',
  `addtime` int(11) unsigned NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`videoid`)
) ENGINE=MyISAM  COMMENT='56视频管理' ;