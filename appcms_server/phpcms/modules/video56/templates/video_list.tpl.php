<?php 
	defined('IN_ADMIN') or exit('No permission resources.');
	include $this->admin_tpl('header','admin');
?>
<div class="pad_10">
<div class="table-list">
<form name="searchform" action="" method="get" >
<input type="hidden" value="video56" name="m">
<input type="hidden" value="video56" name="c">
<input type="hidden" value="init" name="a">
<input type="hidden" value="<?php echo $_GET['menuid']?>" name="menuid">
<div class="explain-col search-form">
<select name="type" ><option value=""><?php echo L('please_select')?></option><option value="1" <?php if ($_GET['type']==1) {?>selected<?php }?>>视频VID</option><option value="2" <?php if ($_GET['type']==2) {?>selected<?php }?>>视频名称</option></select> <input type="text" value="<?php echo $_GET['q']?>" class="input-text" name="q"> 
添加时间  <?php echo form::date('start_addtime',$_GET['start_addtime'])?><?php echo L('to')?>   <?php echo form::date('end_addtime',$_GET['end_addtime'])?> 
<?php echo form::select($trade_status,$status,'name="status"', L('all_status'))?>&nbsp;&nbsp;
<input type="submit" value="<?php echo L('search')?>" class="button" name="dosubmit">
</div>
</form>
<form name="myform" id="myform" action="" method="post" >
    <table width="100%" cellspacing="0">
        <thead>
            <tr>
            <th width="6%">ID</th>
            <th>视频名称</th>
            <th width="10%">vid</th>
            <th width="15%">添加时间</th>
            <th width="10%">屏蔽</th>
            <th width="10%">状态</th>
            <th width="15%">管理操作</th>
            </tr>
        </thead>
    <tbody>
 <?php 
if(is_array($infos)){
	/*
	 * 为了减少API请求，所以统一获取转码中的视频信息
	 * 如果转码审核通过，则改变状态
	 */
	$vids = '';
	foreach($infos as $info){
		if ($info['chk']=='n' && $info['forbid']=='n') {
			$vids .= $info['vid'].',';
		}
	}
	$data = Open::Video_GetVideoInfo(array('vid'=>$vids));
	if ($data['code']==0) {	//获取成功
		unset($data['code']);	// 删除错误码,cod=0为成功
		foreach ($data as $key => $value) {
			if($value['chk_yn']=='y'){
				$this->db->update(array('chk'=>'y'),array('vid'=>$value['vid']));
				$infos[$key]['chk'] = 'y';
			}elseif ($value['chk_yn']=='n' && ( (time()-$infos[$key]['addtime']) > 86400) ) {	// 大于24小时未转码成功的,则视为被屏蔽
				$this->db->update(array('forbid'=>'y'),array('vid'=>$value['vid']));
				$infos[$key]['forbid'] = 'y';
			}
		}
	}
	
	foreach($infos as $info){

?>   
	<tr>
	<td align="center"><?php echo $info['videoid']?></td>
	<td><?php echo str_cut($info['subject'],80);?> <?php if($info['userupload']){?><img src="<?php echo IMG_PATH; ?>yc.jpg" height="16"><?php }?></td>
	<td align="center"><?php echo $info['vid'];?></td>
	<td align="center"><?php echo date('Y-m-d H:i', $info['addtime'])?></td>
	<td align="center"><?php if($info['forbid']=='y'){?><font color="#ff5c5c">被屏蔽</font><?php }else{echo '正常';} ?></td>
	<td align="center"><?php if($info['chk']=='n'){?><font color="#ff5c5c">转码审核中</font><?php }else{echo '<font color="#3a895d">审核通过</font>';} ?></td>
	<td align="center"><?php  if($info['forbid']=='n' && $info['chk']=='y'){echo '<a href="javascript:void(0);" onclick="view_video('.$info['videoid'].')">预览</a> | ';} ?><a href="index.php?m=video56&c=video56&a=edit&videoid=<?php echo $info['videoid']?>&menuid=<?php echo $_GET['menuid']?>">修改</a> | <a href="javascript:confirmurl('index.php?m=video56&c=video56&a=delete&videoid=<?php echo $info['videoid']?>&menuid=<?php echo $_GET['menuid']?>', '确定删除改视频吗？')">删除</a></td>
	</tr>
<?php 
	}
}
?>
    </tbody>
    </table>
<input type="hidden" value="<?php echo $pc_hash;?>" name="pc_hash">
 <div id="pages"> <?php echo $pages?></div>
</div>
</div>
</form>
</body>
</html>
<script type="text/javascript">
window.top.$('#display_center_id').css('display','none');
function view_video(id) {
	window.top.art.dialog({title:'', id:'view', iframe:'?m=video56&c=video56&a=public_view_video&id='+id ,width:'450px',height:'350px'});
}
</script>