<?php 
	defined('IN_ADMIN') or exit('No permission resources.');
	include $this->admin_tpl('header', 'admin');
?>
<div class="pad-10">
<div class="explain-col search-form">
 请到 <a target="_blank" href="http://dev.56.com/">dev.56.com</a> 申请开通帐号，并创建应用后进行配置
</div>
<div class="common-form">
<form name="myform" action="?m=video56&c=video56&a=setting&pc_hash=<?php echo $_GET['pc_hash'];?>" method="post" id="myform">
<fieldset>
	<legend>视频设置</legend>
<table width="100%" class="table_form">
	<tr>
		<td  width="120">56应用appKey</td> 
		<td><input name="app[appkey]"  type="text"  size="40" value="<?php echo $app['appkey'];?>">&nbsp;*</td>
	</tr>
	<tr>
		<td  width="120">56应用secret</td> 
		<td><input name="app[secret]"  type="text" size="40" value="<?php echo $app['secret'];?>">&nbsp;*</td>
	</tr>
	<tr>
		<td  width="120">接口地址</td> 
		<td><input name="app[domain]"  type="text" size="40" value="<?php echo $app['domain'];?>">&nbsp;*&nbsp;&nbsp;&nbsp;默认为：http://oapi.56.com&nbsp;无需修改</td>
	</tr>
	<tr>
		<td  width="120">php错误提示</td> 
		<td>
			<input type="radio" <?php if ($app['is_developer']==1) {echo 'checked=""';} ?> value="1" name="app[is_developer]">
			是    
			<input type="radio" <?php if ($app['is_developer']==0 || empty($app['is_developer'])) {echo 'checked=""';} ?> value="0" name="app[is_developer]">
			否 
			&nbsp;&nbsp;&nbsp;正式环境下这个记得改为“否”
		</td>
	</tr>
	<tr>
		<td  width="120">Token</td> 
		<td><input name="app[token]"  type="text" size="40" value="<?php echo $app['token'];?>"></td>
	</tr>
	<tr>
		<td  width="120">测试用户ID</td> 
		<td><input name="app[test_user_id]"  type="text" size="10" value="<?php echo $app['test_user_id'];?>"></td>
	</tr>


	<tr>
		<td  width="120">上传组件样式</td> 
		<td><input name="setting[css]"  type="text" id="css"  size="40" value="<?php echo $setting['css'];?>">&nbsp;*&nbsp;&nbsp;&nbsp;<a href="http://oapi.56.com/var/oupload/" target="_blank" ><font color="red">配置样式</font></a></td>
	</tr>
	<tr>
		<td  width="120">上传组件字段</td> 
		<td>
			<input  type="checkbox" value="title" name="fields[]" <?php if (in_array('title',$fields)) {echo 'checked=""';} ?> > 标题&nbsp;&nbsp;&nbsp;
			<input  type="checkbox" value="content" name="fields[]" <?php if (in_array('content',$fields)) {echo 'checked=""';} ?> > 简介&nbsp;&nbsp;&nbsp;
			<input  type="checkbox" value="tags" name="fields[]" <?php if (in_array('tags',$fields)) {echo 'checked=""';} ?> > 标签&nbsp;&nbsp;&nbsp;
		</td>
	</tr>
	<tr>
		<td  width="120">上传成功回调地址</td> 
		<td><input type="text" name="setting[ourl]" size="40" value="<?php echo $setting['ourl']?>" id="ourl">&nbsp;*</td>
	</tr>
	<tr>
		<td  width="120">上传失败回调地址</td> 
		<td><input type="text" name="setting[rurl]" size="40" value="<?php echo $setting['rurl']?>" id="rurl">&nbsp;*</td>
	</tr>


</table>
</fieldset>
<div class="bk15"></div>
<input name="dosubmit" type="submit" value="<?php echo L('submit')?>" class="button" id="dosubmit">
</form>
</div>

</body>
</html>