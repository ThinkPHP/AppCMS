<?php
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_class('api56', 'video56', 0);

class callback {
	
	function __construct() {
		$this->video56 = pc_base::load_model('video56_model');
		
 	}
	
	// 上传成功时回调
	public function success() {
		$data['vid']     = $_GET['vid'];	//视频ID
		$data['subject'] = urldecode($_GET["subject"]);	//视频标题
		$data['userid']  = $_GET["sid"];	//第三方用户标识。现在用做上传者用户ID
		$pc_hash         = $_GET["category"];	//第三方分类标识。现用做pc_hash传到
		$msg             = urldecode($_GET["msg"]);	//信息说明
		$result          = $_GET['result'];	//上传成功1，上传失败0
		$data['player']  = $_GET['player'];	//视频播放地址，区分大小写
		$data['cover']   = $_GET['cover'];	//封面图
		$data['forbid']  = strtolower($_GET['forbid']);	//y-被屏蔽了，n-正常--这个必须要的，因为会有继承屏蔽的情况，比如版权原因，不确定时间解开
		$data['chk']     = strtolower($_GET['chk']);	//y-通过审核，n-待审 ，一般情况下新上传视频都是待审状态
		$data['tags']    = urldecode($_GET["tags"]);	//标签
		$data['content'] = urldecode($_GET["content"]);	//内容
		$data['coopid']  = $_GET["coopid"];	//coop_+(appid)   ,例如 coop_1000008  
		$data['userupload']  = 1;	// 1-原创视频，0-导入视频
		$data['addtime'] = time();	//添加时间

		if($result){
			$videoid = $this->video56->insert($data,true);
			if ($videoid) {
				echo '<script language="javascript" type="text/javascript">
							alert("上传成功");
							parent.window.location.href="index.php?m=video56&c=video56&a=init&pc_hash='.$pc_hash.'";
					</script>';
			} else {
				echo '<script language="javascript" type="text/javascript">
							alert("本地入库失败");
							parent.window.location.href="index.php?m=video56&c=video56&a=add&pc_hash='.$pc_hash.'";
					</script>';
			}
		}


	}
	
	// 上传失败时回调
	public function failure() {
		$pc_hash = $_GET["category"];	//第三方分类标识。现用做pc_hash传到
		$msg     = urldecode($_GET["msg"]);	//信息说明
		echo '<script language="javascript" type="text/javascript">
					alert("'.$msg.'");
					parent.window.location.href="index.php?m=video56&c=video56&a=add&pc_hash='.$pc_hash.'";
			</script>';
	}
}
?>