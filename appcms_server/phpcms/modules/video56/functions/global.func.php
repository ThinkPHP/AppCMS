<?php
// 数组保存到文件
function arr2file($filename, $arr=''){
	if(is_array($arr)){
		$con = var_export($arr,true);
	} else{
		$con = $arr;
	}
	$con = "<?php\nreturn $con;\n?>";//\n!defined('IN_MP') && die();\nreturn $con;\n
	write_file($filename, $con);
}
function mkdirss($dirs,$mode=0777) {
	if(!is_dir($dirs)){
		mkdirss(dirname($dirs), $mode);
		return @mkdir($dirs, $mode);
	}
	return true;
}
function write_file($l1, $l2=''){
	$dir = dirname($l1);
	if(!is_dir($dir)){
		mkdirss($dir);
	}
	return @file_put_contents($l1, $l2);
}

function formatSec($second) {
  $output = '';
  foreach (array(31536000 => '年', 86400 => '天', 3600 => '小时', 60 => '分', 1 => '秒') as $key => $value) {
    if ($second >= $key) $output .= floor($second/$key) . $value;
    $second %= $key;
  }
  return $output;
}