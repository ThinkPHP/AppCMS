<?php
defined('IN_PHPCMS') or exit('No permission resources.');
$session_storage = 'session_'.pc_base::load_config('system','session_storage');
pc_base::load_sys_class($session_storage);
pc_base::load_app_class('admin', 'admin', 0);
pc_base::load_sys_class('form', '', 0);
pc_base::load_app_func('global');
pc_base::load_app_class('api56', 'video56', 0);
class video_select extends admin {

    private $video56;

    function __construct() {
        parent::__construct();
        $this->video56 = pc_base::load_model('video56_model');
    }
	
	/**
	 * 
	 * 视频列表
	 */
	public function init() {
		$where = "`forbid`='n'";
		$page = max(intval($_GET['page']), 1);
		$pagesize = 6;
		$infos = $this->video56->listinfo($where, 'videoid DESC', $page, $pagesize);
		$number = $this->video56->number;
		$pages = $this->pages($number, $page, $pagesize, 4, 'get_videoes');
		include $this->admin_tpl('video_select');
	}

	public function search() {
		$where = "`forbid`='n' ";
		$subject = safe_replace($_GET['subject']);
		if (CHARSET=='gbk') {
			$subject = iconv('gbk', 'utf-8', $subject);
		}
		if ($subject) {
			$where .= ' AND `subject` LIKE \'%'.$subject.'%\'';
		}
		$userupload = intval($_GET['userupload']);
		if ($userupload) {
			$where .= ' AND `userupload`=1';
		}
		$page = $_GET['page'];
		$pagesize = 6;
		$infos = $this->video56->listinfo($where, 'videoid DESC', $page, $pagesize);
		$number = $this->video56->number;
		$pages = $this->pages($number, $page, $pagesize, 4, 'get_videoes');
		if (is_array($infos) && !empty($infos)) {
			$html = '';
			foreach ($infos as $info) {
				$html .= '<li><div class="w9"><a href="javascript:void(0);" onclick="a_click(this);" title="'.$info['subject'].'" data-videoid="'.$info['videoid'].'" data-player="'.$info['player'].'" data-tags="'.$info['tags'].'" ><span></span><img src="'.$info['cover'].'" width="90" height="51" /></a><p>'.str_cut($info['subject'], 18).'</p></div></li>';
			}
		}
		$data['pages'] = $pages;
		$data['html'] = $html;
		if (CHARSET=='gbk') {
			$data = array_iconv($data, 'gbk', 'utf-8');
		}
		exit(json_encode($data));
	}

	/**
	 * Funtion pages
	 * 视频分页
	 * @param int $number 总页数 
	 * @param int $page 当前页
 	 * @param int $pagesize 每页数量
 	 * @param string $js JS属性
	 */
	private function pages($num, $curr_page, $perpage = 20, $setpages = 5, $js = '') {
		$urlrule = url_par('page={$page}');
		$multipage = '';
		if($num > $perpage) {
			$page = $setpages+1;
			$offset = ceil($setpages/2-1);
			$pages = ceil($num / $perpage);
			if (defined('IN_ADMIN') && !defined('PAGES')) define('PAGES', $pages);
			$from = $curr_page - $offset;
			$to = $curr_page + $offset;
			$more = 0;
			if($page >= $pages) {
				$from = 2;
				$to = $pages-1;
			} else {
				if($from <= 1) {
					$to = $page-1;
					$from = 2;
				}  elseif($to >= $pages) {
					$from = $pages-($page-2);
					$to = $pages-1;
				}
				$more = 1;
			}
			$multipage .= '<a class="a1">'.$num.L('page_item').'</a>';
			if($curr_page>0) {
				$multipage .= ' <a href="javascript:void(0);" onclick="'.$js.'('.intval($curr_page-1).')" class="a1">'.L('previous').'</a>';
				if($curr_page==1) {
					$multipage .= ' <span>1</span>';
				} elseif($curr_page>3 && $more) {
					$multipage .= ' <a href="javascript:void(0);" onclick="'.$js.'(1)">1</a>..';
				} else {
					$multipage .= ' <a href="javascript:void(0);" onclick="'.$js.'(1)">1</a>';
				}
			}
			for($i = $from; $i <= $to; $i++) {
				if($i != $curr_page) {
					$multipage .= ' <a href="javascript:void(0);" onclick="'.$js.'('.$i.')">'.$i.'</a>';
				} else {
					$multipage .= ' <span>'.$i.'</span>';
				}
			}
			if($curr_page<$pages) {
				if($curr_page<$pages-2 && $more) {
					$multipage .= ' ..<a href="javascript:void(0);" onclick="'.$js.'('.$pages.')">'.$pages.'</a> <a href="javascript:void(0);" onclick="'.$js.'('.intval($curr_page+1).')" class="a1">'.L('next').'</a>';
				} else {
					$multipage .= ' <a href="javascript:void(0);" onclick="'.$js.'('.$pages.')">'.$pages.'</a> <a href="javascript:void(0);" onclick="'.$js.'('.intval($curr_page+1).')" class="a1">'.L('next').'</a>';
				}
			} elseif($curr_page==$pages) {
				$multipage .= ' <span>'.$pages.'</span> <a href="javascript:void(0);" onclick="'.$js.'('.$curr_page.')" class="a1">'.L('next').'</a>';
			} else {
				$multipage .= ' <a href="javascript:void(0);" onclick="'.$js.'('.$pages.')">'.$pages.'</a> <a href="javascript:void(0);" onclick="'.$js.'('.intval($curr_page+1).')" class="a1">'.L('next').'</a>';
			}
		}
		return $multipage;
	}



}