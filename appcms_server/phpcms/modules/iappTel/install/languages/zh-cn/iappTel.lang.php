<?php

/*Language Format:
Add a new file(.lang.php) with your module name at /phpcms/languages/
translation save at the array:$LANG
*/

$LANG['iappTel_del_cofirm'] = '确认删除?';
$LANG['phpcms_friends'] = '手机网友';
$LANG['confirm_delete'] = '确认删除选择的分类吗？';
$LANG['iappTel_add_site'] = '添加手机站点';
$LANG['parameter_error'] = '参数错误';
$LANG['iappTel_add_site'] = '添加手机站点';
$LANG['iappTel_del_succ'] = '删除成功';
$LANG['iappTel_change_status'] = '更新状态成功';
$LANG['iappTel_add_samesite_error'] = '请勿重复添加相同站点';
$LANG['iappTel_empty_type'] = '分类名称为空';
$LANG['iappTel_empty_bound_type'] = '绑定栏目为空';
$LANG['iappTel_repeat_bound_error'] = '该栏目已绑定过，无法重复绑定';
$LANG['iappTel_add_subtype'] = '＋添加子分类';
$LANG['iappTel_repeat_bound'] = '栏目绑定重复';
$LANG['iappTel_type_del_error'] = '该分类含有子栏目，请先删除其子栏目';
$LANG['iappTel_permission_denied_del'] = '无权限删除该站点';

$LANG['basic_config'] = '基本设置';
$LANG['parameter_config'] = '参数设置';
$LANG['iappTel_sel_site'] = '选择要开启的站点：';
$LANG['iappTel_belong_site'] = '所属站点：';
$LANG['iappTel_sitename'] = '站点名称：';
$LANG['iappTel_logo'] = '手机门户站点LOGO：';
$LANG['iappTel_domain'] = '绑定域名';
$LANG['iappTel_listnum'] = '列表页条数：';
$LANG['iappTel_thumb'] = '缩略图尺寸：';
$LANG['iappTel_content_page'] = '内容页分页字节数：';
$LANG['iappTel_index_tpl'] = '首页模板：';
$LANG['iappTel_cat_tpl'] = '频道页模板：';
$LANG['iappTel_list_tpl'] = '列表页模板：';
$LANG['iappTel_show_tpl'] = '内容页模板：';
$LANG['iappTel_hotword'] = '热词';
$LANG['iappTel_open'] = '开启';
$LANG['iappTel_close'] = '关闭';
$LANG['iappTel_type_manage'] = '分类管理';
$LANG['iappTel_type_name'] = '分类名称  ';
$LANG['iappTel_toptype_add'] = '添加顶级分类';
$LANG['iappTel_type_bound'] = '选择要绑定的栏目';
$LANG['iappTel_bound_type'] = '绑定栏目';
$LANG['iappTel_guestbook'] = '发表留言成功!页面正在返回……';
$LANG['iappTel_goback'] = '返回正文页';
$LANG['iappTel_close_status'] = '您访问的站点不存在或者未开启iappTel访问';

?>