DROP TABLE IF EXISTS `phpcms_iappTel`;
CREATE TABLE IF NOT EXISTS `phpcms_iappTel` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '',
  `tels` mediumtext,
  `setting` mediumtext,
  `siteid` smallint(5) DEFAULT '1',
  `listorder` smallint(5) DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) TYPE=MyISAM;

