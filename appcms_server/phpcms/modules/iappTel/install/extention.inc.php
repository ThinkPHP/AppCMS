<?php
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');

$menus = $menu_db->select(array('name'=>'iappPlug', 'm'=>'iapp', 'c'=>'manages', 'a'=>'init'));

$parentid = $menu_db->insert(array('name'=>'iappTel', 'parentid'=>$menus[0][id], 'm'=>'iappTel', 'c'=>'manages', 'a'=>'init', 'data'=>'', 'listorder'=>1, 'display'=>'1'), true);


$menu_db->insert(array('name'=>'iappTel_add', 'parentid'=>$parentid, 'm'=>'iappTel', 'c'=>'manages', 'a'=>'add', 'data'=>'', 'listorder'=>2, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappTel_edit', 'parentid'=>$parentid, 'm'=>'iappTel', 'c'=>'manages', 'a'=>'edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappTel_delete', 'parentid'=>$parentid, 'm'=>'iappTel', 'c'=>'manages', 'a'=>'delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));

$language = array('iappTel'=>'常用电话',
				  'iappTel_add'=>'添加电话',
				  'iappTel_edit'=>'修改电话',
				  'iappTel_delete'=>'删除电话'				  
                  );

?>