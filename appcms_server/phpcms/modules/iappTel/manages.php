<?php 
defined('IN_PHPCMS') or exit('No permission resources.'); 
pc_base::load_app_class('admin','admin',0);
pc_base::load_sys_class('form', '', 0);
class manages extends admin {
	private $db,$type_db;
	function __construct() {
		parent::__construct();
		$this->sites = pc_base::load_app_class('sites','admin');
		$this->db = pc_base::load_model('iappTel_model');
		$this->siteid = $this->get_siteid();
	}
	
	function init() {
		$page = max(intval($_GET['page']), 1);
		$data = $this->db->listinfo(array('siteid'=>$this->get_siteid()), '`listorder` DESC', $page);
		
		include $this->admin_tpl('tel_list');
	}
	
	
	
	function add() {
		if($_POST['dosubmit']) {
			$title = trim($_POST['title']);
			$tels = trim($_POST['tels']);
			
			$telarr = explode("\n", $tels);
			$td = array();
			$a = 0;
			$b = 0;
			foreach ($telarr as $_v) {
				if(trim($_v)=='') continue;
				$_v = str_replace(array("\n", "\r"), '', $_v);
				$level = substr_count($_v, '-')+1;
				if($level==1){
					$td[$a][title]=$_v;
					$a++;
				}else if($level==2){
					$k = $a-1;
					$_vb = str_replace('-', '', $_v);
					$_vb = explode(" ", $_vb);
					$td[$k][items][]= str_replace('-', '', $_vb);
					$b++;
				}
			}
			
			
			
			$setting = array2string($_POST['setting']);
			$siteid = $this->siteid;
			$return_id = $this->db->insert(array('title'=>$title,'tels'=>array2string($td),'setting'=>$setting,'siteid'=>$siteid,'listorder'=>0,'addtime'=>time()),'1');
			showmessage(L('add_success'), '?m=iappTel&c=manages&a=init');
		} else {
			include $this->admin_tpl('tel_add');			
		}		
	}
	
	function edit() {
		if($_POST['dosubmit']) {
			$id = intval($_POST['id']) ? intval($_POST['id']) : showmessage(L('parameter_error'),HTTP_REFERER);
			$title = trim($_POST['title']);
			$tels = trim($_POST['tels']);
			$setting = array2string($_POST['setting']);
			
			
			$telarr = explode("\n", $tels);
			$td = array();
			$a = 0;
			$b = 0;
			foreach ($telarr as $_v) {
				if(trim($_v)=='') continue;
				$_v = str_replace(array("\n", "\r"), '', $_v);
				$level = substr_count($_v, '-')+1;
				if($level==1){
					$td[$a][title]=$_v;
					$a++;
				}else if($level==2){
					$k = $a-1;
					$_vb = str_replace('-', '', $_v);
					$_vb = explode(" ", $_vb);
					$td[$k][items][]= str_replace('-', '', $_vb);
					$b++;
				}
			}
			
			$this->db->update(array('title'=>$title,'tels'=>array2string($td),'setting'=>$setting), array('id'=>$id));

			showmessage(L('operation_success'), '?m=iappTel&c=manages&a=edit&id='.$id.'&menuid='.$_GET['menuid']);
		} else {
			$id = intval($_GET['id']) ? intval($_GET['id']) : showmessage(L('parameter_error'),HTTP_REFERER);
					
			$info = $this->db->get_one(array('id'=>$id));
			if($info) {
				$info[setting] = string2array($info[setting]);
				$tels = string2array($info[tels]);
				$telsstr = '';
				foreach ($tels as $k=>$_v) {
					if(trim($_v)=='') continue;
					$telsstr .= $_v[title]."\n";
					foreach ($tels[$k][items] as $r) {
						if(trim($r)=='') continue;
						$telsstr .= "-".$r[0]." ".$r[1]."\n";
					}
				}
				
				$info[tels] = $telsstr;
			}
			
			include $this->admin_tpl('tel_edit');			
		}
	}
	
	/**
	 * 删除
	 */
	public function delete() {
		$siteid = $this->siteid;
		
		if (isset($_GET['id']) && !empty($_GET['id'])) {
			$id = intval($_GET['id']);
			if($id<10) showmessage(L('禁止删除'),HTTP_REFERER);
			$this->db->delete(array('id'=>$id, 'siteid'=>$siteid));
			showmessage(L('operation_success'), HTTP_REFERER);
		} elseif (isset($_POST['ids']) && !empty($_POST['ids'])) {
			
			if (is_array($_POST['ids'])) {
				foreach ($_POST['ids'] as $fid) {
					if($fid<10) showmessage(L('禁止删除'),HTTP_REFERER);
					$this->db->delete(array('id'=>$fid, 'siteid'=>$siteid));
				}
			}
			showmessage(L('operation_success'), HTTP_REFERER);
		} else {
			showmessage(L('illegal_operation'), HTTP_REFERER);
		}
	}
	
	
	/**
	 * 排序
	 */
	public function listorder() {
	     
		if (isset($_POST['listorders']) && is_array($_POST['listorders'])) {
			$listorder = $_POST['listorders'];
			
			foreach ($listorder as $k => $v) {
				
				$this->db->update(array('listorder'=>$v), array('id'=>$k));
			}
		}
		showmessage(L('operation_success'), HTTP_REFERER);
	}

}
?>