<?php
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_class('admin','admin',0);
pc_base::load_app_func('global');
class category extends admin {
	private $db;
	public $siteid;
	function __construct() {
		parent::__construct();
		$this->db = pc_base::load_model('category_model');
		$this->siteid = $this->get_siteid();
		
	}
	/**
	 * 管理栏目
	 */
	public function init () {
		$show_header = true;
		$show_pc_hash = '';
		$modelid = intval($_GET['modelid']);
		if(!$modelid){
			include $this->admin_tpl('main');
			exit;
		}
	

		$modellist = getcache('modellist', 'commons');
		
		
		
		
		$modename = $modellist[$modelid][name];
		$tree = pc_base::load_sys_class('tree');
		$category_items = array();
		$tree->icon = array('&nbsp;&nbsp;&nbsp;│ ','&nbsp;&nbsp;&nbsp;├─ ','&nbsp;&nbsp;&nbsp;└─ ');
		$tree->nbsp = '&nbsp;&nbsp;&nbsp;';
		$categorys = array();
		
		$category_items = getcache('category_items_'.$modelid,'commons');
		$result = getcache('category_'.$modelid,'commons');
		
		$show_detail = count($result) < 500 ? 1 : 0;
		$parentid = $_GET['parentid'] ? $_GET['parentid'] : 0;

		if(!empty($result)) {
			foreach($result as $r) {
				$r['str_manage'] = '';
				if(!$show_detail) {
					if($r['parentid']!=$parentid) continue;
					$r['parentid'] = 0;
					$r['str_manage'] .= '<a href="?m=category&c=category&a=init&parentid='.$r['catid'].'&modelid='.$modelid.'&menuid='.$_GET['menuid'].'&s='.$r['type'].'&pc_hash='.$_SESSION['pc_hash'].'">管理子分类</a> | ';
				}
				
				$r['str_manage'] .= '<a href="?m=category&c=category&a=add&parentid='.$r['catid'].'&menuid='.$_GET['menuid'].'&modelid='.$r['modelid'].'&pc_hash='. $_SESSION['pc_hash'].'">添加子分类</a> | ';

				$r['str_manage'] .= '<a href="?m=category&c=category&a=edit&catid='.$r['catid'].'&menuid='.$_GET['menuid'].'&modelid='.$r['modelid'].'&pc_hash='. $_SESSION['pc_hash'].'">修改</a> | <a href="javascript:confirmurl(\'?m=category&c=category&a=delete&catid='.$r['catid'].'&modelid='.$r['modelid'].'&menuid='.$_GET['menuid'].'\',\''.L('confirm',array('message'=>addslashes($r['catname']))).'\')">删除</a> ';
				
				if($r['child'] || !isset($category_items[$r['catid']])) {
					$r['items'] = '';
				} else {
					$r['items'] = $category_items[$r['catid']];
				}
				$setting = string2array($r['setting']);
				if($r['url']) {
					$r['url'] = "<a href='$r[url]' target='_blank'>".L('访问')."</a>";
				} else {
					$r['url'] = "<a href='?m=category&c=category&a=public_cache&menuid=".$_GET['menuid']."&modelid=".$modelid."'><font color='red'>".L('更新缓存')."</font></a>";
				}
				$categorys[$r['catid']] = $r;
			}
		}
		
		$str  = "<tr>
					<td align='center'><input name='listorders[\$id]' type='text' size='5' value='\$listorder' class='input-text-c'></td>
					<td align='center'>\$id</td>
					<td >\$spacer\$catname</td>
					<td align='center'>\$items</td>
					<td align='center'>\$url</td>
					<td align='center' >\$str_manage</td>
				</tr>";
				
		$tree->init($categorys);
		$categorys = $tree->get_tree(0, $str);
		$big_menu = array('javascript:window.top.art.dialog({id:\'add\',iframe:\'?m=category&c=category&a=add&modelid='.$modelid.'\', title:\''.'添加'.$modename.'分类'.'\', width:\'700\', height:\'500\', lock:true}, function(){var d = window.top.art.dialog({id:\'add\'}).data.iframe;var form = d.document.getElementById(\'dosubmit\');form.click();return false;}, function(){window.top.art.dialog({id:\'add\'}).close()});void(0);', '添加'.$modename.'分类');
		
		include $this->admin_tpl('category_manage');
	}
	/**
	 * 添加栏目
	 */
	public function add() {
		
		$modelid = intval($_GET['modelid']);
		if (!$modelid){
			showmessage('请先选择模型');
		}
		//$model_db = pc_base::load_model('sitemodel_model');
		//$model = model_k2id($model_db->select(array('siteid'=>$this->siteid)));
		$modellist = getcache('modellist', 'commons');
		$modename = $modellist[$modelid][name];
		
		if(isset($_POST['dosubmit'])) {
			pc_base::load_sys_func('iconv');
			
			$_POST['info']['modelid'] = $modelid;
			$_POST['info']['type'] = intval($_POST['type']);

			if(isset($_POST['batch_add']) && empty($_POST['batch_add'])) {
				if($_POST['info']['catname']=='') showmessage(L('input_catname'));
			}

			$_POST['info']['siteid'] = $this->siteid;
			$_POST['info']['module'] = $modellist[$modelid][module];
			$setting = $_POST['setting'];
			$_POST['info']['setting'] = array2string($setting);
			
			/*
            if ($_POST['additional']) {
                $_POST['info']['additional'] = array2string($_POST['additional']);
            } else {
                $_POST['info']['additional'] = '';
            }
			*/

			if(!isset($_POST['batch_add']) || empty($_POST['batch_add'])) {
				$catname = CHARSET == 'gbk' ? $_POST['info']['catname'] : iconv('utf-8','gbk',$_POST['info']['catname']);
				$letters = new_addslashes(gbk_to_pinyin($catname));
				
				$_POST['info']['letter'] = strtolower(implode('', $letters));
				
				
				
				$data = $_POST['info'];
				$catid = $this->db->insert($data, true);
			} else {
				$end_str = '';
				$batch_adds = explode("\n", $_POST['batch_add']);
				if ($_POST['info']['parentid']) {
					$parentid = $_POST['info']['parentid'];
				}
				foreach ($batch_adds as $_v) {
					if(trim($_v)=='') continue;
					$_v = str_replace(array(' ', "\n", "\r"), '', $_v);
					$_POST['info']['catname'] = $_v;
					$level = substr_count($_v, '-')+1;
					if ($level>1) {
						$_POST['info']['catname'] = $_v = str_replace('-', '', $_v);
					}
					$letters = new_addslashes(gbk_to_pinyin($_v));
					if ($level>1) {
						$plevel = intval($level-1);
						$_POST['info']['parentid'] = ${'parentid_'.$plevel};
					} else {
						$_POST['info']['parentid'] = $parentid;
					}
					$_POST['info']['letter'] = strtolower(implode('', $letters));
					$data = $_POST['info'];
					${'parentid_'.$level} = $this->db->insert($_POST['info'], true);
				}
			}
			
			$this->cache($modelid);
			
			showmessage('栏目添加成功','?m=category&c=category&a=init&modelid='.$modelid);
		} else {

			$show_validator = $select_modelid = '';
			$modelid = intval($_GET['modelid']);
			if (!$modelid) showmessage(L('select_catgory_for_model'));
			$categorys = getcache('category_'.$modelid, 'commons');
            
			
			$commenttypeid = '';
			if(isset($_GET['parentid'])) {
				$parentid = $_GET['parentid'];
				/*
				$parent_arr = $parent_addition = array();
				if ($categorys[$parentid]['parentid']) {
					$parent_arr = substr($categorys[$parentid]['arrparentid'], 2).','.$parentid;
					$parent_arr = explode(',', $parent_arr);
				} else {
					$parent_arr[] = $parentid;
				}
				
				foreach ($parent_arr as $par) {
					$r = $this->db->get_one(array('catid'=>$par), 'additional, commenttypeid');
					$r1 = string2array($r['additional']);
                    if ($r['commenttypeid']) {
                        $commenttypeid = $r['commenttypeid'];
                    }
					if (!empty($r1)) {
						if (empty($parent_addition)) {
							$parent_addition = $r1;
						} else {
							$parent_addition = array_merge($parent_addition, $r1);
						}
					}
					unset($r, $r1);
				}
				*/
			}
			/*
			$additional_field = getcache('additional_field', 'model');
            $dianping_types = getcache('dianping_type', 'dianping');
            if (is_array($dianping_types) && !empty($dianping_types)) {
                foreach ($dianping_types as $did => $dp) {
                    $dianping_arr[$did] = $dp['type_name'];
                }
            }
			*/
			pc_base::load_sys_class('form','',0);
			$show_header = $show_validator = $show_scroll = 1;
			include $this->admin_tpl('category_add');
		}
	}
	/**
	 * 修改栏目
	 */
	public function edit() {
		if(isset($_POST['dosubmit'])) {
			pc_base::load_sys_func('iconv');
			$catid = 0;
			$catid = intval($_POST['catid']);
			$modelid = $_POST['modelid'] ? intval($_POST['modelid']) : 0;
	
			$modellist = getcache('modellist', 'commons');
			
			$setting = $_POST['setting'];

			$_POST['info']['setting'] = array2string($setting);
			$_POST['info']['module'] = $modellist[$modelid][module];
			
			unset($_POST['modelid']);
			$catname = CHARSET == 'gbk' ? $_POST['info']['catname'] : iconv('utf-8','gbk',$_POST['info']['catname']);
			$letters = new_addslashes(gbk_to_pinyin($catname));
			$_POST['info']['letter'] = strtolower(implode('', $letters));
			/*
            if ($_POST['additional']) {
                $_POST['info']['additional'] = array2string($_POST['additional']);
            } else {
                $_POST['info']['additional'] = '';
            }
			*/
		
			$this->db->update($_POST['info'],array('catid'=>$catid,'siteid'=>$this->siteid));
			/*
			$usechild = intval($_POST['usechild']);
			if ($usechild) {
				$arrchildid = $this->db->get_one(array('catid'=>$catid), 'arrchildid');
				if(!empty($arrchildid['arrchildid'])) {
					$arrchildid_arr = explode(',', $arrchildid['arrchildid']);
					if(!empty($arrchildid_arr)) {
						$commenttypeid = $_POST['info']['commenttypeid'];
						foreach ($arrchildid_arr as $arr_v) {
							$this->db->update(array('commenttypeid'=>$commenttypeid), array('catid'=>$arr_v));
						}
					}
				}
			}
			*/
			$this->cache($modelid);
			//更新附件状态
			if($_POST['info']['image'] && pc_base::load_config('system','attachment_stat')) {
				$this->attachment_db = pc_base::load_model('attachment_model');
				$this->attachment_db->api_update($_POST['info']['image'],'catid-'.$catid,1);
			}
			showmessage(L('operation_success'),HTTP_REFERER, '', 'edit');
		} else {

			$show_validator = $catid = $r = '';
			$catid = intval($_GET['catid']);
			$modelid = intval($_GET['modelid']);
			if (!$modelid) showmessage(L('select_catgory_for_model'));
			pc_base::load_sys_class('form','',0);
			$r = $this->db->get_one(array('catid'=>$catid));
            $self_commenttypeid = $r['commenttypeid'];
			if($r) extract($r);
            $categorys = getcache('category_'.$modelid, 'commons');
			
			/*
			$parent_arr = $parent_addition = array();
			if($parentid) {
				if ($categorys[$parentid]['parentid']) {
					$parent_arr = substr($categorys[$parentid]['arrparentid'], 2).','.$parentid;
					$parent_arr = explode(',', $parent_arr);
				} else {
					$parent_arr[] = $parentid;
				}
				foreach ($parent_arr as $par) {
					$r = $this->db->get_one(array('catid'=>$par), 'additional, commenttypeid');
                    if ($r['commenttypeid']) {
                        $commenttypeid = $r['commenttypeid'];
                    }
					$r1 = string2array($r['additional']);
					if (!empty($r1)) {
						if (empty($parent_addition)) {
							$parent_addition = $r1;
						} else {
							$parent_addition = array_merge($parent_addition, $r1);
						}
					}
					unset($r, $r1);
				}
			}
            if ($self_commenttypeid) {
                $commenttypeid = $self_commenttypeid;
            }
			$additional_field = getcache('additional_field', 'model');
			$setting = string2array($setting);
			$additional = string2array($additional);
            $dianping_types = getcache('dianping_type', 'dianping');
            if (is_array($dianping_types) && !empty($dianping_types)) {
                foreach ($dianping_types as $did => $dp) {
                    $dianping_arr[$did] = $dp['type_name'];
                }
            }
			*/

			$show_header = $show_validator = $show_scroll = 1;
			include $this->admin_tpl('category_edit');
		}
	}

    /**
     * 获取附件字段
     */
    public function public_get_additional() {
        $catid = intval($_GET['catid']);
        $parentid = intval($_GET['parentid']);
        $modelid = intval($_GET['modelid']);
        $categorys = getcache('category_'.$modelid, 'commons');
        if ($parentid) {
        	$parent_arr = $parent_addition = array();
			if ($categorys[$parentid]['parentid']) {
				$parent_arr = substr($categorys[$parentid]['arrparentid'], 2).','.$parentid;
				$parent_arr = explode(',', $parent_arr);
			} else {
				$parent_arr[] = $parentid;
			}
			foreach ($parent_arr as $par) {
				$r = $this->db->get_one(array('catid'=>$par), 'additional');
				$r1 = string2array($r['additional']);
				if (!empty($r1)) {
					if (empty($parent_addition)) {
						$parent_addition = $r1;
					} else {
						$parent_addition = array_merge($parent_addition, $r1);
					}
				}
				unset($r, $r1);
			}
        }
        if ($catid) {
            $r = $this->db->get_one(array('catid'=>$catid), 'additional');
			$additional = string2array($r['additional']);
        }
        $additional_field = getcache('additional_field', 'model');
        $data = '';
        if (is_array($additional_field)) {
            foreach ($additional_field as $a) {
                $data .= '<label class="ib" style="width:125px;">';
                if (in_array($a['fieldid'], $parent_addition)) {
                    $data .= '<input type="checkbox" name="additional[]" disabled checked="checked" value="'.$a['fieldid'].'">';
                } else {
	                $data .= '<input type="checkbox" name="additional[]" ';
	                if (in_array($a['fieldid'], $additional)) {
	                    $data .= 'checked ';
	                }
	                $data .= 'value="'.$a['fieldid'].'">';
                }
                $data .= ' '.$a['name'].'</label>';
            }
        }
        exit($data);
    }
	/**
	 * 排序
	 */
	public function listorder() {
		if(isset($_POST['dosubmit'])) {
			$modelid = intval($_GET['modelid']);
			foreach($_POST['listorders'] as $id => $listorder) {
				$this->db->update(array('listorder'=>$listorder),array('catid'=>$id));
			}
			$this->cache($modelid);
			showmessage(L('operation_success'),HTTP_REFERER);
		} else {
			showmessage(L('operation_failure'));
		}
	}
	/**
	 * 删除分类
	 */
	public function delete() {
		$catid = intval($_GET['catid']);
		$modelid = intval($_GET['modelid']);
		$this->delete_child($catid);
		$this->db->delete(array('catid'=>$catid));
		$this->cache($modelid);
		showmessage(L('operation_success'),HTTP_REFERER);
	}
	/**
	 * 递归删除栏目
	 * @param $catid 要删除的栏目id
	 */
	private function delete_child($catid) {
		$catid = intval($catid);
		if (empty($catid)) return false;
		$r = $this->db->get_one(array('parentid'=>$catid));
		if($r) {
			$this->delete_child($r['catid']);
			$this->db->delete(array('catid'=>$r['catid']));
		}
		return true;
	}
	/**
	 * 更新缓存
	 */
	public function cache($modelid = 0) {
		//$categorys = $iappYp_models = $this->categorys = array();
		/*
		if (!$modelid) {
			//获取企业库模型ID
			$sitemodel_db = pc_base::load_model('sitemodel_model');
			$iappYp_company_model = $sitemodel_db->get_one(array('tablename'=>'iappYp_company', 'type'=>'57'), 'modelid');
			$iappYp_company_modelid = $iappYp_company_model['modelid'];
			$iappYp_models = getcache('iappYp_model', 'model');
			if (is_array($iappYp_models)) {
				$iappYp_models = array_keys($iappYp_models);
			}
			$iappYp_models[] = $iappYp_company_modelid;
			if (is_array($iappYp_models)) {
				foreach ($iappYp_models as $mid) {
					$datas = $this->db->select(array('modelid'=>$mid),'catid,items',10000);
					$array = array();
					foreach ($datas as $r) {
						$array[$r['catid']] = $r['items'];
					}
					setcache('category_iappYp_items_'.$modelid, $array,'commons');
					$result = $this->db->select(array('module'=>'iappYp', 'modelid'=>$mid),'*',20000,'listorder ASC');
					foreach ($result as $r) {
						unset($r['module'],$r['catdir']);
						$setting = string2array($r['setting']);
						$r['meta_title'] = $setting['meta_title'];
						$r['meta_keywords'] = $setting['meta_keywords'];
						$r['meta_description'] = $setting['meta_description'];
						$categorys[$r['catid']] = $r;
					}
					setcache('category_iappYp_'.$mid,$categorys,'commons');
					$categorys = array();
				}
			}
		} else {
		*/
			
			$modelid = intval($modelid);
			$this->count_items($modelid);

			$datas = $this->db->select(array('modelid'=>$modelid),'catid,items',10000);
	
			$array = array();
			foreach ($datas as $r) {
				$array[$r['catid']] = $r['items'];
			}
			setcache('category_items_'.$modelid, $array,'commons');
			$result = $this->db->select(array('modelid'=>$modelid),'*',20000,'listorder ASC');
			foreach ($result as $r) {
				unset($r['module'],$r['catdir']);
				$setting = string2array($r['setting']);
				$r['meta_title'] = $setting['meta_title'];
				$r['meta_keywords'] = $setting['meta_keywords'];
				$r['meta_description'] = $setting['meta_description'];
				$categorys[$r['catid']] = $r;
			}
			$this->categorys = $categorys;
			setcache('category_'.$modelid,$categorys,'commons');
			
		//}
		return true;
	}
	
	/**
	 * 更新缓存并修复栏目
	 */
	public function public_cache() {
		$modelid = intval($_GET['modelid']);
		$modellist = getcache('modellist', 'commons');
		$modename = $modellist[$modelid][name];
		if (!$modelid){
			showmessage('请先选择模型');
		}
		$this->repair($modelid);
		$this->cache($modelid);
		
		showmessage("更新『 $modename 』栏目缓存成功",'?m=category&c=category&a=init&modelid='.$modelid);
	}
	
	
	/**
	 * 更新所有栏目缓存
	 */
	public function public_cache_all() {
		
		$modellist = getcache('modellist', 'commons');
		//print_r($modellist);
		//exit;
		//$modename = $modellist[$modelid][name];
		//$t = '';
		$n = 0;
		foreach($modellist as $k => $v) {
			$this->repair($v[modelid]);
			$this->cache($v[modelid]);
	
			
			//$t .= $v[name].'|';
			$n++;
		}
		showmessage(" $n 个模型缓存成功",HTTP_REFERER);
		//showmessage(" $n 个模型缓存成功",'?m=category&c=category&a=init');
	}
	
	
	
	/**
	* 修复栏目数据
	* @param intval $modelid 模型ID
	*/
	private function repair($modelid) {
		pc_base::load_sys_func('iconv');
		@set_time_limit(600);
		$html_root = pc_base::load_config('system','html_root');
		
		$modellist = getcache('modellist', 'commons');
		
		$this->categorys = $categorys = array();
		$this->categorys = $categorys = $this->db->select(array('module'=>$modellist[$modelid][module], 'modelid'=>$modelid), '*', '', 'listorder ASC, catid ASC', '', 'catid');
		
		
		$MODEL = $modellist[$modelid];
		//判断是不是企业库模型的分类，企业库模型分类的url地址特殊
		$is_company_model = false;
		if (!$MODEL || !in_array($modelid, $MODEL)) {
			$is_company_model = true;
		}
		
		//$this->get_categorys($categorys);
		if(is_array($this->categorys)) {
			foreach($this->categorys as $catid => $cat) {
				$arrparentid = $this->get_arrparentid($catid);
				$setting = string2array($cat['setting']);
				$arrchildid = $this->get_arrchildid($catid);
				$child = is_numeric($arrchildid) ? 0 : 1;
				if($categorys[$catid]['arrparentid']!=$arrparentid || $categorys[$catid]['arrchildid']!=$arrchildid || $categorys[$catid]['child']!=$child) $this->db->update(array('arrparentid'=>$arrparentid,'arrchildid'=>$arrchildid,'child'=>$child),array('catid'=>$catid));

				$catname = $cat['catname'];
				$letters = gbk_to_pinyin($catname);
				$letter = new_addslashes(strtolower(implode('', $letters)));
				$listorder = $cat['listorder'] ? $cat['listorder'] : $catid;

				//不生成静态时
				$url = $this->update_url($catid,$modelid,$is_company_model);
				if($cat['url']!=$url) $this->db->update(array('url'=>$url), array('catid'=>$catid));

				$this->db->update(array('letter'=>$letter,'listorder'=>$listorder), array('catid'=>$catid));
			}
		}

		//删除在非正常显示的栏目
		foreach($this->categorys as $catid => $cat) {
			if($cat['parentid'] != 0 && !isset($this->categorys[$cat['parentid']])) {
				$this->db->delete(array('catid'=>$catid));
			}
		}
		return true;
	}

	/**
	 * 找出子目录列表
	 * @param array $categorys
	 */
	private function get_categorys($categorys = array()) {
		if (is_array($categorys) && !empty($categorys)) {
			foreach ($categorys as $catid => $c) {
				$this->categorys[$catid] = $c;
				$result = array();
				foreach ($this->categorys as $_k=>$_v) {
					if($_v['parentid']) $result[] = $_v;
				}
				$this->get_categorys($r);
			}
		}
		return true;
	}
	/**
	* 更新栏目链接地址
	*/
	private function update_url($catid, $modelid, $is_company_model) {
		$catid = intval($catid);
		$modelid = intval($modelid);
		
		$modellist = getcache('modellist', 'commons');
		$url = APP_PATH.'index.php?m='.$modellist[$modelid][module].'&c=index&a=lists&t='.$modellist[$modelid][tablename].'&catid='.$catid;
		return $url;
		exit;
		
		if (!$catid) return false;
		pc_base::load_app_func('global');

        //获取模块缓存，查看是否启用伪静态
        $setting = getcache('iappYp_setting', 'iappYp');
        if ($is_company_model) {
        	if ($setting['enable_rewrite']) {
        		$url = 'iappYp-list-company-'.$catid;
        	} else {
        		$url = APP_PATH.'index.php?m=iappYp&c=index&a=list_company&catid='.$catid;
        	}
        } else {
       		if ($setting['enable_rewrite']) {
            	$url = iappYp_filters_url('catid', array('catid'=>$catid, 'page'=>1), 2, $this->modelid);
	        } else {
	            $url = APP_PATH.'index.php?m=iappYp&c=index&a=lists&catid='.$catid;
	        }
        }
		return $url;
	}

	/**
	 *
	 * 获取父栏目ID列表
	 * @param integer $catid              栏目ID
	 * @param array $arrparentid          父目录ID
	 * @param integer $n                  查找的层次
	 */
	private function get_arrparentid($catid, $arrparentid = '', $n = 1) {
		if($n > 5 || !is_array($this->categorys) || !isset($this->categorys[$catid])) return false;
		$parentid = $this->categorys[$catid]['parentid'];
		$arrparentid = $arrparentid ? $parentid.','.$arrparentid : $parentid;
		if($parentid) {
			$arrparentid = $this->get_arrparentid($parentid, $arrparentid, ++$n);
		} else {
			$this->categorys[$catid]['arrparentid'] = $arrparentid;
		}
		$parentid = $this->categorys[$catid]['parentid'];
		return $arrparentid;
	}

	/**
	 *
	 * 获取子栏目ID列表
	 * @param $catid 栏目ID
	 */
	private function get_arrchildid($catid) {
		$arrchildid = $catid;
		if(is_array($this->categorys)) {
			foreach($this->categorys as $id => $cat) {
				if($cat['parentid'] && $id != $catid && $cat['parentid']==$catid) {
					$arrchildid .= ','.$this->get_arrchildid($id);
				}
			}
		}
		return $arrchildid;
	}

	/**
	 * 更新权限
	 * @param  $catid
	 * @param  $priv_datas
	 * @param  $is_admin
	 */
	private function update_priv($catid,$priv_datas,$is_admin = 1) {
		$this->priv_db = pc_base::load_model('category_priv_model');
		$this->priv_db->delete(array('catid'=>$catid,'is_admin'=>$is_admin));
		if(is_array($priv_datas) && !empty($priv_datas)) {
			foreach ($priv_datas as $r) {
				$r = explode(',', $r);
				$action = $r[0];
				$roleid = $r[1];
				$this->priv_db->insert(array('catid'=>$catid,'roleid'=>$roleid,'is_admin'=>$is_admin,'action'=>$action,'siteid'=>$this->siteid));
			}
		}
	}

	/**
	 * 检查栏目权限
	 * @param $action 动作
	 * @param $roleid 角色
	 * @param $is_admin 是否为管理组
	 */
	private function check_category_priv($action,$roleid,$is_admin = 1) {
		$checked = '';
		foreach ($this->privs as $priv) {
			if($priv['is_admin']==$is_admin && $priv['roleid']==$roleid && $priv['action']==$action) $checked = 'checked';
		}
		return $checked;
	}
	/**
	 * 重新统计栏目信息数量
	 */
	public function count_items($modelid) {
		$content_db = pc_base::load_model('model_content_model');
		$result = getcache('category_'.$modelid,'commons');
		if($result){
			foreach($result as $r) {
				if($r['type'] == 0) {
					$content_db->set_model($r[modelid]);
					$number = $content_db->count(array('catid'=>$r['catid']));
					$this->db->update(array('items'=>$number),array('catid'=>$r['catid']));
				}
			}
		}
		return true;
		//showmessage(L('operation_success'),HTTP_REFERER);
	}
	/**
	 * json方式加载模板
	 */
	public function public_tpl_file_list() {
		$style = isset($_GET['style']) && trim($_GET['style']) ? trim($_GET['style']) : exit(0);
		$catid = isset($_GET['catid']) && intval($_GET['catid']) ? intval($_GET['catid']) : 0;
		$batch_str = isset($_GET['batch_str']) ? '['.$catid.']' : '';
		if ($catid) {
			$cat = getcache('category_content_'.$this->siteid,'commons');
			$cat = $cat[$catid];
			$cat['setting'] = string2array($cat['setting']);
		}
		pc_base::load_sys_class('form','',0);
		if($_GET['type']==1) {
			$html = array('page_template'=>form::select_template($style, 'content',(isset($cat['setting']['page_template']) && !empty($cat['setting']['page_template']) ? $cat['setting']['page_template'] : 'category'),'name="setting'.$batch_str.'[page_template]"','page'));
		} else {
			$html = array('category_template'=> form::select_template($style, 'content',(isset($cat['setting']['category_template']) && !empty($cat['setting']['category_template']) ? $cat['setting']['category_template'] : 'category'),'name="setting'.$batch_str.'[category_template]"','category'),
				'list_template'=>form::select_template($style, 'content',(isset($cat['setting']['list_template']) && !empty($cat['setting']['list_template']) ? $cat['setting']['list_template'] : 'list'),'name="setting'.$batch_str.'[list_template]"','list'),
				'show_template'=>form::select_template($style, 'content',(isset($cat['setting']['show_template']) && !empty($cat['setting']['show_template']) ? $cat['setting']['show_template'] : 'show'),'name="setting'.$batch_str.'[show_template]"','show')
			);
		}
		if ($_GET['module']) {
			unset($html);
			if ($_GET['templates']) {
				$templates = explode('|', $_GET['templates']);
				if ($_GET['id']) $id = explode('|', $_GET['id']);
				if (is_array($templates)) {
					foreach ($templates as $k => $tem) {
						$t = $tem.'_template';
						if ($id[$k]=='') $id[$k] = $tem;
						$html[$t] = form::select_template($style, $_GET['module'], $id[$k], 'name="'.$_GET['name'].'['.$t.']" id="'.$t.'"', $tem);
					}
				}
			}

		}
		if (CHARSET == 'gbk') {
			$html = array_iconv($html, 'gbk', 'utf-8');
		}
		echo json_encode($html);
	}

	/**
	 * 快速进入搜索
	 */
	public function public_ajax_search() {
		if($_GET['catname']) {
			if(preg_match('/([a-z]+)/i',$_GET['catname'])) {
				$field = 'letter';
				$catname = strtolower(trim($_GET['catname']));
			} else {
				$field = 'catname';
				$catname = trim($_GET['catname']);
				if (CHARSET == 'gbk') $catname = iconv('utf-8','gbk',$catname);
			}
			$result = $this->db->select("$field LIKE('$catname%') AND siteid='$this->siteid' AND child=0",'catid,type,catname,letter',10);
			if (CHARSET == 'gbk') {
				$result = array_iconv($result, 'gbk', 'utf-8');
			}
			echo json_encode($result);
		}
	}

	public function export() {
		
		$model_db = pc_base::load_model('sitemodel_model');
		$model = model_k2id($model_db->select(array('siteid'=>$this->siteid)));
		$modelid = intval($_GET['modelid']);
		if (!$modelid){
			showmessage('请先选择模型');
		}
		$modename = $model[$modelid][name];
		
		$tree = pc_base::load_sys_class('tree');
		$category_items = array();
		$tree->icon = array('','','');
		$tree->nbsp = '-';
		$categorys = array();
		$result = getcache('category_'.$modelid,'commons');
		if(!empty($result)) {
			foreach($result as $r) {
				$categorys[$r['catid']]['catname'] = $r['catname'];
				$categorys[$r['catid']]['parentid'] = $r['parentid'];
				$categorys[$r['catid']]['catid'] = $r['catid'];
				$categorys[$r['catid']]['child'] = $r['child'];
			}
		}
		$str  = "\$spacer\$catname\n";
		$tree->init($categorys);
		$categorys = $tree->get_tree(0, $str);
		$show_header = $show_validator = $show_scroll = 1;
		include $this->admin_tpl('category_export');
	}
	
	/**
	 * 显示栏目菜单列表
	 */
	public function public_categorys() {
		$show_header = '';
		$CATEGORY = getcache('category_content_'.$this->siteid,'commons');
		
		
		
		$cfg = getcache('common','commons');
		$ajax_show = intval($cfg['category_ajax']);
		$from = isset($_GET['from']) && in_array($_GET['from'],array('block')) ? $_GET['from'] : 'content';
		$tree = pc_base::load_sys_class('tree');
		if($_SESSION['roleid'] != 1) {
			$this->priv_db = pc_base::load_model('category_priv_model');
			$priv_result = $this->priv_db->select(array('action'=>'init','roleid'=>$_SESSION['roleid'],'siteid'=>$this->siteid,'is_admin'=>1));
			$priv_catids = array();
			foreach($priv_result as $_v) {
				$priv_catids[] = $_v['catid'];
			}
			if(empty($priv_catids)) return '';
		}
		$categorys = array();
		if(!empty($CATEGORY)) {
			foreach($CATEGORY as $r) {
				if($r['siteid']!=$this->siteid || ($r['type']==2 && $r['child']==0)) continue;
				if($_SESSION['roleid'] != 1 && !in_array($r['catid'],$priv_catids)) {
					$arrchildid = explode(',',$r['arrchildid']);
					$array_intersect = array_intersect($priv_catids,$arrchildid);
					if(empty($array_intersect)) continue;
				}
				
				$r['icon_type'] = $r['vs_show'] = '';
				$r['type'] = 'init';
				$r['add_icon'] = "<a target='right' href='?m=iappCoupons&c=manages&a=add&menuid=".$_GET['menuid']."&catid=".$r['catid']."' ><img src='".IMG_PATH."add_content.gif' alt='".L('add')."'></a> ";
				
				$categorys[$r['catid']] = $r;
			}
		}
		
		if(!empty($categorys)) {
			//$model = getcache('model', 'commons');
			
			$model_db = pc_base::load_model('sitemodel_model');
			$model = $model_db->select(array('siteid'=>$this->siteid));
			//print_r($modules);
			//print_r($model);
			//exit;
			
			//$modules = getcache('modules', 'commons');
			
			$_m = array();
			foreach($model as $v) {
				if($v[module]!=''){
					$_m[$v[modelid]][modelid] = $v[modelid];
					$_m[$v[modelid]][siteid] = $v[siteid];
					$_m[$v[modelid]][name] = $v[name];
					$_m[$v[modelid]][tablename] = $v[tablename];
					$_m[$v[modelid]][listorder] = $v[listorder];
				}
			}
			
			//print_r($_m);
			
			$tree->init($_m);
			
			$strs = "<span class='\$icon_type'>\$add_icon<a href='?m=category&c=category&a=init&menuid=".$_GET['menuid']."&modelid=\$modelid' target='right' onclick='open_list(this)'>\$name</a></span>";
			
			$strs2 = "<span class='folder'>\$name</span>";
			
			$categorys = $tree->get_treeview($defaultcatid,'category_tree',$strs,$strs2,$ajax_show);
		} else {
			$categorys = L('please_add_category');
		}
        include $this->admin_tpl('tree');
		exit;
	}
}
?>