<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');?>
<script type="text/javascript">
	$(function(){
		$.formValidator.initConfig({formid:"myform",autotip:true,onerror:function(msg,obj){window.top.art.dialog({content:msg,lock:true,width:'200',height:'50'}, function(){this.close();$(obj).focus();})}});
		$("#catname").formValidator({onshow:"请输入栏目名称",onfocus:"请输入栏目名称",oncorrect:"输入正确"}).inputValidator({min:1,onerror:"请输入栏目名称"});

	})
	function SwapTab(name,cls_show,cls_hide,cnt,cur){
		for(i=1;i<=cnt;i++){
			if(i==cur){
				 $('#div_'+name+'_'+i).show();
				 $('#tab_'+name+'_'+i).attr('class',cls_show);
			}else{
				 $('#div_'+name+'_'+i).hide();
				 $('#tab_'+name+'_'+i).attr('class',cls_hide);
			}
		}
	}
</script>

<div class="subnav">
    <div class="content-menu ib-a blue line-x">
    <?php 
	
	$html.='<a href="index.php?m=category&c=category&a=init&modelid='. $modelid .'&pc_hash='. $_SESSION['pc_hash'].'" ><em>'.$modename.'l栏目管理'.'</em></a><span>|</span>';
	
	$html.='<a href="javascript:;" class="on"><em>添加栏目'.'</em></a><span>|</span>';
	
	$html.='<a href="index.php?m=category&c=category&a=public_cache&modelid='. $modelid .'&pc_hash='. $_SESSION['pc_hash'].'"><em>'.'更新栏目缓存'.'</em></a><span>|</span>';
	
	$html.='<a href="index.php?m=category&c=category&a=export&modelid='. $modelid .'&pc_hash='. $_SESSION['pc_hash'].'"><em>'.'导出栏目</em></a>';
	
	echo $html;
	?>
	</div>
</div>

<form name="myform" id="myform" action="?m=category&c=category&a=add&modelid=<?php echo $modelid?>" method="post">
<div class="pad-lr-10">
<div class="col-tab">
<ul class="tabBut cu-li">
<li id="tab_setting_1" class="on" onclick="SwapTab('setting','on','',2,1);">基本选项</li>
<li id="tab_setting_2" onclick="SwapTab('setting','on','',2,2);">附加字段</li>
</ul>
<div id="div_setting_1" class="contentList pad-10">

<table width="100%" class="table_form">
<tbody>
 	<tr>
     <th width="200"><strong>添加方式：</strong></th>
      <td>
		  <input type='radio' name='addtype' value='0' checked id="normal_addid"> 单条添加&nbsp;&nbsp;&nbsp;&nbsp;
		  <input type='radio' name='addtype' value='1'  onclick="$('#catdir_tr').html(' ');$('#normal_add').html(' ');$('#normal_add').css('display','none');$('#batch_add').css('display','');$('#normal_addid').attr('disabled','true');this.disabled='true'"> 批量添加
	  </td>
    </tr>
      <tr>
        <th><strong>上级分类：</strong></th>
        <td>
		<?php echo form::select_category('category_'.$modelid,$parentid,'name="info[parentid]" id="parentid"',L('≡ 作为一级分类 ≡'),0,-1);
		?>
		</td>
      </tr>
      <tr>
        <th><strong>分类名称：</strong></th>
        <td>
        <span id="normal_add">
		<input type="text" name="info[catname]" id="catname" class="input-text" value="">
		</span>
        <span id="batch_add" style="display:none">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr style="padding:0;">
				<td style="padding:0; width:320px;"><textarea name="batch_add" maxlength="255" style="width:300px;height:60px;"></textarea></td>
				<td style="padding:0;">支持子栏目添加：<br><font color="#959595">手机<br>-品牌手机<br>--诺基亚<br>-山寨机<br></font> </td>
			  </tr>
			</table>
        </span>
		</td>
      </tr>
	<tr>
        <th><strong>分类图片：</strong></th>
        <td><?php echo form::images('info[image]', 'image', $image, 'content');?></td>
      </tr>
	<tr>
        <th><strong>说明：</strong></th>
        <td>
		<textarea name="info[description]" maxlength="255" style="width:300px;height:60px;"><?php echo $description;?></textarea>
		</td>
      </tr>
      <tr>
      <th>META Title（模型名称）：</th>
      <td><input name='setting[meta_title]' type='text' id='meta_title' value='<?php echo $setting['meta_title'];?>' size='60' maxlength='60'></td>
    </tr>
    <tr>
      <th>META Keywords（模型关键词）：</th>
      <td><textarea name='setting[meta_keywords]' id='meta_keywords' style="width:90%;height:40px"><?php echo $setting['meta_keywords'];?></textarea></td>
    </tr>
    <tr>
      <th >META Description（栏目描述）：</th>
      <td><textarea name='setting[meta_description]' id='meta_description' style="width:90%;height:50px"><?php echo $setting['meta_description'];?></textarea></td>
    </tr>
</tbody>
</table>
</div>

<div id="div_setting_2" class="contentList pad-10" style="display:none;">

</div>

	
	<div class="bk10"></div>
	<input name="catid" type="hidden" value="<?php echo $catid;?>">
    <input type="submit" name="dosubmit" id="dosubmit" value="<?php echo L('提交')?>" class="button">
    &nbsp;
    <input type="reset" value=" <?php echo L('重写')?> " class="button">

</form>
<div class="bk10"></div>
</div>
</div>
<!--table_form_off-->
</div>
<script type="text/javascript">

$('#parentid').change(function (){
	var parentid = $('#parentid').val();
	$.get('<?php echo APP_PATH?>index.php', {m:'category', c:'category', a:'public_get_additional', parentid:parentid, modelid:'<?php echo $modelid?>'}, function(data){
		$('#additional_html').html(data);
	})
})
</script>
</body>
</html>