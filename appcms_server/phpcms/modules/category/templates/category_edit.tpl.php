<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');?>
<script type="text/javascript">
$(function(){
	$.formValidator.initConfig({formid:"myform",autotip:true,onerror:function(msg,obj){window.top.art.dialog({content:msg,lock:true,width:'200',height:'50'}, function(){this.close();$(obj).focus();})}});
	$("#catname").formValidator({onshow:"请输入栏目名称",onfocus:"请输入栏目名称",oncorrect:"输入正确"}).inputValidator({min:1,onerror:"请输入栏目名称"});
})
function SwapTab(name,cls_show,cls_hide,cnt,cur){
	for (i=1;i<=cnt;i++){
		if (i==cur){
			$('#div_'+name+'_'+i).show();
			$('#tab_'+name+'_'+i).attr('class',cls_show);
		} else {
			$('#div_'+name+'_'+i).hide();
			$('#tab_'+name+'_'+i).attr('class',cls_hide);
		}
	}
}
</script>

<form name="myform" id="myform" action="?m=category&c=category&a=edit" method="post">
<div class="pad-10">
<div class="col-tab">
<ul class="tabBut cu-li">
<li id="tab_setting_1" class="on" onclick="SwapTab('setting','on','',2,1);">基本选项</li>
<li id="tab_setting_2" onclick="SwapTab('setting','on','',2,2);">附加字段</li>
</ul>
<div id="div_setting_1" class="contentList pad-10">
<table width="100%" class="table_form ">
      <tr>
        <th width="200"><strong>添加方式：</strong></th>
        <td>
		<?php echo form::select_category('category_'.$modelid,$parentid,'name="info[parentid]" id="parentid"',L('please_select_parent_category'),0,-1);?>
		</td>
      </tr>
      <tr>
        <th><strong>分类名称：</strong></th>
        <td><input type="text" name="info[catname]" id="catname" class="input-text" value="<?php echo $catname;?>"></td>
      </tr>
	<tr>
        <th><strong>分类图片：</strong></th>
        <td><?php echo form::images('info[image]', 'image', $image, 'content');?></td>
      </tr>
	<tr>
        <th><strong>说明：</strong></th>
        <td>
		<textarea name="info[description]" maxlength="255" style="width:300px;height:60px;"><?php echo $description;?></textarea>
		</td>
      </tr>
	<tr>
      <th width="200">META Title（模型名称）：</th>
      <td><input name='setting[meta_title]' type='text' id='meta_title' value='<?php echo $setting['meta_title'];?>' size='60' maxlength='60'></td>
    </tr>
    <tr>
      <th >META Keywords（模型关键词）：</th>
      <td><textarea name='setting[meta_keywords]' id='meta_keywords' style="width:90%;height:40px"><?php echo $setting['meta_keywords'];?></textarea></td>
    </tr>
    <input type="hidden" name="info[modelid]" value="<?php echo $modelid?>">
    <tr>
      <th >META Description（栏目描述）：</th>
      <td><textarea name='setting[meta_description]' id='meta_description' style="width:90%;height:50px"><?php echo $setting['meta_description'];?></textarea></td>
    </tr>
</table>
</div>

<div id="div_setting_2" class="contentList pad-10" style="display:none;">

</div>

 <div class="bk15"></div>
	<input name="catid" type="hidden" value="<?php echo $catid;?>">
    <input name="dosubmit" type="submit" id="dosubmit" value="<?php echo L('submit')?>" class="button">

</form>
</div>

</div>
<!--table_form_off-->
</div>
<script type="text/javascript">
$('#parentid').change(function (){
    var parentid = $('#parentid').val();
    var catid = <?php echo $catid?>;
	$.get('<?php echo APP_PATH?>index.php', {m:'category', c:'category', a:'public_get_additional', catid:catid, parentid:parentid, modelid:'<?php echo $modelid?>'}, function(data){
        $('#additional_html').html(data);
    })
})
</script>
</body>
</html>