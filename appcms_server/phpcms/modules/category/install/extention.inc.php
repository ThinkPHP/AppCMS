<?php
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');

$menus = $menu_db->select(array('name'=>'iapp_config', 'm'=>'iapp', 'c'=>'manages', 'a'=>'setting'));
if($menus){
	$menuid=$menus[0][id];
}else{
	$menuid=977;
}


$second_pid = $menu_db->insert(array('name'=>'category_manage', 'parentid'=>$menuid, 'm'=>'category', 'c'=>'category', 'a'=>'init', 'data'=>'', 'listorder'=>99, 'display'=>'1'), true);

$menu_db->insert(array('name'=>'category_add', 'parentid'=>$second_pid, 'm'=>'category', 'c'=>'category', 'a'=>'add', 'data'=>'', 'listorder'=>1, 'display'=>'1'));
$menu_db->insert(array('name'=>'category_edit', 'parentid'=>$second_pid, 'm'=>'category', 'c'=>'category', 'a'=>'edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'category_cache', 'parentid'=>$second_pid, 'm'=>'category', 'c'=>'category', 'a'=>'cache', 'data'=>'', 'listorder'=>2, 'display'=>'1'));
$menu_db->insert(array('name'=>'category_export', 'parentid'=>$second_pid, 'm'=>'category', 'c'=>'category', 'a'=>'export', 'data'=>'', 'listorder'=>3, 'display'=>'1'));
$menu_db->insert(array('name'=>'category_delete', 'parentid'=>$second_pid, 'm'=>'category', 'c'=>'category', 'a'=>'delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'category_listorder', 'parentid'=>$second_pid, 'm'=>'category', 'c'=>'category', 'a'=>'listorder', 'data'=>'', 'listorder'=>0, 'display'=>'0'));

$language = array(
				'category_manage'=>'栏目管理', 
				'category_add'=>'添加栏目', 
				'category_edit'=>'修改栏目',
				'category_cache'=>'更新栏目缓存',
				'category_export'=>'导出栏目', 
				'category_delete'=>'删除栏目', 
				'category_listorder'=>'栏目排序'
                );
				

?>