<?php 
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');

$module = 'category';
$modulename = '万能栏目';
$introduce = '万能栏目模块，PC二次开发利器，利用本模块可以对所有模型的栏目进行统一管理，争议：cms栏目按语义分类与按模块分类的争议。';
$author = 'chuanpu Team';
$authorsite = 'http://www.chuanpu.net';
$authoremail = '657717791@qq.com';
?>