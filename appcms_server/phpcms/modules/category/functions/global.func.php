<?php
	/*
	* 根据模型数组把键值转换为表名
	* cache 数组
	*/
	function model_id2k($cache){
		$_model = array();
		foreach ($cache as $k=>$v) {
			$_model[$v[tablename]] = $v;
		}
		return $_model;
	}
	
	/*
	* 根据模型数组把键值转换为mid
	* cache 数组
	*/
	function model_k2id($cache){
		$_model = array();
		foreach ($cache as $k=>$v) {
			$_model[$v[modelid]] = $v;
		}
		return $_model;
	}
	
	/*
	* 根据模型数组把键值转换为表名
	* cache 数组
	*/
	function model2modelid($model='iappYp_company'){
		$modelid = 1;
		$iappYp_models = getcache('iappYp_model', 'model');
		foreach ($iappYp_models as $k=>$v) {
			if($v[tablename]==$model){
				$modelid = $v[modelid];
			}
		}
		return $modelid;
	}
	
	/*
	* 检查模型id是否正确
	* cache 数组
	*/
	function chek_modelid($modelid){
		$sitemodel_db = pc_base::load_model('sitemodel_model');
		$model_r = $sitemodel_db->get_one(array('modelid'=>$modelid));
		if ($model_r) {
			$modelid = $model_r[modelid];
		}else{
			$model_r = $sitemodel_db->get_one(array('tablename'=>'iappYp_company'), 'modelid');
			$modelid = $model_r[modelid];
		}
		return $modelid;
	}
?>