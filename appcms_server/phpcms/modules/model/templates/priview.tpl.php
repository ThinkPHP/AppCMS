<?php 
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');?>
<script type="text/javascript">
	var charset = '<?php echo CHARSET;?>';
	var uploadurl = '<?php echo pc_base::load_config('system','upload_url')?>';
	var catid=<?php echo $catid;?>
	
</script>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>content_addtop.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>colorpicker.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>hotkeys.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>cookie.js"></script>
<div class="subnav">
    <div class="content-menu ib-a blue line-x">
        <a href='javascript:;'><em>管理内容</em></a><span>|</span><a href='javascript:;' class="on"><em>添加内容</em></a>    </div>
</div>
<div class="pad-lr-10">
    <div class="col-tab">
      <ul class="tabBut cu-li">
        <li id="tab_setting_1" class="on" onclick="SwapTab('setting','on','',2,1);">基本信息</li>
        <li id="tab_setting_2" onclick="SwapTab('setting','on','',2,2);">附加信息</li>
      </ul>
      <div id="div_setting_1" class="contentList pad-10">
       
          <table width="100%" cellspacing="0" class="table_form">
            <tbody>
              <?php
if(is_array($forminfos['base'])) {
 foreach($forminfos['base'] as $field=>$info) {
	 if($info['isomnipotent']) continue;
	 if($info['formtype']=='omnipotent') {
		foreach($forminfos['base'] as $_fm=>$_fm_value) {
			if($_fm_value['isomnipotent']) {
				$info['form'] = str_replace('{'.$_fm.'}',$_fm_value['form'],$info['form']);
			}
		}
		foreach($forminfos['senior'] as $_fm=>$_fm_value) {
			if($_fm_value['isomnipotent']) {
				$info['form'] = str_replace('{'.$_fm.'}',$_fm_value['form'],$info['form']);
			}
		}
	}
 ?>
              <tr>
                <th width="80"><?php if($info['star']){ ?>
                  <font color="red">*</font>
                  <?php } ?>
                  <?php echo $info['name']?> </th>
                <td><?php echo $info['form']?> <?php echo $info['tips']?></td>
              </tr>
              <?php
} }
?>
            </tbody>
          </table>
       
      </div>
      <div id="div_setting_2" class="contentList pad-10 hidden">
          <table width="100%" cellspacing="0" class="table_form">
            <tbody>
              <?php
if(is_array($forminfos['senior'])) {
 foreach($forminfos['senior'] as $field=>$info) {
	if($info['isomnipotent']) continue;
	if($info['formtype']=='omnipotent') {
		foreach($forminfos['base'] as $_fm=>$_fm_value) {
			if($_fm_value['isomnipotent']) {
				$info['form'] = str_replace('{'.$_fm.'}',$_fm_value['form'],$info['form']);
			}
		}
		foreach($forminfos['senior'] as $_fm=>$_fm_value) {
			if($_fm_value['isomnipotent']) {
				$info['form'] = str_replace('{'.$_fm.'}',$_fm_value['form'],$info['form']);
			}
		}
	}
 ?>
              <tr>
                <th width="80"><?php if($info['star']){ ?>
                  <font color="red">*</font>
                  <?php } ?>
                  <?php echo $info['name']?></th>
                <td><?php echo $info['form']?><?php echo $info['tips']?></td>
              </tr>
              <?php
	}
}
?>
              <?php 
	if($_SESSION['roleid']==1 || $priv_status) {
?>
              <tr>
                <th width="80"><?php echo L('c_status');?></th>
                <td><span class="ib" style="width:90px">
                  <label>
                    <input type="radio" name="status" value="99" checked/>
                    <?php echo L('c_publish');?> </label>
                  </span></td>
              </tr>
              <?php if($workflowid) { ?>
              <tr>
                <th width="80">状态</th>
                <td><label>
                    <input type="radio" name="status" value="1" >
                    <?php echo L('c_check');?> </label></td>
              </tr>
              <?php } ?>
              <?php } ?>
            </tbody>
          </table>
      </div>
      <div class="bk10"></div>
      <input type="submit" name="dosubmit" id="dosubmit" value="<?php echo L('提交')?>" class="button">
      &nbsp;
      <input type="reset" value=" <?php echo L('重写')?> " class="button">
    </div>
	
<script type="text/javascript">
	function SwapTab(name,cls_show,cls_hide,cnt,cur){
		for(i=1;i<=cnt;i++){
			if(i==cur){
				 $('#div_'+name+'_'+i).show();
				 $('#tab_'+name+'_'+i).attr('class',cls_show);
			}else{
				 $('#div_'+name+'_'+i).hide();
				 $('#tab_'+name+'_'+i).attr('class',cls_hide);
			}
		}
	}
</script>
</div>
</body></html>