<?php
defined('IN_PHPCMS') or exit('No permission resources.');
//模型缓存路径
define('CACHE_MODEL_PATH',CACHE_PATH.'caches_model'.DIRECTORY_SEPARATOR.'caches_data'.DIRECTORY_SEPARATOR);
//定义在单独操作内容的时候，同时更新相关栏目页面
define('RELATION_HTML',true);

pc_base::load_app_class('admin','admin',0);
pc_base::load_sys_class('form','',0);
pc_base::load_app_func('util');
pc_base::load_sys_class('format','',0);

class content extends admin {
	public function __construct() {
		parent::__construct();
		$this->siteid = $this->get_siteid();
	}
	
	public function priview() {
		$modelid = $_GET['modelid'];
		$categorys = getcache('category_'.$modelid,'commons');
		foreach ($categorys as $k=>$v) {
			$catid =$v[catid];
			break;
		}
		$catid = $_GET['catid'] ? intval($_GET['catid']) : $catid;
		
		$modellist = getcache('modellist','commons');
		param::set_cookie('module',$modellist[$modelid][tablename]);
		
		require CACHE_MODEL_PATH.'model_form.class.php';
		$model_form = new model_form($modelid,$catid,$categorys);
		$forminfos = $model_form->get();
		$formValidator = $model_form->formValidator;
		$setting = string2array($category['setting']);
		$workflowid = $setting['workflowid'];
		$workflows = getcache('workflow_'.$this->siteid,'commons');
		$workflows = $workflows[$workflowid];
		$workflows_setting = string2array($workflows['setting']);
		$nocheck_users = $workflows_setting['nocheck_users'];
		$admin_username = param::get_cookie('admin_username');
		if(!empty($nocheck_users) && in_array($admin_username, $nocheck_users)) {
			$priv_status = true;
		} else {
			$priv_status = false;
		}
		$show_header = $show_dialog = $show_validator = '';
		include $this->admin_tpl('priview');
	}
}
?>