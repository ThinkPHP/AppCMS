<?php 
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');

$module = 'model';
$modulename = '万能模型';
$introduce = '万能模型模块，PC二次开发利器，本模块可以对所有模型进行统一管理，可以对任意模块附加模型。';
$author = 'chuanpu Team';
$authorsite = 'http://www.chuanpu.net';
$authoremail = '657717791@qq.com';
?>