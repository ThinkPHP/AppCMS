<?php
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');

$menus = $menu_db->select(array('name'=>'iapp_config', 'm'=>'iapp', 'c'=>'manages', 'a'=>'setting'));
if($menus){
	$menuid=$menus[0][id];
}else{
	$menuid=977;
}


$second_pid = $menu_db->insert(array('name'=>'model_manage', 'parentid'=>$menuid, 'm'=>'model', 'c'=>'sitemodel', 'a'=>'init', 'data'=>'modelid=1', 'listorder'=>99, 'display'=>'1'), true);

$menu_db->insert(array('name'=>'model_add', 'parentid'=>$second_pid, 'm'=>'model', 'c'=>'sitemodel', 'a'=>'add', 'data'=>'', 'listorder'=>1, 'display'=>'1'));
$menu_db->insert(array('name'=>'model_edit', 'parentid'=>$second_pid, 'm'=>'model', 'c'=>'sitemodel', 'a'=>'edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'model_disabled', 'parentid'=>$second_pid, 'm'=>'model', 'c'=>'sitemodel', 'a'=>'cache', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'model_delete', 'parentid'=>$second_pid, 'm'=>'model', 'c'=>'sitemodel', 'a'=>'delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'model_import', 'parentid'=>$second_pid, 'm'=>'model', 'c'=>'sitemodel', 'a'=>'import', 'data'=>'', 'listorder'=>3, 'display'=>'1'));
$menu_db->insert(array('name'=>'model_export', 'parentid'=>$second_pid, 'm'=>'model', 'c'=>'sitemodel', 'a'=>'export', 'data'=>'', 'listorder'=>3, 'display'=>'0'));

$menu_db->insert(array('name'=>'model_fields_manage', 'parentid'=>$second_pid, 'm'=>'model', 'c'=>'sitemodel_field', 'a'=>'init', 'data'=>'', 'listorder'=>0, 'display'=>'0'));

$language = array(
				'model_manage'=>'模型管理', 
				'model_add'=>'添加模型', 
				'model_edit'=>'修改模型',
				'model_disabled'=>'禁用模型',
				'model_delete'=>'删除模型', 
				'model_export'=>'导出模型', 
				'model_import'=>'导入模型', 
				'model_fields_manage'=>'字段管理'
                );

// category表新增字段module
$sitemodel_db = pc_base::load_model('sitemodel_model');
if (!$sitemodel_db->field_exists('module')) {
	$sitemodel_db->query("ALTER TABLE `phpcms_model` ADD `module` varchar(100) NOT NULL DEFAULT ''");
}

?>