<?php defined('IN_PHPCMS') or exit('No permission resources.');?>
<table cellpadding="2" cellspacing="1" width="98%">
	<tr> 
      <td width="100">文本框长度</td>
      <td><input type="text" name="setting[size]" value="<?php echo $setting['size'];?>" size="10" class="input-text"></td>
    </tr>
	<tr> 
      <td>默认值</td>
      <td><input type="text" name="setting[defaultvalue]" value="<?php echo $setting['defaultvalue'];?>" size="40" class="input-text"></td>
    </tr>
	
	<tr> 
      <td>自动填写标题</td>
      <td><input type="text" name="setting[autotitle]" value="<?php echo $setting['autotitle'];?>" size="40" class="input-text">填写对应标题字段</td>
    </tr>
	<tr> 
      <td>自动填写关键字</td>
      <td><input type="text" name="setting[autotags]" value="<?php echo $setting['autotags'];?>" size="40" class="input-text">填写对应关键字字段</td>
    </tr>
	<tr> 
      <td>自动填写简介</td>
      <td><input type="text" name="setting[autocontent]" value="<?php echo $setting['autocontent'];?>" size="40" class="input-text">填写对应简介字段</td>
    </tr>
</table>