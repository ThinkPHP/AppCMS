<?php
defined('IN_PHPCMS') or exit('No permission resources.');
class url{
	private $urlrules,$categorys,$html_root;
	public function __construct() {
		$this->urlrules = getcache('urlrules','commons');	
	}
	
	/**
	 * 获取栏目的访问路径
	 * 在修复栏目路径处重建目录结构用
	 * @param intval $modelid 模型id
	 * @param intval $id 数据id
	 * @param intval $type 类型
	 */
	public function show($modelid,$id,$type=0) {
		switch ($type)
		{
			case 0:
				$a = 'index';
				break;
			case 1:
				$a = 'app';
				break;
			case 2:
				$a = 'touch';
				break;	
			default:
				$a = 'index';
		}

		$modellist = getcache('modellist','commons');
		$module = $modellist[$modelid][module];
		$tablename = $modellist[$modelid][tablename];
		$urls =  APP_PATH . 'index.php?m='.$module.'&c='.$a.'&a=show&t='.$tablename.'&id='.$id;
		return $urls;
	}
	
	/**
	 * 获取栏目的访问路径
	 * 在修复栏目路径处重建目录结构用
	 * @param intval $catid 栏目ID
	 * @param intval $page 页数
	 */
	public function category_url($catid, $page = 1) {
		$category = $this->categorys[$catid];
		if($category['type']==2) return $category['url'];
		$page = max(intval($page), 1);
		$setting = string2array($category['setting']);
		$category_ruleid = $setting['category_ruleid'];
		$urlrules = $this->urlrules[$category_ruleid];
		$urlrules_arr = explode('|',$urlrules);
		if ($page==1) {
			$urlrule = $urlrules_arr[0];
		} else {
			$urlrule = $urlrules_arr[1];
		}
		if (!$setting['ishtml']) { //如果不生成静态
			
			$url = str_replace(array('{$catid}', '{$page}'), array($catid, $page), $urlrule);
			if (strpos($url, '\\')!==false) {
					$url = APP_PATH.str_replace('\\', '/', $url);
			}
		}  else { //生成静态
			if ($category['arrparentid']) {
				$parentids = explode(',', $category['arrparentid']);
			}
			$parentids[] = $catid;
			$domain_dir = '';
			foreach ($parentids as $pid) { //循环查询父栏目是否设置了二级域名
				$r = $this->categorys[$pid];
				if (strpos(strtolower($r['url']), '://')!==false && strpos($r['url'], '?')===false) {
					$r['url'] = preg_replace('/([(http|https):\/\/]{0,})([^\/]*)([\/]{1,})/i', '$1$2/', $r['url'], -1); //取消掉双'/'情况
					if (substr_count($r['url'], '/')==3 && substr($r['url'],-1,1)=='/') { //如果url中包含‘http://’并且‘/’在3个则为二级域名设置栏目
						$url = $r['url'];
						$domain_dir = $this->get_categorydir($pid).$this->categorys[$pid]['catdir'].'/'; //得到二级域名的目录
					}
				}
			}
			
			$category_dir = $this->get_categorydir($catid);
			$urls = str_replace(array('{$categorydir}','{$catdir}','{$catid}','{$page}'),array($category_dir,$category['catdir'],$catid,$page),$urlrule);
			if ($url && $domain_dir) { //如果存在设置二级域名的情况
				if (strpos($urls, $domain_dir)===0) {
					$url = str_replace(array($domain_dir, '\\'), array($url, '/'), $urls);
				} else {
					$urls = $domain_dir.$urls;
					$url = str_replace(array($domain_dir, '\\'), array($url, '/'), $urls);
				}
			} else { //不存在二级域名的情况
				$url = $urls;
			}
		}
		if (in_array(basename($url), array('index.html', 'index.htm', 'index.shtml'))) {
			$url = dirname($url).'/';
		}
		if (strpos($url, '://')===false) $url = str_replace('//', '/', $url);
		if(strpos($url, '/')===0) $url = substr($url,1);
		return $url;
	}
	/**
	 * 生成列表页分页地址
	 * @param $ruleid 角色id
	 * @param $categorydir 父栏目路径
	 * @param $catdir 栏目路径
	 * @param $catid 栏目id
	 * @param $page 当前页
	 */
	public function get_list_url($ruleid,$categorydir, $catdir, $catid, $page = 1) {
		$urlrules = $this->urlrules[$ruleid];
		$urlrules_arr = explode('|',$urlrules);
		if ($page==1) {
			$urlrule = $urlrules_arr[0];
		} else {
			$urlrule = $urlrules_arr[1];
		}
		$urls = str_replace(array('{$categorydir}','{$catdir}','{$year}','{$month}','{$day}','{$catid}','{$page}'),array($categorydir,$catdir,$year,$month,$day,$catid,$page),$urlrule);
		return $urls;
	}
	
	/**
	 * 获取父栏目路径
	 * @param $catid
	 * @param $dir
	 */
	private function get_categorydir($catid, $dir = '') {
		$setting = array();
		$setting = string2array($this->categorys[$catid]['setting']);
		if ($setting['create_to_html_root']) return $dir;
		if ($this->categorys[$catid]['parentid']) {
			$dir = $this->categorys[$this->categorys[$catid]['parentid']]['catdir'].'/'.$dir;
			return $this->get_categorydir($this->categorys[$catid]['parentid'], $dir);
		} else {
			return $dir;
		}
	}
	/**
	 * 设置当前站点
	 */
	private function set_siteid() {
		if(defined('IN_ADMIN')) {
			$this->siteid = get_siteid();
		} else {
			if (param::get_cookie('siteid')) {
				$this->siteid = param::get_cookie('siteid');
			} else {
				$this->siteid = 1;
			}
		}
	}
}