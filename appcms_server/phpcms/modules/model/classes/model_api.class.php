<?php
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_func('global');
class model_api {
	private $db;
	public $siteid;
	function __construct() {
		$this->db = pc_base::load_model('sitemodel_model');
		$this->siteid = get_siteid();
		if(!$this->siteid) {
			$this->siteid = 1;
		};
	}
	
	/**
	 * 添加模型
	 * @param $modulename  模型名称
	 * @param $tablename   模型表名
	 * @param $siteid      站点ID
	 */
	public function add_model($modulename,$tablename,$module,$siteid) {
		$model_path = PC_PATH.'modules'.DIRECTORY_SEPARATOR.'model'.DIRECTORY_SEPARATOR.'fields'.DIRECTORY_SEPARATOR;
		
		$info['siteid'] = $siteid;
		$info['name'] = $modulename;
		$info['tablename'] = $tablename;
		$info['module'] = $module;
		$info['description'] = '';
		$info['type']=80;
		$info['default_style']='default';
		$info['category_template'] = 'category';
		$info['list_template'] = 'list';
		$info['show_template'] = 'show';
		
		$modelid = $this->db->insert($info,1);
		$model_sql = '';
		$model_sql = file_get_contents($model_path.'model.sql');
		$tablepre = $this->db->db_tablepre;
		$model_sql = str_replace('$basic_table', $tablepre.$tablename, $model_sql);
		$model_sql = str_replace('$table_data',$tablepre.$tablename.'_data', $model_sql);
		$model_sql = str_replace('$table_model_field',$tablepre.'model_field', $model_sql);
		$model_sql = str_replace('$modelid',$modelid,$model_sql);
		$model_sql = str_replace('$siteid',$siteid,$model_sql);
		
		$this->db->sql_execute($model_sql);
		$this->cache_field($modelid);
		$this->public_cache();
		
		//调用全站搜索类别接口
		$this->type_db = pc_base::load_model('type_model');
		$this->type_db->insert(array('name'=>$modulename,'module'=>'search','modelid'=>$modelid,'siteid'=>$siteid));
		//更新缓存
		$cache_api = pc_base::load_app_class('cache_api','admin');
		$cache_api->cache('type');
		$cache_api->search_type();
		return $modelid;
	}
	
	/**
	 * 更新指定模型字段缓存
	 * 
	 * @param $modelid 模型id
	 */
	public function cache_field($modelid = 0) {
		$field_db = pc_base::load_model('sitemodel_field_model');
		$field_array = array();
		$fields = $field_db->select(array('modelid'=>$modelid,'disabled'=>0),'*',100,'listorder ASC');
		foreach($fields as $_value) {
			$setting = string2array($_value['setting']);
			$_value = array_merge($_value,$setting);
			$field_array[$_value['field']] = $_value;
		}
		setcache('model_field_'.$modelid,$field_array,'model');
		return true;
	}
	
	/**
	 * 更新模型缓存
	 */
	public function public_cache() {
		//模型原型存储路径
		define('MODEL_PATH',PC_PATH.'modules'.DIRECTORY_SEPARATOR.'model'.DIRECTORY_SEPARATOR.'fields'.DIRECTORY_SEPARATOR);
		//模型缓存路径
		define('CACHE_MODEL_PATH',CACHE_PATH.'caches_model'.DIRECTORY_SEPARATOR.'caches_data'.DIRECTORY_SEPARATOR);
		require MODEL_PATH.'fields.inc.php';
		//更新内容模型类：表单生成、入库、更新、输出
		$classtypes = array('form','input','update','output');
		foreach($classtypes as $classtype) {
			$cache_data = file_get_contents(MODEL_PATH.'model_'.$classtype.'.class.php');
			$cache_data = str_replace('}?>','',$cache_data);
			foreach($fields as $field=>$fieldvalue) {
				if(file_exists(MODEL_PATH.$field.DIRECTORY_SEPARATOR.$classtype.'.inc.php')) {
					$cache_data .= file_get_contents(MODEL_PATH.$field.DIRECTORY_SEPARATOR.$classtype.'.inc.php');
				}
			}
			$cache_data .= "\r\n } \r\n?>";
			file_put_contents(CACHE_MODEL_PATH.'model_'.$classtype.'.class.php',$cache_data);
			@chmod(CACHE_MODEL_PATH.'model_'.$classtype.'.class.php',0777);
		}
		//更新模型数据缓存
		$model_array = array();
		$_model = array();
		$datas = $this->db->select('');
		foreach ($datas as $r) {
			if(!$r['disabled']) {
				$model_array[$r['modelid']] = $r;
				$_model[$r[tablename]] = $r;
			}
		}
		setcache('modellist', $model_array, 'commons');
		setcache('modellist_t', $_model, 'commons');
		return true;
	}
}
?>