<?php 
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_sys_class('format', '', 0);
class index {
	function __construct() {		
		$this->db = pc_base::load_model('iappTrain_model');
		$this->bus_item = pc_base::load_model('iappTrain_item_model');
	}
	
	public function init() {
		echo "not init";
	}
	
	public function lists() {
		$re = array();
		$data = $this->db->select(array('siteid'=>get_siteid()), 'trainid,trainCode,traindeptCity,trainArriveCity,traindeptStation,trainArriveStation,trainType,traindeptTime,trainarriveTime,trainallMileage,trainallTime,trainsearchStation','','`listorder` DESC');
		if($data){
			$re[data] = $data;
			$re['success'] = 'OK';
			$re['msg'] = '数据加载成功~';
		}else{
			$re['success'] = 'NODATA';
			$re['msg'] = '没有数据~';
		}

		echo json_encode($re);
	}
	public function show() {
		$trainid = intval($_GET['trainid']) ? intval($_GET['trainid']) : showmessage(L('parameter_error'),HTTP_REFERER);
		
		
		/*
		stationID	        "2"	
		trainID			    "1"			
		stationName			"金华西"			
		stationDays			"第一天"			
		stationArriveTime	"15:40"			
		stationStayTime		"14分"			
		stationMileage		"23公里"			
		stationOrder		"2"
	    */
		
	
		$re = array();
		$info = $this->bus_item->select(array('trainid'=>$trainid));
		if($info){
			$info = $info[0];
			$info[items] = string2array($info[items]);
			$data = array();
			foreach ($info[items] as $k=>$v) {
				$a = $k+1;
				$data[$k][stationID] = (string)$a;
				$data[$k][trainID] = $info[id];
				$data[$k][stationName] = $v[0];
				$data[$k][stationDays] = $v[1];
				$data[$k][stationArriveTime] = $v[2];
				$data[$k][stationStayTime] = $v[3];
				$data[$k][stationMileage] = $v[4];
				$data[$k][stationOrder] = (string)$a;
			}
		
		
			$re[data] = $data;
			$re['success'] = 'OK';
			$re['msg'] = '数据加载成功~';
		}else{
			$re['success'] = 'NODATA';
			$re['msg'] = '没有数据~';
		}

		echo json_encode($re);
	}
}
?>