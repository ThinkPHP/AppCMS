<?php 
defined('IN_PHPCMS') or exit('No permission resources.'); 
pc_base::load_app_class('admin','admin',0);
pc_base::load_sys_class('form', '', 0);
pc_base::load_sys_class('format', '', 0);
class manages extends admin {
	private $db;
	function __construct() {
		parent::__construct();
		$this->sites = pc_base::load_app_class('sites','admin');
		$this->db = pc_base::load_model('iappTrain_model');
		$this->train_item = pc_base::load_model('iappTrain_item_model');
		$this->siteid = $this->get_siteid();
	}
	
	function init() {
		$page = max(intval($_GET['page']), 1);
		$data = $this->db->listinfo(array('siteid'=>$this->get_siteid()), '`listorder` DESC', $page);
		
		include $this->admin_tpl('train_list');
	}
		
	
	function add() {
		if($_POST['dosubmit']) {
			$info = $_POST['info'];
			$info[siteid] = $this->siteid;
			$info[listorder] = 0;
			$info[addtime] = time();
			$return_id = $this->db->insert($info,'1');
			showmessage(L('add_success'), '?m=iappTrain&c=manages&a=init');
		} else {
			include $this->admin_tpl('train_add');			
		}		
	}
	
	function edit() {
		if($_POST['dosubmit']) {
			$trainid = intval($_POST['trainid']) ? intval($_POST['trainid']) : showmessage(L('parameter_error'),HTTP_REFERER);
			$info = $_POST['info'];
			$info[addtime] = time();
			$return_id = $this->db->update($info,array('trainid'=>$trainid));
			showmessage(L('operation_success'), '?m=iappTrain&c=manages&a=edit&trainid='.$trainid.'&menuid='.$_GET['menuid']);
		} else {
			$trainid = intval($_GET['trainid']) ? intval($_GET['trainid']) : showmessage(L('parameter_error'),HTTP_REFERER);		
			$info = $this->db->get_one(array('trainid'=>$trainid));
			include $this->admin_tpl('train_edit');			
		}
	}
	
	/**
	 * 删除
	 */
	public function delete() {
		$siteid = $this->siteid;
		
		if (isset($_GET['trainid']) && !empty($_GET['trainid'])) {
			$trainid = intval($_GET['trainid']);
			$this->db->delete(array('trainid'=>$trainid, 'siteid'=>$siteid));
			showmessage(L('operation_success'), HTTP_REFERER);
		} elseif (isset($_POST['ids']) && !empty($_POST['ids'])) {
			if (is_array($_POST['ids'])) {
				foreach ($_POST['ids'] as $fid) {
					$this->db->delete(array('trainid'=>$fid, 'siteid'=>$siteid));
				}
			}
			showmessage(L('operation_success'), HTTP_REFERER);
		} else {
			showmessage(L('illegal_operation'), HTTP_REFERER);
		}
	}
	
	
	/**
	 * 排序
	 */
	public function listorder() {
		if (isset($_POST['listorders']) && is_array($_POST['listorders'])) {
			$listorder = $_POST['listorders'];
			foreach ($listorder as $k => $v) {
				$this->db->update(array('listorder'=>$v), array('trainid'=>$k));
			}
		}
		showmessage(L('operation_success'), HTTP_REFERER);
	}
	
	
	
	function trainitem() {
		$trainid = intval($_GET['trainid']) ? intval($_GET['trainid']) : showmessage(L('parameter_error'),HTTP_REFERER);
		$traininfo = $this->db->get_one(array('trainid'=>$trainid));
		if($traininfo) {
			$traininfo[setting] = string2array($traininfo[setting]);
		}
		$iteminfo = $this->train_item->select(array('trainid'=>$trainid));
		if($iteminfo) {
			$iteminfo = $iteminfo[0];
			$iteminfo[setting] = string2array($iteminfo[setting]);
			$iteminfo[items] = string2array($iteminfo[items]);
			$arra = "";
			foreach ($iteminfo[items] as $ka=>$va){
				$brra = "";
				foreach ($va as $kb=>$vb){
					$brra .= $vb."|";
				}
				$brra =substr($brra,0,-1);
				$arra .= $brra."\n";
			}
			$iteminfo[items] = $arra;
		}
		if($_POST['dosubmit']) {
			$siteid = $this->siteid;
			$items = array();
			$itemsarr = explode("\n", trim($_POST['items']));
			foreach ($itemsarr as $ka=>$va){
				$items[$ka] = explode("|", trim($va));
			}
			if($iteminfo){
				$return_id = $this->train_item->update(array('items'=>array2string($items),'setting'=>'','addtime'=>time()),array('id'=>$iteminfo[id]));
			}else{
				$return_id = $this->train_item->insert(array('trainid'=>$trainid,'items'=>array2string($items),'setting'=>'','addtime'=>time()),'1');
			}
			showmessage(L('operation_success'), HTTP_REFERER);
		}
		include $this->admin_tpl('train_item');			
	}

}
?>