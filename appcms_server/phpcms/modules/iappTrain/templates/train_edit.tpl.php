<?php
defined('IN_ADMIN') or exit('No permission resources.'); 
include $this->admin_tpl('header', 'admin');?>
<form method="post" action="?m=iappTrain&c=manages&a=edit&menuid=<?php echo $_GET['menuid']?>">
<table width="100%" cellpadding="0" cellspacing="1" class="table_form">
 
	<tr>
		<th width="100"><?php echo L('车次')?>：</th>
		<td><input type="text" name="info[trainCode]" id="trainCode"
			size="50" value="<?php echo $info[trainCode]?>" class="input-text">
			<span>如：K8478/K8475</span>
			
			</td>
	</tr>
		<tr>
		<th width="100"><?php echo L('始发城市')?>：</th>
		<td><input type="text" name="info[traindeptCity]" id="traindeptCity"
			size="20" value="<?php echo $info[traindeptCity]?>" class="input-text">
			<?php echo L('目标城市')?>：<input type="text" name="info[trainArriveCity]" id="trainArriveCity"
			size="20" value="<?php echo $info[trainArriveCity]?>" class="input-text">
			<span>如：贵阳 广州</span>
		</td>
	</tr>
	
	<tr>
		<th width="100"><?php echo L('始发站')?>：</th>
		<td><input type="text" name="info[traindeptStation]" id="traindeptStation"
			size="20" value="<?php echo $info[traindeptStation]?>" class="input-text">
			<?php echo L('最终点站')?>：<input type="text" name="info[trainArriveStation]" id="trainArriveStation"
			size="20" value="<?php echo $info[trainArriveStation]?>" class="input-text"> <span>如：贵阳站 广州站</span>
		</td>
	</tr>

	<tr>
		<th width="100"><?php echo L('车型')?>：</th>
		<td><input type="text" name="info[trainType]" id="trainType"
			size="30" value="<?php echo $info[trainType]?>" class="input-text"><span>如：普快</span>
			
			</td>
	</tr>
	<tr>
		<th width="100"><?php echo L('发车时间')?>：</th>
		<td><input type="text" name="info[traindeptTime]" id="traindeptTime"
			size="20" value="<?php echo $info[traindeptTime]?>" class="input-text">
			<?php echo L('到站时间')?>：
			<input type="text" name="info[trainarriveTime]" id="trainarriveTime"
			size="20" value="<?php echo $info[trainarriveTime]?>" class="input-text">
			<span>如：15:15 9:15</span>
			
			
			</td>
	</tr>

	<tr>
		<th width="100"><?php echo L('距离')?>：</th>
		<td><input type="text" name="info[trainallMileage]" id="trainallMileage"
			size="30" value="<?php echo $info[trainallMileage]?>" class="input-text">
			<span>如：285公里</span>
			</td>
	</tr>
	<tr>
		<th width="100"><?php echo L('花费时间')?>：</th>
		<td><input type="text" name="info[trainallTime]" id="trainallTime"
			size="30" value="<?php echo $info[trainallTime]?>" class="input-text">
			<span>如：5小时20分</span>
			</td>
	</tr>
	<tr>
		<th width="100"><?php echo L('查询站点')?>：</th>
		<td><input type="text" name="info[trainsearchStation]" id="trainsearchStation"
			size="30" value="<?php echo $info[trainsearchStation]?>" class="input-text">
			<span>当前城市站点，如：贵阳</span>
			</td>
	</tr>
	


	<tr>
		<td>&nbsp;</td>
		<td>
		<input type="hidden" name="trainid" value="<?php echo $info[trainid];?>">
		<input type="submit" name="dosubmit" id="dosubmit" value=" <?php echo L('提交')?>" class="button">&nbsp;<input type="reset" value=" <?php echo L('重写')?> " class="button"></td>
	</tr>
</table>
</form>

</body>
</html>