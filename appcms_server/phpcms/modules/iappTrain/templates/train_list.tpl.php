<?php 
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');
?>

<div class="pad-lr-10">
<form name="myform" id="myform" action="" method="post">
<div class="table-list">
    <table width="100%" cellspacing="0">
        <thead>
            <tr>
            <th width="35" align="center">
			<input type="checkbox" value="" id="check_box" onclick="selectall('ids[]');"></th>
			<th width="50" align="center"><?php echo L('listorder');?></th>
			<th width="50" align="center">ID</th>
			<th align="center"><?php echo L('车次')?></th>
			<th width="80" align="center"><?php echo L('始发城市')?></th>
			<th width="80" align="center"><?php echo L('目标城市')?></th>
			<th width="80" align="center"><?php echo L('车型')?></th>
			<th width="80" align="center"><?php echo L('始发时间')?></th>
			<th width="80" align="center"><?php echo L('到站时间')?></th>
			<th width="100" align="center"><?php echo L('花费时间')?></th>
			<th width="80" align="center"><?php echo L('距离')?></th>
			<th width="220" align="center"><?php echo L('operations_manage')?></th>
            </tr>
        </thead>
    <tbody>
 <?php 
if(is_array($data)){
	foreach($data as $r){
?>   
	<tr>
	<td align="center"><input class="inputcheckbox " name="ids[]" value="<?php echo $r['trainid'];?>" type="checkbox"></td>
    <td align='center'><input name='listorders[<?php echo $r['trainid'];?>]' type='text' size='3' value='<?php echo $r['listorder'];?>' class='input-text-c'></td>
	<td align='center' ><?php echo $r['trainid'];?></td>
	<td><a href="<?php echo APP_PATH ?>index.php?m=iappTrain&c=index&a=show&trainid=<?php echo $r['trainid']?>" target="_blank"><?php echo $r['trainCode']?></a></td>
	<td align="center"><?php echo $r['traindeptCity']?></td>
	<td align="center"><?php echo $r['trainArriveCity']?></td>
	<td align="center"><?php echo $r['trainType']?></td>
	<td align="center"><?php echo $r['traindeptTime']?></td>
	<td align="center"><?php echo $r['trainarriveTime']?></td>
	<td align="center"><?php echo $r['trainallTime']?></td>
	<td align="center"><?php echo $r['trainallMileage']?></td>
	
	
	<td align="center">
	<a href="?m=iappTrain&c=manages&a=trainitem&trainid=<?php echo $r['trainid']?>&menuid=<?php echo $_GET['menuid']?>"><?php echo L('时刻表管理')?></a> | 
	<a href="?m=iappTrain&c=manages&a=edit&trainid=<?php echo $r['trainid']?>&menuid=<?php echo $_GET['menuid']?>"><?php echo L('修改')?></a> | <a href="?m=iappTrain&c=manages&a=delete&trainid=<?php echo $r['trainid']?>" onClick="return confirm('<?php echo L('确认要删除『'.$r[title].'』吗？');?>')"><?php echo L('删除')?></a>
	</td>
	</tr>
<?php 
	}
}
?>
</tbody>
    </table>
  
    <div class="btn"><label for="check_box"><?php echo L('selected_all')?>/<?php echo L('cancel')?></label>
		<input type="hidden" value="YVt4Dk" name="pc_hash">
    	<input type="button" class="button" value="排序" onclick="myform.action='?m=iappTrain&c=manages&a=listorder&dosubmit=1';myform.submit();"/>
		
		<input type="button" class="button" value="<?php echo L('delete');?>" onclick="myform.action='?m=iappTrain&c=manages&a=delete&dosubmit=1';return confirm_delete()"/>
		
		</div>  

</div>
 <div id="pages"><?php echo $this->db->pages;?></div>
</form>
</div>
<script type="text/javascript">
window.top.$("#display_center_id").css("display","none");
function confirm_delete(){
	if(confirm('<?php echo L('确认要删除『选中』吗？');?>')) $('#myform').submit();
}
</script>
</body>
</html>