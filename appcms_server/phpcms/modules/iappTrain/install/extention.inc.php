<?php
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');

$menus = $menu_db->select(array('name'=>'iappPlug', 'm'=>'iapp', 'c'=>'manages', 'a'=>'init'));
$parentid = $menu_db->insert(array('name'=>'iappTrain', 'parentid'=>$menus[0][id], 'm'=>'iappTrain', 'c'=>'manages', 'a'=>'init', 'data'=>'', 'listorder'=>0, 'display'=>'1'), true);


$menu_db->insert(array('name'=>'iappTrain_add', 'parentid'=>$parentid, 'm'=>'iappTrain', 'c'=>'manages', 'a'=>'add', 'data'=>'', 'listorder'=>0, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappTrain_edit', 'parentid'=>$parentid, 'm'=>'iappTrain', 'c'=>'manages', 'a'=>'edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappTrain_delete', 'parentid'=>$parentid, 'm'=>'iappTrain', 'c'=>'manages', 'a'=>'delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));

$menu_db->insert(array('name'=>'iappTrain_item', 'parentid'=>$parentid, 'm'=>'iappTrain', 'c'=>'manages', 'a'=>'trainitem', 'data'=>'', 'listorder'=>1, 'display'=>'1'));

$language = array('iappTrain'=>'火车时刻',
				  'iappTrain_add'=>'添加车次',
				  'iappTrain_edit'=>'修改车次',
				  'iappTrain_delete'=>'删除车次',
				  'iappTrain_item'=>'时刻表管理'		  
                  );

?>