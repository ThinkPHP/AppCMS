DROP TABLE IF EXISTS `phpcms_iappTrain`;
CREATE TABLE IF NOT EXISTS `phpcms_iappTrain` (
  `trainid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `trainCode` varchar(50) NOT NULL,
  `traindeptCity` varchar(50) NOT NULL,
  `trainArriveCity` varchar(50) NOT NULL,
  `traindeptStation` varchar(50) NOT NULL,
  `trainArriveStation` varchar(50) NOT NULL,
  `trainType` varchar(50) NOT NULL,
  `traindeptTime` varchar(50) NOT NULL,
  `trainarriveTime` varchar(50) NOT NULL,
  `trainallMileage` varchar(50) NOT NULL,
  `trainallTime` varchar(50) NOT NULL,
  `trainsearchStation` varchar(50) NOT NULL,
  `setting` mediumtext,
  `siteid` smallint(5) DEFAULT '1',
  `listorder` smallint(5) DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`trainid`)
) TYPE=MyISAM;
