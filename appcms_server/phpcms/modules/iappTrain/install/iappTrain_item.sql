DROP TABLE IF EXISTS `phpcms_iappTrain_item`;
CREATE TABLE IF NOT EXISTS `phpcms_iappTrain_item` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `trainid` smallint(5) NOT NULL,
  `items` mediumtext,
  `setting` mediumtext,
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) TYPE=MyISAM;
