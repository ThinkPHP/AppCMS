<?php
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');

$menus = $menu_db->select(array('name'=>'iappPlug', 'm'=>'iapp', 'c'=>'manages', 'a'=>'init'));

$parentid = $menu_db->insert(array('name'=>'iappPush', 'parentid'=>$menus[0][id], 'm'=>'iappPush', 'c'=>'manages', 'a'=>'add', 'data'=>'', 'listorder'=>1, 'display'=>'1'), true);

$menu_db->insert(array('name'=>'iappPush_add', 'parentid'=>$parentid, 'm'=>'iappPush', 'c'=>'manages', 'a'=>'init', 'data'=>'', 'listorder'=>2, 'display'=>'1'));

$menu_db->insert(array('name'=>'iappPush_setting', 'parentid'=>$parentid, 'm'=>'iappPush', 'c'=>'manages', 'a'=>'setting', 'data'=>'', 'listorder'=>3, 'display'=>'1'));

$language = array('iappPush'=>'推送',
				  'iappPush_add'=>'推送历史',
				  'iappPush_setting'=>'推送设置'					  
                  );

?>