<?php 
defined('IN_PHPCMS') or exit('No permission resources.');
class index {
	function __construct() {		
		$this->db = pc_base::load_model('iappPush_model');
	}
	
	public function init() {
		$re = array();
		$page = max(intval($_GET['page']), 1);
		$data = $this->db->listinfo(array(), '`id` DESC', $page,2);
		foreach($data as $k=>$v){
			$data[$k][datas] = string2array($v[datas]);
			$data[$k][setting] = string2array($v[setting]);
			$data[$k][addtime] = date('Y-m-d H:i:s', $v['addtime']);
		}
		$re[data] = $data;
		$re["success"]="OK";
		echo json_encode($re);
	}
}
?>