<?php 
defined('IN_PHPCMS') or exit('No permission resources.'); 
pc_base::load_app_class('admin','admin',0);
pc_base::load_sys_class('form', '', 0);
class manages extends admin {
	function __construct() {
		parent::__construct();
		$this->db = pc_base::load_model('iappPush_model');
		$this->siteid = $this->get_siteid();
		$this->iappconfig = getcache('iapp', 'commons');
	}
	
	function init() {
		$page = max(intval($_GET['page']), 1);
		$data = $this->db->listinfo(array(), '`id` DESC', $page);
		include $this->admin_tpl('list');
	}
	
	function add() {
		pc_base::load_app_class('curl', 'iapp', 0);
		$curl = new curl();

		if($_POST['dosubmit']) {
			$info = $_POST[info];
			$config = getcache('iappPush', 'commons');
			
			$content = $info[content];
			$content = substr($content,1,strlen($content)-2);
			$content = str_replace('\"', '"', $content);
			
			$data = 'appId='.$config[appid].
					'&badgeNum='.
					'&body={"msgName": "'.$this->iappconfig[info][sitename].'",'.$content.'}'.
					'&checkbox=checkbox'.
					'&keepHours=12'.
					'&platforms=1'.
					'&pushTime='.
					'&save=2'.
					'&title='.$info[title];

			$re = $curl->vpost("http://newpush.appcan.cn/msg/pushMsg",$data);
			$re = $this->jsonp2json($re);
			
			if($re[status]=='ok'){		
				$info[addtime] = time();
				$return_id = $this->db->insert($info,'1');
				showmessage('推送成功!', '?m=iappPush&c=manages&a=add');
			}else{
				showmessage('推送失败!', '?m=iappPush&c=manages&a=add');
			}
		} else {
			$islogin = $curl->vget("http://dashboard.appcan.cn/login/info?callback=jsonp");
			$islogin = $this->jsonp2json($islogin);
			if($islogin[login]=='ok'){
				$islogin = 'yes';
			}else{
				$islogin = 'no';
			}
		
			include $this->admin_tpl('add');			
		}	
	}
	
	public function login() {
		pc_base::load_app_class('curl', 'iapp', 0);
		$curl = new curl();
		
		$islogin = $curl->vget("http://dashboard.appcan.cn/login/info?callback=jsonp");
		$islogin = $this->jsonp2json($islogin);
		
		if($islogin[login]=='ok'){
			showmessage('已经授权~',HTTP_REFERER);
		}else{
			$config = getcache('iappPush', 'commons');
			
			//获取lt
			$lt = $curl->vget("http://newsso.appcan.cn/login?callback=jsonp&service=http://dashboard.appcan.cn/user/add&get-lt=true");
			$lt = $this->jsonp2json($lt);
			if($config[username]=='' || $config[password]==''){
				showmessage('请先设置推送账号~',HTTP_REFERER);
			}
			
			//登录
			$data = '_eventId=submit'.
			'&callback=parent.doLoginCallback'.
			'&errorUrl=http://dashboard.appcan.cn/login/error?jsDomain=appcan.cn'.
			'&isAjax=true'.
			'&lt='.$lt[retData].
			'&password='.$config[password].
			'&service=http://dashboard.appcan.cn/user/add'.
			'&username='.$config[username];

			$_relogin = $curl->vlogin("http://newsso.appcan.cn/login",$data);
			
			//验证是否登录成功
			$islogin = $curl->vget("http://dashboard.appcan.cn/login/info?callback=jsonp");
			$islogin = $this->jsonp2json($islogin);
			if($islogin[login]=='ok'){
				showmessage('授权成功~',HTTP_REFERER);
			}else{
				showmessage('授权失败，请重新授权~',HTTP_REFERER);
			}
		}
	}
	
	public function setting() {
		$data = array();
		$m_db = pc_base::load_model('module_model');
		$data = $m_db->select(array('module'=>'iappPush'));
		$data = string2array($data[0]['setting']);
 		if(isset($_POST['dosubmit'])) {
			$setting = $_POST['data'];
  			setcache('iappPush', $setting, 'commons');  
 			$set = array2string($setting);
			$m_db->update(array('setting'=>$set), array('module'=>'iappPush'));
			showmessage('修改成功',HTTP_REFERER);
		} else {
			include $this->admin_tpl('setting');
		}
	}
	
	function getlist() {
		$show_header = '';
		$page = max(intval($_GET['page']), 1);
		$catid = intval($_GET['catid'])? intval($_GET['catid']):$this->iappconfig[setting][defaultcatid];
		$cdb = pc_base::load_model('content_model');
		$CATEGORYS = getcache('category_content_'.$this->siteid,'commons');
		$modelid = $CATEGORYS[$catid]['modelid'];
		$cdb->set_model($modelid);

		$model = getcache('model', 'commons');
		$infos = $cdb->listinfo(array('catid'=>$catid), '`id` DESC', $page,7);
		
		foreach($infos as $k=>$v){
			$infos[$k][tablename] = $model[$modelid]['tablename'];
			$infos[$k][modelid] = $modelid;
		}
		
		include $this->admin_tpl('getlist');
	}

	function jsonp2json($str) {
		$str = trim($str);
		$ln = stripos($str,'{'); 
		$rn = strripos($str,'}');
		$str = substr($str,$ln,($rn-$ln)+1);
		$str = json_decode($str, true);
		return $str;
	}
	
}
?>