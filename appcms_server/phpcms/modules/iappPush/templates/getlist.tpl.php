<?php
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header','admin');
?>
<div class="pad-10">

<div class="col-tab">
    <ul class="tabBut cu-li">
      <li id="tab_setting_1" class="on" onclick="SwapTab('setting','on','',7,1);">新闻</li>
      <li id="tab_setting_2" onclick="SwapTab('setting','on','',7,2);">视频</li>
	  <li id="tab_setting_3" onclick="SwapTab('setting','on','',7,3);">优惠券</li>
	  <li id="tab_setting_4" onclick="SwapTab('setting','on','',7,4);">活动</li>
	  <li id="tab_setting_5" onclick="SwapTab('setting','on','',7,5);">今日</li>
	  <li id="tab_setting_6" onclick="SwapTab('setting','on','',7,6);">专题</li>
	  <li id="tab_setting_7" onclick="SwapTab('setting','on','',7,7);">商家</li>
    </ul>
    <div id="div_setting_1" class="contentList pad-10">
		<form name="searchform" action="" method="get" >
			<input type="hidden" value="iappPush" name="m">
			<input type="hidden" value="manages" name="c">
			<input type="hidden" value="getlist" name="a">
			<table width="100%" cellspacing="0" class="search-form">
				<tbody>
					<tr>
					<td align="center">
					<div class="explain-col">
						<select name="field">
							<option value='title' <?php if($_GET['field']=='title') echo 'selected';?>>标题</option>
							<option value='keywords' <?php if($_GET['field']=='keywords') echo 'selected';?> >关键字</option>
							<option value='description' <?php if($_GET['field']=='description') echo 'selected';?>>导读</option>
							<option value='id' <?php if($_GET['field']=='id') echo 'selected';?>>ID</option>
						</select>
						<?php echo form::select_category('',$_GET['catid'],'name="catid"','不限栏目','',0,1);?>
						<input name="keywords" type="text" value="<?php echo stripslashes($_GET['keywords'])?>" style="width:250px;" class="input-text" />
						<input type="submit" name="dosubmit" class="button" value="<?php echo L('search');?>" />
				</div>
					</td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>

</div>


<div class="table-list">
    <table width="100%" cellspacing="0" >
        <thead>
            <tr>
            <th >标题</th>
			<th width="100">所属栏目</th>
            <th width="100">添加时间</th>
            </tr>
        </thead>
    <tbody>
	<?php foreach($infos as $r) { ?>
	<tr onclick="select_list(this,'<?php echo safe_replace($r['title']);?>',<?php echo $r['id'];?>,<?php echo $r['catid'];?>,'<?php echo $r['tablename'];?>',<?php echo $r['modelid'];?>,'<?php echo $r['thumb'];?>')"  title="点击选择">
		<td align='left' ><?php echo $r['title'];?><?php if($r['thumb']!='') {echo '<img src="'.IMG_PATH.'icon/small_img.gif" title="'.L('thumb').'">'; } ?></td>
		<td align='center'><?php echo $CATEGORYS[$r['catid']]['catname'];?></td>
		<td align='center'><?php echo date('Y-m-d', $r['inputtime']);?></td>
	</tr>
	 <?php }?>
	    </tbody>
    </table>
   <div id="pages"><?php echo $cdb->pages;?></div>
</div>
</div>

<input type="hidden" value="listdata" name="listdata" id="listdata">
<style type="text/css">
.line_ff9966{
	background-color:#FF9966;
}
.line_ff9966:hover {
	background-color:#FF9966;
}
.line_ff9966:active {
	background-color:#FF9966;
}

</style>
<script type="text/javascript">
	function select_list(obj,title,id,catid,tablename,modelid,thumb) {
		$('#listdata').val(title+'|'+id+'|'+catid+'|'+tablename+'|'+modelid+'|'+thumb);
		$(obj).addClass('line_ff9966');
		$(obj).siblings().removeClass('line_ff9966');
	}

	function SwapTab(name,cls_show,cls_hide,cnt,cur){
		for(i=1;i<=cnt;i++){
			if(i==cur){
				 $('#div_'+name+'_'+i).show();
				 $('#tab_'+name+'_'+i).attr('class',cls_show);
			}else{
				 $('#div_'+name+'_'+i).hide();
				 $('#tab_'+name+'_'+i).attr('class',cls_hide);
			}
		}
	}

</script>
</body>
</html>