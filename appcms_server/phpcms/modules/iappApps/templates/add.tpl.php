<?php 
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');?>
<form method="post" action="?m=iappApps&c=manages&a=add">
<table width="100%" cellpadding="0" cellspacing="1" class="table_form">
 
	<tr>
		<th width="100"><?php echo L('分类名称')?>：</th>
		<td>
		<select name="info[typeid]" id="">
		<option value="0">默认分类</option>
		<?php
		  $i=0;
		  foreach($types as $typeid=>$type){
		  $i++;
		?>
		<option value="<?php echo $type['id'];?>"><?php echo $type['title'];?></option>
		<?php }?>
		</select>
		
		</td>
	</tr>
	
	<tr>
		<th width="100"><?php echo L('App名称')?>：</th>
		<td><input type="text" name="info[name]" id="name"
			size="30" class="input-text"></td>
	</tr>
	<tr>
		<th width="100"><?php echo L('ios下载地址')?>：</th>
		<td><input type="text" name="info[iosurl]" id="iosurl"
			size="50" class="input-text"></td>
	</tr>
	<tr>
		<th width="100"><?php echo L('安卓下载地址')?>：</th>
		<td><input type="text" name="info[androidurl]" id="androidurl"
			size="50" class="input-text"></td>
	</tr>
	
	<tr id="Applogo">
		<th width="100"><?php echo L('Applogo')?>：</th>
		<td><?php echo form::images('info[logo]', 'logo', '', 'iappApps')?></td>
	</tr>
	
	<tr id="photos">
		<th width="100"><?php echo L('软件截图')?>：</th>
		<td><?php echo form::images('info[photos][]', 'photos_1', '', 'iappApps')?></td>
	</tr>
	<tr id="photos">
		<th width="100"><?php echo L('软件截图')?>：</th>
		<td><?php echo form::images('info[photos][]', 'photos_2', '', 'iappApps')?></td>
	</tr>
	<tr id="photos">
		<th width="100"><?php echo L('软件截图')?>：</th>
		<td><?php echo form::images('info[photos][]', 'photos_3', '', 'iappApps')?></td>
	</tr>
	<tr id="photos">
		<th width="100"><?php echo L('软件截图')?>：</th>
		<td><?php echo form::images('info[photos][]', 'photos_4', '', 'iappApps')?></td>
	</tr>
	
	
		
	<tr>
		<th width="100"><?php echo L('App介绍')?>：</th>
		<td>
			<textarea name="info[content]" id="content" cols="100"
			rows="6"><?php echo $info[content]?></textarea>
			
		</td>
	</tr>
	


	<tr>
		<td>&nbsp;</td>
		<td>
		<input type="submit" name="dosubmit" id="dosubmit" value=" <?php echo L('提交')?>" class="button">&nbsp;<input type="reset" value=" <?php echo L('重写')?> " class="button"></td>
	</tr>
</table>
</form>
</body>
</html>