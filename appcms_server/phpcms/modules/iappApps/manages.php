<?php 
defined('IN_PHPCMS') or exit('No permission resources.'); 
pc_base::load_app_class('admin','admin',0);
pc_base::load_sys_class('form', '', 0);
pc_base::load_sys_class('format', '', 0);
class manages extends admin {
	private $db,$db_iappApps_type,$db_iappApps_reply;
	function __construct() {
		parent::__construct();
		$this->sites = pc_base::load_app_class('sites','admin');
		$this->db = pc_base::load_model('iappApps_model');
		$this->db_iappApps_type = pc_base::load_model('iappApps_type_model');
		$this->siteid = $this->get_siteid();
	}
	
	function init() {
		$page = max(intval($_GET['page']), 1);
		$data = $this->db->listinfo(array('siteid'=>$this->get_siteid()), '`listorder` DESC', $page);
		include $this->admin_tpl('list');
	}
		
	
	function add() {
		$types = $this->db_iappApps_type->select(array('siteid'=>$this->get_siteid()),'*','','`listorder` DESC');
		if($_POST['dosubmit']) {
			$info = $_POST['info'];
			$info[content] = trim($_POST['info'][content]) ? trim($_POST['info'][content]) : showmessage(L('主题不能为空'),HTTP_REFERER);
			$info[logo] = trim($_POST['info'][logo]) ? trim($_POST['info'][logo]) : showmessage(L('logo不能为空'),HTTP_REFERER);
			
			
			$info[siteid] = $this->siteid;
			
			$info[photos] = array2string($info[photos]);
			
			$info[addtime] = time();
			$info[edittime] = time();
			$return_id = $this->db->insert($info,'1');
			showmessage(L('add_success'), '?m=iappApps&c=manages&a=init&menuid='.$_GET['menuid']);
		} else {
			include $this->admin_tpl('add');
		}		
	}
	
	function edit() {
		$types = $this->db_iappApps_type->select(array('siteid'=>$this->get_siteid()),'*','','`listorder` DESC');
		if($_POST['dosubmit']) {
			$id = intval($_POST['id']) ? intval($_POST['id']) : showmessage(L('parameter_error'),HTTP_REFERER);
			$info = $_POST['info'];
			$info[content] = trim($_POST['info'][content]) ? trim($_POST['info'][content]) : showmessage(L('主题不能为空'),HTTP_REFERER);
			$info[logo] = trim($_POST['info'][logo]) ? trim($_POST['info'][logo]) : showmessage(L('logo不能为空'),HTTP_REFERER);
			
			
			$info[photos] = array2string($info[photos]);
			$info[edittime] = time();
			$return_id = $this->db->update($info,array('id'=>$id));
			showmessage(L('operation_success'), '?m=iappApps&c=manages&a=edit&id='.$id.'&menuid='.$_GET['menuid']);
		} else {
			$id = intval($_GET['id']) ? intval($_GET['id']) : showmessage(L('parameter_error'),HTTP_REFERER);		
			$info = $this->db->get_one(array('id'=>$id));
			$info[photos] = string2array($info[photos]);
			include $this->admin_tpl('edit');			
		}
	}
	
	/**
	 * 删除
	 */
	public function delete() {
		$siteid = $this->siteid;
		
		if (isset($_GET['id']) && !empty($_GET['id'])) {
			$id = intval($_GET['id']);
			$this->db->delete(array('id'=>$id));
			showmessage(L('operation_success'), HTTP_REFERER);
		} elseif (isset($_POST['ids']) && !empty($_POST['ids'])) {
			if (is_array($_POST['ids'])) {
				foreach ($_POST['ids'] as $fid) {
					$this->db->delete(array('id'=>$fid));
				}
			}
			showmessage(L('operation_success'), HTTP_REFERER);
		} else {
			showmessage(L('illegal_operation'), HTTP_REFERER);
		}
	}
	/**
	 * 排序
	 */
	public function listorder() {
		if (isset($_POST['listorders']) && is_array($_POST['listorders'])) {
			$listorder = $_POST['listorders'];
			foreach ($listorder as $k => $v) {
				$this->db->update(array('listorder'=>$v), array('id'=>$k));
			}
		}
		showmessage(L('operation_success'), HTTP_REFERER);
	}
	
	
	
	
	
	
	function type_list() {
		$page = max(intval($_GET['page']), 1);
		$data = $this->db_iappApps_type->listinfo(array('siteid'=>$this->get_siteid()), '`listorder` DESC', $page);
		include $this->admin_tpl('type_list');
	}
	function type_add() {
		if($_POST['dosubmit']) {
			$info = $_POST['info'];
			$info[title] = trim($_POST['info'][title]) ? trim($_POST['info'][title]) : showmessage(L('主题不能为空'),HTTP_REFERER);
			$info[siteid] = $this->siteid;
			$info[listorder] = 0;
			$return_id = $this->db_iappApps_type->insert($info,'1');
			showmessage(L('add_success'), '?m=iappApps&c=manages&a=type_list&menuid='.$_GET['menuid']);
		} else {
			include $this->admin_tpl('type_add');
		}		
	}
	
	function type_edit() {
		if($_POST['dosubmit']) {
			$id = intval($_POST['id']) ? intval($_POST['id']) : showmessage(L('parameter_error'),HTTP_REFERER);
			$info = $_POST['info'];
			$info[title] = trim($_POST['info'][title]) ? trim($_POST['info'][title]) : showmessage(L('主题不能为空'),HTTP_REFERER);
			$return_id = $this->db_iappApps_type->update($info,array('id'=>$id));
			showmessage(L('operation_success'), '?m=iappApps&c=manages&a=type_edit&id='.$id.'&menuid='.$_GET['menuid']);
		} else {
			$id = intval($_GET['id']) ? intval($_GET['id']) : showmessage(L('parameter_error'),HTTP_REFERER);		
			$info = $this->db_iappApps_type->get_one(array('id'=>$id));
			include $this->admin_tpl('type_edit');			
		}
	}
	
	/**
	 * 删除
	 */
	public function type_delete() {
		$siteid = $this->siteid;
		
		if (isset($_GET['id']) && !empty($_GET['id'])) {
			$id = intval($_GET['id']);
			$this->db_iappApps_type->delete(array('id'=>$id, 'siteid'=>$siteid));
			showmessage(L('operation_success'), HTTP_REFERER);
		} elseif (isset($_POST['ids']) && !empty($_POST['ids'])) {
			if (is_array($_POST['ids'])) {
				foreach ($_POST['ids'] as $fid) {
					$this->db_iappApps_type->delete(array('id'=>$fid, 'siteid'=>$siteid));
				}
			}
			showmessage(L('operation_success'), HTTP_REFERER);
		} else {
			showmessage(L('illegal_operation'), HTTP_REFERER);
		}
	}
	
	
	/**
	 * 排序
	 */
	public function type_listorder() {
		if (isset($_POST['listorders']) && is_array($_POST['listorders'])) {
			$listorder = $_POST['listorders'];
			foreach ($listorder as $k => $v) {
				$this->db_iappApps_type->update(array('listorder'=>$v), array('id'=>$k));
			}
		}
		showmessage(L('operation_success'), HTTP_REFERER);
	}
	
	


}
?>