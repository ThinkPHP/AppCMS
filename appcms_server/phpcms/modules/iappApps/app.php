<?php 
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_sys_class('format', '', 0);
class app {
	function __construct() {		
		$this->db = pc_base::load_model('iappApps_model');
		$this->siteid = get_siteid();
	}
	
	
	
	public function lists() {
		$page = max(intval($_GET['page']), 1);
		$order = intval($_GET['order'])==0?"id":"replytime";
		$data = $this->db->listinfo(array('siteid'=>$this->siteid), '`'.$order.'` DESC', $page);
		$news = array();
		foreach ($data as $k=>$v) {
			$news[$k][id] = $v[id];
			$news[$k][content] = $v[content];
			$news[$k][renum] = $v[renum];
			$news[$k][replytime] = $v[replytime];			
		}
		
		//更新点击数
		$return_id = $this->db->update(array('htis'=>'+=1'),array('siteid'=>$this->siteid));
		//更新时间
		$upTime = param::get_cookie($MODEL[$modelid]['tablename'].$this->siteid.'upTime');
		param::set_cookie($MODEL[$modelid]['tablename'].$this->siteid.'upTime', date('Y-m-j H:i:s'));
		echo json_encode(array("news"=>$news,"upTime"=>$upTime));
		
	}
	public function show() {
		$re = array();
		$iaskid = intval($_GET['iaskid']) ? intval($_GET['iaskid']) : showmessage(L('parameter_error'),HTTP_REFERER);
		$iaskinfo = $this->db->get_one(array('id'=>$iaskid));
		
		$re[detail][id] = $iaskinfo[id];
		$re[detail][content] = $iaskinfo[content];
		$re[detail][nick] = $iaskinfo[username];
		$re[detail][addtime] = $iaskinfo[addtime];
		
		$re[reply] = array();
		$page = max(intval($_GET['page']), 1);
		$reply = $this->db_iappApps_reply->listinfo(array('iaskid'=>$iaskid), '`id` DESC', $page,20);
		foreach ($reply as $k=>$v) {
			$re[reply][$k][id] = $v[id];
			$re[reply][$k][content] = $v[content];
			$re[reply][$k][nick] = $v[username];
			$re[reply][$k][addtime] = $v[addtime];		
		}
		$re[news] = array();
		$news = $this->db->select(array('siteid'=>$this->siteid), '*','5','`id` DESC');
		foreach ($news as $k=>$v) {
			$re[news][$k][id] = $v[id];
			$re[news][$k][content] = $v[content];
			$re[news][$k][renum] = $v[renum];	
		}
		echo json_encode($re);
	}
	
	
	
}
?>