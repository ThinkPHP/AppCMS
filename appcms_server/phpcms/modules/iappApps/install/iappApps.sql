DROP TABLE IF EXISTS `phpcms_iappApps`;
CREATE TABLE IF NOT EXISTS `phpcms_iappApps` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `siteid` smallint(5) NOT NULL,
  `typeid` smallint(5) NOT NULL COMMENT '栏目',
  `name` varchar(50) NOT NULL COMMENT '标题',
  `iosurl` varchar(200) NOT NULL COMMENT 'ios下载地址',
  `androidurl` varchar(200) NOT NULL COMMENT 'android下载地址',
  `logo` varchar(200) NOT NULL COMMENT 'logo',
  `photos` text NOT NULL COMMENT '图片',
  `content` mediumtext NOT NULL COMMENT 'App简介',
  `htis` smallint(5) DEFAULT '0' COMMENT '查看量',
  `praise` smallint(5) DEFAULT '0' COMMENT '赞',
  `listorder` smallint(5) DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) TYPE=MyISAM;
