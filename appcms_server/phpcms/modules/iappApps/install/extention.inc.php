<?php
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');

$menus = $menu_db->select(array('name'=>'iappPlug', 'm'=>'iapp', 'c'=>'manages', 'a'=>'init'));

$parentid = $menu_db->insert(array('name'=>'iappApps', 'parentid'=>$menus[0][id], 'm'=>'iappApps', 'c'=>'manages', 'a'=>'init', 'data'=>'', 'listorder'=>0, 'display'=>'1'), true);

$menu_db->insert(array('name'=>'iappApps_add', 'parentid'=>$parentid, 'm'=>'iappApps', 'c'=>'manages', 'a'=>'add', 'data'=>'', 'listorder'=>2, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappApps_edit', 'parentid'=>$parentid, 'm'=>'iappApps', 'c'=>'manages', 'a'=>'edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappApps_delete', 'parentid'=>$parentid, 'm'=>'iappApps', 'c'=>'manages', 'a'=>'delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));


$menu_db->insert(array('name'=>'iappApps_type_list', 'parentid'=>$parentid, 'm'=>'iappApps', 'c'=>'manages', 'a'=>'type_list', 'data'=>'', 'listorder'=>6, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappApps_type_add', 'parentid'=>$parentid, 'm'=>'iappApps', 'c'=>'manages', 'a'=>'type_add', 'data'=>'', 'listorder'=>5, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappApps_type_edit', 'parentid'=>$parentid, 'm'=>'iappApps', 'c'=>'manages', 'a'=>'type_edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappApps_type_delete', 'parentid'=>$parentid, 'm'=>'iappApps', 'c'=>'manages', 'a'=>'type_delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappApps_type_listorder', 'parentid'=>$parentid, 'm'=>'iappApps', 'c'=>'manages', 'a'=>'type_listorder', 'data'=>'', 'listorder'=>0, 'display'=>'0'));






$language = array('iappApps'=>'App推荐',
				  'iappApps_add'=>'添加App',
				  'iappApps_edit'=>'修改App',
				  'iappApps_delete'=>'删除App',

				 
				  'iappApps_type_add'=>'添加分类',
				  'iappApps_type_edit'=>'修改分类',
				  'iappApps_type_list'=>'分类列表',
				  'iappApps_type_delete'=>'删除分类',
				  'iappApps_type_listorder'=>'分类排序'  
                  );

?>