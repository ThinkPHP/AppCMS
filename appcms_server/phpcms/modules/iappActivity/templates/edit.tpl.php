<?php
defined('IN_ADMIN') or exit('No permission resources.'); 
include $this->admin_tpl('header', 'admin');?>
<div class="pad-lr-10">
<form method="post" action="?m=iappActivity&c=manages&a=edit&menuid=<?php echo $_GET['menuid']?>">
<fieldset>
<table width="100%" cellpadding="0" cellspacing="1" class="table_form">
	<tr>
		<th width="100"><?php echo L('分类名称')?>：</th>
		<td>
		<select name="info[typeid]" id="">
		<option value="0">默认分类</option>
		<?php
		
		  foreach($types as $typeid=>$type){
		?>
		<option value="<?php echo $type['id'];?>" <?php if($type['id']==$info[typeid]){echo 'selected=""';}?> ><?php echo $type['title'];?></option>
		<?php }?>
		</select>
		
		</td>
	</tr>
	<tr>
		<th width="100"><?php echo L('标题')?>：</th>
		<td>
		<input type="text" name="info[title]" id="name"
			size="30" value="<?php echo $info[title]?>" class="input-text">
		</td>
	</tr>
	<tr>
		<th width="100">长标题：</th>
		<td>
		<input type="text" name="info[ltitle]" id="ltitle"
			size="60" value="<?php echo $info[ltitle]?>" class="input-text">
		</td>
	</tr>
	
	<tr id="image_id">
		<th width="100">活动图片：</th>
		<td><?php echo form::images('info[image]', 'image', $info[image], 'iappCoupons')?></td>
	</tr>
	
	<tr>
		<th width="100">报名时间：</th>
		<td>
				<?php echo form::date('info[start_time]',date('Y-m-d', $info['start_time']),0,0,'false');?>&nbsp;到 &nbsp;<?php echo form::date('info[end_time]',date('Y-m-d', $info['end_time']),0,0,'false');?>	
		</td>
	</tr>
	<tr>
		<th width="100">活动时间：</th>
		<td><?php echo form::date('info[activity_time]',date('Y-m-d', $info['activity_time']),0,0,'false');?>				
		</td>
	</tr>
	
	<tr>
		<th width="100"><?php echo L('活动名额')?>：</th>
		<td><input type="text" name="info[num]" id="num"
			size="15" value="<?php echo $info[num]?>" class="input-text">已报名：<input type="text" name="info[bmnum]" id="bmnum"
			size="15" value="<?php echo $info[bmnum]?>" class="input-text">
			
			
			</td>
	</tr>
</table>
</fieldset>

<div id="items">
<?php
		$i=0;
		foreach($info[content] as $v){
	?>   
		<fieldset id="item_<?php echo $i;?>" title="<?php echo $i;?>">
			<table width="100%"  class="table_form">
				<th width="100">标题</th>
				<td class="y-bg">
				  <input type="text" class="input-text" name="info[content][<?php echo $i?>][title]" id="content_<?php echo $i?>_title" size="30" value="<?php echo $v[title]?>"/>
				  【<a href="#" onclick="$('#item_<?php echo $i;?>').remove();">移除</a>】
				</td>
				</tr>
				<tr>
				<th>简介</th>
				<td class="y-bg">
				  <textarea name="info[content][<?php echo $i?>][description]" id="content_<?php echo $i?>_description" cols="100" rows="6"> <?php echo $v[description]?> </textarea>
				  
				</td>
				</tr>
			</table>
		</fieldset>
	<?php
		$i++;
		}
	?>
	</div>
	<fieldset>
	<input type="button" id="additem" value=" 添加一行 " class="button">如：活动简介、活动流程、活动奖励、注意事项等
	</fieldset>
	<div class="bk10"></div>
	
	<input type="hidden" name="id" value="<?php echo $info[id];?>">
		<input type="submit" name="dosubmit" id="dosubmit" value=" <?php echo L('提交')?>" class="button">&nbsp;<input type="reset" value=" <?php echo L('重写')?> " class="button">

</form>

</div>

<script type="text/javascript">
$("#additem").click(function(){
	var n = parseInt($('#items fieldset:last').attr('title'))+1;
	if(n){
		n = n;
	}else{
		n = 1;
	}
		
	var str = '<fieldset id="item_'+n+'" title="'+n+'">'+
			  '<table width="100%"  class="table_form">'+
			  '<tr>'+
			  '<th width="100">标题</th>'+
			  '<td class="y-bg">'+
			  '<input type="text" name="info[content]['+n+'][title]" id="content_'+n+'_title" value="" size="30" class="input-text" />'+
			  '<a href="#" onclick="$(\'#item_'+n+'\').remove();">移除</a>'+
			  '</td>'+
			  '</tr>'+
			  '<tr>'+
			  '<th>简介</th>'+
			  '<td class="y-bg">'+
			  '<textarea name="info[content]['+n+'][description]" id="content_'+n+'_description" cols="100" rows="6"></textarea>'+
			  '</td>'+
			  '</tr>'+
			  '</table>'+
		      '</fieldset>';	
	$("#items").append(str);
});
</script>
</body>
</html>