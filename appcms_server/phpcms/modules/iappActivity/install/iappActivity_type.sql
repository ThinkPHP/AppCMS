DROP TABLE IF EXISTS `phpcms_iappActivity_type`;
CREATE TABLE IF NOT EXISTS `phpcms_iappActivity_type` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `siteid` smallint(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `listorder` smallint(5) DEFAULT '0',
  `setting` mediumtext,
  PRIMARY KEY (`id`)
) TYPE=MyISAM;
