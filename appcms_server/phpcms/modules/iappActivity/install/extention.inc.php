<?php
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');

$menus = $menu_db->select(array('name'=>'iappPlug', 'm'=>'iapp', 'c'=>'manages', 'a'=>'init'));

$parentid = $menu_db->insert(array('name'=>'iappActivity', 'parentid'=>$menus[0][id], 'm'=>'iappActivity', 'c'=>'manages', 'a'=>'init', 'data'=>'', 'listorder'=>0, 'display'=>'1'), true);

$menu_db->insert(array('name'=>'iappActivity_add', 'parentid'=>$parentid, 'm'=>'iappActivity', 'c'=>'manages', 'a'=>'add', 'data'=>'', 'listorder'=>2, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappActivity_edit', 'parentid'=>$parentid, 'm'=>'iappActivity', 'c'=>'manages', 'a'=>'edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappActivity_delete', 'parentid'=>$parentid, 'm'=>'iappActivity', 'c'=>'manages', 'a'=>'delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));


$menu_db->insert(array('name'=>'iappActivity_type_list', 'parentid'=>$parentid, 'm'=>'iappActivity', 'c'=>'manages', 'a'=>'type_list', 'data'=>'', 'listorder'=>6, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappActivity_type_add', 'parentid'=>$parentid, 'm'=>'iappActivity', 'c'=>'manages', 'a'=>'type_add', 'data'=>'', 'listorder'=>5, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappActivity_type_edit', 'parentid'=>$parentid, 'm'=>'iappActivity', 'c'=>'manages', 'a'=>'type_edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappActivity_type_delete', 'parentid'=>$parentid, 'm'=>'iappActivity', 'c'=>'manages', 'a'=>'type_delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappActivity_type_listorder', 'parentid'=>$parentid, 'm'=>'iappActivity', 'c'=>'manages', 'a'=>'type_listorder', 'data'=>'', 'listorder'=>0, 'display'=>'0'));






$language = array('iappActivity'=>'活动',
				  'iappActivity_add'=>'添加活动',
				  'iappActivity_edit'=>'修改活动',
				  'iappActivity_delete'=>'删除活动',

				 
				  'iappActivity_type_add'=>'添加分类',
				  'iappActivity_type_edit'=>'修改分类',
				  'iappActivity_type_list'=>'分类列表',
				  'iappActivity_type_delete'=>'删除分类',
				  'iappActivity_type_listorder'=>'分类排序'  
                  );

?>