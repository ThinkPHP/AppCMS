DROP TABLE IF EXISTS `phpcms_iappActivity`;
CREATE TABLE IF NOT EXISTS `phpcms_iappActivity` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `typeid` smallint(5) NOT NULL COMMENT '栏目',
  `userid` int(10) unsigned default '0' COMMENT '用户ID',
  `username` varchar(20) default NULL COMMENT '用户名',
  
  `title` varchar(50) NOT NULL COMMENT '标题',
  `ltitle` varchar(80) NOT NULL COMMENT '长标题',
  `image` varchar(200) NOT NULL COMMENT '图片',
  `start_time` int(10) unsigned NOT NULL DEFAULT '0',
  `end_time` int(10) unsigned NOT NULL DEFAULT '0',
  `activity_time` int(10) unsigned NOT NULL DEFAULT '0',
  
  `num` smallint(5) NOT NULL COMMENT '活动名额',
  `bmnum` smallint(5) NOT NULL COMMENT '已报名',

  `content` mediumtext NOT NULL COMMENT '活动简介',
  `process` mediumtext NOT NULL COMMENT '活动流程',
  `reward` mediumtext NOT NULL COMMENT '活动奖励',
  `precautions` mediumtext NOT NULL COMMENT '注意事项',
  
  `collection` smallint(5) DEFAULT '0' COMMENT '收藏量',
  `htis` smallint(5) DEFAULT '0' COMMENT '查看量',
  `praise` smallint(5) DEFAULT '0' COMMENT '赞',
  `listorder` smallint(5) DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) TYPE=MyISAM;
