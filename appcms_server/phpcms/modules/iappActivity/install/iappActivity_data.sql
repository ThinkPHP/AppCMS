DROP TABLE IF EXISTS `phpcms_iappActivity_data`;
CREATE TABLE IF NOT EXISTS `phpcms_iappActivity_data` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `activityid` smallint(5) NOT NULL COMMENT '活动id',
  `userid` int(10) unsigned default '0' COMMENT '用户ID',
  `username` varchar(20) default NULL COMMENT '用户名',
  `name` varchar(20) default NULL COMMENT '姓名',
  `sex` int(10) unsigned default '0' COMMENT '性别',
  `mobile` varchar(50) NOT NULL COMMENT '手机',
  `timeApply` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '报名时间',
  `msg` text NOT NULL COMMENT '短信内容', 
  PRIMARY KEY (`id`)
) TYPE=MyISAM;
