<?php 
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_sys_class('format', '', 0);
class index {
	function __construct() {		
		$this->db = pc_base::load_model('iappActivity_model');
		$this->db_iappActivity_data = pc_base::load_model('iappActivity_data_model');
		$this->siteid = get_siteid();
		$this->config = getcache('iapp', 'commons');
		
	}
	
	public function lists() {
		$page = max(intval($_GET['page']), 1);
		$catid = intval($_GET['catid']) ? intval(trim($_GET['catid'])) : null;
		$order = trim($_GET['order']) ? trim($_GET['order']) : null;
		
		$time =time();
		$_sql = " start_time < $time AND $time < end_time ";
		if($catid){
			$_sql = $_sql." AND typeid= $catid ";
		}
		
		$_order = 'id DESC';
		if($order =='hot'){
			$_order = 'htis DESC';
		}
		
		$data = $this->db->listinfo($_sql, $_order, $page,5);

		$list = array();
		foreach ($data as $k=>$v) {
			$list[$k][id] = $v[id];
			$list[$k][typeid] = $v[typeid];
			$list[$k][userid] = $v[userid];
			$list[$k][username] = $v[username];
			$list[$k][title] = $v[title];
			$list[$k][ltitle] = $v[ltitle];
			$list[$k][image] = thumb($v[image],600,226);
			$list[$k][start_time] = $v[start_time];
			$list[$k][end_time] = $v[end_time];
			$list[$k][activity_time] = $v[activity_time];	
			$list[$k][num] = $v[num];	
			$list[$k][bmnum] = $v[bmnum];	
			$list[$k][collection] = $v[collection];	
			$list[$k][htis] = $v[htis];
			$list[$k][praise] = $v[praise];
		}
		$return_id = $this->db->update(array('htis'=>'+=1'),'1=1');
		echo json_encode(array("stime"=>time(),"list"=>$list));
	}
	
	public function show() {
		$re = array();
		$id = intval($_GET['id']) ? intval($_GET['id']) : $re[status]="ERR";
		if(!$re[status]){
			$activityinfo = $this->db->get_one(array('id'=>$id));
			$activityinfo[image] = thumb($activityinfo[image],600,226);
			$activityinfo[content] = string2array($activityinfo[content]);
			$return_id = $this->db->update(array('htis'=>'+=1'),array('id'=>$id));
			if($activityinfo){
				$re[status]="OK";
				$re[data]=$activityinfo;
				$re[msg]="数据加载成功~";
			}else{
				$re[status]="NODATA";
				$re[msg]="活动不存在~";
			}
		}
		
		echo json_encode($re);
	}
	
	function baoming() {
		$re = array();
		
		$id = isset($_GET[id]) ? trim($_GET[id]) : null;
		$userid = isset($_GET['userid']) && trim($_GET['userid']) ? trim($_GET['userid']) : null;
		$acctoken = isset($_GET['acctoken']) && trim($_GET['acctoken']) ? trim($_GET['acctoken']) : null;
		
		if($id && $userid && $acctoken){
			$userdb = pc_base::load_model('member_model');
			$userdata = $userdb->get_one(array('userid'=>$userid,'password'=>$acctoken));
			if($userdata){
				$activitydata = $this->db->get_one(array('id'=>$id));
				if($activitydata[bmnum]<$activitydata[num]){
					$activity_data_count = $this->db_iappActivity_data->count(array('activityid'=>$id,'userid'=>$userid));
					if($activity_data_count<1){
						$info = array();
						$info[activityid] = $id;
						$info[userid] = $userdata[userid];
						$info[username] = $userdata[username];
						$info[mobile] = $userdata[mobile];
						$info[name] = '';
						$info[sex] = '';
						
						$info[timeApply] = time();
						$info[msg] = "亲，你报名参加了：[$activitydata[title]]$activitydata[ltitle]活动，稍后工作人员会与你联系，请保持手机畅通。【最黔端】";
						
						if($this->config[setting][isactivity] && $mobile){
							pc_base::load_app_class('smsapi', 'sms', 0);
							$sms_setting = getcache('sms','sms');
							$sms_setting = $sms_setting[$this->siteid];
							$smsapi = new smsapi($sms_setting['userid'], $sms_setting['productid'], $sms_setting['sms_key']);
							$mobile = array($info[mobile]);
							$content = $info[msg];
							$sent_time = date('Y-m-d H:i:s',SYS_TIME);
							$id_code = random(6);
							$smsstatus = $smsapi->send_sms($mobile, $content, $sent_time,CHARSET,$id_code,16);
						}
						
						$return_id = $this->db_iappActivity_data->insert($info,'1');
						if($return_id){
							$re[status] = "OK";
							$re[bmnum] = $activitydata[bmnum]+1;
							$re[msg] = "报名成功，稍后工作人员会与你取得联系~";
						}else{
							$re[status] = "ERR";
							$re[msg] = "报名失败，未知错误，请重试~";
						}
						$re_id = $this->db->update(array('bmnum'=>'+=1'),array('id'=>$id));
					}else{
						$re[status] = "ERR";
						$re[msg] = "你已经报名过，不需要重复报名~";
					}
				}else{
					$re[status] = "NOT";
					$re[msg] = "报名失败，活动人数已经满~";
				}
					
			}else{
				$re[status] = "NOTLOGIN";
				$re[msg] = "报名失败，请先登录~";
			}
		
		}else{
			$re[status] = "NODATA";
			$re[msg] = "报名失败，参数错误~";
		}
		echo json_encode($re);
	}
	
}
?>