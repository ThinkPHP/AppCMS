<?php 
defined('IN_PHPCMS') or exit('No permission resources.'); 
pc_base::load_app_class('admin','admin',0);
pc_base::load_sys_class('form', '', 0);
pc_base::load_sys_class('format', '', 0);
class manages extends admin {
	private $db;
	function __construct() {
		parent::__construct();
		$this->sites = pc_base::load_app_class('sites','admin');
		$this->db = pc_base::load_model('iappBus_model');
		$this->bus_item = pc_base::load_model('iappBus_item_model');
		$this->siteid = $this->get_siteid();
	}
	
	function init() {
		$page = max(intval($_GET['page']), 1);
		$data = $this->db->listinfo(array('siteid'=>$this->get_siteid()), '`listorder` DESC', $page);
		
		include $this->admin_tpl('bus_list');
	}
		
	
	function add() {
		if($_POST['dosubmit']) {
	
			$siteid = $this->siteid;
			$title = trim($_POST['title']);
			$tel = trim($_POST['tel']);
			$adds = trim($_POST['adds']);
			$maps = trim($_POST['maps']);
			$photo = trim($_POST['photo']);
			$description = trim($_POST['description']);
			$types = explode("|", $_POST['setting'][type]);
			$setting[type] =$types;
			
			$return_id = $this->db->insert(array('title'=>$title,'tel'=>$tel,'adds'=>$adds,'maps'=>$maps,'photo'=>$photo,'description'=>$description,'setting'=>array2string($setting),'siteid'=>$siteid,'listorder'=>0,'addtime'=>time()),'1');
			
			showmessage(L('add_success'), '?m=iappBus&c=manages&a=init');
			
		} else {
			include $this->admin_tpl('bus_add');			
		}		
	}
	
	function edit() {
		if($_POST['dosubmit']) {
			$busid = intval($_POST['busid']) ? intval($_POST['busid']) : showmessage(L('parameter_error'),HTTP_REFERER);
			$siteid = $this->siteid;
			$title = trim($_POST['title']);
			$tel = trim($_POST['tel']);
			$adds = trim($_POST['adds']);
			$maps = trim($_POST['maps']);
			$photo = trim($_POST['photo']);
			$description = trim($_POST['description']);
			$types = explode("|", $_POST['setting'][type]);
			$setting[type] =$types;

			
			
			$return_id = $this->db->update(array('title'=>$title,'tel'=>$tel,'adds'=>$adds,'maps'=>$maps,'photo'=>$photo,'description'=>$description,'setting'=>array2string($setting),'addtime'=>time()),array('busid'=>$busid));


			showmessage(L('operation_success'), '?m=iappBus&c=manages&a=edit&busid='.$busid.'&menuid='.$_GET['menuid']);
		} else {
			$busid = intval($_GET['busid']) ? intval($_GET['busid']) : showmessage(L('parameter_error'),HTTP_REFERER);
					
			$info = $this->db->get_one(array('busid'=>$busid));
			if($info) {
				$info[setting] = string2array($info[setting]);
				$typestr = '';
				foreach ($info[setting][type] as $k=>$v) {
					if(trim($v)=='') continue;
					$typestr .= $v."|";
					
				}
				
				$info[setting][type] =substr($typestr,0,-1);
			}
			
			include $this->admin_tpl('bus_edit');			
		}
	}
	
	/**
	 * 删除
	 */
	public function delete() {
		$siteid = $this->siteid;
		
		if (isset($_GET['busid']) && !empty($_GET['busid'])) {
			$busid = intval($_GET['busid']);
			$this->db->delete(array('busid'=>$busid, 'siteid'=>$siteid));
			showmessage(L('operation_success'), HTTP_REFERER);
		} elseif (isset($_POST['ids']) && !empty($_POST['ids'])) {
			
			if (is_array($_POST['ids'])) {
				foreach ($_POST['ids'] as $fid) {
					$this->db->delete(array('busid'=>$fid, 'siteid'=>$siteid));
				}
			}
			showmessage(L('operation_success'), HTTP_REFERER);
		} else {
			showmessage(L('illegal_operation'), HTTP_REFERER);
		}
	}
	
	
	/**
	 * 排序
	 */
	public function listorder() {
	     
		if (isset($_POST['listorders']) && is_array($_POST['listorders'])) {
			$listorder = $_POST['listorders'];
			
			foreach ($listorder as $k => $v) {
				
				$this->db->update(array('listorder'=>$v), array('busid'=>$k));
			}
		}
		showmessage(L('operation_success'), HTTP_REFERER);
	}
	
	
	
	function busitem() {
	
		$busid = intval($_GET['busid']) ? intval($_GET['busid']) : showmessage(L('parameter_error'),HTTP_REFERER);
		$businfo = $this->db->get_one(array('busid'=>$busid));
		if($businfo) {
			$businfo[setting] = string2array($businfo[setting]);
		}
		$iteminfo = $this->bus_item->select(array('busid'=>$busid));
		if($iteminfo) {
			$iteminfo = $iteminfo[0];
			$iteminfo[items] = string2array($iteminfo[items]);
			foreach ($iteminfo[items] as $ka=>$va) {
				if(trim($va)=='') continue;
				$arra = "";
				foreach ($va as $kb=>$vb){
					$brra = "";
					foreach ($vb as $kc=>$vc){
						$brra .= $vc."|";
					}
					$brra =substr($brra,0,-1);
					$arra .= $brra."\n";
				}
				$iteminfo[items][$ka] = $arra;
			}	
		}
		
		if($_POST['dosubmit']) {
			$siteid = $this->siteid;
			$items = array();
			$itemsarr = $_POST['items'];
			foreach ($itemsarr as $ka=>$va) {
				$brr = explode("\n", trim($va));
				foreach ($brr as $kb=>$vb) {
					$items[$ka][$kb] = explode("|", trim($vb));
				}
			}
			if($iteminfo) {
				$return_id = $this->bus_item->update(array('busid'=>$busid,'items'=>array2string($items),'setting'=>'','listorder'=>0,'addtime'=>time()),array('id'=>$iteminfo[id]));
			}else{
				$return_id = $this->bus_item->insert(array('busid'=>$busid,'items'=>array2string($items),'setting'=>'','listorder'=>0,'addtime'=>time()),'1');
			}
			showmessage(L('operation_success'), HTTP_REFERER);
			
		}
		include $this->admin_tpl('bus_item');			
	}

}
?>