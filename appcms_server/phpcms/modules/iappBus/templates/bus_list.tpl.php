<?php 
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');
?>

<div class="pad-lr-10">
<form name="myform" id="myform" action="" method="post">
<div class="table-list">
    <table width="100%" cellspacing="0">
        <thead>
            <tr>
            <th width="35" align="center"><input type="checkbox" value="" id="check_box" onclick="selectall('ids[]');"></th>
			<th width="50" align="center"><?php echo L('listorder');?></th>
			<th width="50" align="center">ID</th>
			<th align="center"><?php echo L('标题')?></th>
			<th width="140" align="center"><?php echo L('车站电话')?></th>
			<th width="140" align="center"><?php echo L('添加时间')?></th>
			<th width="220" align="center"><?php echo L('operations_manage')?></th>
            </tr>
        </thead>
    <tbody>
 <?php 
if(is_array($data)){
	foreach($data as $r){
?>   
	<tr>
	<td align="center"><input class="inputcheckbox " name="ids[]" value="<?php echo $r['busid'];?>" type="checkbox"></td>
    <td align='center'><input name='listorders[<?php echo $r['busid'];?>]' type='text' size='3' value='<?php echo $r['listorder'];?>' class='input-text-c'></td>
	<td align='center' ><?php echo $r['busid'];?></td>
	<td><a href="<?php echo APP_PATH ?>index.php?m=iappBus&c=index&a=show&busid=<?php echo $r['busid']?>" target="_blank"><?php echo $r['title']?></a></td>
	<td align="center"><?php echo $r['tel']?></td>
	<td align="center"><?php echo date('Y-m-d H:i:s', $r['addtime'])?></td>
	<td align="center">
	<a href="?m=iappBus&c=manages&a=busitem&busid=<?php echo $r['busid']?>&menuid=<?php echo $_GET['menuid']?>"><?php echo L('时刻表管理')?></a> | 
	<a href="?m=iappBus&c=manages&a=edit&busid=<?php echo $r['busid']?>&menuid=<?php echo $_GET['menuid']?>"><?php echo L('修改')?></a> | <a href="?m=iappBus&c=manages&a=delete&busid=<?php echo $r['busid']?>" onClick="return confirm('<?php echo L('确认要删除『'.$r[title].'』吗？');?>')"><?php echo L('删除')?></a>
	</td>
	</tr>
<?php 
	}
}
?>
</tbody>
    </table>
  
    <div class="btn"><label for="check_box"><?php echo L('selected_all')?>/<?php echo L('cancel')?></label>
		<input type="hidden" value="YVt4Dk" name="pc_hash">
    	<input type="button" class="button" value="排序" onclick="myform.action='?m=iappBus&c=manages&a=listorder&dosubmit=1';myform.submit();"/>
		
		<input type="button" class="button" value="<?php echo L('delete');?>" onclick="myform.action='?m=iappBus&c=manages&a=delete&dosubmit=1';return confirm_delete()"/>
		
		</div>  

</div>
 <div id="pages"><?php echo $this->db->pages;?></div>
</form>
</div>
<script type="text/javascript">
window.top.$("#display_center_id").css("display","none");
function confirm_delete(){
	if(confirm('<?php echo L('确认要删除『选中』吗？');?>')) $('#myform').submit();
}
</script>
</body>
</html>