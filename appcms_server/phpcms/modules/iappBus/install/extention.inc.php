<?php
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');

$menus = $menu_db->select(array('name'=>'iappPlug', 'm'=>'iapp', 'c'=>'manages', 'a'=>'init'));

$parentid = $menu_db->insert(array('name'=>'iappBus', 'parentid'=>$menus[0][id], 'm'=>'iappBus', 'c'=>'manages', 'a'=>'init', 'data'=>'', 'listorder'=>0, 'display'=>'1'), true);


$menu_db->insert(array('name'=>'iappBus_add', 'parentid'=>$parentid, 'm'=>'iappBus', 'c'=>'manages', 'a'=>'add', 'data'=>'', 'listorder'=>0, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappBus_edit', 'parentid'=>$parentid, 'm'=>'iappBus', 'c'=>'manages', 'a'=>'edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappBus_delete', 'parentid'=>$parentid, 'm'=>'iappBus', 'c'=>'manages', 'a'=>'delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));

$menu_db->insert(array('name'=>'iappBus_item', 'parentid'=>$parentid, 'm'=>'iappBus', 'c'=>'manages', 'a'=>'busitem', 'data'=>'', 'listorder'=>1, 'display'=>'1'));


$language = array('iappBus'=>'汽车时刻',
				  'iappBus_add'=>'添加汽车站',
				  'iappBus_edit'=>'修改汽车站',
				  'iappBus_delete'=>'删除汽车站',
				  'iappBus_item'=>'时刻表管理'			  
                  );

?>