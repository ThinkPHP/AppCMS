DROP TABLE IF EXISTS `phpcms_iappBus_item`;
CREATE TABLE IF NOT EXISTS `phpcms_iappBus_item` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `busid` smallint(5) NOT NULL,
  `items` mediumtext NOT NULL,
  `setting` mediumtext,
  `listorder` smallint(5) DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) TYPE=MyISAM;
