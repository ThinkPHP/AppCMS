DROP TABLE IF EXISTS `phpcms_iappBus`;
CREATE TABLE IF NOT EXISTS `phpcms_iappBus` (
  `busid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `tel` varchar(50),
  `adds` varchar(50),
  `maps` varchar(50),
  `photo` varchar(100),
  `description` varchar(2000),
  `setting` mediumtext,
  `siteid` smallint(5) NOT NULL,
  `listorder` smallint(5) DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`busid`)
) TYPE=MyISAM;