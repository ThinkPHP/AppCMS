<?php 
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_sys_class('format', '', 0);
class app {
	function __construct() {		
		$this->db = pc_base::load_model('iappBus_model');
		$this->bus_item = pc_base::load_model('iappBus_item_model');
	}
	
	public function init() {
		echo "not init";
	}
	
	public function lists() {
	
		$data = $this->db->select(array('siteid'=>get_siteid()), 'busid,title,tel,maps,photo,description','','`listorder` DESC');
		echo json_encode($data);
	}
	public function show() {
		$busid = intval($_GET['busid']) ? intval($_GET['busid']) : showmessage(L('parameter_error'),HTTP_REFERER);
		
		$info = $this->db->get_one(array('busid'=>$busid));
		$re = array();
		$class = array();
		if($info){
			
			$re[id] = $info[busid];
			$re[station_name] = $info[title];
			$re[station_add] = $info[adds];
			$re[station_tel] = $info[tel];
			$re[x] = 0;
			$re[y] = 0;
			
			
			$setting = string2array($info[setting]);
			$iteminfo = $this->bus_item->select(array('busid'=>$busid));
			$items = string2array($iteminfo[0][items]);
			
			foreach ($items as $k=>$v) {
				$class[$k][id] = $v[id];
				$class[$k][title] = $setting[type][$k];
				$class[$k]['list'] = $v;
			}
			
		}else
		{
			$info = array();
		}
		echo json_encode(array("state"=>"true","re"=>$re,"class"=>$class));
	}
}
?>