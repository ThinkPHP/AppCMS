<?php 
defined('IN_PHPCMS') or exit('No permission resources.'); 
pc_base::load_app_class('admin','admin',0);
pc_base::load_sys_class('form', '', 0);
pc_base::load_sys_class('format', '', 0);
class manages extends admin {
	private $db,$db_iappTalk_type,$db_iappTalk_reply;
	function __construct() {
		parent::__construct();
		$this->sites = pc_base::load_app_class('sites','admin');
		$this->db = pc_base::load_model('iappTalk_model');
		$this->db_iappTalk_type = pc_base::load_model('iappTalk_type_model');
		$this->db_iappTalk_reply = pc_base::load_model('iappTalk_reply_model');
		$this->siteid = $this->get_siteid();
	}
	
	function init() {
		$page = max(intval($_GET['page']), 1);
		$data = $this->db->listinfo(array('siteid'=>$this->get_siteid()), '`id` DESC', $page);
		include $this->admin_tpl('talk_list');
	}
		
	
	function add() {
		$types = $this->db_iappTalk_type->select(array('siteid'=>$this->get_siteid()),'*','','`listorder` DESC');
		if($_POST['dosubmit']) {
			$info = $_POST['info'];
			$info[content] = trim($_POST['info'][content]) ? trim($_POST['info'][content]) : showmessage(L('说说不能为空'),HTTP_REFERER);
			
			$info[siteid] = $this->siteid;
			
			$info[userid] = 1;
			$info[username] = 'zuiqianduan';
			
			$photos = array();
			$_photos = array();
			$_photoshd = array();
			foreach ($info[photos] as $k => $v) {
				$v = trim($v);
				if($v!=""){
					$photos[] = $v;
				}
			}
			if(count($photos)==1){
				$_photos[0] = thumb($photos[0],474,320);
				$_photoshd[0] = $photos[0];
			}else if(count($photos)>1){
				foreach ($photos as $k => $v) {
					$_photos[] = thumb($v,152,144);
					$_photoshd[] = $v;
				}
			}
			$info[photos] = array2string($_photos);
			$info[photoshd] = array2string($_photoshd);
			
			$info[addtime] = time();
			$info[edittime] = time();
			//print_r($info);
			//exit;
			
			$return_id = $this->db->insert($info,'1');
			showmessage(L('add_success'), '?m=iappTalk&c=manages&a=init&menuid='.$_GET['menuid']);
		} else {
			include $this->admin_tpl('talk_add');
		}		
	}
	
	function edit() {
		$types = $this->db_iappTalk_type->select(array('siteid'=>$this->get_siteid()),'*','','`listorder` DESC');
		if($_POST['dosubmit']) {
			$id = intval($_POST['id']) ? intval($_POST['id']) : showmessage(L('parameter_error'),HTTP_REFERER);
			$info = $_POST['info'];
			$info[content] = trim($_POST['info'][content]) ? trim($_POST['info'][content]) : showmessage(L('主题不能为空'),HTTP_REFERER);
			$photos = array();
			$_photos = array();
			$_photoshd = array();
			foreach ($info[photos] as $k => $v) {
				$v = trim($v);
				if($v!=""){
					$photos[] = $v;
				}
			}
			if(count($photos)==1){
				$_photos[0] = thumb($photos[0],474,320);
				$_photoshd[0] = $photos[0];
			}else if(count($photos)>1){
				foreach ($photos as $k => $v) {
					$_photos[] = thumb($v,152,144);
					$_photoshd[] = $v;
				}
			}
			$info[photos] = array2string($_photos);
			$info[photoshd] = array2string($_photoshd);
			
			$info[edittime] = time();
			$return_id = $this->db->update($info,array('id'=>$id));
			showmessage(L('operation_success'), '?m=iappTalk&c=manages&a=edit&id='.$id.'&menuid='.$_GET['menuid']);
		} else {
			$id = intval($_GET['id']) ? intval($_GET['id']) : showmessage(L('parameter_error'),HTTP_REFERER);		
			$info = $this->db->get_one(array('id'=>$id));
			//print_r($info);
			$info[photos] = string2array($info[photos]);
			include $this->admin_tpl('talk_edit');			
		}
	}
	
	/**
	 * 删除
	 */
	public function delete() {
		$siteid = $this->siteid;
		
		if (isset($_GET['id']) && !empty($_GET['id'])) {
			$id = intval($_GET['id']);
			$this->db->delete(array('id'=>$id));
			showmessage(L('operation_success'), HTTP_REFERER);
		} elseif (isset($_POST['ids']) && !empty($_POST['ids'])) {
			if (is_array($_POST['ids'])) {
				foreach ($_POST['ids'] as $fid) {
					$this->db->delete(array('id'=>$fid));
				}
			}
			showmessage(L('operation_success'), HTTP_REFERER);
		} else {
			showmessage(L('illegal_operation'), HTTP_REFERER);
		}
	}
	
	/**
	 * 排序
	 */
	public function listorder() {
		if (isset($_POST['listorders']) && is_array($_POST['listorders'])) {
			$listorder = $_POST['listorders'];
			foreach ($listorder as $k => $v) {
				$this->db->update(array('listorder'=>$v), array('id'=>$k));
			}
		}
		showmessage(L('operation_success'), HTTP_REFERER);
	}
	
	function reply_list() {
		$page = max(intval($_GET['page']), 1);
		$talkid = intval($_GET['talkid']);
		$w = array();
		if($talkid){
			$w = array('talkid'=>$talkid);
		}
		$data = $this->db_iappTalk_reply->listinfo($w, '`id` DESC', $page);
		include $this->admin_tpl('reply_list');
	}
	function reply_add() {
		$talkid = intval($_GET['talkid']) ? intval($_GET['talkid']) : showmessage(L('parameter_error'),HTTP_REFERER);
		$talkinfo = $this->db->get_one(array('id'=>$talkid));
		
		
		if($_POST['dosubmit']) {
			$info = $_POST['info'];
			$info[content] = trim($_POST['info'][content]) ? trim($_POST['info'][content]) : showmessage(L('主题不能为空'),HTTP_REFERER);
			
			$userid = $_SESSION['userid'];
			$admin_username = param::get_cookie('admin_username');
			
			$info[talkid] = $talkid;
			$info[userid] = 1;
			$info[username] = 'zuiqianduan';
			$info[addtime] = time();
			$info[edittime] = time();
			$return_id = $this->db_iappTalk_reply->insert($info,'1');

			//修改回复时间
			$return_id = $this->db->update(array('replytime'=>time(),'renum'=>'+=1'),array('id'=>$talkid));
			
			showmessage(L('add_success'), '?m=iappTalk&c=manages&a=reply_list&menuid='.$_GET['menuid']);
		} else {
			include $this->admin_tpl('reply_add');
		}		
	}
	
	function reply_edit() {
		$id = intval($_GET['id']) ? intval($_GET['id']) : showmessage(L('parameter_error'),HTTP_REFERER);
		$talkid = intval($_GET['talkid']) ? intval($_GET['talkid']) : showmessage(L('parameter_error'),HTTP_REFERER);
		$talkinfo = $this->db->get_one(array('id'=>$talkid));
		if($_POST['dosubmit']) {
			$info = $_POST['info'];
			$info[content] = trim($_POST['info'][content]) ? trim($_POST['info'][content]) : showmessage(L('主题不能为空'),HTTP_REFERER);
			$info[talkid] = $talkid;
			$info[edittime] = time();
			$return_id = $this->db_iappTalk_reply->update($info,array('id'=>$id));
			showmessage(L('operation_success'), '?m=iappTalk&c=manages&a=reply_edit&talkid='.$talkid.'&id='.$id.'&menuid='.$_GET['menuid']);
		} else {	
			$info = $this->db_iappTalk_reply->get_one(array('id'=>$id));
			include $this->admin_tpl('reply_edit');			
		}
	}
	
	/**
	 * 删除
	 */
	public function reply_delete() {
		$siteid = $this->siteid;
		if (isset($_GET['id']) && !empty($_GET['id'])) {
			$id = intval($_GET['id']);
			$this->db_iappTalk_reply->delete(array('id'=>$id));
			$return = $this->db->update(array('renum'=>'-=1'),array('id'=>$id));
			showmessage(L('operation_success'), HTTP_REFERER);
		} elseif (isset($_POST['ids']) && !empty($_POST['ids'])) {
			if (is_array($_POST['ids'])) {
				foreach ($_POST['ids'] as $fid) {
					$this->db_iappTalk_reply->delete(array('id'=>$fid));
					$return = $this->db->update(array('renum'=>'-=1'),array('id'=>$fid));
				}
			}
			showmessage(L('operation_success'), HTTP_REFERER);
		} else {
			showmessage(L('illegal_operation'), HTTP_REFERER);
		}
	}
	
	function type_list() {
		$page = max(intval($_GET['page']), 1);
		$data = $this->db_iappTalk_type->listinfo(array('siteid'=>$this->get_siteid()), '`listorder` DESC', $page);
		include $this->admin_tpl('type_list');
	}
	function type_add() {
		if($_POST['dosubmit']) {
			$info = $_POST['info'];
			$info[title] = trim($_POST['info'][title]) ? trim($_POST['info'][title]) : showmessage(L('主题不能为空'),HTTP_REFERER);
			$info[siteid] = $this->siteid;
			$info[listorder] = 0;
			$return_id = $this->db_iappTalk_type->insert($info,'1');
			showmessage(L('add_success'), '?m=iappTalk&c=manages&a=type_list&menuid='.$_GET['menuid']);
		} else {
			include $this->admin_tpl('type_add');
		}		
	}
	
	function type_edit() {
		if($_POST['dosubmit']) {
			$id = intval($_POST['id']) ? intval($_POST['id']) : showmessage(L('parameter_error'),HTTP_REFERER);
			$info = $_POST['info'];
			$info[title] = trim($_POST['info'][title]) ? trim($_POST['info'][title]) : showmessage(L('主题不能为空'),HTTP_REFERER);
			$return_id = $this->db_iappTalk_type->update($info,array('id'=>$id));
			showmessage(L('operation_success'), '?m=iappTalk&c=manages&a=type_edit&id='.$id.'&menuid='.$_GET['menuid']);
		} else {
			$id = intval($_GET['id']) ? intval($_GET['id']) : showmessage(L('parameter_error'),HTTP_REFERER);		
			$info = $this->db_iappTalk_type->get_one(array('id'=>$id));
			include $this->admin_tpl('type_edit');			
		}
	}
	
	/**
	 * 删除
	 */
	public function type_delete() {
		$siteid = $this->siteid;
		
		if (isset($_GET['id']) && !empty($_GET['id'])) {
			$id = intval($_GET['id']);
			$this->db_iappTalk_type->delete(array('id'=>$id, 'siteid'=>$siteid));
			showmessage(L('operation_success'), HTTP_REFERER);
		} elseif (isset($_POST['ids']) && !empty($_POST['ids'])) {
			if (is_array($_POST['ids'])) {
				foreach ($_POST['ids'] as $fid) {
					$this->db_iappTalk_type->delete(array('id'=>$fid, 'siteid'=>$siteid));
				}
			}
			showmessage(L('operation_success'), HTTP_REFERER);
		} else {
			showmessage(L('illegal_operation'), HTTP_REFERER);
		}
	}
	
	
	/**
	 * 排序
	 */
	public function type_listorder() {
		if (isset($_POST['listorders']) && is_array($_POST['listorders'])) {
			$listorder = $_POST['listorders'];
			foreach ($listorder as $k => $v) {
				$this->db_iappTalk_type->update(array('listorder'=>$v), array('id'=>$k));
			}
		}
		showmessage(L('operation_success'), HTTP_REFERER);
	}
	
	
	function setting() {
		$data = array();
		$m_db = pc_base::load_model('module_model');
		$data = $m_db->select(array('module'=>'iappTalk'));
		$data = string2array($data[0]['setting']);
		
 		if(isset($_POST['dosubmit'])) {
			$setting = $_POST['data'];
  			setcache('iappTalk', $setting, 'commons');  
 			$setting = array2string($setting);
			$m_db->update(array('setting'=>$setting), array('module'=>'iappTalk'));
			showmessage('修改成功',HTTP_REFERER);
		} else {
			include $this->admin_tpl('setting');
		}
	}
}
?>