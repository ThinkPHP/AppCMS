<?php
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');

$menus = $menu_db->select(array('name'=>'iappPlug', 'm'=>'iapp', 'c'=>'manages', 'a'=>'init'));

$parentid = $menu_db->insert(array('name'=>'iappTalk', 'parentid'=>$menus[0][id], 'm'=>'iappTalk', 'c'=>'manages', 'a'=>'init', 'data'=>'', 'listorder'=>0, 'display'=>'1'), true);

$menu_db->insert(array('name'=>'iappTalk_add', 'parentid'=>$parentid, 'm'=>'iappTalk', 'c'=>'manages', 'a'=>'add', 'data'=>'', 'listorder'=>2, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappTalk_edit', 'parentid'=>$parentid, 'm'=>'iappTalk', 'c'=>'manages', 'a'=>'edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappTalk_delete', 'parentid'=>$parentid, 'm'=>'iappTalk', 'c'=>'manages', 'a'=>'delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));

$menu_db->insert(array('name'=>'iappTalk_reply_add', 'parentid'=>$parentid, 'm'=>'iappTalk', 'c'=>'manages', 'a'=>'reply_add', 'data'=>'', 'listorder'=>3, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappTalk_reply_edit', 'parentid'=>$parentid, 'm'=>'iappTalk', 'c'=>'manages', 'a'=>'reply_edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappTalk_reply_delete', 'parentid'=>$parentid, 'm'=>'iappTalk', 'c'=>'manages', 'a'=>'reply_delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappTalk_reply_list', 'parentid'=>$parentid, 'm'=>'iappTalk', 'c'=>'manages', 'a'=>'reply_list', 'data'=>'', 'listorder'=>4, 'display'=>'1'));

$menu_db->insert(array('name'=>'iappTalk_type_list', 'parentid'=>$parentid, 'm'=>'iappTalk', 'c'=>'manages', 'a'=>'type_list', 'data'=>'', 'listorder'=>6, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappTalk_type_add', 'parentid'=>$parentid, 'm'=>'iappTalk', 'c'=>'manages', 'a'=>'type_add', 'data'=>'', 'listorder'=>5, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappTalk_type_edit', 'parentid'=>$parentid, 'm'=>'iappTalk', 'c'=>'manages', 'a'=>'type_edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappTalk_type_delete', 'parentid'=>$parentid, 'm'=>'iappTalk', 'c'=>'manages', 'a'=>'type_delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappTalk_type_listorder', 'parentid'=>$parentid, 'm'=>'iappTalk', 'c'=>'manages', 'a'=>'type_listorder', 'data'=>'', 'listorder'=>0, 'display'=>'0'));

$menu_db->insert(array('name'=>'iappTalk_setting', 'parentid'=>$parentid, 'm'=>'iappTalk', 'c'=>'manages', 'a'=>'setting', 'data'=>'', 'listorder'=>7, 'display'=>'1'));





$language = array('iappTalk'=>'说说',
				  'iappTalk_add'=>'发布说说',
				  'iappTalk_edit'=>'修改说说',
				  'iappTalk_delete'=>'删除说说',
				  
				  'iappTalk_reply_add'=>'回复',
				  'iappTalk_reply_edit'=>'修改回复',
				  'iappTalk_reply_list'=>'回复列表',
				  'iappTalk_reply_delete'=>'删除回复',
				 
				  'iappTalk_type_add'=>'添加分类',
				  'iappTalk_type_edit'=>'修改分类',
				  'iappTalk_type_list'=>'分类列表',
				  'iappTalk_type_delete'=>'删除分类',
				  'iappTalk_type_listorder'=>'分类排序',
				  'iappTalk_setting'=>'说说配置',
                  );

?>