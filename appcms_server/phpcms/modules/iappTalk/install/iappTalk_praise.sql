DROP TABLE IF EXISTS `phpcms_iappTalk_praise`;
CREATE TABLE IF NOT EXISTS `phpcms_iappTalk_praise` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `talkid` smallint(5) NOT NULL COMMENT '说说id',
  `userid` smallint(5) NOT NULL COMMENT '用户id',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `nick` varchar(50) NOT NULL COMMENT '昵称',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) TYPE=MyISAM;
