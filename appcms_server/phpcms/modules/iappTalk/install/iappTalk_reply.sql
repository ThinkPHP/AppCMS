DROP TABLE IF EXISTS `phpcms_iappTalk_reply`;
CREATE TABLE IF NOT EXISTS `phpcms_iappTalk_reply` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `talkid` smallint(5) NOT NULL COMMENT '说说id',
  `userid` smallint(5) NOT NULL COMMENT '用户id',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `nick` varchar(50) NOT NULL COMMENT '昵称',
  `content` mediumtext COMMENT '回复内容',
  `praise` smallint(5) DEFAULT '0' COMMENT '赞',
  `reply_userid` smallint(5) DEFAULT '0' COMMENT '回复谁',
  `reply_nick` varchar(50) NOT NULL COMMENT '被回复人昵称',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) TYPE=MyISAM;
