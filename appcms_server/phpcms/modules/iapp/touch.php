<?php 
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_func('global');
pc_base::load_sys_class('format', '', 0);
class touch {
	function __construct() {		
		$this->db = pc_base::load_model('content_model');
		$this->member_db = pc_base::load_model('member_model');
		$this->config = getcache('iapp', 'commons');
		$this->hitsdb = pc_base::load_model('hits_model');
		if(intval($_GET['siteid']) > 0){
			$this->siteid = intval($_GET['siteid']);
		}else{
			$this->siteid = $this->config[setting][siteid]? $this->config[setting][siteid]:1;
		}
	}
	
	public function init() {
		$re = '';
		$config = $this->config;
		$SEO = seo($this->siteid);
		include template('iapp', 'index','touch');
	}
	
	/*
	* 获取文章内容数据接口
	*/
	public function show() {
		$re = array();
		
		$catid = $_GET['catid']?intval($_GET['catid']):null;
		$id = $_GET['id']?intval($_GET['id']):null;
		if(!$catid || !$id){
			$re[status] = 'NODATA';
			$re[msg] = '没有数据~';
			exit;
		}
		
		$categoryall = getcache('category_content','commons');
		$siteid = $categoryall[$catid];
		$CATEGORYS = getcache('category_content_'.$siteid,'commons');
		
		$CAT = $CATEGORYS[$catid];
		$CAT['setting'] = string2array($CAT['setting']);
		
		$MODEL = getcache('model','commons');
		$modelid = $CAT['modelid'];
		
		$tableA = $MODEL[$modelid]['tablename'];
		$tableB = $this->db->db_tablepre.$tableA;
		
		$this->db->table_name = $tableB;
		
		$r1 = $this->db->get_one(array('id'=>$id));
		if(!$r1 || $r1['status'] != 99){
			$re[status] = 'NODATA';
			$re[msg] = '没有数据~';
			exit;
		}	
		$this->db->table_name = $tableB.'_data';
		
		$r2 = $this->db->get_one(array('id'=>$id));
		
		$rs = $r2 ? array_merge($r1,$r2) : $r1;
		
		require_once CACHE_MODEL_PATH.'content_output.class.php';
		$content_output = new content_output($modelid,$catid,$CATEGORYS);
		$rs[keywords] = $content_output->keyword("keywords",$rs[keywords]);
		$rs[copyfrom] = $content_output->copyfrom("copyfrom",$rs[copyfrom]);
		
		$hits = $this->hits($id,$modelid);

		switch ($tableA)
		{
			case "news"://新闻
				$news = array("id"=>$rs[id],
								"title"=>$rs[title],
								"ftitle"=>$rs[title],
								"catid"=>$rs[catid],
								"modelid"=>$tableA,
								"uptime"=> date('Y-m-d H:i:s', $rs[updatetime]),
								"content"=>clear_font_wh(wml_strip($rs[content])),
								"url"=>APP_PATH.$rs[url],
								"source"=>$rs[copyfrom],
								"color"=>$rs[style],
								"isimg"=>$rs[thumb]!=""? 1:0,
								"pl"=>0,
								"hits"=>$hits
								);

				echo json_encode(array("news"=>$news));
			    break;
			case "picture"://图片picture
				$pictureurls = array();
				$picturetitles = array();
				foreach(string2array($rs[pictureurls]) as $k=>$v){
					$pictureurls[$k] = $v[url];
					$picturetitles[$k] = $v[alt];
					
				}
				
				$news = array("id"=>$rs[id],
								"title"=>$rs[title],
								"ftitle"=>$rs[title],
								"cid"=>$rs[catid],
								"area_id"=>0,
								"uptime"=> $rs[inputtime],
								"state"=>0,
								"content"=>clear_font_wh(wml_strip($rs[content])),
								"thumb"=>thumb($rs[thumb],650,300),
								"pictureurls"=>$pictureurls,
								"picturetitles"=>$picturetitles,
								"flink"=>"",
								"url"=>$rs[url],
								"source"=>$rs[copyfrom],
								"color"=>$rs[style],
								"istop"=>0,
								"isimg"=>$rs[thumb]!=""? 1:0,
								"pl"=>0,
								"hits"=>$hits
								);
				

				echo json_encode(array("news"=>$news));
				break;
			case "video"://视频
			
				pc_base::load_app_class('api56', 'video56', 0);
				$video56 = pc_base::load_model('video56_model');
				$vinfo = $video56->get_one(array('videoid'=>intval($rs[video])));
				$video56info = Open::Video_Mobile(array('vid'=>$vinfo['vid']));
				
				$video = "http://vxml.56.com/html5/".$vinfo['vid']."/?src=m&res=vga";

				$data = $rs;
				
				$data[content]=clear_font_wh(wml_strip($rs[content]));
				$data[video]=$video;
				$data[videodata]=$video56info[info][rfiles];
				$data[hits]=$hits;
				$data[pl]=$pl;			
				$data[thumb]=thumb($rs[thumb],650,400);
				

				
				//$ContentHtml .='<p><video controls="controls" width="100%" height="400"  autoplay="autoplay" id="video" poster="'.$data[thumb].'" src="'.$data[video].'"></p>';
				
				//$ContentHtml .="<p>$data[content]</p>";
				
				//echo $ContentHtml;
				
				include template('iapp', 'show_video','touch');
				

				break;
			case "business"://商家
				$AREA = getcache(3360,'linkage');
				$rs[r_map] = explode('|',$rs[r_map]);
				$rs[r_photo] = string2array($rs[r_photo]);
				$rs[r_area_cn] = $AREA[data][intval($rs[r_area])][name];
				
				//"good_pl"=>"1",//好评
				//"poor_pl"=>"0",//差评
				//"total_pl"=>"1",//所有评论
				$life = array("id"=>$rs[id],
				              "uid"=>1,
							  "nick"=>$rs[username],
							  "name"=>$rs[title],
							  "mobile"=>$rs[r_mobile],
							  "area_id"=>$rs[r_area],
							  "area_cn"=>$rs[r_area_cn],
							  "cid"=>$rs[catid],
							  "xid"=>$rs[catid],
							  "xid_cn"=>$CATEGORYS[$rs[catid]][catname],
							  "img"=>$rs[thumb],
							  "operate"=>$rs[r_zhuying],
							  "add"=>$rs[r_adds],
							  "tel"=>$rs[r_tel],
							  "tel_two"=>$rs[r_tel],
							  "x"=>$rs[r_map][0],
							  "y"=>$rs[r_map][1],
							  "uptime"=>$rs[updatetime],
							  "addtime"=>$rs[inputtime],
							  "hot"=>$rs[r_hits],
							  "good_pl"=>"1",
							  "poor_pl"=>"0",
							  "total_pl"=>"1",
							  "state"=>"0",
							  "istop"=>"1",
							  "content"=>$rs[content],
							  "url"=>$rs[url]
							);
				//优惠券
				$coupons =array("id"=>"7",
				                "picurl"=>"201305/071135412153.jpg",	
								"title"=>"凭优惠劵购票半价后再减5元",
								"lid"=>"4086",
								"name"=>"瓯江影城",
								"startdate"=>"1367769600",
								"enddate"=>""
								);
				//图片
				$photo = array();
				foreach($rs[r_photo] as $r){
						$photo[b][] = $r[url];
						$photo[m][] = thumb($r[url],133,100);
					}
				echo json_encode(array("life"=>$life,"coupons"=>$coupons,"photo"=>$photo));
				break; 
			default:
			  echo "default";
		}
	}
	
	/**
	 * 获取点击数量
	 * @param $id
	 * @param $modelid
	 */
	public function hits($id,$modelid) {
		$hitsid = 'c-'.$modelid.'-'.intval($id);
		if($modelid && $id) {
			$r = $this->hitsdb->get_one(array('hitsid'=>$hitsid));
			if(!$r) return false;
			$views = $r['views'] + 1;
			$yesterdayviews = (date('Ymd', $r['updatetime']) == date('Ymd', strtotime('-1 day'))) ? $r['dayviews'] : $r['yesterdayviews'];
			$dayviews = (date('Ymd', $r['updatetime']) == date('Ymd', SYS_TIME)) ? ($r['dayviews'] + 1) : 1;
			$weekviews = (date('YW', $r['updatetime']) == date('YW', SYS_TIME)) ? ($r['weekviews'] + 1) : 1;
			$monthviews = (date('Ym', $r['updatetime']) == date('Ym', SYS_TIME)) ? ($r['monthviews'] + 1) : 1;
			$sql = array('views'=>$views,'yesterdayviews'=>$yesterdayviews,'dayviews'=>$dayviews,'weekviews'=>$weekviews,'monthviews'=>$monthviews,'updatetime'=>SYS_TIME);
			$this->hitsdb->update($sql, array('hitsid'=>$hitsid));
		}
		return $views;	
	}
	
	public function softdown() {
		$re = '';
		$config = getcache('upgrade', 'commons');
		$SEO = seo($this->siteid);
		include template('iapp', 'softdown');
	}
	
}
?>