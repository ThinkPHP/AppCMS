<?php
defined('IN_PHPCMS') or exit('No permission resources.');
class cacheAll {
 	public function __construct() {
		$this->db = pc_base::load_model('category_model');
		$this->sitemodel_db = pc_base::load_model('sitemodel_model');
	}
	
	/**
	 * 更新所有栏目缓存
	 */
	public function cache_cat() {
		$modellist = getcache('modellist', 'commons');
		foreach($modellist as $k => $v) {
			$this->cache($v[modelid]);
		}
	}
	public function cache($modelid = 0) {
		$modelid = intval($modelid);
		$datas = $this->db->select(array('modelid'=>$modelid),'catid,items',10000);
		$items = array();
		foreach ($datas as $r) {
			$items[$r['catid']] = $r['items'];
		}
		if($items){
			setcache('category_items_'.$modelid, $items,'commons');
		}
		
		$categorys = array();
		$result = $this->db->select(array('modelid'=>$modelid),'*',20000,'listorder ASC');
		foreach ($result as $r) {
			unset($r['module'],$r['catdir']);
			$setting = string2array($r['setting']);
			$r['meta_title'] = $setting['meta_title'];
			$r['meta_keywords'] = $setting['meta_keywords'];
			$r['meta_description'] = $setting['meta_description'];
			$categorys[$r['catid']] = $r;
		}
		if($categorys){
			setcache('category_'.$modelid,$categorys,'commons');
		}
	}
	
	/**
	 * 更新所有栏目缓存
	 */
	public function cache_model() {
		$sitemodel_data = $this->sitemodel_db->select('');
		$_model = array();
		foreach ($sitemodel_data as $k=>$v) {
			$m_d[$v[modelid]] = $v;
			$_model[$v[tablename]] = $v;
		}
		setcache('modellist', $m_d,'commons');
		setcache('modellist_t', $_model, 'commons');
	}
}