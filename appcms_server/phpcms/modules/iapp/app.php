<?php 
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_func('global');
pc_base::load_sys_class('format', '', 0);
class app {
	function __construct() {		
		$this->db = pc_base::load_model('content_model');
		$this->member_db = pc_base::load_model('member_model');
		$this->config = getcache('iapp', 'commons');
		$this->hitsdb = pc_base::load_model('hits_model');
		if(intval($_GET['siteid']) > 0){
			$this->siteid = intval($_GET['siteid']);
		}else{
			$this->siteid = $this->config[setting][siteid]? $this->config[setting][siteid]:1;
		}
		$this->skin = getcache('iappSkin','commons');
	}
	
	//展示首页
	public function init() {
		$siteid = $this->siteid;
		$MODEL = getcache('modellist','commons');
		$db_position = pc_base::load_model('position_data_model');
		$re = array();
		$sql = "`posid` = 1 AND `siteid` = '".$siteid."'";
		$data=$db_position->select($sql,'*',4,'id DESC');
		foreach($data as $r){
			$rd = string2array($r[data]);
			$type = str_replace("iappYp_", "", $MODEL[$r[modelid]]['tablename']);
			$re[slide][] = array("id"=>$r[id],"type"=>$type,"img"=>thumb($rd[thumb],614,296),"title"=>$rd[title],"catid"=>intval($r[catid]));
		}
		
		
		$sql = "`posid` = 2 AND `siteid` = '".$siteid."'";
		$data=$db_position->select($sql,'*',5,'id DESC');
		foreach($data as $k=>$r){
			$rd = string2array($data[$k][data]);
			$re[headlines][$k] = array("id"=>$r[id],
									"type"=>$MODEL[$r[modelid]]['tablename'],
									"description"=>trim(str_cut($rd[description],115,'')),
									"title"=>$rd[title],
									"thumb"=>thumb($rd[thumb],200,200),
									"catid"=>intval($r[catid])
									);
		}
		
		
		
		$sql = "`posid` = 5 AND `siteid` = '".$siteid."'";
		$data=$db_position->select($sql,'*',1,'id DESC');
		$rd = string2array($data[0][data]);
		$re[img] = array("id"=>$data[0][id],"type"=>$MODEL[$data[0][modelid]]['tablename'],"title"=>$rd[title],"thumb"=>thumb($rd[thumb],612,196),"catid"=>intval($data[0][catid]));

		$stime = date('Y-m-j H:i:s');
		echo json_encode(array("data"=>$re,"stime"=>$stime));
	}
	
	/*
	* 获取栏目列表接口
	*/
	public function category() {
		$catid = intval($_GET['catid']) > 0 ? intval(trim($_GET['catid'])) :0;
		$catids=subcat($catid);
		$t = array();
		foreach ($catids as $r) {
			$t[]=array("id"=>$r[catid],"title"=>$r[catname]);
		}
		echo json_encode($t);
	}
	
	/*
	* 获取地区列表接口
	*/
	public function arealist() {
		$areaid = isset($_GET['areaid']) && (intval($_GET['areaid']) > 0) ? intval(trim($_GET['areaid'])) :3360;
		$AREA = getcache($areaid,'linkage');
		$t = array();
		foreach ($AREA[data] as $r) {
			$t[]=array("id"=>$r[linkageid],"title"=>$r[name]);
		}
		echo json_encode($t);
	}
	
	/*
	* 获取栏目数据接口
	*/
	public function lists() {
		
		$catid = intval($_GET['catid']);
		
		
		$siteid = $this->siteid;
		$CATEGORYS = getcache('category_content_'.$siteid,'commons');
		$modelid = $CATEGORYS[$catid]['modelid'];		
		$MODEL = getcache('model','commons');
		
		$tablename = $MODEL[$modelid]['tablename'];
		//$this->db->table_name = $this->db->db_tablepre.$tablename;
		$this->db->set_model($modelid);
		
		$page = intval($_GET['page']) ? intval($_GET['page']) : 1;
		
		if($catid && $modelid){
			switch ($tablename)
			{
				case "news"://新闻
					$slide = array();
					$list = array();
					//栏目幻灯片推荐
					$db_position = pc_base::load_model('position_data_model');
					$sql = "`posid` = 8 AND `siteid` = '".$siteid."'";
					$data=$db_position->select($sql,'*',4,'id DESC');
					foreach($data as $r){
						$rd = string2array($r[data]);
						$slide[] = array("id"=>$r[id],"type"=>$r[module],"title"=>$rd[title],"img"=>thumb($rd[thumb],640,288),"catid"=>intval($r[catid]));
					}
					
					$pagesize = $this->config[setting][newslistnum];
					$offset = ($page - 1) * $pagesize;
					
					if($catid){
						$catids = get_sql_catid('category_content_'.$siteid,$catid);
						$w = "`status` = 99 AND ".$catids;
					}else{
						$w = "`status` = 99 AND `catid` =".$this->config[setting][defaultcatid];
					}
					$data = $this->db->select($w, '*', $offset.','.$pagesize,'inputtime DESC');
					
					
					foreach($data as $r){
						$list[] = array("id"=>$r[id],
										"model"=>$tablename,
										"title"=>trim($r[title]),
										"description"=>str_cut(trim_textarea($r[description]),120,''),
										"url"=>$r[url],
										"color"=>$r[style],
										"thumb"=>thumb($r[thumb],150,120),
										"source"=>$r[copyfrom],
										"hits"=>$hits = $this->hits($r[id],$modelid),
										"pl"=>0,
										"catid"=>intval($r[catid]),
										"addtime"=>date('Y-m-j H:i:s', $r[inputtime])
										);
					}
					//更新时间
					$upTime = date('Y-m-j H:i:s');
					echo json_encode(array("slide"=>$slide,"list"=>$list,"upTime"=>$upTime));	
					break; 
				case "picture"://图片
					$pagesize = $this->config[setting][piclistnum];
					$offset = ($page - 1) * $pagesize;
					if($catid){
						$catids = get_sql_catid('category_content_'.$siteid,$catid);
						$w = "`status` = 99 AND ".$catids;
					}else{
						$w = "`status` = 99 AND `catid` =".$this->config[setting][defaultcatid];
					}
					$data = $this->db->select($w, '*', $offset.','.$pagesize,'inputtime DESC');
					$list = array();
					foreach($data as $r){
						$list[] = array(
									"id"=>$r[id],
									"model"=>$tablename,
									"title"=>$r[title],
									"url"=>$r[url],
									"color"=>$r[style],
									"thumb"=>thumb($r[thumb],650,250),
									"source"=>$r[username],
									"addtime"=>$r[inputtime],
									"hits"=>0,
									"pl"=>0,
									"catid"=>$r[catid]
									);
									
					}
					$upTime = date('Y-m-j H:i:s');
					echo json_encode(array("list"=>$list,"upTime"=>$upTime));
					break;
				case "video"://视频
					$pagesize = $this->config[setting][videolistnum];
					$offset = ($page - 1) * $pagesize;
					if($catid){
						$catids = get_sql_catid('category_content_'.$siteid,$catid);
						$w = "`status` = 99 AND ".$catids;
					}else{
						$w = "`status` = 99 AND `catid` =".$this->config[setting][defaultcatid];
					}
					$data = $this->db->select($w, '*', $offset.','.$pagesize,'inputtime DESC');
					
					$list = array();
					foreach($data as $r){
						$list[] = array(
									"id"=>$r[id],
									"model"=>$tablename,
									"title"=>$r[title],
									"url"=>$r[url],
									"color"=>$r[style],
									"thumb"=>thumb($r[thumb],650,250),
									"source"=>$r[username],
									"addtime"=>$r[inputtime],
									"hits"=>0,
									"pl"=>0,
									"catid"=>$r[catid]
									);
					}
					$upTime = date('Y-m-j H:i:s');
					echo json_encode(array("list"=>$list,"upTime"=>$upTime));
					break;
				case "business"://商家
					$pagesize = $this->config[setting][lifelistnum];
					$offset = ($page - 1) * $pagesize;
					
					if($catid){
						$catids = get_sql_catid('category_content_'.$siteid,$catid);
						$w = "`status` = 99 AND ".$catids;
					}else{
						$w = "`status` = 99 AND `catid` =".$this->config[setting][defaultcatid];
					}
					$data = $this->db->select($w, '*', $offset.','.$pagesize,'inputtime DESC');
					
					$AREA = getcache(3360,'linkage');
					
					$list = array();
					foreach($data as $r){
						$list[] = array(
									"id"=>$r[id],
									"uid"=>0,
									"nick"=>$r[username],
									"name"=>$r[title],
									"mobile"=>$r[r_mobile],
									"area_id"=>$r[r_area],
									"area_cn"=>$AREA[data][intval($r[r_area])][name],
									"catid"=>$r[catid],
									"catname"=>$CATEGORYS[$r[catid]][catname],
									"thumb"=>$r[thumb],
									
									"distance"=>"6082.4公里",
									"operate"=>$r[r_zhuying],
									"tel"=>$r[r_tel],
									"x"=>$r[r_map][0],
									"y"=>$r[r_map][1],
									"hot"=>$r[r_hits],
									//"uptime"=>$r[updatetime],
									"addtime"=>$r[inputtime],
									
								
									//"good_pl"=>1,
									//"poor_pl"=>0,
									//"total_pl"=>1,
									
									"xid"=>$r[catid],
									"xid_cn"=>$CATEGORYS[$r[catid]][catname],
									"img"=>$r[thumb],
									"star"=>66666
									);
					}
					$upTime = date('Y-m-j H:i:s');
					echo json_encode(array("list"=>$list,"upTime"=>$upTime));	
					break;		
				default:
				  echo "default";
			}
		}else{
			echo json_encode(array("status"=>"ERR","list"=>null,"msg"=>"参数错误"));
		}
	}
	
	/*
	* 获取文章内容数据接口
	*/
	public function show() {
		$re = array();
		
		$catid = $_GET['catid']?intval($_GET['catid']):null;
		$id = $_GET['id']?intval($_GET['id']):null;
		if(!$catid || !$id){
			$re[status] = 'NODATA';
			$re[msg] = '没有数据~';
			exit;
		}
				
		$categoryall = getcache('category_content','commons');
		$siteid = $categoryall[$catid];
		$CATEGORYS = getcache('category_content_'.$siteid,'commons');
		
		$CAT = $CATEGORYS[$catid];
		$CAT['setting'] = string2array($CAT['setting']);
		
		$MODEL = getcache('model','commons');
		$modelid = $CAT['modelid'];
		
		$tableA = $MODEL[$modelid]['tablename'];
		$tableB = $this->db->db_tablepre.$tableA;
		
		$this->db->table_name = $tableB;
		
		$r1 = $this->db->get_one(array('id'=>$id));
		if(!$r1 || $r1['status'] != 99){
			$re[status] = 'NODATA';
			$re[msg] = '没有数据~';
			exit;
		}	
		$this->db->table_name = $tableB.'_data';
		
		$r2 = $this->db->get_one(array('id'=>$id));
		$rs = $r2 ? array_merge($r1,$r2) : $r1;
		
		require_once CACHE_MODEL_PATH.'content_output.class.php';
		$content_output = new content_output($modelid,$catid,$CATEGORYS);
		$rs[keywords] = $content_output->keyword("keywords",$rs[keywords]);
		$rs[copyfrom] = $content_output->copyfrom("copyfrom",$rs[copyfrom]);
		
		$hits = $this->hits($id,$modelid);

		switch ($tableA)
		{
			case "news"://新闻
				$re = array();
				$re[data] = array("id"=>$rs[id],
								"title"=>$rs[title],
								"ftitle"=>$rs[title],
								"catid"=>$rs[catid],
								"modelid"=>$tableA,
								"uptime"=> date('Y-m-d H:i:s', $rs[updatetime]),
								"content"=>clear_font_wh(wml_strip($rs[content])),
								"url"=>str_replace("m=content&c=index", "m=iapp&c=touch", $rs[url]),
								"source"=>$rs[copyfrom],
								"color"=>$rs[style],
								"pl"=>0,
								"hits"=>$hits
								);
								
				$re["success"]="ok";
				$re["msg"]="加载成功~";

				echo json_encode($re);
			    break;
			case "picture"://图片picture
				$pictureurls = array();
				$picturetitles = array();
				foreach(string2array($rs[pictureurls]) as $k=>$v){
					$pictureurls[$k] = $v[url];
					$picturetitles[$k] = $v[alt];
					
				}
				
				$re = array();
				$re[data] = array("id"=>$rs[id],
								"title"=>$rs[title],
								"ftitle"=>$rs[title],
								"catid"=>$rs[catid],
								"modelid"=>$tableA,
								"uptime"=> date('Y-m-d H:i:s', $rs[updatetime]),
								"content"=>clear_font_wh(wml_strip($rs[content])),
								"thumb"=>thumb($rs[thumb],650,300),
								"pictureurls"=>$pictureurls,
								"picturetitles"=>$picturetitles,
								"url"=>str_replace("m=content&c=index", "m=iapp&c=touch", $rs[url]),
								"source"=>$rs[copyfrom],
								"color"=>$rs[style],
								"pl"=>0,
								"hits"=>$hits
								);
				
				$re["success"]="ok";
				$re["msg"]="加载成功~";

				echo json_encode($re);
				break;
			case "video"://视频

			
				//print_r($rs);
				
				$video_url = '';
				$_video_url = string2array($rs[video_url]);
				if($_video_url){
					$video_url = $_video_url[0][fileurl];
				}else{
					pc_base::load_app_class('api56', 'video56', 0);
					$video56 = pc_base::load_model('video56_model');
					$vinfo = $video56->get_one(array('videoid'=>intval($rs[video])));
					
					//print_r($vinfo);
				
					
					//echo '$video56info = Open::Video_Mobile(array("vid"=>'.$vinfo['vid'].'));';
					
					//$video56info = Open::Video_Mobile(array('vid'=>$vinfo['vid']));
					$video56info = Open::GetApi('Video/Mobile',array('vid'=>$vinfo['vid']));
					
					//print_r($video56info);
					$video_url = $video56info[info][rfiles][1][url];
					
					$video = "http://vxml.56.com/html5/".$vinfo['vid']."/?src=m&res=vga";
				
				}
				
				$re = array();
				$re[data] = array("id"=>$rs[id],
								"title"=>$rs[title],
								"ftitle"=>$rs[title],
								"catid"=>$rs[catid],
								"modelid"=>$tableA,
								"uptime"=> date('Y-m-d H:i:s', $rs[updatetime]),
								"content"=>clear_font_wh(wml_strip($rs[content])),
								"thumb"=>thumb($rs[thumb],650,300),
								
								"video"=>$video_url,
								"video_url"=>$video_url,
								"videodata"=>$video56info[info][rfiles],
								"url"=>str_replace("m=content&c=index", "m=iapp&c=touch", $rs[url]),
								"source"=>$rs[copyfrom],
								"color"=>$rs[style],
								"pl"=>0,
								"hits"=>$hits
								);
				
				$re["success"]="ok";
				$re["msg"]="加载成功~";
				
				echo json_encode($re);
				break;
			case "business"://商家
				$AREA = getcache(3360,'linkage');
				$rs[r_map] = explode('|',$rs[r_map]);
				$rs[r_photo] = string2array($rs[r_photo]);
				$rs[r_area_cn] = $AREA[data][intval($rs[r_area])][name];
				
				
				$comment_db = pc_base::load_model('comment_model');
				$commentid = id_encode('content_'.$catid,$id,$this->siteid);
				$comment = $comment_db->get_one(array('commentid'=>$commentid, 'siteid'=>$this->siteid));
				
				$re = array();
				
				$re[life] = array("id"=>$rs[id],
				              "uid"=>1,
							  "nick"=>$rs[username],
							  "title"=>$rs[title],
							  "modelid"=>$tableA,
							  "mobile"=>$rs[r_mobile],
							  "area_id"=>$rs[r_area],
							  "area_cn"=>$rs[r_area_cn],
							  "catid"=>$rs[catid],
							  "hits"=>$hits,
							  "thumb"=>$rs[thumb],
							  
							  "cid"=>$rs[catid],
							  
							  "xid"=>$rs[catid],
							  "xid_cn"=>$CATEGORYS[$rs[catid]][catname],
							  
							  
							  "img"=>$rs[thumb],
							  "operate"=>$rs[r_zhuying],
							  "add"=>$rs[r_adds],
							  "tel"=>$rs[r_tel],
							  "tel_two"=>$rs[r_tel],
							  "x"=>$rs[r_map][0],
							  "y"=>$rs[r_map][1],
							  "uptime"=>$rs[updatetime],
							  "addtime"=>$rs[inputtime],
							  "hot"=>$rs[r_hits],
							  "good_pl"=>"1",
							  "poor_pl"=>"0",
							  "total_pl"=>$comment[total],
							  "state"=>"0",
							  "istop"=>"1",
							  "content"=>$rs[content],
							  "url"=>str_replace("m=content&c=index", "m=iapp&c=touch", $rs[url])
							);
				
				//优惠券
				$re[coupons] =array("id"=>"7",
				                "picurl"=>"201305/071135412153.jpg",	
								"title"=>"凭优惠劵购票半价后再减5元",
								"lid"=>"4086",
								"name"=>"瓯江影城",
								"startdate"=>"1367769600",
								"enddate"=>""
								);
				//图片
				$photo = array();
				foreach($rs[r_photo] as $r){
						$photo[b][] = $r[url];
						$photo[m][] = thumb($r[url],133,100);
					}
					
				$re[photo] = $photo;
				
				echo json_encode($re);
				break; 
			default:
			  echo "default";
		}
	}
	
	//提交评论
	function comment() {
		$re = array();
		$userid = intval($_REQUEST['userid']);
		$acctoken = trim($_REQUEST['acctoken']);
		
		
		$userdata = $this->member_db->get_one(array('userid'=>$userid,'password'=>$acctoken));
		if($userdata){
			$username = $userdata[username];
			$userid = $userdata[userid];	
		}else{
			$username = $this->config[setting][noregcomment];
			$userid = 0;
		}

		$comment = pc_base::load_app_class('comment','comment');
		pc_base::load_app_func('global','comment');
		
		$catid = trim($_REQUEST['catid']) ? intval($_REQUEST['catid']) : 0;	
		$contentid = trim($_REQUEST['contentid']) ? intval($_REQUEST['contentid']) : null;
		$title = trim($_REQUEST['title'])?trim($_REQUEST['title']):null;
		$url = trim($_REQUEST['url'])?trim($_REQUEST['url']):null;
		$id = trim($_REQUEST['id']) ? intval($_REQUEST['id']) : 0;
		$msg = trim($_REQUEST['msg'])?trim($_REQUEST['msg']):null;
		$direction = trim($_REQUEST['direction'])?intval($_REQUEST['direction']):0;
		
		
		if($contentid && $title && $url &&$msg)
		{
			$msg = safe_replace(remove_xss($msg));
			$commentid = id_encode('content_'.$catid,$contentid,$this->siteid);
			$data = array('userid'=>$userid, 'username'=>$username, 'content'=>$msg,'direction'=>$direction);		
			$comment->add($commentid, $this->siteid, $data, $id, $title, $url);
			$re["success"]="OK";
			$re["msg"]="发布成功~";
		}else{
			$re["success"]="NODATA";
			$re["msg"]="参数错误~";
		}	
		echo json_encode($re);
	}
	
	//评论列表
	function commentlist() {
		$re = array();
		$catid = $_GET['catid']?intval($_GET['catid']):0;
		$id = $_GET['id']?intval($_GET['id']):null;
		$page = max(intval($_GET['page']), 1);
		
		if($id){
			$comment_db = pc_base::load_model('comment_model');
			$comment_data_db = pc_base::load_model('comment_data_model');
			$commentid = id_encode('content_'.$catid,$id,$this->siteid);
			$commentone = $comment_db->get_one(array('commentid'=>$commentid, 'siteid'=>$this->siteid));
			$pagesize = 6;
			$offset = ($page - 1) * $pagesize;
			
			if($commentone){
				$comment_data_db->table_name($commentone['tableid']);	
				$sql = array('commentid'=>$commentid, 'status'=>1);
				$commentdata = $comment_data_db->select($sql, '*',$offset.','.$pagesize,'id DESC');
				foreach($commentdata as $r){
					if($r[userid]==0){
						$nickname = '最前端网友';
					}else{
						$memberinfo = get_memberinfo($r[userid]);
						$nickname = $memberinfo[nickname];
					}
					
					$this->_init_phpsso();
					$ssoinfo = unserialize($this->client->ps_get_member_info($memberinfo[phpssouid]));
					if($ssoinfo[avatar]==1){
						$avatar = get_memberavatar($memberinfo[phpssouid],'',90);
					}else{
						$avatar = '';
					}
					
					$re[data][] = array("id"=>$r[id],
									"nid"=>$id,
									"uid"=>0,
									"nick"=>$nickname,
									"avatar"=>$avatar,
									"title"=>$commentdata[title],
									"content"=>$r[content],
									"direction"=>$r[direction],
									"ip"=>$r[ip],
									"addtime"=>date('Y-m-d H:i:s',$r[creat_at]));
				}
				
				$re[status] = 'OK';
				$re[msg] = '加载成功~';
			}else{
				$re[status] = 'OK';
				$re[msg] = '没有数据~';
			}
		}else{
			$re[status] = 'ERR';
			$re[msg] = '参数错误~';
		}
		
		echo json_encode($re);
	}
	

	/**
	 * 初始化phpsso
	 * about phpsso, include client and client configure
	 * @return string phpsso_api_url phpsso地址
	 */
	private function _init_phpsso() {
		pc_base::load_app_class('client', 'member', 0);
		define('APPID', pc_base::load_config('system', 'phpsso_appid'));
		$phpsso_api_url = pc_base::load_config('system', 'phpsso_api_url');
		$phpsso_auth_key = pc_base::load_config('system', 'phpsso_auth_key');
		$this->client = new client($phpsso_api_url, $phpsso_auth_key);
		return $phpsso_api_url;
	}
	
	/*
	* 获取栏目数据接口
	*/
	public function search() {
		$tb = trim($_GET['tb'])? trim($_GET['tb']) :null;
		$keyword = trim($_GET['keyword'])? trim($_GET['keyword']) :null;
		$siteid = $this->siteid;
		$tablename = $tb;
		$this->db->table_name = $this->db->db_tablepre.$tablename;
		
		switch ($tablename)
		{
			case "news"://新闻
			
				$list = array();
				$w = "`status` = 99 AND `title` like '%$keyword%'";
				$data = $this->db->select($w, '*', '1,20','inputtime DESC');
				
				foreach($data as $r){
					$list[] = array("id"=>$r[id],
									"model"=>$tablename,
									"title"=>$r[title],
									"url"=>$r[url],
									"color"=>$r[style],
									"thumb"=>thumb($r[thumb],132,100),
									"source"=>$r[username],
									"hits"=>0,
									"pl"=>0,
									"catid"=>intval($r[catid]),
									"addtime"=>$r[inputtime]
									);
				}
				//更新时间
				$upTime = date('Y-m-j H:i:s');
				echo json_encode(array("list"=>$list,"upTime"=>$upTime));	
			    break; 
			case "picture"://图片
				
				break;
			case "video"://视频
				
				break;
			case "business"://商家
				
				$w = "`status` = 99 AND `title` like '%$keyword%'";
				$data = $this->db->select($w, '*', '1,20','inputtime DESC');
				$AREA = getcache(3360,'linkage');
				
				$list = array();
				foreach($data as $r){
					$list[] = array(
								"id"=>$r[id],
								"uid"=>0,
								"nick"=>$r[username],
								"name"=>$r[title],
								"mobile"=>$r[r_mobile],
								"area_id"=>$r[r_area],
								"area_cn"=>$AREA[data][intval($r[r_area])][name],
								"cid"=>$r[catid],
								"xid"=>$r[catid],
								"xid_cn"=>$CATEGORYS[$r[catid]][catname],
								"img"=>$r[thumb],
								"operate"=>$r[r_zhuying],
								"x"=>0,
								"y"=>0,
								"uptime"=>$r[updatetime],
								"addtime"=>$r[inputtime],
								"hot"=>$r[r_hits],
								"good_pl"=>1,
								"poor_pl"=>0,
								"total_pl"=>1,
								"state"=>0,
								"istop"=>1,
								"distance"=>"6082.4公里",
								"star"=>66666
								);
				}
				$upTime = date('Y-m-j H:i:s');
				echo json_encode(array("list"=>$list,"upTime"=>$upTime));	
			    break;		
			default:
			  echo "default";
		}
	}
	
	/*
	* 本地生活首页栏目列表接口
	*/
	public function business() {
	
		$catid = isset($_GET['catid']) && (intval($_GET['catid']) > 0) ? intval(trim($_GET['catid'])) :0;
		$catid1=subcat($catid);
		$t = array();
		$k=0;
		foreach ($catid1 as $r) {
			$t[$k][id] = $r[catid];
			$t[$k][title] = $r[catname];
			
			$Node = subcat($r[catid]);
			$c = array();
			foreach ($Node as $v) {
				$c[][title] = $v[catname];
			}
			$t[$k][Node] =$c;
			$k++;
		}
		echo json_encode(array("List"=>$t));
	}
	
	/**
	 * 获取点击数量
	 * @param $id
	 * @param $modelid
	 */
	public function hits($id,$modelid) {
		$hitsid = 'c-'.$modelid.'-'.intval($id);
		if($modelid && $id) {
			$r = $this->hitsdb->get_one(array('hitsid'=>$hitsid));
			if(!$r) return false;
			$views = $r['views'] + 1;
			$yesterdayviews = (date('Ymd', $r['updatetime']) == date('Ymd', strtotime('-1 day'))) ? $r['dayviews'] : $r['yesterdayviews'];
			$dayviews = (date('Ymd', $r['updatetime']) == date('Ymd', SYS_TIME)) ? ($r['dayviews'] + 1) : 1;
			$weekviews = (date('YW', $r['updatetime']) == date('YW', SYS_TIME)) ? ($r['weekviews'] + 1) : 1;
			$monthviews = (date('Ym', $r['updatetime']) == date('Ym', SYS_TIME)) ? ($r['monthviews'] + 1) : 1;
			$sql = array('views'=>$views,'yesterdayviews'=>$yesterdayviews,'dayviews'=>$dayviews,'weekviews'=>$weekviews,'monthviews'=>$monthviews,'updatetime'=>SYS_TIME);
			$this->hitsdb->update($sql, array('hitsid'=>$hitsid));
		}
		return $views;	
	}
	
	/*
	* 在线升级
	*/
	public function upgrade() {
		$re = array();
		$db = pc_base::load_model('iapp_model');
		$config = $db->get_one(array('mkey'=>'upgrade'));
		$config = string2array($config[setting]);
		
		//print_r($config);
		
		$ver = trim($_GET['ver'])? trim($_GET['ver']) :null;
		$platform = trim($_GET['platform'])? trim($_GET['platform']) :null;
		
		if($ver){
			if($platform ==0){
				if($config[upgrade][new_iphone_ver]>$ver){
					$re["success"]="yes";
					$re[data] = $config;
					$re[msg] = '发现新版本'.$config[upgrade][new_iphone_ver].',是否现在更新？';
				}else{
					$re["success"]="no";
					$re[msg] = '已经是最新版本,不需要更新~';
				}
			}else{
				if($config[upgrade][new_android_ver]>$ver){
					$re["success"]="yes";
					$re[data] = $config;
					$re[msg] = '发现新版本'.$config[upgrade][new_android_ver].',是否现在更新？';
				}else{
					$re["success"]="no";
					$re[msg] = '已经是最新版本,不需要更新~';
				}
			}
		}else{
			$re["success"]="ERR";
			$re[msg] = '检查更新失败,参数错误~';
		}
		echo json_encode($re);
	}
	
	/*
	* 获取配置
	*/
	public function setting() {
		$re = array();
		$skin = $this->skin;
		pc_base::load_app_func('pattern');
		$pattern = getcache('pattern', 'commons');
		
		$db = pc_base::load_model('iapp_model');
		$upgrade = $db->get_one(array('mkey'=>'upgrade'));
		$upgrade = string2array($upgrade[setting]);
		
		//print_r($pattern);
		
		
		
		if($pattern){
			$re["success"] = "yes";
			$re[data][skin] = $skin[skin];
			$re[data][bg] = $skin[bg];
			$re[data][upgrade] = $upgrade;
			$re[data][config] = pattern2js($pattern);
			
			$re[msg] = '配置获取成功~';
		}else{
			$re["success"]="ERR";
			$re[data] = "";
			$re[msg] = '应用初始化失败,参数错误~';
		}
		echo json_encode($re);
	}
	
	/*
	* Skin
	*/
	public function skin() {
		$skin_all = getcache('iappSkin_all','commons');
		echo json_encode($skin_all);
	}
	/*
	* help
	*/
	public function help() {
		$re = array();
		$db = pc_base::load_model('iapp_model');
		$info = $db->get_one(array('mkey'=>'help'));
		if($info){
			$re["success"]="yes";
			$re[data][content] = $info[setting];
			$re[msg] = '加载成功~';
		}else{
			$re["success"]="ERR";
			$re[msg] = '加载失败~';
		}
		echo json_encode($re);
	}
	
	public function about() {
		$db = pc_base::load_model('iapp_model');
		$info = $db->get_one(array('mkey'=>'about'));
		
		if($info){
			$re["success"]="yes";
			$re[data] = string2array($info[setting]);
			$re[msg] = '加载成功~';
		}else{
			$re["success"]="ERR";
			$re[msg] = '加载失败~';
		}
		
		echo json_encode($re);
	}
}
?>