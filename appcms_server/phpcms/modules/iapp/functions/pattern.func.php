<?php

/*
*转换为js格式
*/
function pattern2js($arr) {
	$_pattern = "";
	foreach($arr as $k=>$r){
		$_pattern .= 'var '.$k.' = '.json_encode($r).'; ';
	}
	return $_pattern;
}

/*
*可用配置
*/
function fun_pattern_data() {

$type = 'news';
if(module_exists('iappActivity') && module_exists('iappCoupons') && module_exists('iappYp') && module_exists('iappTel') && module_exists('iappTrain')){
	$type = 'portal';
}



$pattern = array();
//语言
$pattern[l_common][app_name] = array("val" => "最黔端","label" => "APP名称");
$pattern[l_common][head_txt_home] = array("val" => "最黔端","label" => "首页标题");
if($type == 'portal'){
$pattern[l_common][head_txt_content] = array("val" => "贵新闻","label" => "新闻中心标题");
}
if($type == 'portal'){
$pattern[l_common][head_txt_life] = array("val" => "黔生活","label" => "生活中心标题");
}
if(module_exists('iappTalk')){
$pattern[l_common][head_txt_talk] = array("val" => "喷水池","label" => "论坛标题");
}
$pattern[l_common][head_txt_more] = array("val" => "更多","label" => "更多标题");
$pattern[l_common][head_txt_news] = array("val" => "新闻","label" => "新闻标题");
$pattern[l_common][head_txt_picture] = array("val" => "图片","label" => "图片标题");
$pattern[l_common][head_txt_video] = array("val" => "视频","label" => "视频标题");

$pattern[l_common][head_txt_apps] = array("val" => "百宝箱","label" => "百宝箱标题");
if(module_exists('iappActivity')){
$pattern[l_common][head_txt_activity] = array("val" => "潮活动","label" => "潮活动标题");
}
if(module_exists('iappCoupons')){
$pattern[l_common][head_txt_coupons] = array("val" => "优惠券","label" => "优惠券标题");
}
if(module_exists('iappYp')){
$pattern[l_common][head_txt_company] = array("val" => "黔商家","label" => "黔商家标题");
}
$pattern[l_common][head_txt_integral] = array("val" => "黔币","label" => "积分名称");
$pattern[l_common][head_txt_jifen] = array("val" => "积分商城","label" => "积分商城标题");
if(module_exists('iappBaoliao')){
$pattern[l_common][head_txt_baoliao] = array("val" => "爆料","label" => "爆料标题");
}
if(module_exists('iappJifen')){
$pattern[l_common][head_txt_sign] = array("val" => "签到","label" => "签到标题");
}


//首页
$pattern[l_index][news] = array("model" => "news","url" => "news","title" => "新闻","catid" => 0,"bg"=>"#2D52DC","islogin" => "0");
if(module_exists('iappToday')){
$pattern[l_index][today] = array("model" => "today","url" => "today","title" => "今日推荐","catid" => 0,"bg"=>"#00BEC3","islogin" => "0");
}
$pattern[l_index][picture] = array("model" => "picture","url" => "picture","title" => "图片","catid" => 0,"bg"=>"#00BEC3","islogin" => "0");
if(module_exists('iappVideo')){
$pattern[l_index][video] = array("model" => "video","url" => "video","title" => "视频","catid" => 0,"bg"=>"#00BEC3","islogin" => "0");
}
if(module_exists('iappActivity')){
$pattern[l_index][activity] = array("model" => "activity","url" => "activity","title" => "潮活动","catid" => 0,"bg"=>"#EC5870","islogin" => "0");
}
if(module_exists('iappCoupons')){
$pattern[l_index][coupons] = array("model" => "coupons","url" => "coupons","title" => "优惠券","catid" => 0,"bg"=>"#A151B5","islogin" => "0");
}
if(module_exists('iappYp')){
$pattern[l_index][company] = array("model" => "company","url" => "company","title" => "吃喝玩乐","catid" => 0,"bg"=>"#F98835","islogin" => "0");
}
if($type == 'portal'){
$pattern[l_index][apps] = array("model" => "apps","url" => "apps","title" => "百宝箱","catid" => 0,"bg"=>"#62A4D6","islogin" => "0");
}
if(module_exists('iappTalk')){
$pattern[l_index][talk] = array("model" => "talk","url" => "talk","title" => "喷水池","catid" => 0,"bg"=>"#00BEC3","islogin" => "0");
}
$pattern[l_index][sign] = array("model" => "sign","url" => "sign","title" => "签到","catid" => 0,"bg"=>"#00BEC3","islogin" => "1");
if(module_exists('iappJifen')){
$pattern[l_index][jifen] = array("model" => "jifen","url" => "jifen","title" => "积分","catid" => 0,"bg"=>"#00BEC3","islogin" => "0");
}
if($type == 'portal'){
$pattern[l_index][vicinity] = array("model" => "vicinity","url" => "vicinity","title" => "附近","catid" => 0,"bg"=>"#00BEC3","islogin" => "0");
}
if(module_exists('iappBaoliao')){
$pattern[l_index][baoliao] = array("model" => "baoliao","url" => "baoliao","title" => "爆料","catid" => 0,"bg"=>"#00BEC3","islogin" => "0");
}
$pattern[l_index][more] = array("model" => "more","url" => "more","title" => "更多","catid" => 0,"bg"=>"#00BEC3","islogin" => "0");


//主菜单
$pattern[l_footer][main_portal] = array("model" => "main_portal","title" => "首页-门户","islogin" => "0");
$pattern[l_footer][main_simple] = array("model" => "main_simple","title" => "首页-简洁","islogin" => "0");
$pattern[l_footer][main_winphone] = array("model" => "main_winphone","title" => "首页-winphone","islogin" => "0");

if($type == 'portal'){
$pattern[l_footer][content] = array("model" => "content","title" => "贵新闻","islogin" => "0");
}
if($type == 'portal'){
$pattern[l_footer][life] = array("model" => "life","title" => "贵生活","islogin" => "0");
}
if(module_exists('iappTalk')){
$pattern[l_footer][talk] = array("model" => "talk","title" => "论坛","islogin" => "0");
}
$pattern[l_footer][more] = array("model" => "more","title" => "更多","islogin" => "0");
					
$pattern[l_footer][news] = array("model" => "news","title" => "新闻","islogin" => "0");
$pattern[l_footer][picture] = array("model" => "picture","title" => "图片","islogin" => "0");
if(module_exists('iappVideo')){
$pattern[l_footer][video] = array("model" => "video","title" => "视频","islogin" => "0");
}
if(module_exists('iappActivity')){
$pattern[l_footer][activity] = array("model" => "activity","title" => "活动","islogin" => "0");
}
if(module_exists('iappCoupons')){
$pattern[l_footer][coupons] = array("model" => "coupons","title" => "优惠券","islogin" => "0");
}
if(module_exists('iappYp')){
$pattern[l_footer][company] = array("model" => "company","title" => "商家","islogin" => "0");
}
if(module_exists('iappTel')){
$pattern[l_footer][apps] = array("model" => "apps","title" => "百宝箱","islogin" => "0");
}
if(module_exists('iappBaoliao')){
$pattern[l_footer][baoliao] = array("model" => "baoliao","title" => "爆料","islogin" => "0");
}
$pattern[l_footer][sign] = array("model" => "sign","title" => "签到","islogin" => "1");
if(module_exists('iappJifen')){
$pattern[l_footer][jifen] = array("model" => "jifen","title" => "积分商城","islogin" => "0");
}
if(module_exists('iappToday')){
$pattern[l_footer][today] = array("model" => "today","title" => "今日推荐","islogin" => "0","url" => "today","catid" => 0,"bg"=>"#00BEC3");
}

//新闻中心
if($type == 'portal'){
	$pattern[l_content][news] = array("model" => "news","title" => "新闻","catid" => 0,"filter" => 1);
	$pattern[l_content][picture] = array("model" => "picture","title" => "图片","catid" => 0,"filter" => 1);
	if(module_exists('iappVideo')){
		$pattern[l_content][video] = array("model" => "video","title" => "视频","catid" => 0,"filter" => 1);
	}
}

//生活中心
if($type == 'portal'){
	$pattern[l_life][coupons] = array("model" => "coupons","title" => "优惠券","catid" => 0,"filter" => 1);
	$pattern[l_life][activity] = array("model" => "activity","title" => "潮活动","catid" => 0,"filter" => 1);
	$pattern[l_life][company] = array("model" => "company","title" => "黔商家","catid" => 0,"filter" => 0);
	$pattern[l_life][apps] = array("model" => "apps","title" => "百宝箱","catid" => 0,"filter" => 0);
}
		

$pattern[l_news][news_0] = array("model" => "news","title" => "全部","catid" => 0);

$pattern[l_picture][picture_0] = array("model" => "picture","title" => "全部","catid" => 0);

$pattern[l_video][video_0] = array("model" => "video","title" => "全部","catid" => 0);
		
$pattern[l_coupons][coupons_0] = array("model" => "coupons","title" => "全部","catid" => 0);
		
$pattern[l_activity][activity_0] = array("model" => "activity","title" => "全部","catid" => 0);

$pattern[l_jifen][jifen_0] = array("model" => "jifen","title" => "全部","catid" => 0);
		
$pattern[l_company][company_0] = array("model" => "company","title" => "全部","catid" => 0);



$pattern[l_apps][express] = array("model" => "apps","title" => "快递查询","img" => "skin/images/icon/express.png" ,"bg"=>"#00AEEF");
$pattern[l_apps][bus] = array("model" => "apps","title" => "汽车时刻","img" => "skin/images/icon/bus.png","bg"=>"#00A8EC");
$pattern[l_apps][train] = array("model" => "apps","title" => "火车时刻","img" => "skin/images/icon/train.png" ,"bg"=>"#CE2629");
$pattern[l_apps][tel] = array("model" => "apps","title" => "常用电话","img" => "skin/images/icon/tel.png" ,"bg"=>"#43B51F");
$pattern[l_apps][localapp] = array("model" => "apps","title" => "贵阳APP","img" => "skin/images/icon/localapp.png","bg"=>"#00BEC3");
$pattern[l_apps][daijia] = array("model" => "apps","title" => "贵阳代驾","img" => "skin/images/icon/daijia.png","bg"=>"#2299AF");

$pattern[l_apps][rurntable] = array("model" => "url","title" => "幸运大转盘","url" => "http://www.zuiqianduan.cn/","img" => "skin/images/icon/rurntable.png","bg"=>"#62A4D6");
$pattern[l_apps][goldenegg] = array("model" => "url","title" => "砸金蛋","url" => "http://www.zuiqianduan.cn/","img" => "skin/images/icon/goldenegg.png","bg"=>"#00BEC3");
					
$pattern[l_apps][jifen] = array("model" => "jifen","title" => "积分商城","img" => "skin/images/icon/jifen.png","bg"=>"#FF960E");


if($type == 'portal'){
$pattern[l_tab_default][content] = array('val'=>"today",'label'=>'新闻中心默认TAB');
$pattern[l_tab_default][life] = array('val'=>"coupons",'label'=>'生活中心默认TAB');
}
$pattern[l_tab_default][news] = array('val'=>"news_0",'label'=>'新闻页面默认TAB');
$pattern[l_tab_default][picture] = array('val'=>"picture_0",'label'=>'图片页面默认TAB');
$pattern[l_tab_default][video] = array('val'=>"video_0",'label'=>'视频页面默认TAB');
$pattern[l_tab_default][coupons] = array('val'=>"coupons_0",'label'=>'优惠券默认TAB');
$pattern[l_tab_default][activity] = array('val'=>"activity_0",'label'=>'活动默认TAB');
$pattern[l_tab_default][jifen] = array('val'=>"jifen_0",'label'=>'积分商城默认TAB');
	
$pattern[l_catid_default][news] = array('val'=>"0",'label'=>'新闻默认栏目');
$pattern[l_catid_default][picture] = array('val'=>"0",'label'=>'图片默认栏目');
$pattern[l_catid_default][video] = array('val'=>"0",'label'=>'视频默认栏目');
$pattern[l_catid_default][coupons] = array('val'=>"0",'label'=>'优惠券默认栏目');
$pattern[l_catid_default][activity] = array('val'=>"0",'label'=>'活动默认栏目');
$pattern[l_catid_default][company] = array('val'=>"0",'label'=>'商家默认栏目');
$pattern[l_catid_default][jifen] = array('val'=>"0",'label'=>'积分商城默认栏目');

$pattern[l_connect][qqweibo] = "801407387";
$pattern[l_connect][sina] = "2595200536";


/*
	$pattern = 
	array(
		"l_common" => array(
					"app_name" => array("val" => "最黔端","label" => "APP名称"),
					"head_txt_home" => array("val" => "最黔端","label" => "首页标题"),
					"head_txt_content" => array("val" => "贵新闻","label" => "新闻中心标题"),
					"head_txt_life" => array("val" => "黔生活","label" => "生活中心标题"),
					"head_txt_talk" => array("val" => "喷水池","label" => "论坛标题"),
					"head_txt_more" => array("val" => "更多","label" => "更多标题"),
					"head_txt_news" => array("val" => "贵阳热点","label" => "新闻标题"),
					"head_txt_apps" => array("val" => "百宝箱","label" => "百宝箱标题"),
					"head_txt_activity" => array("val" => "潮活动","label" => "潮活动标题"),
					"head_txt_coupons" => array("val" => "优惠券","label" => "优惠券标题"),
					"head_txt_company" => array("val" => "黔商家","label" => "黔商家标题"),
					"head_txt_picture" => array("val" => "图片","label" => "图片标题"),
					"head_txt_video" => array("val" => "视频","label" => "视频标题"),
					
					
					"head_txt_integral" => array("val" => "黔币","label" => "积分名称"),
					"head_txt_jifen" => array("val" => "积分商城","label" => "积分商城标题")
					),
	
		"l_index" => array(
					"content" => array("model" => "content","url" => "news","title" => "贵阳热点","catid" => 0,"bg"=>"#2D52DC"),
					"today" => array("model" => "content","url" => "today","title" => "今日推荐","catid" => 0,"bg"=>"#00BEC3"),
					"picture" => array("model" => "content","url" => "picture","title" => "图片","catid" => 0,"bg"=>"#00BEC3"),
					"video" => array("model" => "content","url" => "video","title" => "视频","catid" => 0,"bg"=>"#00BEC3"),
					
					"activity" => array("model" => "life","url" => "activity","title" => "潮活动","catid" => 0,"bg"=>"#EC5870"),
					"coupons" => array("model" => "life","url" => "coupons","title" => "优惠券","catid" => 0,"bg"=>"#A151B5"),
					"company" => array("model" => "life","url" => "company","title" => "吃喝玩乐","catid" => 0,"bg"=>"#F98835"),
					"apps" => array("model" => "life","url" => "apps","title" => "百宝箱","catid" => 0,"bg"=>"#62A4D6"),
					
					"talk" => array("model" => "talk","url" => "talk","title" => "喷水池","catid" => 0,"bg"=>"#00BEC3"),
	
					"jifen" => array("model" => "jifen","url" => "jifen","title" => "积分","catid" => 0,"bg"=>"#00BEC3"),
					"user_sign" => array("model" => "user_sign","url" => "user_sign","title" => "签到","catid" => 0,"bg"=>"#00BEC3"),
					"vicinity" => array("model" => "vicinity","url" => "vicinity","title" => "附近","catid" => 0,"bg"=>"#00BEC3")
					),
					
		
	    "l_footer" => array(
					"main" => array("model" => "main","title" => "最黔端"),
					"content" => array("model" => "content","title" => "贵新闻"),
					"life" => array("model" => "life","title" => "黔生活"),
					"talk" => array("model" => "talk","title" => "喷水池"),
					"more" => array("model" => "more","title" => "更多"),
					
					"news" => array("model" => "news","title" => "新闻"),
					"picture" => array("model" => "picture","title" => "图片"),
					"video" => array("model" => "video","title" => "视频"),
					"activity" => array("model" => "activity","title" => "活动"),
					"coupons" => array("model" => "coupons","title" => "优惠券"),
					"company" => array("model" => "company","title" => "商家"),
					"apps" => array("model" => "apps","title" => "百宝箱")
					),
		
	    "l_content" => array(
					"today" => array("model" => "today","title" => "今日","catid" => 0,"filter" => 0),
					"news" => array("model" => "news","title" => "新闻","catid" => 0,"filter" => 1),
					"picture" => array("model" => "picture","title" => "图片","catid" => 0,"filter" => 1),
					"video" => array("model" => "video","title" => "视频","catid" => 0,"filter" => 1)
					),

	    "l_life" => array(
					"coupons" => array("model" => "coupons","title" => "优惠券","catid" => 0,"filter" => 1),
					"activity" => array("model" => "activity","title" => "潮活动","catid" => 0,"filter" => 1),
					"company" => array("model" => "company","title" => "黔商家","catid" => 0,"filter" => 0),
					"apps" => array("model" => "apps","title" => "百宝箱","catid" => 0,"filter" => 0)
					),
		
	    "l_news" => array(
					"news_0" => array("model" => "news","title" => "全部","catid" => 0)
					),

	    "l_picture" => array(
					"picture_0" => array("model" => "picture","title" => "全部","catid" => 0)
					),

	    "l_video" => array(
					"video_0" => array("model" => "video","title" => "全部","catid" => 0)
					),
		
	    "l_coupons" => array(
					"coupons_0" => array("model" => "coupons","title" => "全部","catid" => 0)
					),
		
	    "l_activity" => array(
					"activity_0" => array("model" => "activity","title" => "全部","catid" => 0)
					),

	    "l_jifen" => array(
					"jifen_0" => array("model" => "jifen","title" => "全部","catid" => 0)
					),
		
		"l_company" => array(
					"company_0" => array("model" => "company","title" => "全部","catid" => 0)
					),
		
	    "l_apps" => array(
					"express" => array("model" => "apps","title" => "快递查询","img" => "skin/images/icon/express.png" ,"bg"=>"#00AEEF"),
					"bus" => array("model" => "apps","title" => "汽车时刻","img" => "skin/images/icon/bus.png","bg"=>"#00A8EC"),
					"train" => array("model" => "apps","title" => "火车时刻","img" => "skin/images/icon/train.png" ,"bg"=>"#CE2629"),
					"tel" => array("model" => "apps","title" => "常用电话","img" => "skin/images/icon/tel.png" ,"bg"=>"#43B51F"),
					"localapp" => array("model" => "apps","title" => "贵阳APP","img" => "skin/images/icon/localapp.png","bg"=>"#00BEC3"),
					"daijia" => array("model" => "apps","title" => "贵阳代驾","img" => "skin/images/icon/daijia.png","bg"=>"#2299AF"),

					"rurntable" => array("model" => "url","title" => "幸运大转盘","url" => "http://www.zuiqianduan.cn/","img" => "skin/images/icon/rurntable.png","bg"=>"#62A4D6"),
					"goldenegg" => array("model" => "url","title" => "砸金蛋","url" => "http://www.zuiqianduan.cn/","img" => "skin/images/icon/goldenegg.png","bg"=>"#00BEC3"),
					
					"jifen" => array("model" => "jifen","title" => "积分商城","img" => "skin/images/icon/jifen.png","bg"=>"#FF960E")
					),

	    "l_tab_default" => array(
					"content" => array('val'=>"today",'label'=>'新闻中心默认TAB'),
					"life" => array('val'=>"coupons",'label'=>'生活中心默认TAB'),
					"news" => array('val'=>"news_0",'label'=>'新闻页面默认TAB'),
					"picture" => array('val'=>"picture_0",'label'=>'图片页面默认TAB'),
					"video" => array('val'=>"video_0",'label'=>'视频页面默认TAB'),
					"coupons" => array('val'=>"coupons_0",'label'=>'优惠券默认TAB'),
					"activity" => array('val'=>"activity_0",'label'=>'优惠券默认TAB'),
					"jifen" => array('val'=>"jifen_0",'label'=>'积分商城默认TAB')
					),
		
	    "l_catid_default" => array(
					"news" => array('val'=>"0",'label'=>'新闻默认栏目'),
					"picture" => array('val'=>"0",'label'=>'图片默认栏目'),
					"video" => array('val'=>"0",'label'=>'视频默认栏目'),
					"coupons" => array('val'=>"0",'label'=>'优惠券默认栏目'),
					"activity" => array('val'=>"0",'label'=>'活动默认栏目'),
					"company" => array('val'=>"0",'label'=>'商家默认栏目'),
					"jifen" => array('val'=>"0",'label'=>'积分商城默认栏目')
					),

	    "l_connect" => array(
					"qqweibo" => "801407387",
					"sina" => "2595200536"
					)
	);
	*/
	return $pattern;
}


/*
*默认配置
*/
function fun_pattern_def() {
	$pattern = 
	array(
		"l_common" => array(
					"app_name" => "最黔端",
					"head_txt_home" => "最黔端",
					"head_txt_content" => "贵新闻",
					"head_txt_life" => "黔生活",
					"head_txt_talk" => "喷水池",
					"head_txt_more" => "更多",
					"head_txt_news" => "贵阳热点",
					"head_txt_apps" => "百宝箱",
					"head_txt_activity" => "潮活动",
					"head_txt_coupons" => "优惠券",
					"head_txt_company" => "黔商家",
					"head_txt_integral" => "黔币",
					"head_txt_jifen" => "积分商城"
					),

		"l_index" => array(
					"content" => array("model" => "content","url" => "news","title" => "贵阳热点","catid" => 8,"bg"=>"#2D52DC"),
					"talk" => array("model" => "talk","url" => "talk","title" => "喷水池","catid" => 0,"bg"=>"#00BEC3"),
					"activity" => array("model" => "life","url" => "activity","title" => "潮活动","catid" => 0,"bg"=>"#EC5870"),
					"coupons" => array("model" => "life","url" => "coupons","title" => "优惠券","catid" => 0,"bg"=>"#A151B5"),
					"company" => array("model" => "life","url" => "company","title" => "吃喝玩乐","catid" => 0,"bg"=>"#F98835"),
					"apps" => array("model" => "life","url" => "apps","title" => "百宝箱","catid" => 0,"bg"=>"#62A4D6")
					),
		
	    "l_footer" => array(
					"main" => array("model" => "main","title" => "最黔端"),
					"content" => array("model" => "content","title" => "贵新闻"),
					"life" => array("model" => "life","title" => "黔生活"),
					"talk" => array("model" => "talk","title" => "喷水池"),
					"more" => array("model" => "more","title" => "更多")
					),
		
	    "l_content" => array(
					"today" => array("model" => "today","title" => "今日","catid" => 0,"filter" => 0),
					"news" => array("model" => "news","title" => "新闻","catid" => 0,"filter" => 1),
					"picture" => array("model" => "picture","title" => "图片","catid" => 0,"filter" => 1),
					"video" => array("model" => "video","title" => "视频","catid" => 0,"filter" => 1)
					),

	    "l_life" => array(
					"coupons" => array("model" => "coupons","title" => "优惠券","catid" => 0,"filter" => 1),
					"activity" => array("model" => "activity","title" => "潮活动","catid" => 0,"filter" => 1),
					"business" => array("model" => "business","title" => "黔商家","catid" => 0,"filter" => 0),
					"apps" => array("model" => "apps","title" => "百宝箱","catid" => 0,"filter" => 0)
					),
		
	    "l_news" => array(
					"news_6" => array("model" => "news","title" => "全部","catid" => 6),
					"news_8" => array("model" => "news","title" => "本地新闻","catid" => 8),
					"news_9" => array("model" => "news","title" => "国际国内","catid" => 9)
					),

	    "l_picture" => array(
					"picture_0" => array("model" => "picture","title" => "全部","catid" => 20),
					"picture_1" => array("model" => "picture","title" => "旅游","catid" => 20),
					"picture_2" => array("model" => "picture","title" => "娱乐","catid" => 20)
					),

	    "l_video" => array(
					"video_0" => array("model" => "video","title" => "全部","catid" => 40),
					"video_1" => array("model" => "video","title" => "贵阳","catid" => 40),
					"video_2" => array("model" => "video","title" => "音乐","catid" => 40)
					),
		
	    "l_coupons" => array(
					"coupons_0" => array("model" => "coupons","title" => "全部","catid" => 7),
					"coupons_13" => array("model" => "coupons","title" => "本地美食","catid" => 13),
					"coupons_14" => array("model" => "coupons","title" => "休闲娱乐","catid" => 14),
					"coupons_15" => array("model" => "coupons","title" => "酒店住宿","catid" => 15),
					"coupons_16" => array("model" => "coupons","title" => "便民家政","catid" => 16),
					"coupons_17" => array("model" => "coupons","title" => "美容美发","catid" => 17),
					"coupons_18" => array("model" => "coupons","title" => "汽车服务","catid" => 18),
					"coupons_19" => array("model" => "coupons","title" => "生活购物","catid" => 19)
					),
		
	    "l_activity" => array(
					"activity_0" => array("model" => "activity","title" => "全部","catid" => 0),
					"activity_1" => array("model" => "activity","title" => "官方组织","catid" => 1),
					"activity_2" => array("model" => "activity","title" => "网友发起","catid" => 2)
					),

	    "l_jifen" => array(
					"jifen_0" => array("model" => "jifen","title" => "全部","catid" => 0),
					"jifen_1" => array("model" => "jifen","title" => "话费","catid" => 1),
					"jifen_2" => array("model" => "jifen","title" => "流量包","catid" => 2)
					),
		
	    "l_apps" => array(
					"express" => array("model" => "apps","title" => "快递查询"),
					"bus" => array("model" => "apps","title" => "汽车时刻"),
					"train" => array("model" => "apps","title" => "火车时刻"),
					"business_28" => array("model" => "business","title" => "快递公司","catid" => 28),
					"business_21" => array("model" => "business","title" => "点外卖","catid" => 21),
					"business_24" => array("model" => "business","title" => "咖啡馆","catid" => 24),
					"business_25" => array("model" => "business","title" => "KTV","catid" => 25),
					"business_17" => array("model" => "business","title" => "美容美发","catid" => 17),
					"tel" => array("model" => "apps","title" => "常用电话"),
					"daijia" => array("model" => "url","title" => "贵阳代驾","url" => "more_content","img" => "skin/images/app/browser.png"),
					"localapp" => array("model" => "apps","title" => "贵阳APP"),
					"jifen" => array("model" => "jifen","title" => "积分商城")
					),

	    "l_tab_default" => array(
					"content" => "today",
					"life" => "coupons",
					"news" => "news_8",
					"picture" => "picture_0",
					"video" => "video_0",
					"coupons" => "coupons_0",
					"activity" => "activity_0"
					),
		
	    "l_catid_default" => array(
					"news" => "6",
					"picture" => "20",
					"video" => "40",
					"coupons" => "0",
					"activity" => "0",
					"business" => "7"
					),

	    "l_connect" => array(
					"qqweibo" => "801407387",
					"sina" => "2595200536"
					)
	);
	
	return $pattern;
}

?>