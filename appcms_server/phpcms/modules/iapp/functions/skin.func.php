<?php

/*
*默认配置
*/
function fun_skin_def() {
	$skin = Array(
					'blue' => Array(
							'key' => 'blue',
							'title'=>'默认风格',
							'skin'=>'#header {background-color:#3498DB;} #header .tab div.cur {color:#3498DB;} #header .stab div.cur span {border-bottom-color:#3498DB;} .btn .button{background-color:#3498DB;}',
							'vskin'=>'background-color:#3498DB;'
						),

					'black'=>Array(
							'key'=>'black',
							'title'=>'酷黑',
							'skin'=>'#header {background-color:#2B2B2B;} #header .tab div.cur {color:#2B2B2B;} #header .stab div.cur span {border-bottom-color:#2B2B2B;} .btn .button{background-color:#2B2B2B;}',
							'vskin'=>'background-color:#2B2B2B;'
						),
						
					'green' => Array(
							'key'=>'green',
							'title'=>'淡绿',
							'skin'=>'#header {background-color:#84C009;} #header .tab div.cur {color:#84C009;} #header .stab div.cur span {border-bottom-color:#84C009;} .btn .button{background-color:#84C009;}',
							'vskin'=>'background-color:#84C009;'
						),

					'red' => Array(
							'key'=>'red',
							'title'=>'中国红',
							'skin'=>'#header {background-color:#D3241D;} #header .tab div.cur {color:#D3241D;} #header .stab div.cur span {border-bottom-color:#D3241D;} .btn .button{background-color:#D3241D;}',
							'vskin'=>'background-color:#D3241D;'
						)

				);
			
	
	return $skin;
}

?>