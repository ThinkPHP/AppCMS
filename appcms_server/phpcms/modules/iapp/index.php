<?php 
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_sys_class('format', '', 0);
class index {
	function __construct() {		
		$this->db = pc_base::load_model('content_model');
		$this->member_db = pc_base::load_model('member_model');
		$this->iapp_model_db = pc_base::load_model('iapp_model');
		
		$this->config = getcache('iapp', 'commons');
		if(intval($_GET['siteid']) > 0){
			$this->siteid = intval($_GET['siteid']);
		}else{
			$this->siteid = $this->config[setting][siteid]? $this->config[setting][siteid]:1;
		}
	}
	
	public function init() {
		$promotion = array();
		$promotion = $this->iapp_model_db->get_one(array('mkey'=>'promotion'));
		$promotion = string2array($promotion[setting]);
		
		$upgrade = $this->iapp_model_db->get_one(array('mkey'=>'upgrade'));
		$upgrade = string2array($upgrade[setting]);
		
		$SEO = seo($this->siteid);
		include template('iapp', 'index');
	}
	
	public function video() {
		$html +='<p><video controls="controls" width="100%" height="300"  autoplay="autoplay" id="video" poster="" src=""></p>';
	}
}
?>