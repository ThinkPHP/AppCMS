<?php
defined('IN_ADMIN') or exit('No permission resources.'); 
include $this->admin_tpl('header','admin');?>
<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>iappadmin/iapp.css"/>
<form action="?m=iapp&c=manages&a=about" method="post" id="myform">
<input type="hidden" value='<?php echo $siteid?>' name="siteid">

<div class="pad-lr-10">
	<fieldset>
		<legend>关于我们设置</legend>
		
		<div id="items">
		<?php
			$i=0;
			foreach($info[setting] as $k=>$v){
		?>   
			<fieldset id="item_<?php echo $i;?>" title="<?php echo $i;?>">
				项目名称<input type="text" class="input-text" name="data[<?php echo $i?>][label]" id="data_<?php echo $i?>_label" size="20" value="<?php echo $v[label]?>"/> 
				项目值<input type="text" class="input-text" name="data[<?php echo $i?>][val]" id="data_<?php echo $i?>_val" size="20" value="<?php echo $v[val]?>"/>
				链接/事件<input type="text" class="input-text" name="data[<?php echo $i?>][url]" id="data_<?php echo $i?>_url" size="40" value="<?php echo $v[url]?>"/>
				<a href="#" onclick="$('#item_<?php echo $i;?>').remove();">移除</a>
			</fieldset>
		<?php
			$i++;
			}
		?>
		</div>
		<div class="bk10"></div>
		<input type="button" id="additem" value=" 添加一行 " class="button">
	</fieldset>
	<div class="bk15"></div>
    <input type="submit" id="dosubmit" name="dosubmit" class="button" value="<?php echo L('submit')?>" />
    <input type="reset" value=" <?php echo L('重置')?> " class="button">
</div>
</form>

<script type="text/javascript">
window.top.$("#display_center_id").css("display","none");

$("#additem").click(function(){
	var n = parseInt($('#items fieldset:last').attr('title'))+1;
	if(n){
		n = n;
	}else{
		n = 1;
	}
		
	var str = '<fieldset id="item_'+n+'" title="'+n+'">'+
			  '项目名称<input type="text" name="data['+n+'][label]" id="data_'+n+'_label" value="" size="20" class="input-text" />'+ 
			  '项目值<input type="text" name="data['+n+'][val]" id="data_'+n+'_val" value="" size="20" class="input-text" />'+
			  '链接/事件<input type="text" name="data['+n+'][url]" id="data_'+n+'_url" value="" size="40" class="input-text" />'+
			  '<a href="#" onclick="$(\'#item_'+n+'\').remove();">移除</a>'+
		      '</fieldset>';
	$("#items").append(str);
});

</script>
</body>
</html>