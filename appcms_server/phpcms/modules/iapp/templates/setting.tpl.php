<?php
defined('IN_ADMIN') or exit('No permission resources.'); 
include $this->admin_tpl('header','admin');?>
<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>iappadmin/iapp.css"/>
<form action="?m=iapp&c=manages&a=setting" method="post" id="myform">
<input type="hidden" value='<?php echo $siteid?>' name="siteid">

<div class="pad-lr-10">
<div class="col-tab">
	<ul class="tabBut cu-li">
		<li id="tab_setting_1" class="on" onclick="SwapTab('setting','on','',3,1);">基本参数设置</li>
		<li id="tab_setting_2" onclick="SwapTab('setting','on','',3,2);">客户端打包</li>
		<li id="tab_setting_3" onclick="SwapTab('setting','on','',3,3);">系统信息</li>
	</ul>

	
<div id="div_setting_1" class="contentList pad-10">
	<table width="100%"  class="table_form">
    
	<tr>
    <th width="150">绑定站点：</th>
    <td class="y-bg"><input type="text" class="input-text" name="data[setting][siteid]" id="siteid" size="10" value="<?php echo $data[setting][siteid]?>"/>绑定站点ID</td>
    </tr>

   
    <tr>
    <th width="150">新闻列表条数</th>
    <td class="y-bg"><input type="text" class="input-text" name="data[setting][newslistnum]" id="newslistnum" size="10" value="<?php echo $data[setting][newslistnum]?>"/> 条</td>
    </tr>
	
	<tr>
    <th width="150">新闻缺损默认栏目</th>
    <td class="y-bg"><input type="text" class="input-text" name="data[setting][defaultcatid]" id="defaultcatid" size="10" value="<?php echo $data[setting][defaultcatid]?>"/>只能是末级分类</td>
    </tr>

	<tr>
    <th width="150">图片列表分页条数</th>
    <td class="y-bg"><input type="text" class="input-text" name="data[setting][piclistnum]" id="piclistnum" size="10" value="<?php echo $data[setting][piclistnum]?>"/> 条</td>
    </tr>
	
	<tr>
    <th width="150">视频列表分页条数</th>
    <td class="y-bg"><input type="text" class="input-text" name="data[setting][videolistnum]" id="videolistnum" size="10" value="<?php echo $data[setting][videolistnum]?>"/> 条</td>
    </tr>
	
	<tr>
    <th width="150">说说分页条数</th>
    <td class="y-bg"><input type="text" class="input-text" name="data[setting][talklistnum]" id="talklistnum" size="10" value="<?php echo $data[setting][talklistnum]?>"/> 条</td>
    </tr>
	
	<tr>
    <th width="150">本地商家分页条数</th>
    <td class="y-bg"><input type="text" class="input-text" name="data[setting][lifelistnum]" id="lifelistnum" size="10" value="<?php echo $data[setting][lifelistnum]?>"/> 条</td>
    </tr>
	
	<tr>
    <th width="150">商家缺损默认栏目</th>
    <td class="y-bg"><input type="text" class="input-text" name="data[setting][businesscatid]" id="businesscatid" size="10" value="<?php echo $data[setting][businesscatid]?>"/></td>
    </tr>
	
	<tr>
    <th width="150">优惠券是否发送短信</th>
    <td class="y-bg"><input type="text" class="input-text" name="data[setting][iscoupons]" id="iscoupons" size="10" value="<?php echo $data[setting][iscoupons]?>"/>0不提示，1提示</td>
    </tr>
	
	<tr>
    <th width="150">活动是否发送短信</th>
    <td class="y-bg"><input type="text" class="input-text" name="data[setting][isactivity]" id="isactivity" size="10" value="<?php echo $data[setting][isactivity]?>"/>0不提示，1提示</td>
    </tr>
	
	<tr>
    <th width="150">注册成功是否短信提示</th>
    <td class="y-bg"><input type="text" class="input-text" name="data[setting][isregts]" id="isregts" size="10" value="<?php echo $data[setting][isregts]?>"/>0不提示，1提示</td>
    </tr>
	
	<tr>
    <th width="150">未注册用户评论时显示</th>
    <td class="y-bg"><input type="text" class="input-text" name="data[setting][noregcomment]" id="noregcomment" size="10" value="<?php echo $data[setting][noregcomment]?>"/>如：最前端贵阳网友</td>
    </tr>
	
	
	
    <tr>
    <th width="150">缩略图</th>
    <td class="y-bg"><input type="text" class="input-text" name="data[setting][thumb_w]" id="thumb_w" size="5" value="<?php echo $data[setting][thumb_w]?>"/>px　*　<input type="text" class="input-text" name="data[setting][thumb_h]" id="thumb_h" size="5" value="<?php echo $data[setting][thumb_h]?>"/>px</td>
    </tr>
	
	
	<tr>
    <th width="150"><?php echo L('推荐大图缩略图大小')?></th>
    <td class="y-bg"><input type="text" class="input-text" name="data[setting][p_thumb_w]" id="p_thumb_w" size="5" value="<?php echo $data[setting][p_thumb_w]?>"/>px　*　<input type="text" class="input-text" name="data[setting][p_thumb_h]" id="p_thumb_h" size="5" value="<?php echo $data[setting][p_thumb_h]?>"/>px</td>
    </tr>
    
               
</table>

</div>
	
<div id="div_setting_2" class="contentList pad-10 hidden">
	
	<h3 class="iappmenuh3">第一步，设置客户端名称与打包服务账号</h3>
	<div class="bk10"></div>
	<div class="explain-col">
		<table width="100%"  class="table_form">
		<tr>
		<th width="150">应用名称：</th>
		<td class="y-bg"><input type="text" class="input-text" name="data[info][sitename]" id="sitename" size="30" value="<?php echo $data[info][sitename]?>"/></td>
		</tr>
		<tr>
		<th width="150">客户端打包用户名：</th>
		<td class="y-bg">
		<input type="text" class="input-text" name="data[appcan][username]" id="username" size="30" value="<?php echo $data[appcan][username]?>"/> 没有账号？【<a href="http://dev.appcan.cn/user/register" target="_blank">注册</a>】</td>
		</tr>
		<tr>
		<th>密码：</th>
		<td class="y-bg">
		<input type="password" class="input-text" name="data[appcan][password]" id="password" size="30" value="<?php echo $data[appcan][password]?>"/></td>
		</tr>
		<?php if($data[appcan][appid]){?>
		<tr>
		<th width="100">appid：</th>
		<td class="y-bg">
		<input type="text" class="input-text" name="data[appcan][appid]" id="appid" size="30" value="<?php echo $data[appcan][appid]?>" readonly="true"/> 系统自动生成，请勿修改   <label><input name="isnew" type="checkbox" value="1" />  重新打包</label>
		</td>
		</tr>
		<tr>
		<th width="100">appguid：</th>
		<td class="y-bg">
		<input type="text" class="input-text" name="data[appcan][appguid]" id="appguid" size="50" value="<?php echo $data[appcan][appguid]?>" readonly="true"/> 系统自动生成，请勿修改</td>
		</tr>
		<?php }?>
		</table>
	</div>

	<h3 class="iappmenuh3">第二步，打包客户端</h3>
	<ul class="iappmenu">
	<?php
		if($data[appcan][appid]!="")
		{
			if($status==0){
				echo '<li class="cur"><span>客户端初始化中...</span></li>';
			}else{
				echo '<li class="cur"><span><a href="http://dev.appcan.cn/app/index" target="_blank">客户端初始化完成,可以点这里去打包并下载你的客户端了！</a></span></li>';
			}
		}else{
			echo '<li ><span>请先第一步设置打包服务器账号</span></li>';
		}
	?>
	</ul>
   
</div>

<div id="div_setting_3" class="contentList pad-10 hidden">
	
		<div class="col-2 lf mr10">
			<h6><?php echo L('系统信息')?></h6>
			<div class="pad-10">
			<?php echo L('APPCMS程序版本：')?>appcms <?php echo $info[c][version];?>  Release <?php echo $info[c][release];?> [<a href="http://www.appcms.org" target="_blank"><?php echo L('查看最新版本')?></a>]<br />
			<?php echo L('操作系统：')?><?php echo $sysinfo['os']?> <br />
			<?php echo L('服务器软件：')?><?php echo $sysinfo['web_server']?> <br />
			<?php echo L('MySQL 版本：')?><?php echo $sysinfo['mysqlv']?><br />
			<?php echo L('上传文件：')?><?php echo $sysinfo['fileupload']?><br />	
			</div>
		</div>
	
		<div class="col-2 lf mr10">
			<h6><?php echo L('APPCMS版本信息')?></h6>
			<div class="pad-10">
			版权所有：www.appcms.org<br />
			总策划：传朴TEAM<br />
			APPCMS官方网站：<a href="http://www.appcms.org/" target="_blank">http://www.appcms.org/</a><br />
			当前版本：<?php echo $info[c][version];?><br />
			最新版本：<?php echo $info[n][version];?><br />
			
			</div>
		</div>
		<div class="col-2 lf mr10">
			<h6><?php echo L('授权信息')?></h6>
			<div class="pad-10">
			授权类型：<?php if($info[n][license]){?>
			（<?php echo $info[n][license];?><a href="http://www.appcms.org/index.php?weburl=<?php echo $info[url];?>&m=license&c=index&a=certificate" target="_blank" style="color:red">查看授权</a>）
			<?php }else{?>
			未授权（<a href="http://www.appcms.org/index.php?m=content&c=index&a=lists&catid=10&menuid=2" target="_blank" style="color:red">点击购买</a>）
			<?php }?>		
			</div>
		</div>
	
</div>

	<div class="bk15"></div>
    <input type="submit" id="dosubmit" name="dosubmit" class="button" value="<?php echo L('submit')?>" />
    <input type="reset" value=" <?php echo L('重置')?> " class="button">
</div>
</div>

</form>
<script type="text/javascript">
window.top.$("#display_center_id").css("display","none");

function SwapTab(name,cls_show,cls_hide,cnt,cur){
	for(i=1;i<=cnt;i++){
		if(i==cur){
			 $('#div_'+name+'_'+i).show();
			 $('#tab_'+name+'_'+i).attr('class',cls_show);
		}else{
			 $('#div_'+name+'_'+i).hide();
			 $('#tab_'+name+'_'+i).attr('class',cls_hide);
		}
	}
}
</script>
</body>
</html>