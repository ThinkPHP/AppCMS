<?php
defined('IN_ADMIN') or exit('No permission resources.'); 
include $this->admin_tpl('header','admin');?>
<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>iappadmin/iapp.css"/>
<form action="?m=iapp&c=manages&a=upgrade" method="post" id="myform">
<div class="pad-lr-10">
	<fieldset>
		<legend>ios版本</legend>
		<table width="100%"  class="table_form">
		 <tr>
		<th width="150"><?php echo L('iPhone客户端最新版本号')?></th>
		<td class="y-bg"><input type="text" class="input-text" name="info[new_iphone_ver]" id="new_iphone_ver" size="30" value="<?php echo $data[new_iphone_ver]?>"/>
		1.1 </td>
		</tr>
		<tr>
		<th width="150"><?php echo L('AppStore应用品商店地址')?></th>
		<td class="y-bg"><input type="text" class="input-text" name="info[iphone_updateFileUrl]" id="iphone_updateFileUrl" size="50" value="<?php echo $data[iphone_updateFileUrl]?>"/></td>
		</tr>
		<tr>
		<th width="150"><?php echo L('Ipa本地文件下载')?></th>
		<td class="y-bg">
		<?php echo form::upfiles('info[iphone_File]', 'iphone_File', $data[iphone_File], 'iapp','','','','','ipa')?>
		</td>
		</tr>
		<tr>
		<th width="150"><?php echo L('文件大小')?></th>
		<td class="y-bg"><input type="text" class="input-text" name="info[iphone_fileSize]" id="iphone_fileSize" size="10" value="<?php echo $data[iphone_fileSize]?>"/>KB</td>
		</tr>
		
		</table> 
	</fieldset>

	<div class="bk15"></div>
	<fieldset>
		<legend>Android版本</legend>
		<table width="100%"  class="table_form">
		<tr>
		<th width="150"><?php echo L('Android客户端最新版本号')?></th>
		<td class="y-bg"><input type="text" class="input-text" name="info[new_android_ver]" id="new_android_ver" size="30" value="<?php echo $data[new_android_ver]?>"/>1.1</td>
		</tr>
		<tr>
		<th width="150"><?php echo L('应用品商店地址')?></th>
		<td class="y-bg"><input type="text" class="input-text" name="info[android_updateFileUrl]" id="android_updateFileUrl" size="50" value="<?php echo $data[android_updateFileUrl]?>"/></td>
		</tr>
		<tr>
		<th width="150"><?php echo L('Apk本地文件下载')?></th>
		<td class="y-bg">
		<?php echo form::upfiles('info[android_File]', 'android_File', $data[android_File], 'iapp','','','','','apk')?>
		</td>
		</tr>
		<tr>
		<th width="150"><?php echo L('文件大小')?></th>
		<td class="y-bg"><input type="text" class="input-text" name="info[android_fileSize]" id="android_fileSize" size="10" value="<?php echo $data[android_fileSize]?>"/>KB</td>
		</tr>
		</table> 
	</fieldset>	
	
	
	<div class="bk15"></div>
    <input type="submit" id="dosubmit" name="dosubmit" class="button" value="<?php echo L('submit')?>" />
    <input type="reset" value=" <?php echo L('重置')?> " class="button">
</div>
</form>
</body>
</html>