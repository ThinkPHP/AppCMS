<?php
defined('IN_ADMIN') or exit('No permission resources.'); 
include $this->admin_tpl('header','admin');?>
<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>iappadmin/iapp.css"/>
<script type="text/javascript" src="<?php echo JS_PATH?>jquery.dragsort-0.5.1.js"></script>
<form action="?m=iapp&c=manages&a=pattern" method="post" id="myform">
<input type="hidden" value='<?php echo $siteid?>' name="siteid">
<div class="pad-lr-10">
	
	<!--
	<div class="explain-col">【<a href="index.php?m=category&c=category&a=public_cache_all&menuid=<?php echo $_GET['menuid']?>" style="color:red">更新所有栏目缓存</a>】</div>
	-->
	
	
	
	
	
	<h3 class="iappmenuh3">基础语言【l_common】</h3>
	
		<ul class="iappmenu" id="ul_l_common">
		<?php
			foreach($pattern_all[l_common] as $k=>$v){
			
				if(!$pattern_my[l_common][$k]){
					$pattern_my[l_common][$k] = $v[val];
				}
		?>
		<li onclick="set_common(this)">
		<span id="l_common__<?php echo $k;?>__v"><?php echo $pattern_my[l_common][$k];?></span><h3><?php echo $v[label];?></h3>
		<input type="hidden" id="l_common_<?php echo $k;?>" name="data[l_common][<?php echo $k;?>]" value="<?php echo $pattern_my[l_common][$k];?>">
		</li>
		<?php
			}
		?>
		</ul>
		
	<h3 class="iappmenuh3">首页栏目设置【l_index】，拖动可以进行排序，双击可以编辑文字</h3>

		<ul class="iappmenu" id="ul_l_index">
		<?php echo $l_index_html;?>
		</ul>
		<script type="text/javascript">
			$("#ul_l_index").dragsort();
		</script>

	<h3 class="iappmenuh3">底部导航设置【l_footer】，最多可以选择5个按钮，拖动可以进行排序，双击可以编辑文字</h3>
		
		<ul class="iappmenu" id="ul_l_footer">
		<?php echo $l_footer_html;?>
		</ul>
		<script type="text/javascript">
			$("#ul_l_footer").dragsort();
		</script>
		
		<?php echo $html;?>
		
	<!--
	<h3 class="iappmenuh3">新闻中心头部导航设置【l_content】，最多可以选择5个按钮，拖动可以进行排序，双击可以编辑文字</h3>
		<ul class="iappmenu" id="ul_l_content">
		<?php echo $l_content_html;?>
		</ul>
		<script type="text/javascript">
			$("#ul_l_content").dragsort();
		</script>
	
	<h3 class="iappmenuh3">生活中心头部导航设置【l_life】，最多可以选择5个按钮，拖动可以进行排序，双击可以编辑文字</h3>
		<ul class="iappmenu" id="ul_l_life">
		<?php echo $l_life_html;?>
		</ul>
		<script type="text/javascript">
			$("#ul_l_life").dragsort();
		</script>
		
	<h3 class="iappmenuh3">新闻页面栏目设置【l_news】，最多可以选择5个栏目，拖动可以进行排序，双击可以编辑文字</h3>
		<ul class="iappmenu" id="ul_l_news">
		<?php echo $l_news_html;?>
		</ul>
		<script type="text/javascript">
			$("#ul_l_news").dragsort();
		</script>
	
	<h3 class="iappmenuh3">图片页面栏目设置【l_picture】，最多可以选择5个栏目，拖动可以进行排序，双击可以编辑文字</h3>
		<ul class="iappmenu" id="ul_l_picture">
		<?php echo $l_picture_html;?>
		</ul>
		<script type="text/javascript">
			$("#ul_l_picture").dragsort();
		</script>
	
	<h3 class="iappmenuh3">视频页面栏目设置【l_video】，最多可以选择5个栏目，拖动可以进行排序，双击可以编辑文字</h3>
		<ul class="iappmenu" id="ul_l_video">
		<?php echo $l_video_html;?>
		</ul>
		<script type="text/javascript">
			$("#ul_l_video").dragsort();
		</script>
	
	<h3 class="iappmenuh3">优惠券页面栏目设置【l_coupons】，最多可以选择5个栏目，拖动可以进行排序，双击可以编辑文字</h3>
		<ul class="iappmenu" id="ul_l_coupons">
		<?php echo $l_coupons_html;?>
		</ul>
		<script type="text/javascript">
			$("#ul_l_coupons").dragsort();
		</script>
	
	<h3 class="iappmenuh3">活动页面栏目设置【l_activity】，最多可以选择5个栏目，拖动可以进行排序，双击可以编辑文字</h3>
		<ul class="iappmenu" id="ul_l_activity">
		<?php echo $l_activity_html;?>
		</ul>
		<script type="text/javascript">
			$("#ul_l_activity").dragsort();
		</script>
	
	<h3 class="iappmenuh3">积分商城栏目设置【l_jifen】，最多可以选择5个栏目，拖动可以进行排序，双击可以编辑文字</h3>
		<ul class="iappmenu" id="ul_l_jifen">
		<?php echo $l_jifen_html;?>
		</ul>
		<script type="text/javascript">
			$("#ul_l_jifen").dragsort();
		</script>
	
	<h3 class="iappmenuh3">百宝箱模块设置【l_apps】，模块数量不限，拖动可以进行排序，双击可以编辑文字</h3>
		<ul class="iappmenu" id="ul_l_apps">
		<?php echo $l_apps_html;?>
		</ul>
		<script type="text/javascript">
			$("#ul_l_apps").dragsort();
		</script>
	-->
	
	<h3 class="iappmenuh3">首页布局设置【pattern_type】</h3>
		<table width="100%" class="table_form">
		<tbody>
			<tr>
				<th width="120">首页布局设置：</th>
				<td class="y-bg">
				<label><input type="radio" value="portal" name="data[pattern_type]" <?php if($pattern_my[pattern_type]=='portal'){echo 'checked="checked"';}?> > 门户 </label> 
				<label><input type="radio" value="simple" name="data[pattern_type]" <?php if($pattern_my[pattern_type]=='simple'){echo 'checked="checked"';}?> > 简洁 </label> 
				<label><input type="radio" value="winphone" name="data[pattern_type]" <?php if($pattern_my[pattern_type]=='winphone'){echo 'checked="checked"';}?> > winphone </label> 
				</td>
			</tr>
		</tbody>
		</table>
	
	<h3 class="iappmenuh3">头部默认TAB设置【l_tab_default】</h3>
		<?php echo $l_tab_default_html;?>
	
	<h3 class="iappmenuh3">默认栏目设置【l_catid_default_html】</h3>
		<?php echo $l_catid_default_html;?>
		
	</fieldset>
	<div class="bk10"></div>
	
	<table width="100%"  class="table_form">
	<tr>
    <th width="100">布局模式选择：</th>
    <td class="y-bg">
		<label><input type="checkbox" value="1" id="isdefault" name="isdefault"> 恢复默认设置</label>
	</td>
    </tr>
	
    </table> 
	
	<div class="bk15"></div>
    <input type="submit" id="dosubmit" name="dosubmit" class="button" value="<?php echo L('submit')?>" />
    <input type="reset" value=" <?php echo L('重置')?> " class="button">
</div>
</form>

<script type="text/javascript">
<?php echo $pattern_js ?>

function set_common(obj){
	
	var ids = $(obj).find("span").attr('id');
	var _ids=ids.split('__');
	var g = _ids[0];
	var id = _ids[1];
	var t = $(obj).find("h3").html();
	var thtml = '<p style="padding:4px">变量名称：'+t+'<br/>变量：'+g+'['+id+']</p>';
	
	window.top.art.dialog(
	{
		id:'set_content',
		content: thtml+'<p style="padding:4px"><input id="coentent_input" value="'+$("#"+g+'__'+id+'__v').html()+'" style="width:15em; padding:4px" /></p>',
		title:'修改变量', 
		width:'500', 
		height:'300'
	}, 
	function(){
		var v = window.top.$("#coentent_input").val();
		$("#"+g+'__'+id+'__v').html(v);
		$("#"+g+'_'+id).val(v);
	}, 
	function(){
		window.top.art.dialog({id:'get_content'}).close()
	});
}

function getli(obj){
	
	var ids = $(obj).find("span").attr('id');
	var _ids=ids.split('__');
	var g = _ids[0];
	var id = _ids[1];
	var t = $(obj).find("h3").html();

	var _g = eval(g);
	var _r = _g[id];
	
	var thtml = '<table width="100%" class="table_form">';
	thtml +='<tr>';
	thtml +='<td width="50" style="text-align:right; padding:5px;">变量：</td>';
	thtml +='<td style="padding:5px;">'+g+'['+id+']</td>';
	thtml +='</tr>';
	
	
	for (k in _r) {
		var readonly = ' readonly="true" ';
		if(k=='title' || k=="catid" || k=="filter" || k=="img" || k=="islogin" || (k=="url" && _g[id]['model'] =='url')){
			readonly = '';
		}
		thtml +='<tr>';
		thtml +='<td width="50" style="text-align:right; padding:5px;">'+k+'：</td>';
		thtml +='<td style="padding:5px;"><input id="'+g+'_'+k+'" value="'+_r[k]+'" style="width:15em; padding:4px" '+readonly+' /></td>';
		thtml +='</tr>';
	}
	thtml += "</table>";

	window.top.art.dialog(
	{
		id:'set_content',
		content:thtml,
		title:'修改变量', 
		width:'500', 
		height:'300',
		button: [
			{
				name: '确定',
				callback: function () {
					$(obj).addClass("cur");
					var thtml = '';
					for (k in _r) {
						var v = window.top.$('#'+g+'_'+k).val();
						thtml +='<input type="hidden" id="'+g+'_'+id+'_'+k+'" name="data['+g+']['+id+']['+k+']" value="'+v+'" >';
						if(k=='title'){
							$("#"+g+'__'+id+'__v').html(v);
						}
					}
					$(obj).find("p").html(thtml);
					
					//return false;
				}
			},
			{
				name: '取消',
				callback: function () {
					$(obj).removeClass("cur");
					$(obj).find("p").html('');
					window.top.art.dialog({id:'get_content'}).close()
				}
			},
			{
				name: '关闭',
				focus: true
			}
		]
	});
}

function select_c(obj){
	$(obj).addClass("cur");
	//$(obj).removeClass("cur");
}

function cur_c(obj){
	if($(obj).attr("class")=='cur'){
		$(obj).removeClass("cur");
		$(obj).find("p").html('');
	}else{
		$(obj).addClass("cur");
	}
}


function printobj(obj){
	var msg = '';
	for (var item in obj) {
		msg += item + ":" + obj[item]+'\n';
	}
	alert(msg);
}
</script>
</body>
</html>