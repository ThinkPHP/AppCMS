<?php
defined('IN_ADMIN') or exit('No permission resources.'); 
include $this->admin_tpl('header','admin');?>
<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>iappadmin/iapp.css"/>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>colorpicker.js"></script>

<div class="pad-lr-10">

<div class="col-tab">
	<ul class="tabBut cu-li">
		<li id="tab_skin_1" class="on" onclick="SwapTab('skin','on','',2,1);">风格选择</li>
		<li id="tab_skin_2" onclick="SwapTab('skin','on','',2,2);">风格制作</li>
	</ul>
<div id="div_skin_1" class="contentList pad-10">
<form action="?m=iapp&c=manages&a=skin" method="post" id="myform">



<!--
<h3 class="iappmenuh3">风格设置</h3>
<div class="pad-10">

<label><input name="data[skintype]" value="portal" id="data_skintype_plane" <?php if($info[skintype]=='portal'){ echo 'checked="checked"';} ?> type="radio"> 门户风格 </label> 
<label><input name="data[skintype]" value="winphone" id="data_skintype_winphone" <?php if($info[skintype]=='winphone'){ echo 'checked="checked"';} ?> type="radio"> winphone风格 </label> 
<label><input name="data[skintype]" value="simple" id="data_skintype_simple" <?php if($info[skintype]=='simple'){ echo 'checked="checked"';} ?> type="radio"> 简约风格 </label> 


</div>
-->



<h3 class="iappmenuh3">配色方案设置</h3>
<ul class="iappskin" id="myskin">
<?php
$i=0;
foreach($skin_info as $v){
?>
<li>
<h3><?php echo $v[title]?></h3>
<span style="<?php echo $v[vskin]?>" title="<?php echo $v[key]?>"></span>
<?php
if($v[key]==$info[key]){
	echo "<div></div>";
}
?>
</li>
<?php
	$i++;
}
?>
</ul>

<h3 class="iappmenuh3">背景颜色设置</h3>
<div class="pad-10">
<input id="style_color" type="text" size="12" value="<?php echo $info[bg]?>" name="data[bg]">
<img src="<?php echo IMG_PATH?>icon/colour.png" width="15" height="16" onclick="colorpicker('title_colorpanel','set_title_color');" style="cursor:hand"/> 
<span id="title_colorpanel" style="position:absolute;" class="colorpanel"></span>
</div>


<input type="hidden" class="input-text" name="data[key]" id="skin_key" size="30" value="<?php echo $info[key]?>"/>

<div class="bk15"></div>
    <input type="submit" id="dosubmit" name="dosubmit" class="button" value="<?php echo L('submit')?>" />
</form>

</div>

<div id="div_skin_2" class="contentList pad-10 hidden">
<form action="?m=iapp&c=manages&a=skin_renew" method="post" id="myform">
<div id="items">
	<?php
		$i=0;
		foreach($skin_info as $k=>$v){
	?>   
		<fieldset id="item_<?php echo $i;?>" title="<?php echo $i;?>">
			  <table width="100%"  class="table_form">
				<tr>
				<th width="139">风格名称</th>
				<td class="y-bg">
				 <input type="text" class="input-text" name="data[title][]" id="data_<?php echo $i?>_title" size="20" value="<?php echo $v[title]?>"/> 
				 风格KEY
				 <input type="text" class="input-text" name="data[key][]" id="data_<?php echo $i?>_key" size="20" value="<?php echo $v[key]?>"/>
				</td>
				<td rowspan="3" class="y-bg" width="50">
				<a href="#" onclick="$('#item_<?php echo $i;?>').remove();">移除</a>
				</td>
				</tr>
				
				<tr>
				<th>风格</th>
				<td class="y-bg">
				  <textarea name='data[skin][]' id='data_<?php echo $i?>_skin' style="width:98%;height:50px"><?php echo $v[skin]?></textarea>
				</td>
				</tr>
				
				<tr>
				<th>对应样式</th>
				<td class="y-bg">
				  <input type="text" class="input-text" name="data[vskin][]" id="data_<?php echo $i?>_vskin" size="80" value="<?php echo $v[vskin]?>"/>
				</td>
				</tr>
			</table>  
		</fieldset>
	<?php
		$i++;
		}
	?>
		
	</div>
	
	

	<div class="bk10"></div>
	<input type="button" id="additem" value=" 添加一行 " class="button">

	
	<div class="bk15"></div>
    <input type="submit" id="dosubmit" name="dosubmit" class="button" value="<?php echo L('submit')?>" />
    <input type="reset" value=" <?php echo L('重置')?> " class="button">
	</form>
</div>




	
</div>


<script type="text/javascript">
window.top.$("#display_center_id").css("display","none");

$("#additem").click(function(){
	var n = parseInt($('#items fieldset:last').attr('title'))+1;
	if(n){
		n = n;
	}else{
		n = 1;
	}
		
	var str = '<fieldset id="item_'+n+'" title="'+n+'">'+
			  '<table width="100%"  class="table_form">'+
			  '<tr>'+
			  '<th width="139">风格名称</th>'+
			  '<td class="y-bg">'+
			  '<input type="text" name="data[title][]" id="data_'+n+'_title" value="" size="20" class="input-text" /> 风格KEY '+ 
			  '<input type="text" name="data[key][]" id="data_'+n+'_key" value="" size="20" class="input-text" />'+ 
				
			  '</td>'+
			  '<td rowspan="3" class="y-bg" width="50">'+
			  '<a href="#" onclick="$(\'#item_'+n+'\').remove();">移除</a>'+
			  '</td>'+
			  '</tr>'+
			  
			  '<tr>'+
			  '<th>风格</th>'+
			  '<td class="y-bg">'+
			  '<textarea name="data[skin][]" id="data_'+n+'_skin" style="width:98%;height:50px"></textarea>'+
			  '</td>'+
			  '</tr>'+
			  '<tr>'+
			  '<th>对应样式</th>'+
			  '<td class="y-bg">'+
			  '<input type="text" name="data[vskin][]" id="data_'+n+'_vskin" value="" size="80" class="input-text" />'+
			  '</td>'+
			  '</tr>'+
			  '</table>'+
		      '</fieldset>';
	$("#items").append(str);
});

$("#myskin li").click(function(){
	$("#myskin li").children("div").remove();
	$(this).append("<div></div>");	
	var key = $(this).children("span").attr("title");
	$("#skin_key").val(key);
});

function SwapTab(name,cls_show,cls_hide,cnt,cur){
	for(i=1;i<=cnt;i++){
		if(i==cur){
			 $('#div_'+name+'_'+i).show();
			 $('#tab_'+name+'_'+i).attr('class',cls_show);
		}else{
			 $('#div_'+name+'_'+i).hide();
			 $('#tab_'+name+'_'+i).attr('class',cls_hide);
		}
	}
}

function set_title_color(color) {
	$('#style_color').css('color',color);
	$('#style_color').val(color);
}

</script>
</body>
</html>