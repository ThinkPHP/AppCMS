<?php
defined('IN_ADMIN') or exit('No permission resources.'); 
include $this->admin_tpl('header','admin');?>
<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>iappadmin/iapp.css"/>
<form action="?m=iapp&c=manages&a=promotion" method="post" id="myform">

<div class="pad-lr-10">
	<fieldset>
	<legend>关于我们设置 [<a target="_blank" href="<?php echo $APP_PATH;?>index.php?m=iapp">访问前台</a>]</legend>
	<div id="items">
	<?php
		$i=0;
		foreach($data as $v){
	?>   
		<fieldset id="item_<?php echo $i;?>" title="<?php echo $i;?>">
			<table width="100%"  class="table_form">
				<tr>
				<th width="139">背景图片</th>
				<td class="y-bg" width="420">
				<?php echo form::images('info['.$i.'][bg]', 'photos_'.$i.'_bg', $v[bg], 'iapp')?>
				</td>
				<td rowspan="3" class="y-bg" >
				<a href="#" onclick="$('#item_<?php echo $i;?>').remove();">移除</a>
				</td>
				</tr>
				<tr>
				<th>软件图片1</th>
				<td class="y-bg">
				  <?php echo form::images('info['.$i.'][img1]', 'photos_'.$i.'_img1', $v[img1], 'iapp')?>
				</td>
				</tr>
				<tr>
				<th>软件图片2</th>
				<td class="y-bg">
				  <?php echo form::images('info['.$i.'][img2]', 'photos_'.$i.'_img2', $v[img2], 'iapp')?>
				</td>
				</tr>
				<tr>
				<th>标题</th>
				<td class="y-bg">
				  <input type="text" class="input-text" name="info[<?php echo $i?>][title]" id="photos_<?php echo $i?>_title" size="30" value="<?php echo $v[title]?>"/>
				</td>
				</tr>
				<tr>
				<th>简介</th>
				<td class="y-bg">
				  <input type="text" class="input-text" name="info[<?php echo $i?>][description]" id="photos_<?php echo $i?>_description" size="61" value="<?php echo $v[description]?>"/>
				</td>
				</tr>
			</table>
		</fieldset>
	<?php
		$i++;
		}
	?>
		
	</div>

	<div class="bk10"></div>
	<input type="button" id="additem" value=" 添加一行 " class="button">
	</fieldset>

	
	<div class="bk15"></div>
    <input type="submit" id="dosubmit" name="dosubmit" class="button" value="<?php echo L('submit')?>" />
    <input type="reset" value=" <?php echo L('重置')?> " class="button">
</div>
</form>
<script type="text/javascript">
$("#additem").click(function(){
	var n = parseInt($('#items fieldset:last').attr('title'))+1;
	if(n){
		n = n;
	}else{
		n = 1;
	}
		
	var str = '<fieldset id="item_'+n+'" title="'+n+'">'+
			  '<table width="100%"  class="table_form">'+
			  '<tr>'+
			  '<th width="139">背景图片</th>'+
			  '<td class="y-bg" width="420">'+
			  '<input type="text" name="info[photos]['+n+'][bg]" id="photos_'+n+'_bg" value="" size="50" class="input-text" />'+ 
			  '<input type="button" class="button" onclick="javascript:flashupload(\'photos_'+n+'_bg_images\', \'附件上传\',\'photos_'+n+'_bg\',submit_images,\'1,jpg|jpeg|gif|bmp|png,1,,,0\',\'iapp\',\'\',\'58ae91c638ee797f3572dfe841793455\')"/ value="上传图片">'+
				
			  '</td>'+
			  '<td rowspan="4" class="y-bg" >'+
			  '<a href="#" onclick="$(\'#item_'+n+'\').remove();">移除'+n+'</a>'+
			  '</td>'+
			  '</tr>'+
			  '<tr>'+
			  '<th>软件图片1</th>'+
			  '<td class="y-bg">'+
			  '<input type="text" name="info[photos]['+n+'][img1]" id="photos_'+n+'_img1" value="" size="50" class="input-text" />'+ 
			  '<input type="button" class="button" onclick="javascript:flashupload(\'photos_'+n+'_img1_images\', \'附件上传\',\'photos_'+n+'_img1\',submit_images,\'1,jpg|jpeg|gif|bmp|png,1,,,0\',\'iapp\',\'\',\'58ae91c638ee797f3572dfe841793455\')"/ value="上传图片">'+
			  '</td>'+
			  '</tr>'+
			  '<tr>'+
			  '<th>软件图片2</th>'+
			  '<td class="y-bg">'+
			  '<input type="text" name="info[photos]['+n+'][img2]" id="photos_'+n+'_img2" value="" size="50" class="input-text" />'+ 
			  '<input type="button" class="button" onclick="javascript:flashupload(\'photos_'+n+'_img2_images\', \'附件上传\',\'photos_'+n+'_img2\',submit_images,\'1,jpg|jpeg|gif|bmp|png,1,,,0\',\'iapp\',\'\',\'58ae91c638ee797f3572dfe841793455\')"/ value="上传图片">'+
			  '</td>'+
			  '</tr>'+
			  '<tr>'+
			  '<th>标题</th>'+
			  '<td class="y-bg">'+
			  '<input type="text" name="info[photos]['+n+'][title]" id="photos_'+n+'_title" value="" size="30" class="input-text" />'+
			  '</td>'+
			  '</tr>'+
			  '<tr>'+
			  '<th>简介</th>'+
			  '<td class="y-bg">'+
			  '<input type="text" name="info[photos]['+n+'][description]" id="photos_'+n+'_description" value="" size="61" class="input-text" />'+
			  '</td>'+
			  '</tr>'+
			  '</table>'+
		      '</fieldset>';	
	$("#items").append(str);
});


</script>
</body>
</html>