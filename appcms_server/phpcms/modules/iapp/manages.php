<?php 
defined('IN_PHPCMS') or exit('No permission resources.'); 
pc_base::load_app_class('admin','admin',0);
pc_base::load_sys_class('form', '', 0);
class manages extends admin {
	function __construct() {
		parent::__construct();
		$this->db = pc_base::load_model('iapp_model');
	}
	
	function init() {
		
	}
	
	function apiinfo() {
		include $this->admin_tpl('apiinfo');
		
		//$SvnUtils = pc_base::load_app_class('SvnUtils', 'iapp', 0);
		//$SvnUtils->SVN_CONFIG_DIR = 'http://svn.appcan.cn/sourceCode/113/293/69/trunk';
		//$SvnUtils->update('./robots.txt');
	}
	
	/*
	*基础配置
	*/
	function setting() {
		pc_base::load_app_func('global');
		pc_base::load_app_func('version', 'iapp', 0);
		$info = array();
		
		$info[c] = version();
		
		
		//pc_base::load_app_class('curl', 'iapp', 0);
		//$curl = new curl();
		
		$sysinfo = get_sysinfo();
		$sysinfo['mysqlv'] = mysql_get_server_info();

		//获取最新版本与授权信息
		$http = pc_base::load_sys_class('http');
		$url = 'http://www.appcms.org/index.php?m=license&c=index&a=stats_add';
		$data = array(
				'url'=>$_SERVER['SERVER_NAME'],
				'ip'=>ip(),
				'content'=>array2string($_SERVER)
				);
		$http->post($url,$data);
		$re = json_decode($http->get_data(),true);

		$info[n] = $re[0];
		$info[url] = $data[url];
		
		$data = array();
		$m_db = pc_base::load_model('module_model');
		$data = $m_db->select(array('module'=>'iapp'));
		$data = string2array($data[0]['setting']);
		
		/*
		if($data[appcan][appid]!="")
		{
			$url = "http://www.appcms.org/index.php?m=AcServer&c=index&a=getstatus&appid=" .$data[appcan][appid];
			$status = $curl->vget($url);
			$status = trim($status);
		}
		*/
		
 		if(isset($_POST['dosubmit'])) {
			$setting = $_POST['data'];
			
			/*
			if($setting[info][sitename]==""){
				showmessage('客户端名称不能为空！',HTTP_REFERER);
			}
			if($setting[appcan][username]==""){
				showmessage('打包服务器账号不能为空！',HTTP_REFERER);
			}
			if(!is_email($setting[appcan][username])){
				showmessage('打包服务器账号必须是邮箱号码！',HTTP_REFERER);
			}
			if($setting[appcan][password]==""){
				showmessage('打包服务器密码不能为空！',HTTP_REFERER);
			}
			
			
			//获取appcan lt
			$lt = $curl->vget("http://sso.tx100.com/login?service=http://www.appcan.cn/app/create_app_robot_ide.action&get-lt=true");
			$lt = trim($lt);
			$lt = substr($lt,1,strlen($lt)-3);
			$lt = json_decode($lt, true);
		
			$data = 'username='.$setting[appcan][username].
			'&password='.$setting[appcan][password].
			'&lt='.$lt[retData].
			'&service=http://www.appcan.cn/login/loginStatus.do'.
			'&isAjax=true&isFrame=true&_eventId=submit';
			$relogin = $curl->vlogin("http://sso.tx100.com/login",$data);
			
			
			$islogin = $curl->vget("http://sso.tx100.com/login?service=http%3A%2F%2Fdev.appcan.cn%2Fsec%2Fj_spring_cas_security_check&noLoginPage=true");
			$islogin = trim($islogin);
			if($islogin==''){
				showmessage('打包服务器账号或者密码不正确~',HTTP_REFERER);
			}
			
			//第一次保存配置
			if($_POST[isnew]=="1"){
				$setting[appcan][appid] ="";
			}
			
			if($setting[appcan][appid]==""){
				//创建应用
				$reapp = $curl->vget("http://dev.appcan.cn/ide/newAppWithKey.action?appName=".$setting[info][sitename]."&appDes=&appCategory=休闲娱乐&appVersion=00.00.0000");
				$reapp = explode(',',trim($reapp));
				$a = '4171cd55-bd8a-4068-bcd6-123272971a06';
				if(strlen($reapp[1])==36){
					$setting[appcan][appid] = $reapp[0];
					$setting[appcan][appguid] = $reapp[1];
					
					//写入上传应用队列，等候打包
					$data = 'url='.$_SERVER['SERVER_NAME'].
					'&ip='.ip().
					'&username='.$setting[appcan][username].
					'&password='.$setting[appcan][password].
					'&appcanAppId='.$setting[appcan][appid].
					'&acdzversion=standard'.
					'&filedir='.
					'&sdir='.
					'&content=';
				
					$dl = $curl->vpost("http://www.appcms.org/index.php?m=AcServer&c=index&a=init",$data);
					$dl = json_decode(trim($dl), true);
					
					if(!($dl[status]>0)){
						showmessage('打包服务初始化失败，请重新操作~',HTTP_REFERER);
					}
					
				}else{
					showmessage('打包服务初始化失败,请重新操作~',HTTP_REFERER);
				}
			}
			
			*/
			
  			setcache('iapp', $setting, 'commons');  
 			$setting = array2string($setting);
			$m_db->update(array('setting'=>$setting), array('module'=>'iapp'));
			
			showmessage('保存成功',HTTP_REFERER);
		} else {
			include $this->admin_tpl('setting');
		}
	}
	
	/*
	*风格设置
	*/
	function skin() {
		pc_base::load_app_func('skin');
		
		$skin_info = getcache('iappSkin_all','commons');
		if(!$skin_info){
			$skin_info = fun_skin_def();
			setcache('iappSkin_all', $skin_info, 'commons');
		}
		
		$info = getcache('iappSkin','commons');

 		if(isset($_POST['dosubmit'])) {
			$data = $_POST['data'];
			$arr = $skin_info[$data[key]];
			$arr[skintype] = $data[skintype];
			$arr[bg] = $data[bg];
			
			setcache('iappSkin', $arr, 'commons');
			showmessage('风格设置成功',HTTP_REFERER);
		} else {
			include $this->admin_tpl('skin');
		}
	}
	
	
	/*
	* 风格制作
	*/
	function skin_renew() {
		$info = array();
		$info = $this->db->get_one(array('mkey'=>'skin'));
		if($info){
			$info[setting] = string2array($info[setting]);
		}
	
	
		if(isset($_POST['dosubmit'])) {
			$data = $_POST['data'];
			
			$arr = array();
			foreach($data[key] as $k=>$r){
				$arr[$r] = array('key'=>$data[key][$k],
								 'title'=>$data[title][$k],
								 'skin'=>$data[skin][$k],
								 'vskin'=>$data[vskin][$k]);
			}
			setcache('iappSkin_all', $arr, 'commons');
			$arr = array2string($arr);
			
			
			if($info){
				$this->db->update(array('setting'=>$arr), array('mkey'=>'skin'));
			}else{
				$return_id = $this->db->insert(array('mkey'=>'skin','setting'=>$arr),'1');
			}
			
			showmessage('风格设置成功',HTTP_REFERER);
		}
	}
	
	
	/*
	* 关于我们
	*/
	function about() {
		$info = $this->db->get_one(array('mkey'=>'about'));
		$info[setting] = string2array($info[setting]);
		
 		if(isset($_POST['dosubmit'])) {
			$data = $_POST['data'];
			setcache('iappAbout', $data, 'commons');
			$_data = array2string($data);
			
			if($info){
				$this->db->update(array('setting'=>$_data), array('mkey'=>'about'));
			}else{
				$return_id = $this->db->insert(array('mkey'=>'about','setting'=>$_data),'1');
			}
			showmessage('关于我们保存成功',HTTP_REFERER);
		} else {
			include $this->admin_tpl('about');
		}
	}
	
	/*
	* 帮助设置
	*/
	function help() {
		$info = $this->db->get_one(array('mkey'=>'help'));
 		if(isset($_POST['dosubmit'])) {
			$data = $_POST['content'];
			if($info){
				$this->db->update(array('setting'=>$data), array('mkey'=>'help'));
			}else{
				$return_id = $this->db->insert(array('mkey'=>'help','setting'=>$data),'1');
			}
			showmessage('保存成功',HTTP_REFERER);
		} else {
			include $this->admin_tpl('help');
		}
	}
	
	/*
	* 推广设置
	*/
	function promotion() {

		$data = array();
		$data = $this->db->get_one(array('mkey'=>'promotion'));
		if($data){
			$data = string2array($data[setting]);
		}

 		if(isset($_POST['dosubmit'])) {
			$info = $_POST['info'];
			setcache('promotion', $info, 'iapp');
			$info = array2string($info);
			if($data){
				$this->db->update(array('setting'=>$info), array('mkey'=>'promotion'));
			}else{
				$return_id = $this->db->insert(array('mkey'=>'promotion','setting'=>$info),'1');
			}
			showmessage('保存成功',HTTP_REFERER);
		} else {
			include $this->admin_tpl('promotion');
		}
		
	}
	
	/*
	* 升级设置
	*/
	function upgrade() {
		$data = array();
		$data = $this->db->get_one(array('mkey'=>'upgrade'));

		if($data){
			$data = string2array($data[setting]);
		}

 		if(isset($_POST['dosubmit'])) {

			$info = $_POST['info'];
			
			pc_base::load_app_class('phpqrcode', 'iapp', 0);
			$url = APP_PATH.'index.php?m=iapp&c=touch&a=softdown';
			
			$qrurl = "./uploadfile/qrcode/softdown.png";
			
			QRcode::png($url, $qrurl,'L',4,0);
			$info[softdown_qrcode] = APP_PATH.'uploadfile/qrcode/softdown.png';
			
			setcache('upgrade', $info, 'iapp');  
			$info = array2string($info);
			
			if($data){
				$this->db->update(array('setting'=>$info), array('mkey'=>'upgrade'));
			}else{
				$return_id = $this->db->insert(array('mkey'=>'upgrade','setting'=>$info),'1');
			}

			showmessage('保存成功',HTTP_REFERER);
		} else {
			include $this->admin_tpl('upgrade');
		}
	}
	
	/*
	* 初始化可用配置
	*/
	function pattern_renew() {
		pc_base::load_app_func('pattern');
		$pattern_data = fun_pattern_data();
		$modellist_t = getcache('modellist_t','commons');
	
		//新闻
		$modelid = $modellist_t['news'][modelid];
		$categorys = getcache('category_'.$modelid,'commons');
		$cate = array();
		
		
		$conut = 0;
		foreach ($categorys as $k=>$v) {
			if($v[parentid]==0){
				$conut++;
			}
		}
		if($conut >1){
			$cate[news_0] = array("model" => "news","title" => "全部","catid" => 0);
			foreach ($categorys as $k=>$v) {
				if($v[parentid]==0 && $v[type]==0){
					$cate['news_'.$k][model] = 'news';
					$cate['news_'.$k][title] = $v[catname];
					$cate['news_'.$k][catid] = $k;
				}
			}
		}else if($conut ==1){
			foreach ($categorys as $k=>$v) {
				if($v[parentid]==0 && $v[type]==0){
					$cate['news_'.$k][model] = 'news';
					$cate['news_'.$k][title] = '全部';
					$cate['news_'.$k][catid] = $k;
					
					foreach ($categorys as $n=>$r) {
						if($v[catid] == $r[parentid] && $v[type]==0){
							$cate['news_'.$n][model] = 'news';
							$cate['news_'.$n][title] = $r[catname];
							$cate['news_'.$n][catid] = $n;
						}
					}
				}
			}
		}else{
			$cate['news_0'][model] = 'news';
			$cate['news_0'][title] = '全部';
			$cate['news_0'][catid] = 0;
		}
		$pattern_data[l_news] = $cate;
		
		//图片
		$modelid = $modellist_t['picture'][modelid];
		$categorys = getcache('category_'.$modelid,'commons');
		$cate = array();
		$conut = 0;
		foreach ($categorys as $k=>$v) {
			if($v[parentid]==0){
				$conut++;
			}
		}
		if($conut >1){
			$cate[picture_0] = array("model" => "picture","title" => "全部","catid" => 0);
			foreach ($categorys as $k=>$v) {
				if($v[parentid]==0 && $v[type]==0){
					$cate['picture_'.$k][model] = 'picture';
					$cate['picture_'.$k][title] = $v[catname];
					$cate['picture_'.$k][catid] = $k;
				}
			}
		}else if($conut ==1){
			foreach ($categorys as $k=>$v) {
				if($v[parentid]==0 && $v[type]==0){
					$cate['picture_'.$k][model] = 'picture';
					$cate['picture_'.$k][title] = '全部';
					$cate['picture_'.$k][catid] = $k;
					
					foreach ($categorys as $n=>$r) {
						if($v[catid] == $r[parentid] && $v[type]==0){
							$cate['picture_'.$n][model] = 'picture';
							$cate['picture_'.$n][title] = $r[catname];
							$cate['picture_'.$n][catid] = $n;
						}
					}
				}
			}
		}else{
			$cate['picture_0'][model] = 'picture';
			$cate['picture_0'][title] = '全部';
			$cate['picture_0'][catid] = 0;
		}
		$pattern_data[l_picture] = $cate;
		
		//视频
		$db_type = pc_base::load_model('iappVideo_category_model');
		$types = $db_type->select('','*','','`listorder` DESC');
		$cate = array();
		if($types){
			$cate[video_0] = array("model" => "video","title" => "全部","catid" => 0);
			foreach ($types as $k=>$v) {
				$cate['video_'.$v[id]][model] = 'video';
				$cate['video_'.$v[id]][title] = $v[title];
				$cate['video_'.$v[id]][catid] = $v[id];
			}
		}else{
			//showmessage('没有视频栏目，请先添加视频栏目！',HTTP_REFERER);
			$cate['video_0'][model] = 'video';
			$cate['video_0'][title] = '全部';
			$cate['video_0'][catid] = 0;
		}
		$pattern_data[l_video] = $cate;
		
		//商家
		if(module_exists('iappYp')){
			$modellist_t = getcache('modellist_t','commons');
			$modelid = $modellist_t['iappYp_company'][modelid];
			$categorys = getcache('category_'.$modelid,'commons');
			$cate = array();
			$conut = 0;
			foreach ($categorys as $k=>$v) {
				if($v[parentid]==0){
					$conut++;
				}
			}
			if($conut >1){
				$cate[business_0] = array("model" => "company","title" => "全部","catid" => 0);
				foreach ($categorys as $k=>$v) {
					if($v[parentid]==0 && $v[type]==0){
						$cate['company_'.$k][model] = 'company';
						$cate['company_'.$k][title] = $v[catname];
						$cate['company_'.$k][catid] = $k;
					}
				}
			}else if($conut ==1){
				foreach ($categorys as $k=>$v) {
					if($v[parentid]==0 && $v[type]==0){
						$cate['company_'.$k][model] = 'company';
						$cate['company_'.$k][title] = '全部';
						$cate['company_'.$k][catid] = $k;
						
						foreach ($categorys as $n=>$r) {
							if($v[catid] == $r[parentid] && $v[type]==0){
								$cate['company_'.$n][model] = 'company';
								$cate['company_'.$n][title] = $r[catname];
								$cate['company_'.$n][catid] = $n;
							}
						}
					}
				}
			}
			$pattern_data[l_company] = $cate;
		}else{
			unset($pattern_data[l_company]);
		}
		
		
		//优惠券
		if(module_exists('iappCoupons')){
			$modellist_t = getcache('modellist_t','commons');
			$modelid = $modellist_t['iappYp_company'][modelid];
			$categorys = getcache('category_'.$modelid,'commons');
			$cate = array();
			$conut = 0;
			foreach ($categorys as $k=>$v) {
				if($v[parentid]==0){
					$conut++;
				}
			}
			if($conut >1){
				$cate[coupons_0] = array("model" => "coupons","title" => "全部","catid" => 0);
				foreach ($categorys as $k=>$v) {
					if($v[parentid]==0 && $v[type]==0){
						$cate['coupons_'.$k][model] = 'coupons';
						$cate['coupons_'.$k][title] = $v[catname];
						$cate['coupons_'.$k][catid] = $k;
					}
				}
			}else if($conut ==1){
				foreach ($categorys as $k=>$v) {
					if($v[parentid]==0 && $v[type]==0){
						$cate['coupons_'.$k][model] = 'coupons';
						$cate['coupons_'.$k][title] = '全部';
						$cate['coupons_'.$k][catid] = $k;
						
						foreach ($categorys as $n=>$r) {
							if($v[catid] == $r[parentid] && $v[type]==0){
								$cate['coupons_'.$n][model] = 'coupons';
								$cate['coupons_'.$n][title] = $r[catname];
								$cate['coupons_'.$n][catid] = $n;
							}
						}
					}
				}
			}
			$pattern_data[l_coupons] = $cate;
		}else{
			unset($pattern_data[l_coupons]);
		}
		
		//活动
		if(module_exists('iappActivity')){
			$db_type = pc_base::load_model('iappActivity_type_model');
			$types = $db_type->select('','*','','`listorder` DESC');
			$cate = array();
			$cate[activity_0] = array("model" => "activity","title" => "全部","catid" => 0);
			if($types){
				foreach ($types as $k=>$v) {
					$cate['activity_'.$v[id]][model] = 'activity';
					$cate['activity_'.$v[id]][title] = $v[title];
					$cate['activity_'.$v[id]][catid] = $v[id];
				}
			}
			$pattern_data[l_activity] = $cate;
		}else{
			unset($pattern_data[l_activity]);
		}
		

		//积分商城
		if(module_exists('iappJifen')){
			$db_type = pc_base::load_model('iappJifen_category_model');
			$types = $db_type->select('','*','','`listorder` DESC');
			$cate = array();
			$cate[jifen_0] = array("model" => "jifen","title" => "全部","catid" => 0);
			if($types){
				foreach ($types as $k=>$v) {
					$cate['jifen_'.$v[id]][model] = 'jifen';
					$cate['jifen_'.$v[id]][title] = $v[title];
					$cate['jifen_'.$v[id]][catid] = $v[id];
				}
			}
			$pattern_data[l_jifen] = $cate;
		}else{
			unset($pattern_data[l_jifen]);
		}
		
		//百宝箱
		if(module_exists('iappTel')){
			$_arr_a = $pattern_data[l_apps];
			$_arr_b = $pattern_data[l_company];
			foreach ($_arr_b as $k=>$v) {
				if($v[title] == '全部'){
					unset($_arr_b[$k]);  
				}
			}
			$pattern_data[l_apps] = array_merge($_arr_a,$_arr_b);
		}else{
			unset($pattern_data[l_apps]);
		}
		
		//首页
		
		
		setcache('pattern_all', $pattern_data, 'commons');
	}
	
	/*
	* 界面设置
	*/
	function pattern() {
		pc_base::load_app_class('cacheAll','iapp',0);
		$ca = new cacheAll();
		$ca->cache_cat();
		$ca->cache_model();
		$this->pattern_renew();
		
		$skin_info = getcache('iappSkin_all','commons');
		
		if(!$skin_info){
			pc_base::load_app_func('skin');
			$skin_info = fun_skin_def();
			setcache('iappSkin', $skin_info, 'commons');
		}
		$skin_info = getcache('iappSkin','commons');
		
		pc_base::load_app_func('pattern');
		$info = $this->db->get_one(array('mkey'=>'pattern'));
		$info[setting] = string2array($info[setting]);
		
		$pattern_all = getcache('pattern_all', 'commons');
		
		
		
		$pattern_my = getcache('pattern', 'commons');
		$pattern_js = pattern2js($pattern_all);
		
		
		
		//echo "'" .$pattern_js . "'";
		//print_r($this->arr_merge($pattern_all,$pattern_my));
		
		echo "<!--";
		//print_r($pattern_all);
		print_r($pattern_my);
		echo "-->";

		$l_index_html = $this->arr_order_merge('l_index',$pattern_all,$pattern_my);
		
		$l_footer_html = $this->arr_order_merge('l_footer',$pattern_all,$pattern_my);
		
		
		
		$html = '';
		
		
		
		$txt = '新闻中心头部导航设置【l_content】，最多可以选择5个按钮，拖动可以进行排序，双击可以编辑文字';
		$htm = $this->arr_order_merge('l_content',$pattern_all,$pattern_my);
		$html .= $this->html_txt('l_content',$txt,$htm);
		
		
		$txt = '生活中心头部导航设置【l_life】，最多可以选择5个按钮，拖动可以进行排序，双击可以编辑文字';
		$htm = $this->arr_order_merge('l_life',$pattern_all,$pattern_my);
		$html .= $this->html_txt('l_life',$txt,$htm);
		
		$txt = '新闻页面栏目设置【l_news】，最多可以选择5个栏目，拖动可以进行排序，双击可以编辑文字';
		$htm = $this->arr_order_merge('l_news',$pattern_all,$pattern_my);
		$html .= $this->html_txt('l_news',$txt,$htm);
		
		$txt = '图片页面栏目设置【l_picture】，最多可以选择5个栏目，拖动可以进行排序，双击可以编辑文字';
		$htm = $this->arr_order_merge('l_picture',$pattern_all,$pattern_my);
		$html .= $this->html_txt('l_picture',$txt,$htm);
		
		$txt = '视频页面栏目设置【l_video】，最多可以选择5个栏目，拖动可以进行排序，双击可以编辑文字';
		$htm = $this->arr_order_merge('l_video',$pattern_all,$pattern_my);
		$html .= $this->html_txt('l_video',$txt,$htm);
		
		$txt = '优惠券页面栏目设置【l_coupons】，最多可以选择5个栏目，拖动可以进行排序，双击可以编辑文字';
		$htm = $this->arr_order_merge('l_coupons',$pattern_all,$pattern_my);
		$html .= $this->html_txt('l_coupons',$txt,$htm);
		
		$txt = '活动页面栏目设置【l_activity】，最多可以选择5个栏目，拖动可以进行排序，双击可以编辑文字';
		$htm = $this->arr_order_merge('l_activity',$pattern_all,$pattern_my);
		$html .= $this->html_txt('l_activity',$txt,$htm);
		
		$txt = '积分商城栏目设置【l_jifen】，最多可以选择5个栏目，拖动可以进行排序，双击可以编辑文字';
		$htm = $this->arr_order_merge('l_jifen',$pattern_all,$pattern_my);
		$html .= $this->html_txt('l_jifen',$txt,$htm);
		
		$txt = '百宝箱模块设置【l_apps】，模块数量不限，拖动可以进行排序，双击可以编辑文字';
		$htm = $this->arr_order_merge('l_apps',$pattern_all,$pattern_my);
		$html .= $this->html_txt('l_apps',$txt,$htm);
		
		$l_tab_default_html = $this->html_tab_default($pattern_all,$pattern_my);
		$l_catid_default_html = $this->html_catid_default($pattern_all,$pattern_my);
		
 		if(isset($_POST['dosubmit'])) {
		
			$isdefault = $_POST['isdefault'];
			
			$data = $_POST['data'];
			
			$data[l_connect][qqweibo] = pc_base::load_config('system', 'qq_akey');
			$data[l_connect][sina] = pc_base::load_config('system', 'sina_akey');
			
			if(module_exists('iappTalk')){
				$data[l_models][talk] = 1;
			}else{
				$data[l_models][talk] = 0;
			}
			if(module_exists('iappActivity')){
				$data[l_models][activity] = 1;
			}else{
				$data[l_models][activity] = 0;
			}
			if(module_exists('iappCoupons')){
				$data[l_models][coupons] = 1;
			}else{
				$data[l_models][coupons] = 0;
			}
			if(module_exists('iappJifen')){
				$data[l_models][jifen] = 1;
			}else{
				$data[l_models][jifen] = 0;
			}
			if(module_exists('iappYp')){
				$data[l_models][yp] = 1;
			}else{
				$data[l_models][yp] = 0;
			}

			
			foreach($data[l_footer] as $k=>$v){
				$data[footer_index] = $v[model];
				break;
			}
			

			setcache('pattern', $data, 'commons');

			if($info){
				$this->db->update(array('setting'=>array2string($data)), array('mkey'=>'pattern'));
			}else{
				$return_id = $this->db->insert(array('mkey'=>'pattern','setting'=>array2string($data)),'1');
			}
			showmessage('保存成功',HTTP_REFERER);
		} else {
			include $this->admin_tpl('pattern');
		}
	}
	
	/*
	*数组处理、排序
	*/
	function arr_order_merge($g,$arr_all,$arr_my) {
	
		//print_r($arr_all);
		//print_r($arr_my);
		
		$_d = array();
		$_u = array();
		$_html = '';
		if($arr_all[$g]){
			//print_r($arr_my[$g]);
			foreach($arr_my[$g] as $k=>$v){
				if($arr_all[$g][$k]){
					$_u[$k] = $arr_all[$g][$k];
					$_u[$k][title] =  $v[title];
					
					//$_u[$k][title] = $arr_my[$g][$k][title];
					$_u[$k][is_select] = 1;
				}
			}
			
			//print_r($_u);
			
			foreach($arr_all[$g] as $k=>$v){
				if(!$_u[$k]){
					$_d[$k] = $v;
					$_d[$k][is_select] = 0;
				}
			}
			//print_r($_d);
			
			$_n = array_merge($_u,$_d);
			
			//print_r($_n);
			
			foreach($_n as $k=>$v){
				$class = '';
				$input = '';
				if($v[is_select]){
					$class = 'class="cur"';
					
					foreach($v as $n=>$r){
						if($n!='is_select'){
							$input .= '<input type="hidden" id="'.$g.'_'.$k.'_'.$n.'" name="data['.$g.']['.$k.']['.$n.']" value="'.$r.'">';
						}
					}
					
				}else{
					$class = '';
					$input = '';
				}
				$_html .='<li '.$class.' onDblClick="getli(this)"><span id="'.$g.'__'.$k.'__v">'. $v[title].'</span><p>'.$input.'</p></li>';
			}
		
		}else{
			$_html ='';
		}
		
		return $_html;  
	}
	
	function arr_merge($arr_all,$arr_my) {
		
		$_n = array();
		foreach($arr_all as $ak=>$av){
			$_d = array();
			$_u = array();
			foreach($arr_my[$ak] as $k=>$v){
				if($arr_all[$ak][$k]){
					$_u[$k] = $arr_all[$ak][$k];
					$_u[$k][title] =  $v[title];
					$_u[$k][is_select] = 1;
				}
			}
			foreach($arr_all[$ak] as $k=>$v){
				if(!$_u[$k]){
					$_d[$k] = $v;
					$_d[$k][is_select] = 0;
				}
			}
			$_n[$ak] = array_merge($_u,$_d);
		}
		
		return $_n; 
	}
	

	/*
	*html_tab_default
	*/
	function html_tab_default($arr_all,$arr_my) {
		
			$_html = '';
			foreach($arr_all[l_tab_default] as $k=>$v){
				$_hc = '';
				foreach($arr_all['l_'.$k] as $n=>$r){
					if($n == $arr_my[l_tab_default][$k]){
						$checked='checked="checked"';
					}else{
						$checked='';
					}
				
					$_hc .='<label><input type="radio" name="data[l_tab_default]['.$k.']" value="'.$n.'" id="l_tab_default_'.$k.'_'.$n.'" '.$checked.' /> '.$r[title].' </label> ';
				}
				
				if($_hc !=''){
					$_html .='<tr><th width="120">'.$v[label].'：</th><td class="y-bg">'.$_hc.'</td></tr>';
				}
			}
			
			
		if($_html !=''){
			$_html = '<table width="100%"  class="table_form">'.$_html.'</table>';
		}
		return $_html;  
	}
	
	function html_txt($id,$txt,$html) {
		$_html ='';
		if(trim($html) !=''){
			$_html = '<h3 class="iappmenuh3">'.$txt.'</h3>
						<ul class="iappmenu" id="ul_'.$id.'">'.$html.'</ul>
						<script type="text/javascript">
							$("#ul_'.$id.'").dragsort();
						</script>';
		}
		return $_html; 
	}
	
	/*
	*html_catid_default
	*/
	function html_catid_default($arr_all,$arr_my) {
		$_html = '';
		foreach($arr_all[l_catid_default] as $k=>$v){
			$_hc = '';
			foreach($arr_all['l_'.$k] as $n=>$r){
				if($r[catid] == $arr_my[l_catid_default][$k]){
					$checked='checked="checked"';
				}else{
					$checked='';
				}

				$_hc .='<label><input type="radio" name="data[l_catid_default]['.$k.']" value="'.$r[catid].'" id="l_tab_default_'.$k.'_'.$n.'" '.$checked.' /> '.$r[title].'('.$r[catid].') </label> ';
			}
			if($_hc !=''){
				$_html .='<tr><th width="120">'.$v[label].'：</th><td class="y-bg">'.$_hc.'</td></tr>';
			}
		}
		if($_html !=''){
			$_html = '<table width="100%"  class="table_form">'.$_html.'</table>';
		}
		return $_html;  
	}
}
?>