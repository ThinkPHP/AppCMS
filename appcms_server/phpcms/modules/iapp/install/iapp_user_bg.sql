DROP TABLE IF EXISTS `phpcms_iapp_user_bg`;
CREATE TABLE IF NOT EXISTS `phpcms_iapp_user_bg` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned default '0' COMMENT '用户ID',
  `bg` varchar(200) NOT NULL COMMENT '图片',
  `setting` mediumtext,
  PRIMARY KEY (`id`)
) TYPE=MyISAM;