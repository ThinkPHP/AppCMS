<?php
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');
define('SQL_PATH', dirname(__FILE__).DIRECTORY_SEPARATOR);


$parentid0 = $menu_db->insert(array('name'=>'iapp', 'parentid'=>0, 'm'=>'iapp', 'c'=>'manages', 'a'=>'init', 'data'=>'', 'listorder'=>99, 'display'=>'1'), true);
$parentid1 = $menu_db->insert(array('name'=>'iapp_config', 'parentid'=>$parentid0, 'm'=>'iapp', 'c'=>'manages', 'a'=>'setting', 'data'=>'', 'listorder'=>1, 'display'=>'1'), true);
$parentid99 = $menu_db->insert(array('name'=>'iappPlug', 'parentid'=>$parentid0, 'm'=>'iapp', 'c'=>'manages', 'a'=>'init', 'data'=>'', 'listorder'=>2, 'display'=>'1'), true);

$parentid2 = $menu_db->insert(array('name'=>'iapp_setting', 'parentid'=>$parentid1, 'm'=>'iapp', 'c'=>'manages', 'a'=>'setting', 'data'=>'', 'listorder'=>1, 'display'=>'1'), true);
$menu_db->insert(array('name'=>'iapp_pattern', 'parentid'=>$parentid2, 'm'=>'iapp', 'c'=>'manages', 'a'=>'pattern', 'data'=>'', 'listorder'=>2, 'display'=>'1'));

$menu_db->insert(array('name'=>'iapp_skin', 'parentid'=>$parentid2, 'm'=>'iapp', 'c'=>'manages', 'a'=>'skin', 'data'=>'', 'listorder'=>3, 'display'=>'1'));
$menu_db->insert(array('name'=>'iapp_about', 'parentid'=>$parentid2, 'm'=>'iapp', 'c'=>'manages', 'a'=>'about', 'data'=>'', 'listorder'=>4, 'display'=>'1'));
$menu_db->insert(array('name'=>'iapp_help', 'parentid'=>$parentid2, 'm'=>'iapp', 'c'=>'manages', 'a'=>'help', 'data'=>'', 'listorder'=>5, 'display'=>'1'));
$menu_db->insert(array('name'=>'iapp_apiinfo', 'parentid'=>$parentid2, 'm'=>'iapp', 'c'=>'manages', 'a'=>'apiinfo', 'data'=>'', 'listorder'=>6, 'display'=>'1'));
$menu_db->insert(array('name'=>'iapp_upgrade', 'parentid'=>$parentid2, 'm'=>'iapp', 'c'=>'manages', 'a'=>'upgrade', 'data'=>'', 'listorder'=>7, 'display'=>'1'));
$menu_db->insert(array('name'=>'iapp_promotion', 'parentid'=>$parentid2, 'm'=>'iapp', 'c'=>'manages', 'a'=>'promotion', 'data'=>'', 'listorder'=>8, 'display'=>'1'));
$menu_db->insert(array('name'=>'iapp_pattern_renew', 'parentid'=>$parentid2, 'm'=>'iapp', 'c'=>'manages', 'a'=>'pattern_renew', 'data'=>'', 'listorder'=>99, 'display'=>'0'));

$language = array('iapp'=>'手机APP',
				  'iappPlug'=>'APP插件',
				  'iapp_config'=>'基础设置',
				  
				  'iapp_setting'=>'基础设置',
				  
				  'iapp_pattern'=>'界面设置',

				  'iapp_skin'=>'风格设置',
				  'iapp_about'=>'关于我们',
				  'iapp_help'=>'软件帮助',
				  'iapp_apiinfo'=>'API调用方法',
				  'iapp_upgrade'=>'客户端升级设置',
				  'iapp_promotion'=>'软件推广设置',
				  'iapp_pattern_renew'=>'初始化可用配置');				  
				  
?>