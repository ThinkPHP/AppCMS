DROP TABLE IF EXISTS `phpcms_iapp_user_praise`;
CREATE TABLE IF NOT EXISTS `phpcms_iapp_user_praise` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) NOT NULL COMMENT '用户id',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `nick` varchar(50) NOT NULL COMMENT '昵称',
  `praise_userid` int(10) unsigned default '0' COMMENT '被赞用户ID',
  `praise` int(10) DEFAULT '0' COMMENT '赞/踩',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) TYPE=MyISAM;