<?php 
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_sys_class('format', '', 0);
class app {
	function __construct() {		
		$this->db = pc_base::load_model('iappJifen_model');
		$this->db_iappJifen_category = pc_base::load_model('iappJifen_category_model');
		$this->db_iappJifen_orders = pc_base::load_model('iappJifen_orders_model');
		$this->siteid = get_siteid();
		$this->config = getcache('iapp', 'commons');
	}
	
	public function lists() {
		$re = array();
		$page = max(intval($_GET['page']), 1);
		$catid = intval($_GET['catid']) ? intval(trim($_GET['catid'])) : null;
		$order = trim($_GET['order']) ? trim($_GET['order']) : null;
		
		$time =time();
		$_sql = " start_time < $time AND $time < end_time ";
		if($catid){
			$_sql = $_sql." AND catid= $catid ";
		}
		
		$_order = 'id DESC';
		if($order =='hot'){
			$_order = 'htis DESC';
		}
		
		$data = $this->db->listinfo($_sql, $_order, $page,5);
		$iappJifen_category = $this->db_iappJifen_category->select('','*','','`listorder` DESC');
		
		$_category = array();
		foreach ($iappJifen_category as $k=>$v) {
			$_category[$v[id]] = $v;
		}

		$list = array();
		foreach ($data as $k=>$v) {
			$list[$k][id] = $v[id];
			$list[$k][catid] = $v[catid];
			$list[$k][catname] = $_category[$v[catid]][title];
			$list[$k][title] = $v[title];
			$list[$k][ltitle] = $v[ltitle];
			$list[$k][image] = thumb($v[image],600,226);
			$list[$k][start_time] = $v[start_time];
			$list[$k][end_time] = $v[end_time];
			$list[$k][price] = $v[price];	
			$list[$k][stock] = $v[stock]-$v[sales];	
			$list[$k][sales] = $v[sales];
		}
		$re[status]="OK";
		$re[data]=$list;
		$re[msg]="数据加载成功~";
		
		$return_id = $this->db->update(array('htis'=>'+=1'),'1=1');
		echo json_encode($re);
	}
	
	public function show() {
		$re = array();
		$id = intval($_GET['id']) ? intval($_GET['id']) : $re[status]="ERR";
		if(!$re[status]){
			$info = $this->db->get_one(array('id'=>$id));
			$info[image] = thumb($info[image],600,226);
			$info[specifications] = string2array($info[specifications]);
			$info[stock] = $info[stock] - $info[sales];
			$return_id = $this->db->update(array('htis'=>'+=1'),array('id'=>$id));
			if($info){
				$re[status]="OK";
				$re[data]=$info;
				$re[msg]="数据加载成功~";
			}else{
				$re[status]="NODATA";
				$re[msg]="活动不存在~";
			}
		}
		
		echo json_encode($re);
	}
	
	function orders() {
		$re = array();
		
		$id = isset($_GET[id]) ? intval(trim($_GET[id])) : null;
		$userid = trim($_GET['userid']) ? intval(trim($_GET['userid'])) : null;
		$acctoken = isset($_GET['acctoken']) && trim($_GET['acctoken']) ? trim($_GET['acctoken']) : null;
		
		if($id && $userid && $acctoken){
			$userdb = pc_base::load_model('member_model');
			$userdata = $userdb->get_one(array('userid'=>$userid,'password'=>$acctoken));
			if($userdata){
				$jifen_data = $this->db->get_one(array('id'=>$id));
				if($jifen_data[sales]<$jifen_data[stock]){
					pc_base::load_app_class('spend','pay',0);
					spend::point($jifen_data[price],"积分商城兑换【$jifen_data[title]】",$userdata['userid'], $userdata['username']);
					
					if(spend::$msg==0){
						$info = array();
						$info[jifenid] = $id;
						$info[jifentitle] = $jifen_data[title];
						$info[price] = $jifen_data[price];
						$info[userid] = $userdata[userid];
						$info[username] = $userdata[username];
						$info[mobile] = $userdata[mobile];
						$info[name] = $userdata[nickname];
						$info[distribution] = '';
						$info[addtime] = time();
						
						$return_id = $this->db_iappJifen_orders->insert($info,'1');
						$re_id = $this->db->update(array('sales'=>'+=1'),array('id'=>$id));
						if($return_id){
							$re[status] = "OK";
							$re[stock] = $jifen_data[stock]-$jifen_data[sales]-1;
							$re[msg] = "兑换成功~";
						}else{
							$re[status] = "ERR";
							$re[msg] = "兑换失败，未知错误，请重试~";
						}
					}else{
						$re[status] = "ERR";
						$re[msg] = spend::get_msg();
					}
					
				}else{
					$re[status] = "NOT";
					$re[msg] = "兑换失败，暂无库存~";
				}
					
			}else{
				$re[status] = "NOTLOGIN";
				$re[msg] = "兑换失败，请先登录~";
			}
		
		}else{
			$re[status] = "NODATA";
			$re[msg] = "报名失败，参数错误~";
		}
		echo json_encode($re);
	}
	
}
?>