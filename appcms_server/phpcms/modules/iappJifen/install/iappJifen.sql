DROP TABLE IF EXISTS `phpcms_iappJifen`;
CREATE TABLE IF NOT EXISTS `phpcms_iappJifen` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) NOT NULL COMMENT '栏目',
  `userid` int(10) unsigned default '0' COMMENT '用户ID',
  `username` varchar(20) default NULL COMMENT '用户名',
  
  `title` varchar(50) NOT NULL COMMENT '标题',
  `ltitle` varchar(80) NOT NULL COMMENT '长标题',
  `image` varchar(200) NOT NULL COMMENT '图片',
  `start_time` int(10) unsigned NOT NULL DEFAULT '0',
  `end_time` int(10) unsigned NOT NULL DEFAULT '0',
  `price` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '价格',
  
  `stock` smallint(5) NOT NULL COMMENT '库存',
  `sales` smallint(5) NOT NULL COMMENT '销量',

  `content` mediumtext NOT NULL COMMENT '产品简介',
  `specifications` mediumtext NOT NULL COMMENT '规格参数',
  `photos` text NOT NULL COMMENT '图片',
  
  `collection` smallint(5) DEFAULT '0' COMMENT '收藏量',
  `htis` smallint(5) DEFAULT '0' COMMENT '查看量',
  `praise` smallint(5) DEFAULT '0' COMMENT '赞',
  `comment` smallint(5) DEFAULT '0' COMMENT '评论',
  `listorder` smallint(5) DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) TYPE=MyISAM;
