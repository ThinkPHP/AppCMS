DROP TABLE IF EXISTS `phpcms_iappJifen_orders`;
CREATE TABLE IF NOT EXISTS `phpcms_iappJifen_orders` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `jifenid` smallint(5) NOT NULL COMMENT '产品id',
  `jifenname` varchar(20) default NULL COMMENT '产品名称',
  `price` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '产品价格',
  
  `userid` int(10) unsigned default '0' COMMENT '用户ID',
  `username` varchar(20) default NULL COMMENT '用户名',
  
  `name` varchar(20) default NULL COMMENT '姓名',
  `mobile` varchar(50) NOT NULL COMMENT '手机',
  `distribution` varchar(100) NOT NULL COMMENT '配送方式',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '下单时间',
  PRIMARY KEY (`id`)
) TYPE=MyISAM;
