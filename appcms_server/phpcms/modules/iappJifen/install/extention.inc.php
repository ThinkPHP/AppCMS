<?php
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');

$menus = $menu_db->select(array('name'=>'iappPlug', 'm'=>'iapp', 'c'=>'manages', 'a'=>'init'));

$parentid = $menu_db->insert(array('name'=>'iappJifen', 'parentid'=>$menus[0][id], 'm'=>'iappJifen', 'c'=>'manages', 'a'=>'init', 'data'=>'', 'listorder'=>0, 'display'=>'1'), true);

$menu_db->insert(array('name'=>'iappJifen_add', 'parentid'=>$parentid, 'm'=>'iappJifen', 'c'=>'manages', 'a'=>'add', 'data'=>'', 'listorder'=>2, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappJifen_edit', 'parentid'=>$parentid, 'm'=>'iappJifen', 'c'=>'manages', 'a'=>'edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappJifen_delete', 'parentid'=>$parentid, 'm'=>'iappJifen', 'c'=>'manages', 'a'=>'delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));


$menu_db->insert(array('name'=>'iappJifen_category_list', 'parentid'=>$parentid, 'm'=>'iappJifen', 'c'=>'manages', 'a'=>'category_list', 'data'=>'', 'listorder'=>6, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappJifen_category_add', 'parentid'=>$parentid, 'm'=>'iappJifen', 'c'=>'manages', 'a'=>'category_add', 'data'=>'', 'listorder'=>5, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappJifen_category_edit', 'parentid'=>$parentid, 'm'=>'iappJifen', 'c'=>'manages', 'a'=>'category_edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappJifen_category_delete', 'parentid'=>$parentid, 'm'=>'iappJifen', 'c'=>'manages', 'a'=>'category_delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappJifen_category_listorder', 'parentid'=>$parentid, 'm'=>'iappJifen', 'c'=>'manages', 'a'=>'category_listorder', 'data'=>'', 'listorder'=>0, 'display'=>'0'));






$language = array('iappJifen'=>'积分商城',
				  'iappJifen_add'=>'添加产品',
				  'iappJifen_edit'=>'修改产品',
				  'iappJifen_delete'=>'删除产品',

				 
				  'iappJifen_category_add'=>'添加分类',
				  'iappJifen_category_edit'=>'修改分类',
				  'iappJifen_category_list'=>'分类列表',
				  'iappJifen_category_delete'=>'删除分类',
				  'iappJifen_category_listorder'=>'分类排序'  
                  );

?>