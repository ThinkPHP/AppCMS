<?php
defined('IN_ADMIN') or exit('No permission resources.'); 
include $this->admin_tpl('header', 'admin');?>

<form method="post" action="?m=iappJifen&c=manages&a=edit&menuid=<?php echo $_GET['menuid']?>">
  <div class="pad-lr-10">
  <div class="col-tab">
    <ul class="tabBut cu-li">
      <li id="tab_setting_1" class="on" onclick="SwapTab('setting','on','',3,1);">基本信息</li>
      <li id="tab_setting_2" onclick="SwapTab('setting','on','',3,2);">产品简介</li>
      <li id="tab_setting_3" onclick="SwapTab('setting','on','',3,3);">规格参数</li>
    </ul>
    <div id="div_setting_1" class="contentList pad-10">
      <table width="100%" cellpadding="0" cellspacing="1" class="table_form">
        <tr>
          <th width="100"><?php echo L('分类名称')?>：</th>
          <td>
			<select name="info[catid]" id="">
				<option value="0">默认分类</option>
				<?php
				  foreach($iappJifen_category as $catid=>$type){
				?>
				<option value="<?php echo $type['id'];?>" <?php if($type['id']==$info[catid]){echo 'selected=""';}?> ><?php echo $type['title'];?></option>
				<?php }?>
			</select>
			</td>
        </tr>
        <tr>
          <th width="100"><?php echo L('标题')?>：</th>
          <td><input type="text" name="info[title]" value="<?php echo $info[title]?>" id="name"
			size="30" class="input-text"></td>
        </tr>
        <tr>
          <th width="100">长标题：</th>
          <td><input type="text" name="info[ltitle]" value="<?php echo $info[ltitle]?>" id="ltitle"
			size="60" class="input-text"></td>
        </tr>
        <tr id="image_id">
          <th width="100">产品图片：</th>
          <td><?php echo form::images('info[image]', 'image', $info[image], 'iappJifen')?></td>
        </tr>
        <tr>
          <th width="100">上线时间：</th>
          <td><?php echo form::date('info[start_time]',date('Y-m-d', $info['start_time']),0,0,'false');?>&nbsp;到 &nbsp;<?php echo form::date('info[end_time]',date('Y-m-d', $info['end_time']),0,0,'false');?></td>
        </tr>
        <tr>
          <th width="100">产品价格：</th>
          <td><input type="text" value="<?php echo $info[price]?>" name="info[price]" id="price" size="15" class="input-text">
            积分 </td>
        </tr>
        <tr>
          <th width="100">库存：</th>
          <td><input type="text" value="<?php echo $info[stock]?>" name="info[stock]" id="stock" size="15" class="input-text">
            销量：
            <input type="text" value="<?php echo $info[sales]?>" name="info[sales]" id="sales" size="15" class="input-text"></td>
        </tr>
      </table>
    </div>
    <div id="div_setting_2" class="contentList pad-10 hidden">
      <textarea name="content" id="content"><?php echo $info[content] ?></textarea>
      <?php echo form::editor('content','basic','iappJifen', '','',1,1,'',300,1)?> 
	</div>
    <div id="div_setting_3" class="contentList pad-10 hidden">
      <div id="items">
	    <?php
			$i=0;
			foreach($info[specifications] as $v){
		?>   
			<fieldset id="item_<?php echo $i;?>" title="<?php echo $i;?>">
				<table width="100%"  class="table_form">
					<th width="100">标题</th>
					<td class="y-bg">
					  <input type="text" class="input-text" name="info[specifications][<?php echo $i?>][title]" id="specifications_<?php echo $i?>_title" size="30" value="<?php echo $v[title]?>"/>
					  【<a href="#" onclick="$('#item_<?php echo $i;?>').remove();">移除</a>】
					</td>
					</tr>
					<tr>
					<th>简介</th>
					<td class="y-bg">
					  <textarea name="info[specifications][<?php echo $i?>][description]" id="specifications_<?php echo $i?>_description" cols="100" rows="6"> <?php echo $v[description]?> </textarea>
					  
					</td>
					</tr>
				</table>
			</fieldset>
		<?php
			$i++;
			}
		?>
	  </div>
      <fieldset>
        <input type="button" id="additem" value=" 添加一行 " class="button">
        如：使用说明、生效时间、注意事项、话费额等
      </fieldset>
    </div>
    <div class="bk10"></div>
	<input type="hidden" name="id" value="<?php echo $info[id];?>">
    <input type="submit" name="dosubmit" id="dosubmit" value="<?php echo L('提交')?>" class="button">
    &nbsp;
    <input type="reset" value=" <?php echo L('重写')?> " class="button">
  </div>
</form>

<script type="text/javascript">
$("#additem").click(function(){
	var n = parseInt($('#items fieldset:last').attr('title'))+1;
	if(n){
		n = n;
	}else{
		n = 1;
	}
	
	var str = '<fieldset id="item_'+n+'" title="'+n+'">'+
			  '<table width="100%"  class="table_form">'+
			  '<tr>'+
			  '<th width="100">标题</th>'+
			  '<td class="y-bg">'+
			  '<input type="text" name="info[specifications]['+n+'][title]" id="specifications_'+n+'_title" value="" size="30" class="input-text" />'+
			  '<a href="javascript:void();" onclick="$(\'#item_'+n+'\').remove();">移除</a>'+
			  '</td>'+
			  '</tr>'+
			  '<tr>'+
			  '<th>简介</th>'+
			  '<td class="y-bg">'+
			  '<textarea name="info[specifications]['+n+'][description]" id="specifications_'+n+'_description" cols="100" rows="6"></textarea>'+
			  '</td>'+
			  '</tr>'+
			  '</table>'+
		      '</fieldset>';
	$("#items").append(str);
});

function SwapTab(name,cls_show,cls_hide,cnt,cur){
	for(i=1;i<=cnt;i++){
		if(i==cur){
			 $('#div_'+name+'_'+i).show();
			 $('#tab_'+name+'_'+i).attr('class',cls_show);
		}else{
			 $('#div_'+name+'_'+i).hide();
			 $('#tab_'+name+'_'+i).attr('class',cls_hide);
		}
	}
}
</script>
</body>
</html>