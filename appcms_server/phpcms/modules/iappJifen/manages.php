<?php 
defined('IN_PHPCMS') or exit('No permission resources.'); 
pc_base::load_app_class('admin','admin',0);
pc_base::load_sys_class('form', '', 0);
pc_base::load_sys_class('format', '', 0);
class manages extends admin {
	
	function __construct() {
		parent::__construct();
		$this->sites = pc_base::load_app_class('sites','admin');
		$this->db = pc_base::load_model('iappJifen_model');
		$this->db_iappJifen_orders = pc_base::load_model('iappJifen_orders_model');
		$this->db_iappJifen_category = pc_base::load_model('iappJifen_category_model');
		$this->siteid = $this->get_siteid();
	}
	
	function init() {
		$page = max(intval($_GET['page']), 1);
		$data = $this->db->listinfo("", '`id` DESC', $page);
		include $this->admin_tpl('list');
	}
	
	function add() {
		$iappJifen_category = $this->db_iappJifen_category->select('','*','','`listorder` DESC');
		if($_POST['dosubmit']) {
			$info = $_POST['info'];
			
			$info[start_time] = trim($info[start_time]) ?  strtotime(trim($info[start_time])) : showmessage(L('开始时间不能为空'),HTTP_REFERER);
			$info[end_time] = trim($info[end_time]) ? strtotime($info[end_time]) : showmessage(L('结束时间不能为空'),HTTP_REFERER);
			
			$info[stock] = trim($info[stock]) ? intval(trim($info[stock])) : showmessage(L('库存不能为空'),HTTP_REFERER);
			$info[price] = trim($info[price]) ? intval(trim($info[price])) : showmessage(L('产品价格不能为空'),HTTP_REFERER);
			
			
			
			$info[userid] = $_SESSION['userid'];;
			$info[username] = param::get_cookie('admin_username');
			
			$info[content] = $_POST['content'];
			$info[specifications] = array2string($info[specifications]);
			
			$info[addtime] = time();
			$info[edittime] = time();
			
			$return_id = $this->db->insert($info,'1');
			showmessage(L('add_success'), '?m=iappJifen&c=manages&a=init&menuid='.$_GET['menuid']);
		} else {
			include $this->admin_tpl('add');
		}		
	}
	
	function edit() {
		$iappJifen_category = $this->db_iappJifen_category->select('','*','','`listorder` DESC');
		if($_POST['dosubmit']) {
			$id = intval($_POST['id']) ? intval($_POST['id']) : showmessage(L('parameter_error'),HTTP_REFERER);
			$info = $_POST['info'];
			
			$info[start_time] = trim($info[start_time]) ?  strtotime(trim($info[start_time])) : showmessage(L('开始时间不能为空'),HTTP_REFERER);
			$info[end_time] = trim($info[end_time]) ? strtotime($info[end_time]) : showmessage(L('结束时间不能为空'),HTTP_REFERER);
			$info[stock] = trim($info[stock]) ? intval(trim($info[stock])) : showmessage(L('库存不能为空'),HTTP_REFERER);
			$info[price] = trim($info[price]) ? intval(trim($info[price])) : showmessage(L('产品价格不能为空'),HTTP_REFERER);				
		
			$info[edittime] = time();
			$info[content] = $_POST['content'];
			$info[specifications] = array2string($info[specifications]);
			$return_id = $this->db->update($info,array('id'=>$id));
			showmessage(L('operation_success'), '?m=iappJifen&c=manages&a=edit&id='.$id.'&menuid='.$_GET['menuid']);
		} else {
			$id = intval($_GET['id']) ? intval($_GET['id']) : showmessage(L('parameter_error'),HTTP_REFERER);		
			$info = $this->db->get_one(array('id'=>$id));
			$info[content] = $info[content];
			$info[specifications] = string2array($info[specifications]);
			include $this->admin_tpl('edit');			
		}
	}
	
	/**
	 * 删除
	 */
	public function delete() {
		$siteid = $this->siteid;
		
		if (isset($_GET['id']) && !empty($_GET['id'])) {
			$id = intval($_GET['id']);
			$this->db->delete(array('id'=>$id));
			$this->db_iappJifen_orders->delete(array('id'=>$id));
			showmessage(L('operation_success'), HTTP_REFERER);
		} elseif (isset($_POST['ids']) && !empty($_POST['ids'])) {
			if (is_array($_POST['ids'])) {
				foreach ($_POST['ids'] as $fid) {
					$this->db->delete(array('id'=>$fid));
					$this->db_iappJifen_orders->delete(array('activityid'=>$fid));
				}
			}
			showmessage(L('operation_success'), HTTP_REFERER);
		} else {
			showmessage(L('illegal_operation'), HTTP_REFERER);
		}
	}
	/**
	 * 排序
	 */
	public function listorder() {
		if (isset($_POST['listorders']) && is_array($_POST['listorders'])) {
			$listorder = $_POST['listorders'];
			foreach ($listorder as $k => $v) {
				$this->db->update(array('listorder'=>$v), array('id'=>$k));
			}
		}
		showmessage(L('operation_success'), HTTP_REFERER);
	}
	
	
	
	function category_list() {
		$page = max(intval($_GET['page']), 1);
		$data = $this->db_iappJifen_category->listinfo('', '`listorder` DESC', $page);
		include $this->admin_tpl('category_list');
	}
	
	function category_add() {
		if($_POST['dosubmit']) {
			$info = $_POST['info'];
			$info[title] = trim($_POST['info'][title]) ? trim($_POST['info'][title]) : showmessage(L('主题不能为空'),HTTP_REFERER);
			$info[listorder] = 0;
			$return_id = $this->db_iappJifen_category->insert($info,'1');
			showmessage(L('add_success'), '?m=iappJifen&c=manages&a=category_list&menuid='.$_GET['menuid']);
		} else {
			include $this->admin_tpl('category_add');
		}		
	}
	
	function category_edit() {
		if($_POST['dosubmit']) {
			$id = intval($_POST['id']) ? intval($_POST['id']) : showmessage(L('parameter_error'),HTTP_REFERER);
			$info = $_POST['info'];
			$info[title] = trim($_POST['info'][title]) ? trim($_POST['info'][title]) : showmessage(L('主题不能为空'),HTTP_REFERER);
			$return_id = $this->db_iappJifen_category->update($info,array('id'=>$id));
			showmessage(L('operation_success'), '?m=iappJifen&c=manages&a=category_edit&id='.$id.'&menuid='.$_GET['menuid']);
		} else {
			$id = intval($_GET['id']) ? intval($_GET['id']) : showmessage(L('parameter_error'),HTTP_REFERER);		
			$info = $this->db_iappJifen_category->get_one(array('id'=>$id));
			include $this->admin_tpl('category_edit');			
		}
	}
	
	/**
	 * 删除
	 */
	public function category_delete() {
		$siteid = $this->siteid;
		
		if (isset($_GET['id']) && !empty($_GET['id'])) {
			$id = intval($_GET['id']);
			$this->db_iappJifen_category->delete(array('id'=>$id));
			showmessage(L('operation_success'), HTTP_REFERER);
		} elseif (isset($_POST['ids']) && !empty($_POST['ids'])) {
			if (is_array($_POST['ids'])) {
				foreach ($_POST['ids'] as $fid) {
					$this->db_iappJifen_category->delete(array('id'=>$fid));
				}
			}
			showmessage(L('operation_success'), HTTP_REFERER);
		} else {
			showmessage(L('illegal_operation'), HTTP_REFERER);
		}
	}
	
	/**
	 * 排序
	 */
	public function category_listorder() {
		if (isset($_POST['listorders']) && is_array($_POST['listorders'])) {
			$listorder = $_POST['listorders'];
			foreach ($listorder as $k => $v) {
				$this->db_iappJifen_category->update(array('listorder'=>$v), array('id'=>$k));
			}
		}
		showmessage(L('operation_success'), HTTP_REFERER);
	}
	
	
	function orders() {
		$id = trim($_GET[id]) ? trim($_GET[id]) : showmessage(L('产品不存在'),HTTP_REFERER);
		$jifen_data = $this->db->get_one(array('id'=>$id));
		$page = max(intval($_GET['page']), 1);
		$data = $this->db_iappJifen_orders->listinfo(array('jifenid'=>$id), '`id` DESC', $page);	
		include $this->admin_tpl('orders');		
	}

}
?>