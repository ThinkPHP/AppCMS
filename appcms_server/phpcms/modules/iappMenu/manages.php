<?php 
defined('IN_PHPCMS') or exit('No permission resources.'); 
pc_base::load_app_class('admin','admin',0);
pc_base::load_sys_class('form', '', 0);
class manages extends admin {
	function __construct() {
		parent::__construct();
		$this->db = pc_base::load_model('iappMenu_model');
	}
	
	function init() {
		$page = max(intval($_GET['page']), 1);
		$data = $this->db->listinfo(array(), '`listorder` DESC', $page);
		include $this->admin_tpl('list');
	}
	
	function add() {
		if($_POST['dosubmit']) {
			$info = $_POST[info];  
			$return_id = $this->db->insert($info,'1');
			$this->msc();
			showmessage(L('add_success'), '?m=iappMenu&c=manages&a=init');
		} else {
			include $this->admin_tpl('add');			
		}		
	}
	
	function edit() {
		if($_POST['dosubmit']) {
			$id = intval($_POST['id']) ? intval($_POST['id']) : showmessage(L('parameter_error'),HTTP_REFERER);
			$info = $_POST[info];
			$this->db->update($info, array('id'=>$id));
			$this->msc();
			showmessage(L('operation_success'), '?m=iappMenu&c=manages&a=edit&id='.$id.'&menuid='.$_GET['menuid']);
		}else{
			$id = intval($_GET['id']) ? intval($_GET['id']) : showmessage(L('parameter_error'),HTTP_REFERER);
			$info = $this->db->get_one(array('id'=>$id));
			include $this->admin_tpl('edit');
		}
	}
	
	public function delete() {
		if (isset($_GET['id']) && !empty($_GET['id'])) {
			$id = intval($_GET['id']);
			$this->db->delete(array('id'=>$id));
			$this->msc();
			showmessage(L('operation_success'), HTTP_REFERER);
		} elseif (isset($_POST['ids']) && !empty($_POST['ids'])) {
			if (is_array($_POST['ids'])) {
				foreach ($_POST['ids'] as $fid) {
					$this->db->delete(array('id'=>$fid));
				}
			}
			$this->msc();
			showmessage(L('operation_success'), HTTP_REFERER);
		} else {
			showmessage(L('illegal_operation'), HTTP_REFERER);
		}
	}
	
	/**
	 * 排序
	 */
	public function listorder() {
		if (isset($_POST['listorders']) && is_array($_POST['listorders'])) {
			$listorder = $_POST['listorders'];
			foreach ($listorder as $k => $v) {
				$this->db->update(array('listorder'=>$v), array('id'=>$k));
			}
		}
		$this->msc();
		showmessage(L('operation_success'), HTTP_REFERER);
	}
	
	
	public function msc() {
		$data = $this->db->select('','*','','`listorder` ASC');
		$sc = array();
		foreach ($data as $v) {
			$vd = $v[data];
			if($vd){
				$d = explode("&",$vd);
				foreach ($d as $k=>$r){
					$d[$k] = explode("=",$r);
				}
			}
			$v[f] = $d;
			if($v[url]==''){
				$v[url] = APP_PATH.'index.php?m='.$v[m].'&c='.$v[c].'&a='.$v[a].'&'.$v[data].'&menuid='.$v[id];
			}
			$sc[$v[id]]= $v;
		}
		setcache('iappMenu', $sc, 'commons');
	}
}
?>