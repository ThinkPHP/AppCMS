DROP TABLE IF EXISTS `phpcms_iappMenu`;
CREATE TABLE IF NOT EXISTS `phpcms_iappMenu` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '菜单名称',
  `url` varchar(100) NOT NULL DEFAULT '',
  `m` varchar(50) NOT NULL DEFAULT '' COMMENT '模块名',
  `c` varchar(50) NOT NULL DEFAULT '' COMMENT '文件名',
  `a` varchar(50) NOT NULL DEFAULT '' COMMENT '方法名',
  `data` varchar(50) NOT NULL DEFAULT '' COMMENT '附加参数',
  `display` int(10) unsigned default '1' COMMENT '是否显示',
  `listorder` smallint(5) DEFAULT '0',
  PRIMARY KEY (`id`)
) TYPE=MyISAM;