<?php
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');

$parentid = $menu_db->insert(array('name'=>'iappMenu', 'parentid'=>826, 'm'=>'iappMenu', 'c'=>'manages', 'a'=>'init', 'data'=>'', 'listorder'=>99, 'display'=>'1'), true);

$menu_db->insert(array('name'=>'iappMenu_add', 'parentid'=>$parentid, 'm'=>'iappMenu', 'c'=>'manages', 'a'=>'add', 'data'=>'', 'listorder'=>2, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappMenu_edit', 'parentid'=>$parentid, 'm'=>'iappMenu', 'c'=>'manages', 'a'=>'edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappMenu_delete', 'parentid'=>$parentid, 'm'=>'iappMenu', 'c'=>'manages', 'a'=>'delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));

$language = array('iappMenu'=>'页面菜单',
				  'iappMenu_add'=>'添加菜单',
				  'iappMenu_edit'=>'修改菜单',
				  'iappMenu_delete'=>'删除菜单'				  
                  );

?>