<?php
defined('IN_ADMIN') or exit('No permission resources.'); 
include $this->admin_tpl('header', 'admin');?>
<form method="post" action="?m=iappMenu&c=manages&a=edit&menuid=<?php echo $_GET['menuid']?>">
<table width="100%" cellpadding="0" cellspacing="1" class="table_form">

	<tr>
        <th width="150">菜单名称：</th>
        <td><input type="text" name="info[title]" value="<?php echo $info[title]?>" id="title" class="input-text" ></td>
    </tr>
	<tr>
		<th><?php echo L('url')?>：</th>
		<td><input type="text" name="info[url]" value="<?php echo $info[url]?>" class="input-text" size=40>外部链接请填写URL，内部模块为空</td>
	</tr>
	<tr>
        <th>模块名：</th>
        <td><input type="text" name="info[m]" value="<?php echo $info[m]?>" id="m" class="input-text" ></td>
      </tr>
	<tr>
        <th>文件名：</th>
        <td><input type="text" name="info[c]" value="<?php echo $info[c]?>" id="c" class="input-text" ></td>
    </tr>
	<tr>
        <th>方法名：</th>
        <td><input type="text" name="info[a]" value="<?php echo $info[a]?>" id="a" class="input-text" ></td>
    </tr>
	<tr>
        <th>附加参数：</th>
        <td><input type="text" name="info[data]" value="<?php echo $info[data]?>" class="input-text" ></td>
    </tr>
	
	<tr>
        <th>是否显示：</th>
        <td><input type="radio" name="info[display]" value="1" <?php if($info[display]==1){echo 'checked';}?> > 是<input type="radio" name="info[display]" value="0"> 否</td>
    </tr>
	
	<tr>
		<td>&nbsp;</td>
		<td>
		<input type="hidden" name="id" value="<?php echo $info[id];?>">
		<input type="submit" name="dosubmit" id="dosubmit" value=" <?php echo L('提交')?>" class="button">&nbsp;<input type="reset" value=" <?php echo L('重写')?> " class="button"></td>
	</tr>
</table>
</form>
</body>
</html>