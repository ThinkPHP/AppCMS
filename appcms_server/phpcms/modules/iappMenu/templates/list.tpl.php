<?php 
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');
?>

<div class="pad-lr-10">
<form name="myform" id="myform" action="" method="post">
<div class="table-list">
    <table width="100%" cellspacing="0">
        <thead>
            <tr>
            <th width="35" align="center"><input type="checkbox" value="" id="check_box" onclick="selectall('ids[]');"></th>
			<th width="50" align="center"><?php echo L('listorder');?></th>
			<th width="35" align="center">ID</th>
			<th width="120" align="center">标题</th>
			<th width="50" align="center">模块</th>
			<th width="50" align="center">文件</th>
			<th width="50" align="center">方法</th>
			<th width="100" align="center">附加参数</th>
			<th width="80" align="center">是否显示</th>
			<th align="center">URL</th>
			<th width="120" align="center"><?php echo L('operations_manage')?></th>
            </tr>
        </thead>
    <tbody>
 <?php 
if(is_array($data)){
	foreach($data as $r){
?>   
	<tr>
	<td align="center"><input class="inputcheckbox " name="ids[]" value="<?php echo $r['id'];?>" type="checkbox"></td>
	<td align='center'><input name='listorders[<?php echo $r['id'];?>]' type='text' size='3' value='<?php echo $r['listorder'];?>' class='input-text-c'></td>
	<td align='center' ><?php echo $r['id'];?></td>
	<td align='center' ><?php echo $r['title'];?></td>
	<td align='center' ><?php echo $r['m'];?></td>
	<td align='center' ><?php echo $r['c'];?></td>
	<td align='center' ><?php echo $r['a'];?></td>
	<td align="center" ><?php echo $r['data'];?></td>
	<td align="center" ><?php echo $r['display'];?></td>
	<td align="center" ><?php echo $r['url'];?></td>
	<td align="center">
		<a href="?m=iappMenu&c=manages&a=edit&id=<?php echo $r['id']?>&menuid=<?php echo $_GET['menuid']?>" onClick="return confirm('确认要删除『'.$r[title].'』吗？')">修改</a>
		<a href="?m=iappMenu&c=manages&a=delete&id=<?php echo $r['id']?>" onClick="return confirm('确认要删除『'.$r[title].'』吗？')">删除</a>
	</td>
	</tr>
<?php 
	}
}
?>
</tbody>
    </table>
  
    <div class="btn">
		<label for="check_box"><?php echo L('selected_all')?>/<?php echo L('cancel')?></label>
		<input type="hidden" value="YVt4Dk" name="pc_hash">
		<input type="button" class="button" value="排序" onclick="myform.action='?m=iappMenu&c=manages&a=listorder&dosubmit=1';myform.submit();"/>
		<input type="button" class="button" value="<?php echo L('delete');?>" onclick="myform.action='?m=iappMenu&c=manages&a=delete&dosubmit=1';return confirm_delete()"/>
	</div>  

</div>
 <div id="pages"><?php echo $this->db->pages;?></div>
</form>
</div>
<script type="text/javascript">
window.top.$("#display_center_id").css("display","none");
function confirm_delete(){
	if(confirm('<?php echo L('确认要删除『选中』吗？');?>')) $('#myform').submit();
}
</script>
</body>
</html>