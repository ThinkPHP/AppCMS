<?php 
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');?>
<form method="post" action="?m=iappMenu&c=manages&a=add">
<table width="100%" class="table_form contentWrap">
    
	<tr>
        <th width="150">菜单名称：</th>
        <td><input type="text" name="info[title]" id="title" class="input-text" ></td>
    </tr>
	<tr>
		<th><?php echo L('url')?>：</th>
		<td><input type="text" name="info[url]" class="input-text" size=40>外部链接请填写URL，内部模块为空</td>
	</tr>
	<tr>
        <th>模块名：</th>
        <td><input type="text" name="info[m]" id="m" class="input-text" ></td>
      </tr>
	<tr>
        <th>文件名：</th>
        <td><input type="text" name="info[c]" id="c" class="input-text" ></td>
    </tr>
	<tr>
        <th>方法名：</th>
        <td><input type="text" name="info[a]" id="a" class="input-text" ></td>
    </tr>
	<tr>
        <th>附加参数：</th>
        <td><input type="text" name="info[data]" class="input-text" ></td>
    </tr>
	
	<tr>
        <th>是否显示：</th>
        <td><input type="radio" name="info[display]" value="1" checked> <?php echo L('yes')?><input type="radio" name="info[display]" value="0"> <?php echo L('no')?></td>
    </tr>
	
	<tr>
		<td>&nbsp;</td>
		<td><input type="submit" name="dosubmit" id="dosubmit" value=" <?php echo L('提交')?>" class="button">&nbsp;<input type="reset" value=" <?php echo L('重写')?> " class="button"></td>
	</tr>
</table>
</form>
</body>
</html>