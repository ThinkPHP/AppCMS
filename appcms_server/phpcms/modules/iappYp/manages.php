<?php 
defined('IN_PHPCMS') or exit('No permission resources.'); 
//模型缓存路径
define('CACHE_MODEL_PATH',CACHE_PATH.'caches_model'.DIRECTORY_SEPARATOR.'caches_data'.DIRECTORY_SEPARATOR);
//定义在单独操作内容的时候，同时更新相关栏目页面
define('RELATION_HTML',true);
pc_base::load_app_class('admin','admin',0);
pc_base::load_sys_class('form', '', 0);
pc_base::load_sys_class('format', '', 0);
pc_base::load_app_func('global');
class manages extends admin {
	function __construct() {
		parent::__construct();
		$this->sites = pc_base::load_app_class('sites','admin');
		$this->db = pc_base::load_model('model_content_model');
		$this->siteid = $this->get_siteid();
	}
	
	function init() {
	
		$show_header = true;
		$page = max(intval($_GET['page']), 1);
		$catid = max(intval($_GET['catid']), 1);
		$modelid = intval($_GET['modelid']);
		
		if($modelid){
			$this->db->set_model($modelid);
			$data = $this->db->listinfo(array('catid'=>$catid), '`id` DESC', $page);
			include $this->admin_tpl('list');
		} else {
			include $this->admin_tpl('main');
		}
	}
	
	function import() {
		$show_header = true;
		$catid = max(intval($_GET['catid']), 1);
		$modelid = intval($_GET['modelid']);
		
		/*
		$json = '{"list":{"7":{"title":"\u672c\u5730\u7f8e\u98df","node":[{"id":"21","title":"\u5feb\u9910\u5916\u5356"},{"id":"20","title":"\u4e2d\u9910\u5c0f\u7092"},{"id":"19","title":"\u706b\u9505"},{"id":"18","title":"\u5c0f\u5403\u5bb5\u591c"},{"id":"17","title":"\u9762\u9986"},{"id":"16","title":"\u9152\u5e97\u5bb4\u8bf7"},{"id":"15","title":"\u5496\u5561\u5385"},{"id":"14","title":"\u519c\u5bb6\u5c71\u5e84"},{"id":"59","title":"\u86cb\u7cd5\u751c\u70b9"},{"id":"13","title":"\u6599\u7406"},{"id":"74","title":"\u897f\u9910\u5385"}]},"6":{"title":"\u4f11\u95f2\u5a31\u4e50","node":[{"id":"29","title":"\u8336\u697c"},{"id":"28","title":"KTV"},{"id":"27","title":"\u9152\u5427"},{"id":"26","title":"\u7535\u5f71\u9662"},{"id":"24","title":"\u7f51\u5427"},{"id":"22","title":"\u8db3\u6d74"},{"id":"76","title":"\u684c\u7403\u3001\u684c\u6e38"},{"id":"97","title":"\u65c5\u884c\u793e"},{"id":"101","title":"\u6e38\u4e50\u56ed"}]},"5":{"title":"\u5bbe\u9986\u4f4f\u5bbf","node":[{"id":"34","title":"\u661f\u7ea7\u9152\u5e97"},{"id":"33","title":"\u5feb\u6377\u9152\u5e97"},{"id":"31","title":"\u5c0f\u5bbe\u9986"},{"id":"30","title":"\u519c\u5bb6\u5c71\u5e84"}]},"4":{"title":"\u4fbf\u6c11\u5bb6\u653f","node":[{"id":"40","title":"\u5feb\u9012"},{"id":"39","title":"\u642c\u5bb6"},{"id":"38","title":"\u5f00\u9501"},{"id":"37","title":"\u57f9\u8bad"},{"id":"36","title":"\u5bb6\u653f\u516c\u53f8"},{"id":"35","title":"\u5a5a\u5e86\u6444\u5f71"},{"id":"62","title":"\u94f6\u884c"},{"id":"71","title":"\u533b\u9662\u8bca\u6240"},{"id":"96","title":"\u6570\u7801\u4ea7\u54c1\u4e0e\u7ef4\u4fee"},{"id":"99","title":"\u5bb6\u5c45\u88c5\u4fee"},{"id":"102","title":"\u5e7f\u544a\u7b56\u5212"}]},"3":{"title":"\u7f8e\u5bb9\u7f8e\u53d1","node":[{"id":"52","title":"\u7f8e\u53d1"},{"id":"42","title":"\u7f8e\u5bb9\u7f8e\u4f53"},{"id":"41","title":"\u7f8e\u7532\u5316\u5986"},{"id":"70","title":"\u5065\u8eab\u4f1a\u6240"}]},"2":{"title":"\u6c7d\u8f66\u670d\u52a1","node":[{"id":"49","title":"4S\u5e97"},{"id":"48","title":"\u6c7d\u8f66\u7ef4\u4fee"},{"id":"47","title":"\u6d17\u8f66"},{"id":"46","title":"\u6c7d\u8f66\u88c5\u6f62"},{"id":"45","title":"\u52a0\u6cb9\u7ad9"},{"id":"44","title":"\u4fdd\u9669\u516c\u53f8"},{"id":"110","title":"\u6c7d\u8f66\u79df\u8d41"},{"id":"111","title":"\u6c7d\u8f66\u79df\u8d41"}]},"87":{"title":"\u751f\u6d3b\u8d2d\u7269","node":[{"id":"95","title":"\u5546\u573a"},{"id":"89","title":"\u8d85\u5e02"},{"id":"91","title":"\u836f\u5e97"},{"id":"94","title":"\u82b1\u5e97"},{"id":"104","title":"\u5bb6\u5c45\u9986"},{"id":"107","title":"\u5ba0\u7269\u5e97"},{"id":"108","title":"\u4e2a\u4f53\u5c0f\u5e97"},{"id":"109","title":"\u6bcd\u5a74\u7528\u54c1"}]}},"uptime":1392540852}';
		
		$obj = json_decode($json,true);
		//print_r($obj);
		
		$l = $obj['list'];
		
		$a = '';
		//$catid1=subcat(7);
		//$t = array();
		foreach ($l as $r) {
			
			$a .= $r[title]."\n";
			
			$Node = $r[node];
			foreach ($Node as $v) {
				$a .= "-".$v[title]."\n";
			}
		}
		echo $a;
		exit;
		
		
		
		*/
		
		/*
		
	

		$modelid = 19;
		$category = getcache('category_content_1','commons');
		$_category = getcache('category_'.$modelid,'commons');
		$cate = array();
		foreach ($category as $k=>$v) {
			foreach ($_category as $n=>$r) {
				if($v[catname]==$r[catname]){
					$cate[$k] = $n;
				}
			}	 
		}
		
		$ai_db = pc_base::load_model('attachment_index_model');
		$a_db = pc_base::load_model('attachment_model');
		
		$content_db = pc_base::load_model('content_model');
		$content_db->set_model(12);
		$c = $content_db->select(array('status'=>99));
		
		$info = array();

		foreach ($c as $k=>$v) {
			$r = $content_db->get_content($v[catid],$v[id]);
			
			if($cate[$r[catid]]){
				$catid = $cate[$r[catid]];
			}else{
				$catid = 88;
			}
			
			$info[$k] = Array(
				"catid" => $catid,
				"title" => $r[title],
				"keywords" => $r[keywords],
				"description" => $r[description],
				"thumb" => $r[thumb],
				"areaid" => 2642,
				"c_mode" => Array
					(
						"0" => -99,
						"1" => 3,
					),

				"c_type" => 1,
				"c_size" => '1-49人',
				"business" => $r[r_zhuying],

				"address" => $r[r_adds],
				"postcode" => '550008',
				"map" => $r[r_map],
				"tel" => $r[r_tel],
				"content" => $r[content],
				"photo" => 1,
				
				"allow_comment" => 1,
				"paytype" => 0,
				"islink" => 0,
				"status" => 99
			);
			
			$photo[$k] = string2array($r[r_photo]);
		}
		
		
		$this->db->set_model($modelid);
		$modellist = getcache('modellist','commons');
		param::set_cookie('module',$modellist[$modelid][tablename]);
		foreach ($info as $k=>$v) {
			try{
				//设置cookie 在附件添加处调用
				$_POST[info] = $v;
				$p = $photo[$k];
				foreach ($p as $n=>$r) {
					$photo_url[$n] = $r[url];
					$photo_alt[$n] = $r[alt];
				}
				$_POST[photo_url] = $photo_url;
				$_POST[photo_alt] = $photo_alt;
			
				$id = $this->db->add_content($_POST[info]);
			}catch(Exception $e){
				//echo 'Message: ' .$e->getMessage();
			}
		}
		
		*/
		
	}
	
	function add() {
		$show_header = true;
		$modelid = intval($_GET['modelid']);
		if(!$modelid){
			showmessage(L('必须选择模型！'),HTTP_REFERER);
		}
		$this->db->set_model($modelid);
		$categorys = getcache('category_'.$modelid,'commons');
		
		if(isset($_POST['dosubmit']) || isset($_POST['dosubmit_continue'])) {
			define('INDEX_HTML',true);
			$catid = $_POST['info']['catid'] = intval($_POST['info']['catid']);
			if(trim($_POST['info']['title'])=='') showmessage(L('title_is_empty'));
			$category = $this->categorys[$catid];	
			
			//如果该栏目设置了工作流，那么必须走工作流设定
			$setting = string2array($category['setting']);
			$workflowid = $setting['workflowid'];
			if($workflowid && $_POST['status']!=99) {
				//如果用户是超级管理员，那么则根据自己的设置来发布
				$_POST['info']['status'] = $_SESSION['roleid']==1 ? intval($_POST['status']) : 1;
			} else {
				$_POST['info']['status'] = 99;
			}
			
			$this->db->add_content($_POST['info']);
			if(isset($_POST['dosubmit'])) {
				showmessage('保存成功，<font color="red"><B><span id="secondid">2</span></B> 秒后自动关闭</font>','blank','','','function set_time() {$("#secondid").html(1);}setTimeout("set_time()", 500);setTimeout("window.close()", 1200);');
			} else {
				showmessage(L('add_success'),HTTP_REFERER);
			}
			
		} else {
			$show_dialog = '';
			$show_validator = '';
			$modellist = getcache('modellist','commons');
			
			foreach ($categorys as $k=>$v) {
				$catid =$v[catid];
				break;
			}
			$catid = $_GET['catid'] ? intval($_GET['catid']) : $catid;
			
			//设置cookie 在附件添加处调用
			param::set_cookie('module',$modellist[$modelid][tablename]);
			require CACHE_MODEL_PATH.'model_form.class.php';
			$model_form = new model_form($modelid,$catid,$categorys);

			$forminfos = $model_form->get();
			
			$formValidator = $model_form->formValidator;
			$setting = string2array($categorys['setting']);
			$workflowid = $setting['workflowid'];
			$workflows = getcache('workflow_'.$this->siteid,'commons');
			$workflows = $workflows[$workflowid];
			$workflows_setting = string2array($workflows['setting']);
			$nocheck_users = $workflows_setting['nocheck_users'];
			$admin_username = param::get_cookie('admin_username');
			if(!empty($nocheck_users) && in_array($admin_username, $nocheck_users)) {
				$priv_status = true;
			} else {
				$priv_status = false;
			}
					
			include $this->admin_tpl('add');
		}		
	}
	
	function edit() {
		$show_header = true;
		$modelid = intval($_GET['modelid']);
		if(!$modelid){
			showmessage(L('必须选择模型！'),HTTP_REFERER);
		}
		$modellist = getcache('modellist','commons');
		
		$this->db->set_model($modelid);
		$categorys = getcache('category_'.$modelid,'commons');
		//设置cookie 在附件添加处调用
		param::set_cookie('module',$modellist[$modelid][tablename]);
		//param::set_cookie('module', 'content');
		if(isset($_POST['dosubmit']) || isset($_POST['dosubmit_continue'])) {
			
			define('INDEX_HTML',true);
			$id = $_POST['info']['id'] = intval($_POST['id']);
			$catid = $_POST['info']['catid'] = intval($_POST['info']['catid']);
			if(trim($_POST['info']['title'])=='') {
				showmessage(L('title_is_empty'));
			}
			
			$this->db->edit_content($_POST['info'],$id);
			
			if(isset($_POST['dosubmit'])) {
				showmessage('保存成功，<font color="red"><B><span id="secondid">2</span></B> 秒后自动关闭</font>','blank','','','function set_time() {$("#secondid").html(1);}setTimeout("set_time()", 500);setTimeout("window.close()", 1200);');
			} else {
				showmessage(L('add_success'),HTTP_REFERER);
			}
			
		} else {
			$show_header = $show_dialog = $show_validator = '';
			//从数据库获取内容
			$id = intval($_GET['id']);
			if(!isset($_GET['catid']) || !$_GET['catid']) showmessage(L('missing_part_parameters'));
			$catid = $_GET['catid'] = intval($_GET['catid']);
			
			$this->model = getcache('modellist', 'commons');
			
			param::set_cookie('catid', $catid);
			//$category = $this->categorys[$catid];
			//$modelid = $category['modelid'];
			$this->db->table_name = $this->db->db_tablepre.$this->model[$modelid]['tablename'];
			$r = $this->db->get_one(array('id'=>$id));
			$this->db->table_name = $this->db->table_name.'_data';
			$r2 = $this->db->get_one(array('id'=>$id));
			if(!$r2) showmessage(L('subsidiary_table_datalost'),'blank');
			$data = array_merge($r,$r2);
			$data = array_map('htmlspecialchars_decode',$data);
			require CACHE_MODEL_PATH.'model_form.class.php';
			$content_form = new model_form($modelid,$catid,$this->categorys);

			$forminfos = $content_form->get($data);
			$formValidator = $content_form->formValidator;
			include $this->admin_tpl('edit');
		}
	}
	
	/**
	 * 删除
	 */
	public function delete() {
		if(isset($_GET['dosubmit'])) {
		
			$this->db->set_model($modelid);
			$catid = intval($_GET['catid']);
			if(!$catid) showmessage(L('missing_part_parameters'));
			$modelid = intval($_GET['modelid']);
			$sethtml = $this->categorys[$catid]['sethtml'];
			$siteid = $this->categorys[$catid]['siteid'];
			
			$html_root = pc_base::load_config('system','html_root');
			if($sethtml) $html_root = '';
			
			$setting = string2array($this->categorys[$catid]['setting']);
			$content_ishtml = $setting['content_ishtml']; 
			$this->db->set_model($modelid);
			$this->hits_db = pc_base::load_model('hits_model');
			$this->queue = pc_base::load_model('queue_model');
			if(isset($_GET['ajax_preview'])) {
				$ids = intval($_GET['id']);
				$_POST['ids'] = array(0=>$ids);
			}
			if(empty($_POST['ids'])) showmessage(L('you_do_not_check'));
			//附件初始化
			$attachment = pc_base::load_model('attachment_model');
			$this->content_check_db = pc_base::load_model('content_check_model');
			$this->position_data_db = pc_base::load_model('position_data_model');
			$this->search_db = pc_base::load_model('search_model');
			//判断视频模块是否安装 
			if (module_exists('video') && file_exists(PC_PATH.'model'.DIRECTORY_SEPARATOR.'video_content_model.class.php')) {
				$video_content_db = pc_base::load_model('video_content_model');
				$video_install = 1;
			}
			$this->comment = pc_base::load_app_class('comment', 'comment');
			$search_model = getcache('search_model_'.$this->siteid,'search');
			$typeid = $search_model[$modelid]['typeid'];
			$this->url = pc_base::load_app_class('url', 'content');
			
			foreach($_POST['ids'] as $id) {
				$r = $this->db->get_one(array('id'=>$id));
				if($content_ishtml && !$r['islink']) {
					$urls = $this->url->show($id, 0, $r['catid'], $r['inputtime']);
					$fileurl = $urls[1];
					if($this->siteid != 1) {
						$sitelist = getcache('sitelist','commons');
						$fileurl = $html_root.'/'.$sitelist[$this->siteid]['dirname'].$fileurl;
					}
					//删除静态文件，排除htm/html/shtml外的文件
					$lasttext = strrchr($fileurl,'.');
					$len = -strlen($lasttext);
					$path = substr($fileurl,0,$len);
					$path = ltrim($path,'/');
					$filelist = glob(PHPCMS_PATH.$path.'*');
					foreach ($filelist as $delfile) {
						$lasttext = strrchr($delfile,'.');
						if(!in_array($lasttext, array('.htm','.html','.shtml'))) continue;
						@unlink($delfile);
						//删除发布点队列数据
						$delfile = str_replace(PHPCMS_PATH, '/', $delfile);
						$this->queue->add_queue('del',$delfile,$this->siteid);
					}
				} else {
					$fileurl = 0;
				}
				//删除内容
				$this->db->delete_content($id,$fileurl,$catid);
				//删除统计表数据
				$this->hits_db->delete(array('hitsid'=>'c-'.$modelid.'-'.$id));
				//删除附件
				$attachment->api_delete('c-'.$catid.'-'.$id);
				//删除审核表数据
				$this->content_check_db->delete(array('checkid'=>'c-'.$id.'-'.$modelid));
				//删除推荐位数据
				$this->position_data_db->delete(array('id'=>$id,'catid'=>$catid,'module'=>'content'));
				//删除全站搜索中数据
				$this->search_db->delete_search($typeid,$id);
				//删除视频库与内容对应关系数据
				if ($video_install ==1) {
					$video_content_db->delete(array('contentid'=>$id, 'modelid'=>$modelid));
				}
				
				//删除相关的评论,删除前应该判断是否还存在此模块
				if(module_exists('comment')){
					$commentid = id_encode('content_'.$catid, $id, $siteid);
					$this->comment->del($commentid, $siteid, $id, $catid);
				}
				
 			}
			//更新栏目统计
			$this->db->cache_items();
			showmessage(L('operation_success'),HTTP_REFERER);
		} else {
			showmessage(L('operation_failure'));
		}
	}
	
	/**
	 * 排序
	 */
	public function listorder() {
		if (isset($_POST['listorders']) && is_array($_POST['listorders'])) {
			$listorder = $_POST['listorders'];
			foreach ($listorder as $k => $v) {
				$this->db->update(array('listorder'=>$v), array('id'=>$k));
			}
		}
		showmessage(L('operation_success'), HTTP_REFERER);
	}

	public function subnav() {
		$modelid = intval($_GET['modelid']);
		$catid = intval($_GET['catid']);
		$a = $_GET['a'];
		$modellist = getcache('modellist','commons');
		$modename = $modellist[$modelid][name];
		if($a=='add'){
			$html.='<a href="index.php?m=iappYp&c=manages&a=init&modelid='. $modelid .'&catid='. $catid .'&pc_hash='. $_SESSION['pc_hash'].'"><em>'.$modename.'内容管理</em></a><span>|</span>';
			$html.='<a href="javascript:;" class="on"><em>添加'.$modename.'内容</em></a>';
		}else{
			$html.='<a href="javascript:;" class="on"><em>'.$modename.'内容管理'.'</em></a><span>|</span>';
			$html.='<a onclick="javascript:openwinx(\'index.php?m=iappYp&c=manages&a=add&modelid='. $modelid .'&catid='. $catid .'&pc_hash='. $_SESSION['pc_hash'].'\',\'\')" href="" ><em>添加'.$modename.'内容</em></a>';
		}
		$html.='<span>|</span><a href="index.php?m=iappYp&c=manages&a=import&modelid='. $modelid .'&catid='. $catid .'&pc_hash='. $_SESSION['pc_hash'].'"><em>导入数据</em></a>';
		
		
		echo $html;
	}
	
	/**
	 * 显示栏目菜单列表
	 */
	public function public_categorys() {
		$show_header = '';
		$cfg = getcache('common','commons');
		$ajax_show = intval($cfg['category_ajax']);

		$model_db = pc_base::load_model('sitemodel_model');
		$model = $model_db->select(array('module'=>'iappYp'),'*','','modelid DESC');
		
		$_m = array();
		$_categorys = array();
		foreach($model as $k=>$v) {
			
			$_m[$v[modelid]][catid] = $v[modelid];
			$_m[$v[modelid]][modelid] = $v[modelid];
			$_m[$v[modelid]][parentid] = 0;
			$_m[$v[modelid]][arrparentid] = 0;
			$_m[$v[modelid]][catname] = $v[name];
			$_m[$v[modelid]][tablename] = $v[tablename];
			$_m[$v[modelid]][listorder] = $v[modelid];
			
			$_categorys = getcache('category_'.$v[modelid],'commons');
			//加入图标
			foreach($_categorys as $r) {
				
					$r['icon_type'] = $r['vs_show'] = '';
					$r['type'] = 'init';
					$r['add_icon'] = "<a onclick=\"javascript:openwinx('?m=iappYp&c=manages&a=add&menuid=".$_GET['menuid']."&modelid=".$r['modelid']."&catid=".$r['catid']."&hash_page=".$_SESSION['hash_page']."','')\"><img src='".IMG_PATH."add_content.gif' alt='".L('add')."'></a> ";
				
				$_categorys[$r['catid']] = $r;
			}
			
			foreach($_categorys as $n=>$r) {
				if($r[parentid]==0){
					$_categorys[$n][parentid] = $v[modelid];
					$_categorys[$n][arrparentid] = '0,'.$v[modelid];
				}
			}
			
			foreach($_categorys as $n=>$r) {
			
				$categorys[$n] = $r;
			}
		}

		foreach($_m as $k=>$r) {
			$categorys[$k] = $r;
		}
		
		$tree = pc_base::load_sys_class('tree');
		$tree->init($categorys);
		$strs = "<span class='\$icon_type'>\$add_icon<a href='?m=iappYp&c=manages&a=init&menuid=".$_GET['menuid']."&modelid=\$modelid&catid=\$catid' target='right' onclick='open_list(this)'>\$catname</a></span>";
		
		$strs2 = "<span class='folder'>\$catname</span>";
		
		$categorys = $tree->get_treeview(0,'category_tree',$strs,$strs2,$ajax_show);
		
        include $this->admin_tpl('tree');
	}
	
	function orders() {
		$id = trim($_GET[id]) ? trim($_GET[id]) : showmessage(L('产品不存在'),HTTP_REFERER);
		$jifen_data = $this->db->get_one(array('id'=>$id));
		$page = max(intval($_GET['page']), 1);
		$data = $this->db_iappYp_orders->listinfo(array('jifenid'=>$id), '`id` DESC', $page);	
		include $this->admin_tpl('orders');		
	}
	
}
?>