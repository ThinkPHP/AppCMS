 <?php 
defined('IN_PHPCMS') or exit('Access Denied');
defined('UNINSTALL') or exit('Access Denied');

$model_db = pc_base::load_model('sitemodel_model');
$field_db = pc_base::load_model('sitemodel_field_model');
$category_db = pc_base::load_model('category_model');
$type_db = pc_base::load_model('type_model');

$_modellist = $model_db->select(array('module'=>'iappYp'));
foreach ($_modellist as $k => $v) {
	$field_db->delete(array('modelid'=>$v[modelid]));
	$type_db->delete(array('modelid'=>$v[modelid]));
	
	$table_name = 'phpcms_'.$v[tablename];
	$model_db->sql_execute("DROP TABLE IF EXISTS `$table_name`");
	$table_name_data = $table_name.'_data';
	$model_db->sql_execute("DROP TABLE IF EXISTS `$table_name_data`");
}
$model_db->delete(array('module'=>'iappYp'));
$category_db->delete(array('module'=>'iappYp'));
delcache('iappYp_model', 'model');

 ?>