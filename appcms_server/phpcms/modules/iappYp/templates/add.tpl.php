<?php
defined('IN_ADMIN') or exit('No permission resources.');$addbg=1;
include $this->admin_tpl('header','admin');?>
<script type="text/javascript">
	var charset = '<?php echo CHARSET;?>';
	var uploadurl = '<?php echo pc_base::load_config('system','upload_url')?>';
	var catid=<?php echo $catid;?>;
</script>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>content_addtop.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>colorpicker.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>hotkeys.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo JS_PATH?>cookie.js"></script>


<form name="myform" id="myform" action="?m=iappYp&c=manages&a=add&modelid=<?php echo $modelid;?>&catid=<?php echo $catid;?>" method="post">
  <div class="addContent">
    <div class="crumbs"><?php echo L('add_content_position');?></div>
	<div class="col-1">
    <div class="content pad-6">
      <div class="col-tab">
        <ul class="tabBut cu-li">
          <li id="tab_setting_1" class="on" onclick="SwapTab('setting','on','',3,1);">基本信息</li>
          <li id="tab_setting_2" onclick="SwapTab('setting','on','',3,2);">附加参数</li>
        </ul>
        <div id="div_setting_1" class="contentList pad-10">
          <table width="100%" cellspacing="0" class="table_form">
            <tbody>
              <?php
if(is_array($forminfos['base'])) {
 foreach($forminfos['base'] as $field=>$info) {
	 if($info['isomnipotent']) continue;
	 if($info['formtype']=='omnipotent') {
		foreach($forminfos['base'] as $_fm=>$_fm_value) {
			if($_fm_value['isomnipotent']) {
				$info['form'] = str_replace('{'.$_fm.'}',$_fm_value['form'],$info['form']);
			}
		}
		foreach($forminfos['senior'] as $_fm=>$_fm_value) {
			if($_fm_value['isomnipotent']) {
				$info['form'] = str_replace('{'.$_fm.'}',$_fm_value['form'],$info['form']);
			}
		}
	}
 ?>
              <tr>
                <th width="80"><?php if($info['star']){ ?>
                  <font color="red">*</font>
                  <?php } ?>
                  <?php echo $info['name']?> </th>
                <td><?php echo $info['form']?> <?php echo $info['tips']?></td>
              </tr>
              <?php
} }
?>
            </tbody>
          </table>
        </div>
        <div id="div_setting_2" class="contentList pad-10 hidden">
          <table width="100%" cellspacing="0" class="table_form">
            <tbody>
              <?php
if(is_array($forminfos['senior'])) {
 foreach($forminfos['senior'] as $field=>$info) {
	if($info['isomnipotent']) continue;
	if($info['formtype']=='omnipotent') {
		foreach($forminfos['base'] as $_fm=>$_fm_value) {
			if($_fm_value['isomnipotent']) {
				$info['form'] = str_replace('{'.$_fm.'}',$_fm_value['form'],$info['form']);
			}
		}
		foreach($forminfos['senior'] as $_fm=>$_fm_value) {
			if($_fm_value['isomnipotent']) {
				$info['form'] = str_replace('{'.$_fm.'}',$_fm_value['form'],$info['form']);
			}
		}
	}
 ?>
              <tr>
                <th width="80"><?php if($info['star']){ ?>
                  <font color="red">*</font>
                  <?php } ?>
                  <?php echo $info['name']?></th>
                <td><?php echo $info['form']?><?php echo $info['tips']?></td>
              </tr>
              <?php
	}
}
?>
              <?php 
	if($_SESSION['roleid']==1 || $priv_status) {
?>
              <tr>
                <th width="80"><?php echo L('c_status');?></th>
                <td><span class="ib" style="width:90px">
                  <label>
                    <input type="radio" name="status" value="99" checked/>
                    <?php echo L('c_publish');?> </label>
                  </span></td>
              </tr>
              <?php if($workflowid) { ?>
              <tr>
                <th width="80">状态</th>
                <td><label>
                    <input type="radio" name="status" value="1" >
                    <?php echo L('c_check');?> </label></td>
              </tr>
              <?php } ?>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
	</div>
  </div>
  <div class="fixed-bottom">
    <div class="fixed-but text-c">
      <div class="button">
        <input value="<?php echo L('save_close');?>" type="submit" name="dosubmit" class="cu" style="width:145px;" onclick="refersh_window()">
      </div>
      <div class="button">
        <input value="<?php echo L('save_continue');?>" type="submit" name="dosubmit_continue" class="cu" style="width:130px;" title="Alt+X" onclick="refersh_window()">
      </div>
      <div class="button">
        <input value="<?php echo L('c_close');?>" type="button" name="close" onclick="refersh_window();close_window();" class="cu" style="width:70px;">
      </div>
    </div>
  </div>
</form>
<script type="text/javascript"> 
//只能放到最下面
var openClose = $("#RopenClose"), rh = $(".addContent .col-auto").height(),colRight = $(".addContent .col-right"),valClose = getcookie('openClose');
$(function(){
	if(valClose==1){
		colRight.hide();
		openClose.addClass("r-open");
		openClose.removeClass("r-close");
	}else{
		colRight.show();
	}
	openClose.height(rh);
	$.formValidator.initConfig({formid:"myform",autotip:true,onerror:function(msg,obj){window.top.art.dialog({id:'check_content_id',content:msg,lock:true,width:'200',height:'50'}, 	function(){$(obj).focus();
	boxid = $(obj).attr('id');
	if($('#'+boxid).attr('boxid')!=undefined) {
		check_content(boxid);
	}
	})}});
	<?php echo $formValidator;?>
	
/*
 * 加载禁用外边链接
 */

	$('#linkurl').attr('disabled',true);
	$('#islink').attr('checked',false);
	$('.edit_content').hide();
	jQuery(document).bind('keydown', 'Alt+x', function (){close_window();});
})
document.title='<?php echo L('add_content');?>';
self.moveTo(-4, -4);
function refersh_window() {
	setcookie('refersh_time', 1);
}
openClose.click(
	  function (){
		if(colRight.css("display")=="none"){
			setcookie('openClose',0,1);
			openClose.addClass("r-close");
			openClose.removeClass("r-open");
			colRight.show();
		}else{
			openClose.addClass("r-open");
			openClose.removeClass("r-close");
			colRight.hide();
			setcookie('openClose',1,1);
		}
	}
)

function SwapTab(name,cls_show,cls_hide,cnt,cur){
	for(i=1;i<=cnt;i++){
		if(i==cur){
			 $('#div_'+name+'_'+i).show();
			 $('#tab_'+name+'_'+i).attr('class',cls_show);
		}else{
			 $('#div_'+name+'_'+i).hide();
			 $('#tab_'+name+'_'+i).attr('class',cls_hide);
		}
	}
}
</script>
</body>
</html>
