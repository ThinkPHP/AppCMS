<?php 
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');
?>

<SCRIPT LANGUAGE="JavaScript">
	if(window.top.$("#current_pos").data('clicknum')==1 || window.top.$("#current_pos").data('clicknum')==null) {
	parent.document.getElementById('display_center_id').style.display='';
	parent.document.getElementById('center_frame').src = '?m=iappYp&c=manages&a=public_categorys&menuid=<?php echo $_GET['menuid'];?>&pc_hash=<?php echo $_SESSION['pc_hash'];?>';
	window.top.$("#current_pos").data('clicknum',0);
}
</SCRIPT>
<div id="closeParentTime" style="display:none"></div>
<div class="subnav">
    <div class="content-menu ib-a blue line-x">
    <?php $this->subnav();	?>
	</div>
</div>

<div class="pad-lr-10">
<form name="myform" id="myform" action="" method="post">
<div class="table-list">
    <table width="100%" cellspacing="0">
        <thead>
            <tr>
            <th width="35" align="center">
			<input type="checkbox" value="" id="check_box" onclick="selectall('ids[]');"></th>
			<th width="50" align="center"><?php echo L('listorder');?></th>
			<th width="30" align="center">ID</th>
			<th width="30" align="center">栏目</th>
			<th align="center">活动名称</th>
			<th width="50" align="center">活动时间</th>
			<th width="50" align="center">发布时间</th>
			
			<th width="220" align="center"><?php echo L('operations_manage')?></th>
            </tr>
        </thead>
    <tbody>
 <?php 
if(is_array($data)){
	foreach($data as $r){
?>   
	<tr>
	<td align="center"><input class="inputcheckbox " name="ids[]" value="<?php echo $r['id'];?>" type="checkbox"></td>
    <td align='center'><input name='listorders[<?php echo $r['id'];?>]' type='text' size='3' value='<?php echo $r['listorder'];?>' class='input-text-c'></td>
	
	<td align='center' ><?php echo $r['id'];?></td>
	<td align='center' ><?php echo $r['catid'];?></td>
	
	<td>
	<a href="<?php echo $r['url']?>" target="_blank"><?php echo $r['title']?></a>
	<?php if($r['thumb']!='') {echo '<img src="'.IMG_PATH.'icon/small_img.gif" title="'.L('thumb').'">'; } if($r['posids']) {echo '<img src="'.IMG_PATH.'icon/small_elite.gif" title="'.L('elite').'">';} if($r['islink']) {echo ' <img src="'.IMG_PATH.'icon/link.png" title="'.L('islink_url').'">';}?>
	</td>
	
	<td align='center' ><?php echo date('Y-m-d', $r['activity_time']);?></td>
	<td align='center' ><?php echo date('Y-m-d', $r['addtime']);?></td>
	
	<td align="center">
	<a onclick="javascript:openwinx('?m=iappYp&c=manages&a=edit&id=<?php echo $r['id']?>&modelid=<?php echo $modelid ;?>&catid=<?php echo $catid ;?>&menuid=<?php echo $_GET['menuid']?>','')"  href="javascript:;" ><?php echo L('修改')?></a> | <a href="?m=iappYp&c=manages&a=delete&id=<?php echo $r['id']?>&modelid=<?php echo $modelid ;?>&catid=<?php echo $catid ;?>&dosubmit=1" onClick="return confirm('<?php echo L('确认要删除『'.$r[title].'』吗？');?>')"><?php echo L('删除')?></a>
	</td>
	</tr>
<?php 
	}
}
?>
</tbody>
    </table>
  
    <div class="btn"><label for="check_box"><?php echo L('selected_all')?>/<?php echo L('cancel')?></label>
		<input type="hidden" value="YVt4Dk" name="pc_hash">
    	<input type="button" class="button" value="排序" onclick="myform.action='?m=iappYp&c=manages&a=listorder&modelid=<?php echo $modelid ;?>&catid=<?php echo $catid ;?>&menuid=<?php echo $_GET['menuid']?>&dosubmit=1';myform.submit();"/>
		
		<input type="button" class="button" value="<?php echo L('delete');?>" onclick="myform.action='?m=iappYp&c=manages&a=delete&modelid=<?php echo $modelid ;?>&catid=<?php echo $catid ;?>&menuid=<?php echo $_GET['menuid']?>&dosubmit=1';return confirm_delete()"/>
		
		</div>  

</div>
 <div id="pages"><?php echo $this->db->pages;?></div>
</form>
</div>
<script type="text/javascript"> 
function confirm_delete(){
	if(confirm('<?php echo L('确认要删除『选中』吗？');?>')) $('#myform').submit();
}

$(document).ready(function(){
	setInterval(closeParent,3000);
});
function closeParent() {
	if($('#closeParentTime').html() == '') {
		window.top.$(".left_menu").addClass("left_menu_on");
		window.top.$("#openClose").addClass("close");
		window.top.$("html").addClass("on");
		$('#closeParentTime').html('1');
		window.top.$("#openClose").data('clicknum',1);
	}
}

</script>
</body>
</html>