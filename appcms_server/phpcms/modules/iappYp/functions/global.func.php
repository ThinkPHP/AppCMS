<?php
	
	/*
	* 根据模块获得模块id
	* model 模块名称
	*/
	function model2modelid($model){
		$db = pc_base::load_model('sitemodel_model');
		$_model = $db->get_one(array('tablename'=>$model));
		return $_model[modelid];
	}
	
	/*
	* 检查模型id是否正确
	* cache 数组
	*/
	function chek_modelid($modelid){
		$sitemodel_db = pc_base::load_model('sitemodel_model');
		$model_r = $sitemodel_db->get_one(array('modelid'=>$modelid));
		if ($model_r) {
			$modelid = $model_r[modelid];
		}else{
			$model_r = $sitemodel_db->get_one(array('tablename'=>'iappYp_company'), 'modelid');
			$modelid = $model_r[modelid];
		}
		return $modelid;
	}
	
	
?>