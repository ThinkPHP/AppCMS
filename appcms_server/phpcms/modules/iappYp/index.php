<?php 
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_sys_class('format', '', 0);
class index {
	function __construct() {		
		$this->db = pc_base::load_model('model_content_model');
		$this->siteid = get_siteid();
	}
	
	public function init() {
	
		if(isset($_GET['siteid'])) {
			$siteid = intval($_GET['siteid']);
		} else {
			$siteid = 1;
		}
		$siteid = $GLOBALS['siteid'] = max($siteid,1);
		define('SITEID', $siteid);
		$_userid = $this->_userid;
		$_username = $this->_username;
		$_groupid = $this->_groupid;
		//SEO
		$SEO = seo($siteid);
		$sitelist  = getcache('sitelist','commons');
		$default_style = $sitelist[$siteid]['default_style'];
		$CATEGORYS = getcache('category_content_'.$siteid,'commons');
		include template('content','index',$default_style);
	}
	
	//列表页
	public function lists() {
		$page = max(intval($_GET['page']), 1);
		$catid = intval($_GET['catid']);
		$t = $_GET['t'];
		
		$modellist_t = getcache('modellist_t','commons');
		$modelid = $modellist_t[$t][modelid];
		if(!$modelid){
			showmessage(L('必须选择模型！'),HTTP_REFERER);
		}
		
		$this->db->set_model($modelid);
		$info = $this->db->listinfo(array('catid'=>$catid), '`id` DESC', $page);
		
		$CATEGORYS = getcache('category_'.$modelid,'commons');
		require_once CACHE_MODEL_PATH.'model_output.class.php';
		$content_output = new model_output($modelid,$catid,$CATEGORYS);
		
		foreach ($info as $k=>$v) {
			$info[$k][c_mode] = $content_output->box("c_mode",$v[c_mode]);
			$info[$k][c_type] = $content_output->box("c_type",$v[c_type]);
			$info[$k][areaid] = get_linkage($v[areaid], 1, '>', 1);
		}
		print_r($info);
	
		exit;
		
		include $this->admin_tpl('list');
			
			
		
		$catid = $_GET['catid'] = intval($_GET['catid']);
		$_priv_data = $this->_category_priv($catid);
		if($_priv_data=='-1') {
			$forward = urlencode(get_url());
			showmessage(L('login_website'),APP_PATH.'index.php?m=member&c=index&a=login&forward='.$forward);
		} elseif($_priv_data=='-2') {
			showmessage(L('no_priv'));
		}
		$_userid = $this->_userid;
		$_username = $this->_username;
		$_groupid = $this->_groupid;

		if(!$catid) showmessage(L('category_not_exists'),'blank');
		$siteids = getcache('category_content','commons');
		$siteid = $siteids[$catid];
		$CATEGORYS = getcache('category_content_'.$siteid,'commons');
		if(!isset($CATEGORYS[$catid])) showmessage(L('category_not_exists'),'blank');
		$CAT = $CATEGORYS[$catid];
		$siteid = $GLOBALS['siteid'] = $CAT['siteid'];
		extract($CAT);
		$setting = string2array($setting);
		//SEO
		if(!$setting['meta_title']) $setting['meta_title'] = $catname;
		$SEO = seo($siteid, '',$setting['meta_title'],$setting['meta_description'],$setting['meta_keywords']);
		define('STYLE',$setting['template_list']);
		$page = intval($_GET['page']);

		$template = $setting['category_template'] ? $setting['category_template'] : 'category';
		$template_list = $setting['list_template'] ? $setting['list_template'] : 'list';
		
		if($type==0) {
			$template = $child ? $template : $template_list;
			$arrparentid = explode(',', $arrparentid);
			$top_parentid = $arrparentid[1] ? $arrparentid[1] : $catid;
			$array_child = array();
			$self_array = explode(',', $arrchildid);
			//获取一级栏目ids
			foreach ($self_array as $arr) {
				if($arr!=$catid && $CATEGORYS[$arr][parentid]==$catid) {
					$array_child[] = $arr;
				}
			}
			$arrchildid = implode(',', $array_child);
			//URL规则
			$urlrules = getcache('urlrules','commons');
			$urlrules = str_replace('|', '~',$urlrules[$category_ruleid]);
			$tmp_urls = explode('~',$urlrules);
			$tmp_urls = isset($tmp_urls[1]) ?  $tmp_urls[1] : $tmp_urls[0];
			preg_match_all('/{\$([a-z0-9_]+)}/i',$tmp_urls,$_urls);
			if(!empty($_urls[1])) {
				foreach($_urls[1] as $_v) {
					$GLOBALS['URL_ARRAY'][$_v] = $_GET[$_v];
				}
			}
			define('URLRULE', $urlrules);
			$GLOBALS['URL_ARRAY']['categorydir'] = $categorydir;
			$GLOBALS['URL_ARRAY']['catdir'] = $catdir;
			$GLOBALS['URL_ARRAY']['catid'] = $catid;
			include template('content',$template);
		} else {
		//单网页
			$this->page_db = pc_base::load_model('page_model');
			$r = $this->page_db->get_one(array('catid'=>$catid));
			if($r) extract($r);
			$template = $setting['page_template'] ? $setting['page_template'] : 'page';
			$arrchild_arr = $CATEGORYS[$parentid]['arrchildid'];
			if($arrchild_arr=='') $arrchild_arr = $CATEGORYS[$catid]['arrchildid'];
			$arrchild_arr = explode(',',$arrchild_arr);
			array_shift($arrchild_arr);
			$keywords = $keywords ? $keywords : $setting['meta_keywords'];
			$SEO = seo($siteid, 0, $title,$setting['meta_description'],$keywords);
			include template('content',$template);
		}
	}
	
	//内容页
	public function show() {
		$catid = intval($_GET['catid']);
		$id = intval($_GET['id']);
		$t = $_GET['t'];
		
		$modellist_t = getcache('modellist_t','commons');
		$modelid = $modellist_t[$t][modelid];
		if(!$modelid){
			showmessage(L('必须选择模型！'),HTTP_REFERER);
		}
		
		$this->db->set_model($modelid);
		$info = $this->db->get_content($modelid,$id);
		$CATEGORYS = getcache('category_'.$modelid,'commons');
		require_once CACHE_MODEL_PATH.'model_output.class.php';
		$content_output = new model_output($modelid,$catid,$CATEGORYS);
		$data = $content_output->get($info);
		print_r($data);
		
		//include template('content',$template);
	}
	
}
?>