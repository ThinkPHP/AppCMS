ALTER TABLE `phpcms_iappYp_company_data` 
ADD `sell` varchar(255) NOT NULL DEFAULT '' COMMENT '销售的产品',
ADD `buy` varchar(255) NOT NULL DEFAULT '' COMMENT '采购的产品',

ADD `c_size` varchar(100) NOT NULL DEFAULT '' COMMENT '公司规模',
ADD `regyear` varchar(4) NOT NULL DEFAULT '' COMMENT '注册年份',
ADD `capital` float unsigned NOT NULL DEFAULT '0' COMMENT '注册资本',
ADD `regcity` varchar(30) NOT NULL DEFAULT '' COMMENT '注册城市',
ADD `postcode` varchar(20) NOT NULL DEFAULT '' COMMENT '邮编',
ADD `logo` varchar(120) NOT NULL COMMENT 'logo',
ADD `banner` varchar(120) NOT NULL COMMENT '形象图片',
ADD `photo` mediumtext,
ADD `skin` varchar(30) NOT NULL DEFAULT '' COMMENT '风格',

ADD `homepage` varchar(255) NOT NULL DEFAULT '' COMMENT '公司网址',
ADD `domain` varchar(100) NOT NULL DEFAULT '' COMMENT '绑定顶级域名',
ADD `icp` varchar(100) NOT NULL DEFAULT '' COMMENT 'ICP备案号';

INSERT INTO `phpcms_model_field` (`modelid`, `siteid`, `field`, `name`, `tips`, `css`, `minlength`, `maxlength`, `pattern`, `errortips`, `formtype`, `setting`, `formattribute`, `unsetgroupids`, `unsetroleids`, `iscore`, `issystem`, `isunique`, `isbase`, `issearch`, `isadd`, `isfulltext`, `isposition`, `listorder`, `disabled`, `isomnipotent`) VALUES
($modelid, $siteid, 'buy', '采购产品', '多个产品或服务请用''''|''''号隔开', '', 0, 0, '', '', 'text', 'array (\n  ''size'' => ''50'',\n  ''defaultvalue'' => '''',\n  ''ispassword'' => ''0'',\n)', '', '', '', 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0),

($modelid, $siteid, 'sell', '销售产品', '多个产品或服务请用''''|''''号隔开', '', 0, 0, '', '', 'text', 'array (\n  ''size'' => ''50'',\n  ''defaultvalue'' => '''',\n  ''ispassword'' => ''0'',\n)', '', '', '', 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0),

($modelid, $siteid, 'homepage', '公司网址', '', '', 0, 0, '', '', 'text', 'array (\n  ''size'' => ''50'',\n  ''defaultvalue'' => '''',\n  ''ispassword'' => ''0'',\n)', '', '', '', 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0),

($modelid, $siteid,'c_size', '公司规模', '', '', 1, 0, '', '', 'box', 'array (\n  ''options'' => ''1-49人|1-49人\r\n50-99人|50-99人\r\n100-499人|100-499人\r\n500-999人|500-999人\r\n1000-3000人|1000-3000人\r\n3000-5000人|3000-5000人\r\n10000人以上|10000人以上'',\n  ''boxtype'' => ''select'',\n  ''fieldtype'' => ''varchar'',\n  ''minnumber'' => ''1'',\n  ''width'' => ''80'',\n  ''size'' => ''1'',\n  ''defaultvalue'' => '''',\n  ''outputtype'' => ''0'',\n  ''filtertype'' => ''0'',\n)', '', '', '', 0, 0, 0, 0, 0, 1, 0, 0, 25, 0, 0),

($modelid, $siteid,'capital', '注册资本', '', '', 0, 0, '', '', 'text', 'array (\n  ''size'' => ''15'',\n  ''defaultvalue'' => '''',\n  ''ispassword'' => ''0'',\n)', '', '', '', 0, 0, 0, 0, 0, 1, 1, 0, 24, 0, 0),

($modelid, $siteid,'regyear', '注册年份', '', '', 0, 0, '', '', 'text', 'array (\n  ''size'' => ''15'',\n  ''defaultvalue'' => '''',\n  ''ispassword'' => ''0'',\n)', '', '', '', 0, 0, 0, 0, 0, 1, 1, 0, 26, 0, 0),

($modelid, $siteid,'regcity', '注册城市', '', '', 0, 0, '', '', 'text', 'array (\n  ''size'' => ''15'',\n  ''defaultvalue'' => '''',\n  ''ispassword'' => ''0'',\n)', '', '', '', 0, 0, 0, 0, 0, 1, 1, 0, 27, 0, 0),

($modelid, $siteid,'postcode', '邮编', '', '', 0, 0, '/^[0-9]{6}$/', '', 'text', 'array (\n  ''size'' => ''10'',\n  ''defaultvalue'' => '''',\n  ''ispassword'' => ''0'',\n)', '', '', '', 0, 0, 0, 0, 0, 1, 1, 0, 35, 0, 0),

($modelid, $siteid,'logo', 'logo', '', '', 0, 0, '', '', 'image', 'array (\n  ''size'' => ''40'',\n  ''defaultvalue'' => '''',\n  ''show_type'' => ''0'',\n  ''upload_maxsize'' => '''',\n  ''upload_allowext'' => ''gif|jpg|jpeg|png|bmp'',\n  ''watermark'' => ''0'',\n  ''isselectimage'' => ''1'',\n  ''images_width'' => '''',\n  ''images_height'' => '''',\n)', '', '', '', 0, 0, 0, 0, 0, 1, 0, 0, 37, 0, 0),

($modelid, $siteid,'banner', '形象图片', '', '', 0, 0, '', '', 'image', 'array (\n  ''size'' => ''40'',\n  ''defaultvalue'' => '''',\n  ''show_type'' => ''0'',\n  ''upload_maxsize'' => '''',\n  ''upload_allowext'' => ''gif|jpg|jpeg|png|bmp'',\n  ''watermark'' => ''0'',\n  ''isselectimage'' => ''1'',\n  ''images_width'' => '''',\n  ''images_height'' => '''',\n)', '', '', '', 0, 0, 0, 0, 0, 1, 0, 0, 38, 0, 0),

($modelid, $siteid,'photo', '公司图片', '', '', 0, 0, '', '', 'images', 'array (\n  ''upload_allowext'' => ''gif|jpg|jpeg|png|bmp'',\n  ''isselectimage'' => ''0'',\n  ''upload_number'' => ''10'',\n)', '', '', '', 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0);
