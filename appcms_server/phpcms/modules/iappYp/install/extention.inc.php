<?php
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');
define('SQL_PATH', dirname(__FILE__).DIRECTORY_SEPARATOR);

$menus = $menu_db->select(array('name'=>'iapp', 'm'=>'iapp', 'c'=>'manages', 'a'=>'init'));
if($menus){
	$menuid=$menus[0][id];
}else{
	$menuid=6;
}

$parentid = $menu_db->insert(array('name'=>'iappYp', 'parentid'=>$menuid, 'm'=>'iappYp', 'c'=>'manages', 'a'=>'init', 'data'=>'', 'listorder'=>99, 'display'=>'1'), true);

$parentid1 = $menu_db->insert(array('name'=>'iappYp_manages', 'parentid'=>$parentid, 'm'=>'iappYp', 'c'=>'manages', 'a'=>'init', 'data'=>'', 'listorder'=>0, 'display'=>'1'), true);
$menu_db->insert(array('name'=>'iappYp_add', 'parentid'=>$parentid1, 'm'=>'iappYp', 'c'=>'manages', 'a'=>'add', 'data'=>'', 'listorder'=>2, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappYp_edit', 'parentid'=>$parentid1, 'm'=>'iappYp', 'c'=>'manages', 'a'=>'edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappYp_delete', 'parentid'=>$parentid1, 'm'=>'iappYp', 'c'=>'manages', 'a'=>'delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));


$language = array('iappYp'=>'企业库',
				'iappYp_manages'=>'商家管理',
				'iappYp_add'=>'添加企业',
				'iappYp_edit'=>'修改企业',
				'iappYp_delete'=>'删除企业'
                );
	
	//添加category表两个新字段
	$sitemodel_db = pc_base::load_model('sitemodel_model');
	if (!$sitemodel_db->field_exists('module')) {
		$sitemodel_db->query("ALTER TABLE `phpcms_model` ADD `module` varchar(100) NOT NULL DEFAULT ''");
	}

	//添加4个模型
	$models = array();
	$models[iappYp_company] = '企业模型';
	$models[iappYp_product] = '产品模型';
	$models[iappYp_buy] = '商机模型';
	$models[iappYp_news] = '企业新闻';

	$model_api = pc_base::load_app_class('model_api','model');
	foreach ($models as $k => $v) {
		$modelid = $model_api->add_model($v, $k,'iappYp', 1);
		$siteid =1;
		$sql = '';
		$sql = file_get_contents(SQL_PATH.$k.'.sql');
		$sql = str_replace('$modelid',$modelid,$sql);
		$sql = str_replace('$siteid',$siteid,$sql);
		$sitemodel_db->sql_execute($sql);
		$sql = '';
		$sql = file_get_contents(SQL_PATH.$k.'_data.sql');
		$sql = str_replace('$modelid',$modelid,$sql);
		$sql = str_replace('$siteid',$siteid,$sql);
		$sitemodel_db->sql_execute($sql);
		$modelid = $model_api->cache_field($modelid);//更新缓存
	}
	
	
?>