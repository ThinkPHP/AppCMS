ALTER TABLE `phpcms_iappYp_company` 
ADD `c_type` varchar(100) NOT NULL DEFAULT '' COMMENT '公司类型',
ADD `areaid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '地区ID',
ADD `c_mode` varchar(100) NOT NULL DEFAULT '' COMMENT '经营模式',

ADD `operate` varchar(255) NOT NULL DEFAULT '' COMMENT '主营',
ADD `tel` varchar(50) NOT NULL DEFAULT '' COMMENT '电话',
ADD `fax` varchar(50) NOT NULL DEFAULT '' COMMENT '传真',
ADD `mail` varchar(50) NOT NULL DEFAULT '' COMMENT '电子邮件',
ADD `address` varchar(255) NOT NULL DEFAULT '' COMMENT '地址',
ADD `map` varchar(50) NOT NULL COMMENT '地图',

ADD `fromtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'VIP开始时间',
ADD `totime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'VIP过期时间',
ADD `styletime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '风格模板过期时间';



INSERT INTO `phpcms_model_field` (`modelid`, `siteid`, `field`, `name`, `tips`, `css`, `minlength`, `maxlength`, `pattern`, `errortips`, `formtype`, `setting`, `formattribute`, `unsetgroupids`, `unsetroleids`, `iscore`, `issystem`, `isunique`, `isbase`, `issearch`, `isadd`, `isfulltext`, `isposition`, `listorder`, `disabled`, `isomnipotent`) VALUES
($modelid, $siteid,'c_type', '公司类型', '', '', 1, 0, '', '请选择公司类型', 'box', 'array (\n  ''options'' => ''企业单位|1\r\n事业单位或社会团体|2\r\n个体经营|3\r\n其他|4'',\n  ''boxtype'' => ''select'',\n  ''fieldtype'' => ''varchar'',\n  ''minnumber'' => ''1'',\n  ''width'' => ''80'',\n  ''size'' => ''1'',\n  ''defaultvalue'' => '''',\n  ''outputtype'' => ''0'',\n  ''filtertype'' => ''0'',\n)', '', '', '', 0, 1, 0, 1, 0, 1, 0, 0, 21, 0, 0),

($modelid, $siteid,'areaid', '地区', '请选择所在城市', '', 1, 0, '/^[0-9.-]+$/', '请选择地区', 'linkage', 'array (\n  ''linkageid'' => ''1'',\n  ''showtype'' => ''1'',\n  ''space'' => '''',\n  ''filtertype'' => ''0'',\n)', '', '', '', 0, 1, 0, 1, 0, 1, 0, 0, 22, 0, 0),
($modelid, $siteid,'c_mode', '经营模式', '', '', 1, 0, '', '', 'box', 'array (\n  ''options'' => ''制造商|1\r\n贸易商|2\r\n服务商|3\r\n其他机构|4'',\n  ''boxtype'' => ''checkbox'',\n  ''fieldtype'' => ''tinyint'',\n  ''minnumber'' => ''1'',\n  ''width'' => ''80'',\n  ''size'' => ''1'',\n  ''defaultvalue'' => '''',\n  ''outputtype'' => ''0'',\n  ''filtertype'' => ''0'',\n)', '', '', '', 0, 1, 0, 1, 0, 1, 0, 0, 23, 0, 0),

($modelid, $siteid,'operate', '主营', '', '', 0, 0, '', '', 'text', 'array (\n  ''size'' => ''50'',\n  ''defaultvalue'' => '''',\n  ''ispassword'' => ''0'',\n)', '', '', '', 0, 1, 0, 1, 0, 1, 1, 0, 28, 0, 0),

($modelid, $siteid,'tel', '联系电话', '', '', 1, 0, '/^[0-9-]{6,13}$/', '请输入联系电话', 'text', 'array (\n  ''size'' => ''15'',\n  ''defaultvalue'' => '''',\n  ''ispassword'' => ''0'',\n)', '', '', '', 0, 1, 0, 1, 0, 1, 1, 0, 30, 0, 0),
($modelid, $siteid,'fax', '传真', '', '', 0, 0, '/^[0-9-]{6,13}$/', '', 'text', 'array (\n  ''size'' => ''15'',\n  ''defaultvalue'' => '''',\n  ''ispassword'' => ''0'',\n)', '', '', '', 0, 1, 0, 1, 0, 1, 1, 0, 31, 0, 0),
($modelid, $siteid,'mail', '电子邮件', '', '', 0, 0, '/^[\\w\\-\\.]+@[\\w\\-\\.]+(\\.\\w+)+$/', '', 'text', 'array (\n  ''size'' => ''20'',\n  ''defaultvalue'' => '''',\n  ''ispassword'' => ''0'',\n)', '', '', '', 0, 1, 0, 1, 0, 1, 1, 0, 32, 0, 0),
($modelid, $siteid,'address', '地址', '', '', 2, 0, '', '地址不能为空', 'text', 'array (\n  ''size'' => ''50'',\n  ''defaultvalue'' => '''',\n  ''ispassword'' => ''0'',\n)', '', '', '', 0, 1, 0, 1, 0, 1, 1, 0, 33, 0, 0),
($modelid, $siteid,'map', '地图', '', '', 0, 0, '', '', 'map', 'array (\n  ''maptype'' => ''2'',\n  ''api_key'' => ''515d8f09b55e76739daa924b48e6ccb8'',\n  ''defaultcity'' => ''贵阳'',\n  ''hotcitys'' => '''',\n  ''width'' => ''600'',\n  ''height'' => ''350'',\n)', '', '', '', 0, 1, 0, 1, 0, 1, 0, 0, 34, 0, 0);
