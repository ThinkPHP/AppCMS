<?php 
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_sys_class('format', '', 0);
class app {
	function __construct() {		
		$this->db = pc_base::load_model('model_content_model');
		$this->url = pc_base::load_app_class('url', 'model');
		$this->siteid = get_siteid();
		$this->config = getcache('iapp', 'commons');
		$this->hitsdb = pc_base::load_model('hits_model');
	}
	
	public function init() {
		echo "no init!";
	}
	
	/*
	* 栏目列表接口
	*/
	public function get_category() {
		$re = array();
		$t = trim($_GET['t']) ? trim($_GET['t']) : null;
		$modellist_t = getcache('modellist_t','commons');
		$modelid = $modellist_t[$t][modelid];
		$CATEGORYS = getcache('category_'.$modelid,'commons');
		if(intval(trim($_GET['catid']))>0){
			$catid = intval(trim($_GET['catid']));
		}else{
			foreach ($CATEGORYS as $k=>$v) {
				$catid =$v[catid];
				break;
			}
		}
		
		$t = array();
		$k=0;
		foreach ($CATEGORYS as $ck=>$v) {
			if($v[parentid]==0){
				$t[$k][id] = $v[catid];
				$t[$k][title] = $v[catname];
				$c = array();
				foreach ($CATEGORYS as $n=>$r) {
					if($v[catid] == $r[parentid]){
						$c[][title] = $r[catname];
					}
				}
				$t[$k][node] =$c;
				$k++;
			}
		}
		echo json_encode(array("list"=>$t));
	}
	
	//列表页
	public function lists() {
		$re = array();
		$page = max(intval($_GET['page']), 1);
		$t = trim($_GET['t'])? trim($_GET['t']) : null;
		$modellist_t = getcache('modellist_t','commons');
		$modelid = $modellist_t[$t][modelid]? $modellist_t[$t][modelid]:null ;
		if($modelid){
			$catid = intval(trim($_GET['catid']));
			$this->db->set_model($modelid);
			$pattern = getcache('pattern', 'commons');
			$pagesize = $this->config[setting][lifelistnum];
			$offset = ($page - 1) * $pagesize;
			if($catid){
				$catids = get_sql_catid('category_'.$modelid,$catid);
				$w = "`status` = 99 AND ".$catids;
			}else{
				$w = "`status` = 99 ";
			}
			$info = $this->db->select($w, '*', $offset.','.$pagesize, 'id DESC');
			
			$CATEGORYS = getcache('category_'.$modelid,'commons');
			require_once CACHE_MODEL_PATH.'model_output.class.php';
			$content_output = new model_output($modelid,$catid,$CATEGORYS);
			
			foreach ($info as $k=>$v) {
				$info[$k][c_mode] = $content_output->box("c_mode",$v[c_mode]);
				$info[$k][c_type] = $content_output->box("c_type",$v[c_type]);
				//$info[$k][map] = $content_output->map("map",$v[map]);
				//$info[$k][areaid] = get_linkage($v[areaid], 1, '>', 1);
			}

			$AREA = getcache(1,'linkage');
			foreach($info as $r){
				$map = explode('|',$r[map]);
				$re['list'][] = array(
							"id"=>$r[id],
							"uid"=>$r[userid],
							"username"=>$r[username],
							"title"=>$r[title],
							"thumb"=>thumb($r[thumb],168,124),
							"areaid"=>$r[areaid],
							"areaname"=>$AREA[data][$r[areaid]][name],
							"catid"=>$r[catid],
							"catname"=>$CATEGORYS[$r[catid]][catname],
							"distance"=>"6082.4公里",
							"operate"=>$r[r_zhuying],
							"tel"=>$r[tel],
							"x"=>$map[0],
							"y"=>$map[1],
							"hits"=>$r[hits],
							"addtime"=>$r[inputtime],
							"star"=>66666
							);
			}
			
			$re[status]="OK";
			$re[msg]="数据加载成功~";
			
		}else{
			$re[status]="OK";
			$re['list']=null;
			$re[msg]="参数错误~";
		}
		$re[upTime] = date('Y-m-j H:i:s');
		echo json_encode($re);	
	}
	
	//内容页
	public function show() {
		$re = array();
		$t = trim($_GET['t'])? trim($_GET['t']) : null;
		$modellist_t = getcache('modellist_t','commons');
		$modelid = $modellist_t[$t][modelid]? $modellist_t[$t][modelid]:null ;
		$id = intval(trim($_GET['id']));
		if($modelid && $id){
			$this->db->set_model($modelid);
			$info = $this->db->get_content($modelid,$id);
			$CATEGORYS = getcache('category_'.$modelid,'commons');
			require_once CACHE_MODEL_PATH.'model_output.class.php';
			$content_output = new model_output($modelid,$info[catid],$CATEGORYS);
			$hits = $this->hits($id,$modelid);
			$AREA = getcache(3360,'linkage');
			$map = explode('|',$info[map]);
			$comment_db = pc_base::load_model('comment_model');
			$commentid = id_encode('content_'.$catid,$id,$this->siteid);
			$comment = $comment_db->get_one(array('commentid'=>$commentid, 'siteid'=>$this->siteid));
			
			$re[data] = array(
						  "id"=>$info[id],
						  "userid"=>$info[userid],
						  "username"=>$info[username],
						  "title"=>$info[title],
						  "modelid"=>$modelid,
						  "tablename"=>$modellist_t[$t][tablename],
						  "mobile"=>"",
						  "areaid"=>$info[areaid],
						  "areaname"=>$AREA[data][intval($info[areaid])][name],
						  "catid"=>$info[catid],
						  "catname"=>$CATEGORYS[$info[catid]][catname],
						  "thumb"=>$info[thumb],
						  "operate"=>$info[operate],
						  "address"=>$info[address],
						  "tel"=>$info[tel],
						  "fax"=>$info[fax],
						  "x"=>$map[0],
						  "y"=>$map[1],
						  "distance"=>"6082.4公里",
						  "uptime"=>$info[updatetime],
						  "addtime"=>$info[inputtime],
						  "hits"=>$hits,
						  "comment_proportion"=>"90%",
						  "comment_total"=>$comment[total]?$comment[total]:0,
						  "content"=>$info[content],
						  "url"=>$this->url->show($modelid,$id,2)
						);
			
			//优惠券
			$re[coupons] =array("id"=>"7",
							"picurl"=>"201305/071135412153.jpg",	
							"title"=>"凭优惠劵购票半价后再减5元",
							"lid"=>"4086",
							"name"=>"XX影城",
							"startdate"=>"1367769600",
							"enddate"=>""
							);
			//图片
			$photo = array();
			foreach(string2array($info[photo]) as $r){
					$photo[b][] = $r[url];
					$photo[m][] = thumb($r[url],133,100);
				}
			$re[photo] = $photo;
			
			$re[status]="OK";
			$re[msg]="数据加载成功~";
		}else{
			$re[status]="ERR";
			$re[msg]="参数错误~";
		}
		echo json_encode($re);
	}
	
	/**
	 * 获取点击数量
	 * @param $id
	 * @param $modelid
	 */
	public function hits($id,$modelid) {
		$hitsid = 'c-'.$modelid.'-'.intval($id);
		if($modelid && $id) {
			$r = $this->hitsdb->get_one(array('hitsid'=>$hitsid));
			if(!$r) return false;
			$views = $r['views'] + 1;
			$yesterdayviews = (date('Ymd', $r['updatetime']) == date('Ymd', strtotime('-1 day'))) ? $r['dayviews'] : $r['yesterdayviews'];
			$dayviews = (date('Ymd', $r['updatetime']) == date('Ymd', SYS_TIME)) ? ($r['dayviews'] + 1) : 1;
			$weekviews = (date('YW', $r['updatetime']) == date('YW', SYS_TIME)) ? ($r['weekviews'] + 1) : 1;
			$monthviews = (date('Ym', $r['updatetime']) == date('Ym', SYS_TIME)) ? ($r['monthviews'] + 1) : 1;
			$sql = array('views'=>$views,'yesterdayviews'=>$yesterdayviews,'dayviews'=>$dayviews,'weekviews'=>$weekviews,'monthviews'=>$monthviews,'updatetime'=>SYS_TIME);
			$this->hitsdb->update($sql, array('hitsid'=>$hitsid));
		}
		return $views;	
	}
}
?>