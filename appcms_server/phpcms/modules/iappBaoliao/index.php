<?php 
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_sys_class('format', '', 0);
class index {
	function __construct() {		
		$this->db = pc_base::load_model('iappBaoliao_model');
		$this->db_iappBaoliao_type = pc_base::load_model('iappBaoliao_type_model');
		$this->db_iappBaoliao_reply = pc_base::load_model('iappBaoliao_reply_model');
		$this->db_iappBaoliao_praise = pc_base::load_model('iappBaoliao_praise_model');
		$this->upload_url = pc_base::load_config('system','upload_url');
		$this->upload_path = pc_base::load_config('system','upload_path');	
		
		$this->config = getcache('iapp', 'commons');
		if(intval($_GET['siteid']) > 0){
			$this->siteid = intval($_GET['siteid']);
		}else{
			$this->siteid = $this->config[setting][siteid]? $this->config[setting][siteid]:1;
		}
	}
	
	public function init() {
		$data = $this->db->select(array('siteid'=>$this->siteid), '*','5','`id` DESC');
		$list = array();
		foreach ($data as $k=>$v) {
			$list[$k][id] = $v[id];
			$list[$k][content] = $v[content];
			$list[$k][renum] = $v[renum];
		}
		
		$data = $this->db->select(array('siteid'=>$this->siteid), '*','5','`replytime` DESC');
		$reply = array();
		foreach ($data as $k=>$v) {
			$reply[$k][id] = $v[id];
			$reply[$k][content] = $v[content];
			$reply[$k][renum] = $v[renum];
		}
		
		//更新点击数
		$return_id = $this->db->update(array('htis'=>'+=1'),array('siteid'=>$this->siteid));
		//更新时间
		$upTime = param::get_cookie($MODEL[$modelid]['tablename'].$this->siteid.'upTime');
		param::set_cookie($MODEL[$modelid]['tablename'].$this->siteid.'upTime', date('Y-m-j H:i:s'));

		echo json_encode(array("list"=>$list,"reply"=>$reply,"upTime"=>$upTime));
	}
	
	public function lists() {
		$page = max(intval($_GET['page']), 1);
		$order = intval($_GET['order'])==0?"id":"replytime";
		$pagesize = $this->config[setting][Baoliaolistnum];
		$data = $this->db->listinfo(array('siteid'=>$this->siteid), '`'.$order.'` DESC', $page,$pagesize);
		$re = array();
		foreach ($data as $k=>$v) {
			$re[lists][$k][id] = $v[id];
			$re[lists][$k][typeid] = $v[typeid];
			$re[lists][$k][userid] = $v[userid];
			$re[lists][$k][username] = $v[username];
  
			$re[lists][$k][content] = $v[content];
			$re[lists][$k][photos] = string2array($v[photos]);
			$re[lists][$k][photoshd] = string2array($v[photoshd]);
			$re[lists][$k][voice] = trim($v[voice]);
			$re[lists][$k][address] = trim($v[address]);
			
			
			$re[lists][$k][htis] = $v[htis];
			$re[lists][$k][renum] = $v[renum];
			$re[lists][$k][praise] = $v[praise];
			$re[lists][$k][replytime] = date('m-d H:i:s', $v['replytime']);
			$re[lists][$k][addtime] = date('m-d H:i:s', $v['addtime']);
			$memberinfo = get_memberinfo($v[userid]);
			$re[lists][$k][nickname] = $memberinfo[nickname];
			
			
			
			$this->_init_phpsso();
			$ssoinfo = unserialize($this->client->ps_get_member_info($memberinfo[phpssouid]));
			if($ssoinfo[avatar]==1){
				$re[lists][$k][avatar] = get_memberavatar($memberinfo[phpssouid],'',90);
			}else{
				$re[lists][$k][avatar] = '';
			}
			$re[lists][$k][reply] = $this->db_iappBaoliao_reply->select(array('baoliaoid'=>$v[id]), '*','10','`id` DESC');
			
			$re[lists][$k][praisedata] = $this->db_iappBaoliao_praise->select(array('baoliaoid'=>$v[id]), '*','3','`id` DESC');
			
		}
		$re[upTime] = date('Y-m-j H:i:s');
		//更新点击数
		$return_id = $this->db->update(array('htis'=>'+=1'),array('siteid'=>$this->siteid));
		echo json_encode($re);
	}
	
	/**
	 * 初始化phpsso
	 * about phpsso, include client and client configure
	 * @return string phpsso_api_url phpsso地址
	 */
	private function _init_phpsso() {
		pc_base::load_app_class('client', 'member', 0);
		define('APPID', pc_base::load_config('system', 'phpsso_appid'));
		$phpsso_api_url = pc_base::load_config('system', 'phpsso_api_url');
		$phpsso_auth_key = pc_base::load_config('system', 'phpsso_auth_key');
		$this->client = new client($phpsso_api_url, $phpsso_auth_key);
		return $phpsso_api_url;
	}
	
	public function show() {
		$re = array();
		$baoliaoid = intval($_GET['baoliaoid']) ? intval($_GET['baoliaoid']) : null;
		if($baoliaoid){
			$Baoliaoinfo = $this->db->get_one(array('id'=>$baoliaoid));
			if($Baoliaoinfo){
				$re[data][id] = $Baoliaoinfo[id];
				$re[data][typeid] = $Baoliaoinfo[typeid];
				$re[data][userid] = $Baoliaoinfo[userid];
				$re[data][username] = $Baoliaoinfo[username];

				$re[data][content] = $Baoliaoinfo[content];
				$re[data][photos] = string2array($Baoliaoinfo[photos]);
				$re[data][photoshd] = string2array($Baoliaoinfo[photoshd]);
				$re[data][voice] = trim($Baoliaoinfo[voice]);
				$re[data][address] = trim($Baoliaoinfo[address]);

				$re[data][htis] = $Baoliaoinfo[htis];
				$re[data][renum] = $Baoliaoinfo[renum];
				$re[data][praise] = $Baoliaoinfo[praise];
				$re[data][replytime] = date('m-d H:i:s', $Baoliaoinfo['replytime']);
				$re[data][addtime] = date('m-d H:i:s', $Baoliaoinfo['addtime']);
					
				$memberinfo = get_memberinfo($Baoliaoinfo[userid]);
				$re[data][nickname] = $memberinfo[nickname];
					
				$this->_init_phpsso();
				$ssoinfo = unserialize($this->client->ps_get_member_info($memberinfo[phpssouid]));
				if($ssoinfo[avatar]==1){
					$re[data][avatar] = get_memberavatar($memberinfo[phpssouid],'',90);
				}else{
					$re[data][avatar] = '';
				}
				$re[data][reply] = $this->db_iappBaoliao_reply->select(array('baoliaoid'=>$baoliaoid), '*','10','`id` DESC');
				$re[data][praisedata] = $this->db_iappBaoliao_praise->select(array('baoliaoid'=>$baoliaoid), '*','3','`id` DESC');
				
				$re[success]="OK";
				$re[msg]="数据加载成功~";
			}else{
				$re[success]="NODATA";
				$re[msg]="没有数据~";
			}
			
		}else{
			$re[success]="ERR";
			$re[msg]="参数错误~";
		}
		echo json_encode($re);
	}
	
	function reply_list() {
		$re = array();
		$page = max(intval($_GET['page']), 1);
		$baoliaoid = intval($_GET['baoliaoid']) ? intval($_GET['baoliaoid']) : null;
		$pagesize = $this->config[setting][Baoliaolistnum];
		if($baoliaoid){
			$data = $this->db_iappBaoliao_reply->listinfo(array('baoliaoid'=>$baoliaoid), '`id` DESC', $page,$pagesize);
			if($data){
				$this->_init_phpsso();
				foreach ($data as $k=>$r) {
					$re[data][$k]=$r;
					$re[data][$k][addtime] = date('m-d H:i:s', $r[addtime]);
					$memberinfo = get_memberinfo($r[userid]);
					$ssoinfo = unserialize($this->client->ps_get_member_info($memberinfo[phpssouid]));
					if($ssoinfo[avatar]==1){
						$re[data][$k][avatar] = get_memberavatar($memberinfo[phpssouid],'',90);
					}else{
						$re[data][$k][avatar] = '';
					}
				}
				
				$re[success]="OK";
				$re[msg]="数据加载成功~";
			}else{
				$re[success]="NODATA";
				$re[msg]="没有数据~";
			}
		}else{
			$re[success]="ERR";
			$re[msg]="参数错误~";
		}
		echo json_encode($re);
	}
	
	function praise(){
		$data = array();
		$id = intval($_GET['id']) ? intval($_GET['id']) : null;
		$userid = isset($_GET['userid'])? trim($_GET['userid']) : null;
		$acctoken = isset($_GET['acctoken'])? trim($_GET['acctoken']) : null;
	
		if($id && $userid && $acctoken){
			$Baoliao_c = getcache('iappBaoliao','commons');
			
			if($Baoliao_c[praise]==0){
				$d = date('Y-m-j',time());
				$start_time = strtotime($d.' 00:00:00');
				$end_time = strtotime($d.' 23:59:59');
				
				$praisedata = $this->db_iappBaoliao_praise->select("`userid` = '$userid' AND `baoliaoid` = '$id' AND '$start_time'<= `addtime` AND `addtime`<='$end_time'");
				
				if($praisedata){
					$ispraise = true;
				}else{
					$ispraise = false;
				}
			}else{
				$ispraise = false;
			}
			
			if($ispraise){
				$data['success'] = 'ERR';
				$data['msg'] = "亲，你已经暂过~";
			}else{
				$userdb = pc_base::load_model('member_model');
				$userdata = $userdb->get_one(array('userid'=>$userid,'password'=>$acctoken));
				if($userdata){
					$memberinfo = get_memberinfo($userdata[userid]);
					
					$info = array("baoliaoid"=>$id,"userid"=>$userdata[userid],"username"=>$userdata[username],"nick"=>$memberinfo[nickname],"addtime"=>time());
					$return_id = $this->db_iappBaoliao_praise->insert($info,'1');
					
					$return_id = $this->db->update(array('praise'=>'+=1'),array('id'=>$id));
					if($return_id){
						$data[success]="OK";
						$data[data][id] = $id;
						$data[data][userid] = $userdata[userid];
						$data[data][username] = $userdata[username];
						$data[data][nick] = $memberinfo[nickname];
						$data['msg'] = "成功赞~";
						
					}else{
						$data[success]="ERR";
						$data['msg'] = "暂失败，未知错误~";
					}
				}else{
					$data[success]="NOTLOGIN";
					$data['msg'] = "请先登录~";
				}
			}
		}else{
			$data[success]="NODATA";
			$data['msg'] = "参数错误~";
		}
		echo json_encode($data);	
	}
	
	
	
	/**
	*发布说说
	*/
	function Baoliaoadd() {
		$data = array();
		$userid = isset($_GET['userid'])? intval(trim($_GET['userid'])) : null;
		$acctoken = isset($_GET['acctoken'])? trim($_GET['acctoken']) : null;
		$content = isset($_POST[content]) ? trim($_POST[content]) : null;

		$address = trim($_POST[address]);
		if($userid && $acctoken && $content){
			$userdb = pc_base::load_model('member_model');
			$userdata = $userdb->get_one(array('userid'=>$userid,'password'=>$acctoken));
			if($userdata){
										
				$info = array();
				$info[siteid] = $this->siteid;
				$info[typeid] = 1;
				$info[userid] = $userdata[userid];
				$info[username] = $userdata[username];
				$info[content] = safe_replace(remove_xss($content));
				
				$voice = $this->upload('voice','mp3|amr');
				$_voice = array();
				foreach ($voice as $v) {
					$_voice[] = $this->upload_url.$v[filepath];
				}
				if($_voice){
					$info[voice] = $_voice[0];
				}
				$info[address] = $address;
				$info[addtime] = time();
				$info[edittime] = time();
				$return_id = $this->db->insert($info,'1');
				
				//分享
				$share_db = pc_base::load_model('iappShare_model');
				if($_POST[qqweibo]){
					//$t=true;
					$share = $share_db->get_one(array('userid'=>$userid,'name'=>'qqweibo'));
					if($share){
						pc_base::load_app_class('Tencent', 'iapp', 0);
						$client_id = pc_base::load_config('system', 'qq_akey');
						$client_secret = pc_base::load_config('system', 'qq_skey');
						OAuth::init($client_id, $client_secret);
						$info = array(
							'access_token'=>$share[access_token],
							'openid'=>$share[connectid],
							'content' => $content
						);
						$r = Tencent::api('t/add_pic_url', $info, 'POST');
					}
				}
				
				if($_POST[sina]){
					//$t=true;
					$share = $share_db->get_one(array('userid'=>$userid,'name'=>'sina'));
					if($share){
						pc_base::load_app_class('sinaweibo', 'iapp', 0);
						$WB_AKEY = pc_base::load_config('system', 'sina_akey');
						$WB_SKEY = pc_base::load_config('system', 'sina_skey');
						$SaeTOAuthV2 = new SaeTOAuthV2(WB_AKEY , WB_SKEY);
						$SaeTClientV2 = new SaeTClientV2(WB_AKEY , WB_SKEY , $share[connectid]);
						$r = $SaeTClientV2->update($content);
					}
				}
				
				if($_POST[qzone]){
					//$t=true;
					//$re['success'] = 'OK';
					//$re['msg'] = '分享失败，接口未开放~';
				}
				
				//分享结束
				
				
				if($return_id){
					$data[success]='OK';
					$data[id]=$return_id;
				}else{
					$data[success]='ERR';
				}
			}else{
				$data[success]='NOLOGIN';
			}
		}else{
			$data[success]='NODATA';
		}
		echo json_encode($data);		
	}
	
	
	
	public function share($type,$content) {
		$params = $_POST;
		$re = array();
		$content = $params[text];

		if($content && $params[userid]){
			$userdata = $this->member->get_one(array('userid'=>$params[userid],'password'=>$params[acctoken]));
			if($userdata){
				$t=false;
				if($params[qqweibo]){
					$t=true;
					$share = $this->db->get_one(array('userid'=>$params[userid],'name'=>'qqweibo'));
					if($share){
						pc_base::load_app_class('Tencent', 'iapp', 0);
						$client_id = pc_base::load_config('system', 'qq_akey');
						$client_secret = pc_base::load_config('system', 'qq_skey');
						OAuth::init($client_id, $client_secret);
						$info = array(
							'access_token'=>$share[access_token],
							'openid'=>$share[connectid],
							'content' => $content
						);
						$r = Tencent::api('t/add_pic_url', $info, 'POST');
						$re['success'] = 'OK';
						$re['msg'] = '分享成功~';
					}else{
						$re['success'] = 'ERR';
						$re['msg'] = '分享失败,未授权~';
					}
				}
				
				if($params[sina]){
					$t=true;
					$share = $this->db->get_one(array('userid'=>$params[userid],'name'=>'sina'));
					if($share){
						pc_base::load_app_class('sinaweibo', 'iapp', 0);
						$WB_AKEY = pc_base::load_config('system', 'sina_akey');
						$WB_SKEY = pc_base::load_config('system', 'sina_skey');
						$SaeTOAuthV2 = new SaeTOAuthV2(WB_AKEY , WB_SKEY);
						$SaeTClientV2 = new SaeTClientV2(WB_AKEY , WB_SKEY , $share[connectid]);
						$r = $SaeTClientV2->update($content);
						$re['success'] = 'OK';
						$re['msg'] = '分享成功~';
					}else{
						$re['success'] = 'ERR';
						$re['msg'] = '分享失败,未授权~';
					}
				}
				
				if($params[qzone]){
					$t=true;
					$re['success'] = 'OK';
					$re['msg'] = '分享失败，接口未开放~';
				}
				
				if(!$t){
					$re['success'] = 'ERR';
					$re['msg'] = '分享失败，type错误~';
				}
			}else{
				$re['success'] = 'NOTLOGIN';
				$re['msg'] = '分享失败,没有登录~';
			}
		}else{
			$re['success'] = 'ERR';
			$re['msg'] = '分享失败,参数不完整~';
		}
		
		echo json_encode($re);
	}
	
	
	
	
	
	function del(){
		$data = array();
		$id = intval($_GET['id']) ? intval($_GET['id']) : null;
		$userid = isset($_GET['userid'])? trim($_GET['userid']) : null;
		$acctoken = isset($_GET['acctoken'])? trim($_GET['acctoken']) : null;
		if($id && $userid && $acctoken){
			$userdb = pc_base::load_model('member_model');
			$userdata = $userdb->get_one(array('userid'=>$userid,'password'=>$acctoken));
			if($userdata){
				$_d = $this->db->delete(array('id'=>$id,'userid'=>$userid));
				if($_d){
					$data[success]="OK";
					$data['msg'] = "删除成功~";
					
				}else{
					$data[success]="ERR";
					$data['msg'] = "删除失败~";
				}
			}else{
				$data[success]="NOTLOGIN";
				$data['msg'] = "请先登录~";
			}
		}else{
			$data[success]="NODATA";
			$data['msg'] = "参数错误~";
		}
		echo json_encode($data);	
	}
	
	/**
	*独立图片上传
	*/
	function Baoliaoupload() {
		$re = array();
		$info = array();
		$id = intval($_POST['id'])? intval($_POST['id']) : null;
		$num = intval($_POST['num'])? intval($_POST['num']) : null;
		
		if($id && $num){
			$Baoliaoinfo = $this->db->get_one(array('id'=>$id));
			$info[photos] = string2array($Baoliaoinfo[photos]);
			$info[photoshd] = string2array($Baoliaoinfo[photoshd]);
			
			$photos = $this->upload('photos','jpg|jpeg|gif|png');
		
			$_photos = array();
			foreach ($photos as $v) {
				if($v[filepath]!=''){
					$_photos[] = $this->upload_url.$v[filepath];
				}
			}
			
			if(count($_photos)>0){
				if($num==1){
					$ap = array_push($info[photos],thumb($_photos[0],474,320));
					$ap = array_push($info[photoshd],$_photos[0]);
				}
				if($num>1){
					$ap = array_push($info[photos],thumb($_photos[0],152,144));
					$ap = array_push($info[photoshd],$_photos[0]);
				}
			}
			
			$info[photos] = array2string($info[photos]);
			$info[photoshd] = array2string($info[photoshd]);
			$return_id = $this->db->update($info,array('id'=>$id));
			$re[success]="OK";
		}else{
			$re[success]="ERR";
		}
		echo json_encode($re);
	}

	/**
	 * 常规上传
	 */
	function upload($file,$ext) {
		pc_base::load_sys_class('attachment','',0);
		$siteinfo = getcache('sitelist', 'commons');
		$site_setting = string2array($siteinfo[$this->siteid]['setting']);
		$attachment = new attachment('iappBaoliao',0,$this->siteid);
		$attachment->set_userid(1);
		$a = $attachment->upload($file,$ext,$site_setting['upload_maxsize']*1024,0,array(),0);
		return $attachment->uploadedfiles;
	}
	
	/**
	 * 回复
	 */
	function reply_add() {
		$data = array();
		
		$userid = intval($_GET['userid']) ? intval($_GET['userid']) : null;
		$acctoken = isset($_GET['acctoken']) ? trim($_GET['acctoken']) : null;
		
		$baoliaoid = isset($_POST['baoliaoid']) ? intval(trim($_POST['baoliaoid'])) : null;
		$content = trim($_POST[content]) ? trim($_POST[content]) : null;
		
		$reply_userid = isset($_POST['reply_userid'])? intval(trim($_POST['reply_userid'])) : null;
		$reply_nick = isset($_POST['reply_nick'])? trim($_POST['reply_nick']) : null;
		
		if($userid && $acctoken && $baoliaoid && $content && $reply_userid && $reply_nick) {
			$userdb = pc_base::load_model('member_model');
			$userdata = $userdb->get_one(array('userid'=>$userid,'password'=>$acctoken));
			if($userdata) {
				$Baoliaoinfo = $this->db->get_one(array('id'=>$baoliaoid));
				if($Baoliaoinfo) {
					$info = array();
					$info[baoliaoid] = $baoliaoid;
					$info[userid] = $userdata[userid];
					$info[username] = $userdata[username];
					$memberinfo = get_memberinfo($userdata[userid]);
					$info[nick] = $memberinfo[nickname];
					$info[content] = safe_replace(remove_xss($content));
					$info[reply_userid] = $reply_userid;
					$info[reply_nick] = $reply_nick;
					
					$info[addtime] = time();
					$info[edittime] = time();
					$return_id = $this->db_iappBaoliao_reply->insert($info,'1');
					$return_id = $this->db->update(array('replytime'=>time(),'renum'=>'+=1'),array('id'=>$baoliaoid));
					
					$data[success]="OK";
					$data[msg]="回复成功~";
				} else {
					$data[success]="NOBaoliao";
					$data[msg]="说说不存在~";
				}
				
			} else {
				$data[success]="NOLOGIN";
				$data[msg]="没有登录~";
			}
		}else{
			$data[success]="NODATA";
			$data[msg]="参数错误~";
		}
		
		echo json_encode($data);
	}
}
?>