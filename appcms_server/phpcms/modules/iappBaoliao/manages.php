<?php 
defined('IN_PHPCMS') or exit('No permission resources.'); 
pc_base::load_app_class('admin','admin',0);
pc_base::load_sys_class('form', '', 0);
pc_base::load_sys_class('format', '', 0);
class manages extends admin {
	
	function __construct() {
		parent::__construct();
		$this->sites = pc_base::load_app_class('sites','admin');
		$this->db = pc_base::load_model('iappBaoliao_model');
		$this->db_iappBaoliao_type = pc_base::load_model('iappBaoliao_type_model');
		$this->db_iappBaoliao_reply = pc_base::load_model('iappBaoliao_reply_model');
		$this->siteid = $this->get_siteid();
	}
	
	function init() {
		$page = max(intval($_GET['page']), 1);
		$data = $this->db->listinfo(array('siteid'=>$this->get_siteid()), '`id` DESC', $page);
		include $this->admin_tpl('baoliao_list');
	}
		
	
	function add() {
		$types = $this->db_iappBaoliao_type->select(array('siteid'=>$this->get_siteid()),'*','','`listorder` DESC');
		if($_POST['dosubmit']) {
			$info = $_POST['info'];
			$info[content] = trim($_POST['info'][content]) ? trim($_POST['info'][content]) : showmessage(L('说说不能为空'),HTTP_REFERER);
			
			$info[siteid] = $this->siteid;
			
			$info[userid] = 1;
			$info[username] = 'zuiqianduan';
			
			$photos = array();
			$_photos = array();
			$_photoshd = array();
			foreach ($info[photos] as $k => $v) {
				$v = trim($v);
				if($v!=""){
					$photos[] = $v;
				}
			}
			if(count($photos)==1){
				$_photos[0] = thumb($photos[0],474,320);
				$_photoshd[0] = $photos[0];
			}else if(count($photos)>1){
				foreach ($photos as $k => $v) {
					$_photos[] = thumb($v,152,144);
					$_photoshd[] = $v;
				}
			}
			$info[photos] = array2string($_photos);
			$info[photoshd] = array2string($_photoshd);
			
			$info[addtime] = time();
			$info[edittime] = time();
			//print_r($info);
			//exit;
			
			$return_id = $this->db->insert($info,'1');
			showmessage(L('add_success'), '?m=iappBaoliao&c=manages&a=init&menuid='.$_GET['menuid']);
		} else {
			include $this->admin_tpl('baoliao_add');
		}		
	}
	
	function edit() {
		$types = $this->db_iappBaoliao_type->select(array('siteid'=>$this->get_siteid()),'*','','`listorder` DESC');
		if($_POST['dosubmit']) {
			$id = intval($_POST['id']) ? intval($_POST['id']) : showmessage(L('parameter_error'),HTTP_REFERER);
			$info = $_POST['info'];
			$info[content] = trim($_POST['info'][content]) ? trim($_POST['info'][content]) : showmessage(L('主题不能为空'),HTTP_REFERER);
			$photos = array();
			$_photos = array();
			$_photoshd = array();
			foreach ($info[photos] as $k => $v) {
				$v = trim($v);
				if($v!=""){
					$photos[] = $v;
				}
			}
			if(count($photos)==1){
				$_photos[0] = thumb($photos[0],474,320);
				$_photoshd[0] = $photos[0];
			}else if(count($photos)>1){
				foreach ($photos as $k => $v) {
					$_photos[] = thumb($v,152,144);
					$_photoshd[] = $v;
				}
			}
			$info[photos] = array2string($_photos);
			$info[photoshd] = array2string($_photoshd);
			
			$info[edittime] = time();
			$return_id = $this->db->update($info,array('id'=>$id));
			showmessage(L('operation_success'), '?m=iappBaoliao&c=manages&a=edit&id='.$id.'&menuid='.$_GET['menuid']);
		} else {
			$id = intval($_GET['id']) ? intval($_GET['id']) : showmessage(L('parameter_error'),HTTP_REFERER);		
			$info = $this->db->get_one(array('id'=>$id));
			//print_r($info);
			$info[photos] = string2array($info[photos]);
			include $this->admin_tpl('baoliao_edit');			
		}
	}
	
	/**
	 * 删除
	 */
	public function delete() {
		$siteid = $this->siteid;
		
		if (isset($_GET['id']) && !empty($_GET['id'])) {
			$id = intval($_GET['id']);
			$this->db->delete(array('id'=>$id));
			showmessage(L('operation_success'), HTTP_REFERER);
		} elseif (isset($_POST['ids']) && !empty($_POST['ids'])) {
			if (is_array($_POST['ids'])) {
				foreach ($_POST['ids'] as $fid) {
					$this->db->delete(array('id'=>$fid));
				}
			}
			showmessage(L('operation_success'), HTTP_REFERER);
		} else {
			showmessage(L('illegal_operation'), HTTP_REFERER);
		}
	}
	
	/**
	 * 排序
	 */
	public function listorder() {
		if (isset($_POST['listorders']) && is_array($_POST['listorders'])) {
			$listorder = $_POST['listorders'];
			foreach ($listorder as $k => $v) {
				$this->db->update(array('listorder'=>$v), array('id'=>$k));
			}
		}
		showmessage(L('operation_success'), HTTP_REFERER);
	}
	
	public function show() {
		$re = array();
		$talkid = intval($_GET['talkid']) ? intval($_GET['talkid']) : null;
		if($talkid){
			$talkinfo = $this->db->get_one(array('id'=>$talkid));
			if($talkinfo){
				$re[data][id] = $talkinfo[id];
				$re[data][typeid] = $talkinfo[typeid];
				$re[data][userid] = $talkinfo[userid];
				$re[data][username] = $talkinfo[username];

				$re[data][content] = $talkinfo[content];
				$re[data][photos] = string2array($talkinfo[photos]);
				$re[data][photoshd] = string2array($talkinfo[photoshd]);
				$re[data][voice] = trim($talkinfo[voice]);
				$re[data][address] = trim($talkinfo[address]);

				$re[data][htis] = $talkinfo[htis];
				$re[data][renum] = $talkinfo[renum];
				$re[data][praise] = $talkinfo[praise];
				$re[data][replytime] = date('m-d H:i:s', $talkinfo['replytime']);
				$re[data][addtime] = date('m-d H:i:s', $talkinfo['addtime']);
					
				$memberinfo = get_memberinfo($talkinfo[userid]);
				$re[data][nickname] = $memberinfo[nickname];
					
				$this->_init_phpsso();
				$ssoinfo = unserialize($this->client->ps_get_member_info($memberinfo[phpssouid]));
				if($ssoinfo[avatar]==1){
					$re[data][avatar] = get_memberavatar($memberinfo[phpssouid],'',90);
				}else{
					$re[data][avatar] = '';
				}
				$re[data][reply] = $this->db_iappTalk_reply->select(array('talkid'=>$talkid), '*','10','`id` DESC');
				$re[data][praisedata] = $this->db_iappTalk_praise->select(array('talkid'=>$talkid), '*','3','`id` DESC');
				
				$re[success]="OK";
				$re[msg]="数据加载成功~";
			}else{
				$re[success]="NODATA";
				$re[msg]="没有数据~";
			}
			
		}else{
			$re[success]="ERR";
			$re[msg]="参数错误~";
		}
		echo json_encode($re);
	}
	
	
	function type_list() {
		$page = max(intval($_GET['page']), 1);
		$data = $this->db_iappBaoliao_type->listinfo(array('siteid'=>$this->get_siteid()), '`listorder` DESC', $page);
		include $this->admin_tpl('type_list');
	}
	function type_add() {
		if($_POST['dosubmit']) {
			$info = $_POST['info'];
			$info[title] = trim($_POST['info'][title]) ? trim($_POST['info'][title]) : showmessage(L('主题不能为空'),HTTP_REFERER);
			$info[siteid] = $this->siteid;
			$info[listorder] = 0;
			$return_id = $this->db_iappBaoliao_type->insert($info,'1');
			showmessage(L('add_success'), '?m=iappBaoliao&c=manages&a=type_list&menuid='.$_GET['menuid']);
		} else {
			include $this->admin_tpl('type_add');
		}		
	}
	
	function type_edit() {
		if($_POST['dosubmit']) {
			$id = intval($_POST['id']) ? intval($_POST['id']) : showmessage(L('parameter_error'),HTTP_REFERER);
			$info = $_POST['info'];
			$info[title] = trim($_POST['info'][title]) ? trim($_POST['info'][title]) : showmessage(L('主题不能为空'),HTTP_REFERER);
			$return_id = $this->db_iappBaoliao_type->update($info,array('id'=>$id));
			showmessage(L('operation_success'), '?m=iappBaoliao&c=manages&a=type_edit&id='.$id.'&menuid='.$_GET['menuid']);
		} else {
			$id = intval($_GET['id']) ? intval($_GET['id']) : showmessage(L('parameter_error'),HTTP_REFERER);		
			$info = $this->db_iappBaoliao_type->get_one(array('id'=>$id));
			include $this->admin_tpl('type_edit');			
		}
	}
	
	/**
	 * 删除
	 */
	public function type_delete() {
		$siteid = $this->siteid;
		
		if (isset($_GET['id']) && !empty($_GET['id'])) {
			$id = intval($_GET['id']);
			$this->db_iappBaoliao_type->delete(array('id'=>$id, 'siteid'=>$siteid));
			showmessage(L('operation_success'), HTTP_REFERER);
		} elseif (isset($_POST['ids']) && !empty($_POST['ids'])) {
			if (is_array($_POST['ids'])) {
				foreach ($_POST['ids'] as $fid) {
					$this->db_iappBaoliao_type->delete(array('id'=>$fid, 'siteid'=>$siteid));
				}
			}
			showmessage(L('operation_success'), HTTP_REFERER);
		} else {
			showmessage(L('illegal_operation'), HTTP_REFERER);
		}
	}
	
	
	/**
	 * 排序
	 */
	public function type_listorder() {
		if (isset($_POST['listorders']) && is_array($_POST['listorders'])) {
			$listorder = $_POST['listorders'];
			foreach ($listorder as $k => $v) {
				$this->db_iappBaoliao_type->update(array('listorder'=>$v), array('id'=>$k));
			}
		}
		showmessage(L('operation_success'), HTTP_REFERER);
	}
	
	
	function setting() {
		$data = array();
		$m_db = pc_base::load_model('module_model');
		$data = $m_db->select(array('module'=>'iappBaoliao'));
		$data = string2array($data[0]['setting']);
		
 		if(isset($_POST['dosubmit'])) {
			$setting = $_POST['data'];
  			setcache('iappBaoliao', $setting, 'commons');  
 			$setting = array2string($setting);
			$m_db->update(array('setting'=>$setting), array('module'=>'iappBaoliao'));
			showmessage('修改成功',HTTP_REFERER);
		} else {
			include $this->admin_tpl('setting');
		}
	}
}
?>