<?php 
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');?>
<form method="post" action="?m=iappBaoliao&c=manages&a=add">

<table width="100%" cellpadding="0" cellspacing="1" class="table_form">
 
	<tr>
		<th width="100"><?php echo L('分类名称')?>：</th>
		<td>
		<select name="info[typeid]" id="">
		<option value="0">默认分类</option>
		<?php
		  $i=0;
		  foreach($types as $typeid=>$type){
		  $i++;
		?>
		<option value="<?php echo $type['id'];?>"><?php echo $type['title'];?></option>
		<?php }?>
		</select>
		
		</td>
	</tr>
	
	<tr>
		<th width="100"><?php echo L('联系人')?>：</th>
		<td>
			<input type="text" name="info[contacts]" id="contacts" size="50" value="" class="input-text">
		</td>
	</tr>
	<tr>
		<th width="100"><?php echo L('联系方式')?>：</th>
		<td>
			<input type="text" name="info[tel]" id="tel" size="50" value="" class="input-text">
		</td>
	</tr>
	
		
	<tr>
		<th width="100"><?php echo L('内容')?>：</th>
		<td>
			<textarea name="info[content]" id="content" cols="100"
			rows="6"><?php echo $info[content]?></textarea>
			
		</td>
	</tr>
	
	<tr>
		<th width="100"><?php echo L('回复')?>：</th>
		<td>
			<textarea name="info[reply]" id="reply" cols="100"
			rows="6"><?php echo $info[reply]?></textarea>
		</td>
	</tr>
	
	<tr>
		<th width="100">图片1：</th>
		<td>
		<?php echo form::images('info[photos][]', 'photos_1', '', 'iappBaoliao')?>
		</td>
	</tr>
	<tr>
		<th width="100">图片2：</th>
		<td>
		<?php echo form::images('info[photos][]', 'photos_2', '', 'iappBaoliao')?>
		</td>
	</tr>
	<tr>
		<th width="100">图片3：</th>
		<td>
		<?php echo form::images('info[photos][]', 'photos_3', '', 'iappBaoliao')?>
		</td>
	</tr>
	
	
	<tr>
		<th width="100">录音：</th>
		<td>
		<?php echo form::upfiles('info[voice]', 'voice', '', 'iappBaoliao','','','','','mp3')?>
		</td>
	</tr>
	<tr>
		<th width="100">视频：</th>
		<td>
		<?php echo form::upfiles('info[video]', 'video', '', 'iappBaoliao','','','','','mp4')?>
		</td>
	</tr>
	


	<tr>
		<td>&nbsp;</td>
		<td>
		<input type="submit" name="dosubmit" id="dosubmit" value=" <?php echo L('提交')?>" class="button">&nbsp;<input type="reset" value=" <?php echo L('重写')?> " class="button"></td>
	</tr>
</table>
</form>
</body>
</html>