<?php
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');

$menus = $menu_db->select(array('name'=>'iappPlug', 'm'=>'iapp', 'c'=>'manages', 'a'=>'init'));

$parentid = $menu_db->insert(array('name'=>'iappBaoliao', 'parentid'=>$menus[0][id], 'm'=>'iappBaoliao', 'c'=>'manages', 'a'=>'init', 'data'=>'', 'listorder'=>0, 'display'=>'1'), true);

$menu_db->insert(array('name'=>'iappBaoliao_add', 'parentid'=>$parentid, 'm'=>'iappBaoliao', 'c'=>'manages', 'a'=>'add', 'data'=>'', 'listorder'=>2, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappBaoliao_edit', 'parentid'=>$parentid, 'm'=>'iappBaoliao', 'c'=>'manages', 'a'=>'edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappBaoliao_delete', 'parentid'=>$parentid, 'm'=>'iappBaoliao', 'c'=>'manages', 'a'=>'delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));

$menu_db->insert(array('name'=>'iappBaoliao_reply', 'parentid'=>$parentid, 'm'=>'iappBaoliao', 'c'=>'manages', 'a'=>'reply_add', 'data'=>'', 'listorder'=>0, 'display'=>'0'));

$menu_db->insert(array('name'=>'iappBaoliao_type_list', 'parentid'=>$parentid, 'm'=>'iappBaoliao', 'c'=>'manages', 'a'=>'type_list', 'data'=>'', 'listorder'=>6, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappBaoliao_type_add', 'parentid'=>$parentid, 'm'=>'iappBaoliao', 'c'=>'manages', 'a'=>'type_add', 'data'=>'', 'listorder'=>5, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappBaoliao_type_edit', 'parentid'=>$parentid, 'm'=>'iappBaoliao', 'c'=>'manages', 'a'=>'type_edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappBaoliao_type_delete', 'parentid'=>$parentid, 'm'=>'iappBaoliao', 'c'=>'manages', 'a'=>'type_delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappBaoliao_type_listorder', 'parentid'=>$parentid, 'm'=>'iappBaoliao', 'c'=>'manages', 'a'=>'type_listorder', 'data'=>'', 'listorder'=>0, 'display'=>'0'));

$menu_db->insert(array('name'=>'iappBaoliao_setting', 'parentid'=>$parentid, 'm'=>'iappBaoliao', 'c'=>'manages', 'a'=>'setting', 'data'=>'', 'listorder'=>7, 'display'=>'1'));





$language = array('iappBaoliao'=>'报料',
				  'iappBaoliao_add'=>'发布报料',
				  'iappBaoliao_edit'=>'修改报料',
				  'iappBaoliao_delete'=>'删除报料',
				  
				  'iappBaoliao_reply'=>'回复',
				 
				  'iappBaoliao_type_add'=>'添加分类',
				  'iappBaoliao_type_edit'=>'修改分类',
				  'iappBaoliao_type_list'=>'分类列表',
				  'iappBaoliao_type_delete'=>'删除分类',
				  'iappBaoliao_type_listorder'=>'分类排序',
				  'iappBaoliao_setting'=>'报料配置',
                  );

?>