<?php 
defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_sys_class('format', '', 0);
class index {
	function __construct() {		
		$this->db = pc_base::load_model('iappCoupons_model');
		$this->db_iappCoupons_data = pc_base::load_model('iappCoupons_data_model');
		$this->siteid = get_siteid();
		$this->config = getcache('iapp', 'commons');
		$this->defaultcatid = $this->config[setting][businesscatid];
	}
	
	public function lists() {
		$page = max(intval($_GET['page']), 1);
		
		$catid = intval($_GET['catid']) ? intval(trim($_GET['catid'])) : null;
		$order = trim($_GET['order']) ? trim($_GET['order']) : null;
		
		$time =time();
		$_sql = " start_time < $time AND $time < end_time ";
		
		if($catid){
			$catids = get_sql_catid('category_content_'.$this->siteid,$catid);
			$_sql = $_sql.' AND '.$catids;
		}
		
		if($catid){
			//$_sql = $_sql." AND catid= $catid ";
		}
		
		$_order = 'id DESC';
		if($order =='hot'){
			$_order = 'htis DESC';
		}
		
		$contentdb = pc_base::load_model('content_model');
		$CATEGORY = getcache('category_content_'.$this->siteid,'commons');
		$modelid = $CATEGORY[$this->defaultcatid][modelid];
		$contentdb->set_model($modelid);
		$time =time();
		$data = $this->db->listinfo($_sql, $_order, $page);
		$list = array();
		foreach ($data as $k=>$v) {
			$list[$k][id] = $v[id];
			$list[$k][picurl] = thumb($v[image],640,250);
			$list[$k][title] = $v[title];
			$list[$k][businessid] = $v[businessid];
			
			$businessdata = $contentdb->get_one(array('id'=>$v[businessid]));
			
			$list[$k][name] = $businessdata[title];	
			$list[$k][startdate] = $v[start_time];	
			$list[$k][enddate] = $v[end_time];	
		}
		$return_id = $this->db->update(array('htis'=>'+=1'),"1=1");
		$upTime = date('Y-m-j H:i:s');
		echo json_encode(array("list"=>$list,"utime"=>$upTime));
	}
	
	public function show() {
		$re = array();
		$id = intval($_GET['id']) ? intval($_GET['id']) : exit("nodata");;
		$info = $this->db->get_one(array('id'=>$id));
		
		$contentdb = pc_base::load_model('content_model');
		$CATEGORY = getcache('category_content_'.$this->siteid,'commons');
		$modelid = $CATEGORY[$this->defaultcatid][modelid];
		$contentdb->set_model($modelid);
		
		$businessdata = $contentdb->get_one(array('id'=>$info[businessid]));
		
		$re[status] = 'OK';
		$re[item][id] = $info[id];
		$re[item][catid] = $info[catid];
		$re[item][picurl] = thumb($info[image],640,250);
		$re[item][title] = $info[title];
		$re[item][businessid] = $info[businessid];
		$re[item][name] = $businessdata[title];
		$re[item][lids] = $info[businessid];
		$re[item][prop] = '[{"4094":"私房便当"}]';
		$re[item][start_time] = $info[start_time];
		$re[item][end_time] = $info[end_time];
		$re[item][lynum] = $info[lynum];
		$re[item][lavenum] = $info[lavenum];
		$re[item][ltitle] = $info[ltitle];
		$re[item][content] = $info[content];
		$re[item][Precautions] = $info[Precautions];
		
		echo json_encode($re);
	}
	
	function lave() {
		$re = array();
		$id = isset($_GET[id]) ? intval(trim($_GET[id])) : null;
		$userid = isset($_GET['userid']) ? intval(trim($_GET['userid'])) : null;
		$acctoken = isset($_GET['acctoken']) ? trim($_GET['acctoken']) : null;
		if($id && $userid && $acctoken){
			$data = array();
			$userdb = pc_base::load_model('member_model');
			$userdata = $userdb->get_one(array('userid'=>$userid,'password'=>$acctoken));
			if($userdata){
				$mobile = $userdata[mobile];
				$couponsdata = $this->db->get_one(array('id'=>$id));
				if($couponsdata[end_time]>time() && $couponsdata[lavenum]>0){
					$re_id = $this->db->update(array('lynum'=>'+=1'),array('id'=>$id));
					$re_id = $this->db->update(array('lavenum'=>'-=1'),array('id'=>$id));
					
					$contentdb = pc_base::load_model('content_model');
					$CATEGORY = getcache('category_content_'.$this->siteid,'commons');
					$modelid = $CATEGORY[$this->defaultcatid][modelid];
					$contentdb->set_model($modelid);
					
					$businessdata = $contentdb->get_one(array('id'=>$couponsdata[businessid]));
					
					$info = array();
					$info[couponsid] = $id;
					$info[userid] = $userid;
					$info[mobile] = $mobile;
					$info[pwd] = rand(10000000,99999999);
					$info[timeApply] = time();
					$info[msg] = "[$businessdata[title]]$couponsdata[ltitle]，优惠码：$info[pwd]，有效期至".date('Y-m-d', $couponsdata['end_time'])."，商户电话：$businessdata[r_tel]。【最黔端】";
					
					if($this->config[setting][iscoupons] && $mobile){
						pc_base::load_app_class('smsapi', 'sms', 0);
						$sms_setting = getcache('sms','sms');
						$sms_setting = $sms_setting[$this->siteid];
						$smsapi = new smsapi($sms_setting['userid'], $sms_setting['productid'], $sms_setting['sms_key']);
						$mobile = array($info[mobile]);
						$content = $info[msg];
						$sent_time = date('Y-m-d H:i:s',SYS_TIME);
						$id_code = random(6);//唯一吗，用于扩展验证
						$smsstatus = $smsapi->send_sms($mobile, $content, $sent_time,CHARSET,$id_code,16);
					}
					
					$return_id = $this->db_iappCoupons_data->insert($info,'1');
					
					if($return_id){
						$re[status] = "OK";
						$re[msg] = "优惠券获取成功~";
						$re[data] = array('lynum'=>$couponsdata[lynum]+1,'lavenum' =>$couponsdata[lavenum]-1);
					}else{
						$re[status] = "ERR";
						$re[msg] = "优惠券获取错误，请重新获取~";
					}
				}else if($couponsdata[end_time]<time()){
					$re[status] = "EXPIRED";
					$re[msg] = "优惠券已经过期~";
				}else if($couponsdata[lavenum]<=0){
					$re[status] = "RUNOUT";
					$re[msg] = "优惠券已经领完~";
				}else{
					$re[status] = "ERR";
					$re[msg] = "未知错误~";
				}
			}else{
				$re[status] = "NOLOGIN";
				$re[msg] = "请先登录~";
			}
		}else{
			$re[status] = "NODATA";
			$re[msg] = "参数错误~";
		}
		echo json_encode($re);
	}
	
	function forthputting() {
		$re = array();
		$id = isset($_GET[id]) ? intval(trim($_GET[id])) : null;
		$userid = isset($_GET['userid']) ? intval(trim($_GET['userid'])) : null;
		$acctoken = isset($_GET['acctoken']) ? trim($_GET['acctoken']) : null;
		if($id && $userid && $acctoken){
			$data = array();
			$userdb = pc_base::load_model('member_model');
			$userdata = $userdb->get_one(array('userid'=>$userid,'password'=>$acctoken));
			if($userdata){
				$re_id = $this->db_iappCoupons_data->update(array('isConsume'=>'1'),array('id'=>$id));
				if($re_id){
					$re[success] = "OK";
					$re[msg] = "成功使用~";
					$re[data] = array('id'=>$id);
					
				}else{
					$re[success] = "ERR";
					$re[msg] = "失败，请重试~";
				}
				
			}else{
				$re[success] = "NOTLOGIN";
				$re[msg] = "请先登录~";
			}
		}else{
			$re[success] = "NODATA";
			$re[msg] = "参数错误~";
		}
		echo json_encode($re);
	}
}
?>