DROP TABLE IF EXISTS `phpcms_iappCoupons_data`;
CREATE TABLE IF NOT EXISTS `phpcms_iappCoupons_data` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `couponsid` smallint(5) NOT NULL COMMENT '优惠券id',
  `userid` int(10) unsigned default '0' COMMENT '用户ID',
  `username` varchar(20) default NULL COMMENT '用户名',
  `mobile` varchar(50) NOT NULL COMMENT '手机',
  `pwd` varchar(50) NOT NULL COMMENT '消费密码',
  `timeApply` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '领取时间',
  `isConsume` smallint(5) NOT NULL COMMENT '是否消费',
  `timeConsume` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '消费时间',
  `msg` text NOT NULL COMMENT '短信内容', 
  PRIMARY KEY (`id`)
) TYPE=MyISAM;
