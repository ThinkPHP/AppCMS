DROP TABLE IF EXISTS `phpcms_iappCoupons`;
CREATE TABLE IF NOT EXISTS `phpcms_iappCoupons` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `businessid` smallint(5) NOT NULL,
  `catid` smallint(5) NOT NULL COMMENT '栏目',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `ltitle` varchar(80) NOT NULL COMMENT '长标题',
  `image` varchar(200) NOT NULL COMMENT '图片',
  `start_time` int(10) unsigned NOT NULL DEFAULT '0',
  `end_time` int(10) unsigned NOT NULL DEFAULT '0',
  `num` smallint(5) NOT NULL COMMENT '总量',
  `lynum` smallint(5) NOT NULL COMMENT '已领取',
  `synum` smallint(5) NOT NULL COMMENT '已使用',
  `lavenum` smallint(5) NOT NULL COMMENT '剩余量',
  `content` mediumtext NOT NULL COMMENT '简介',
  `Precautions` mediumtext NOT NULL COMMENT '使用须知',
  `collection` smallint(5) DEFAULT '0' COMMENT '收藏量',
  `htis` smallint(5) DEFAULT '0' COMMENT '查看量',
  `praise` smallint(5) DEFAULT '0' COMMENT '赞',
  `listorder` smallint(5) DEFAULT '0',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0',
  `edittime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) TYPE=MyISAM;
