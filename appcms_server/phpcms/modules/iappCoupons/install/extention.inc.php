<?php
defined('IN_PHPCMS') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');

$menus = $menu_db->select(array('name'=>'iappPlug', 'm'=>'iapp', 'c'=>'manages', 'a'=>'init'));

$parentid = $menu_db->insert(array('name'=>'iappCoupons', 'parentid'=>$menus[0][id], 'm'=>'iappCoupons', 'c'=>'manages', 'a'=>'init', 'data'=>'', 'listorder'=>0, 'display'=>'1'), true);

$menu_db->insert(array('name'=>'iappCoupons_add', 'parentid'=>$parentid, 'm'=>'iappCoupons', 'c'=>'manages', 'a'=>'add', 'data'=>'', 'listorder'=>2, 'display'=>'1'));
$menu_db->insert(array('name'=>'iappCoupons_edit', 'parentid'=>$parentid, 'm'=>'iappCoupons', 'c'=>'manages', 'a'=>'edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu_db->insert(array('name'=>'iappCoupons_delete', 'parentid'=>$parentid, 'm'=>'iappCoupons', 'c'=>'manages', 'a'=>'delete', 'data'=>'', 'listorder'=>0, 'display'=>'0'));



$language = array('iappCoupons'=>'优惠券',
				  'iappCoupons_add'=>'添加优惠券',
				  'iappCoupons_edit'=>'修改优惠券',
				  'iappCoupons_delete'=>'删除优惠券'
                  );

?>