<?php 
defined('IN_PHPCMS') or exit('No permission resources.'); 
pc_base::load_app_class('admin','admin',0);
pc_base::load_sys_class('form', '', 0);
pc_base::load_sys_class('format', '', 0);
class manages extends admin {
	private $db,$db_iappCoupons_data,$db_iappCoupons_reply;
	function __construct() {
		parent::__construct();
		$this->db = pc_base::load_model('iappCoupons_model');
		$this->db_iappCoupons_data = pc_base::load_model('iappCoupons_data_model');
		$this->siteid = $this->get_siteid();
		$this->config = getcache('iapp', 'commons');
		$this->defaultcatid = $this->config[setting][businesscatid];
	}
	
	function init() {
		if (intval($_GET['catid'])){
			$catid = intval($_GET['catid']);
			param::set_cookie("catid",$catid);
		}
		$page = max(intval($_GET['page']), 1);
		if($catid){
			$data = $this->db->listinfo(array('catid'=>$catid), '`id` DESC', $page);
		}else{
			$data = $this->db->listinfo('', '`id` DESC', $page);
		}
		
		include $this->admin_tpl('list');
	}
	
	function add() {
		if (intval($_GET['catid'])){
			$catid = intval($_GET['catid']);
			param::set_cookie("catid",$catid);
		}else if(param::get_cookie('catid')){
			$catid = param::get_cookie('catid');
		}else{
			showmessage('请从右侧选择分类添加！',HTTP_REFERER);
		}
		
		$content_db = pc_base::load_model('model_content_model');
		$modellist_t = getcache('modellist_t','commons');
		$modelid = $modellist_t['iappYp_company'][modelid];
		
		$content_db->set_model($modelid);
		$data = $content_db->listinfo(array('catid'=>$catid), '`listorder` DESC', 1);
		
		if($_POST['dosubmit']) {
			$info = $_POST['info'];
			$info[businessid] = trim($info[businessid]) ? intval(trim($info[businessid])) : showmessage(L('商家id不能为空'),HTTP_REFERER);
			$info[catid] = trim($info[catid]) ? intval(trim($info[catid])) : showmessage(L('栏目不能为空'),HTTP_REFERER);
			$info[title] = trim($info[title]) ? trim($info[title]) : showmessage(L('标题不能为空'),HTTP_REFERER);
			$info[image] = trim($info[image]) ? trim($info[image]) : showmessage(L('图片不能为空'),HTTP_REFERER);
			
			$info[start_time] = trim($info[start_time]) ?  strtotime(trim($info[start_time])) : showmessage(L('开始时间不能为空'),HTTP_REFERER);
			$info[end_time] = trim($info[end_time]) ? strtotime($info[end_time]) : showmessage(L('结束时间不能为空'),HTTP_REFERER);
			
			$info[num] = trim($info[num]) ? intval(trim($info[num])) : showmessage(L('数量不能为空'),HTTP_REFERER);
			
			$info[addtime] = time();
			$info[edittime] = time();
			$return_id = $this->db->insert($info,'1');
			showmessage(L('add_success'), '?m=iappCoupons&c=manages&a=init&catid='.$info[catid].'&menuid='.$_GET['menuid']);
		} else {
			include $this->admin_tpl('add');
		}		
	}
	
	function edit() {
		$id = intval($_REQUEST['id']) ? intval($_REQUEST['id']) : showmessage(L('parameter_error'), HTTP_REFERER);
		$catid = intval($_GET['catid']);
		
		$content_db = pc_base::load_model('model_content_model');
		$modellist_t = getcache('modellist_t','commons');
		$modelid = $modellist_t['iappYp_company'][modelid];
		
		$content_db->set_model($modelid);
		$data = $content_db->listinfo(array('catid'=>$catid), '`listorder` DESC', 1);
		
		if($_POST['dosubmit']) {
			$info = $_POST['info'];
			$info[businessid] = trim($info[businessid]) ? intval(trim($info[businessid])) : showmessage(L('商家id'),HTTP_REFERER);
			$info[catid] = trim($info[catid]) ? intval(trim($info[catid])) : showmessage(L('栏目不能为空'),HTTP_REFERER);
			$info[title] = trim($info[title]) ? trim($info[title]) : showmessage(L('标题不能为空'),HTTP_REFERER);
			$info[image] = trim($info[image]) ? trim($info[image]) : showmessage(L('图片不能为空'),HTTP_REFERER);
			$info[start_time] = trim($info[start_time]) ? strtotime(trim($info[start_time])) : showmessage(L('开始时间不能为空'),HTTP_REFERER);
			$info[end_time] = trim($info[end_time]) ? strtotime(trim($info[end_time])) : showmessage(L('结束时间不能为空'),HTTP_REFERER);
			$info[num] = trim($info[num]) ? intval(trim($info[num])) : showmessage(L('数量不能为空'),HTTP_REFERER);
			
			$info[edittime] = time();
			$return_id = $this->db->update($info,array('id'=>$id));

			showmessage(L('operation_success'), HTTP_REFERER);
		} else {	
			$info = $this->db->get_one(array('id'=>$id));
			include $this->admin_tpl('edit');			
		}
	}
	
	/**
	 * 删除
	 */
	public function delete() {
		
		if (isset($_GET['id']) && !empty($_GET['id'])) {
			$id = intval($_GET['id']);
			$this->db->delete(array('id'=>$id));
			$this->db_iappCoupons_data->delete(array('couponsid'=>$id));
			showmessage(L('operation_success'), HTTP_REFERER);
		} elseif (isset($_POST['ids']) && !empty($_POST['ids'])) {
			if (is_array($_POST['ids'])) {
				foreach ($_POST['ids'] as $fid) {
					$this->db->delete(array('id'=>$fid));
					$this->db_iappCoupons_data->delete(array('couponsid'=>$fid));
				}
			}
			showmessage(L('operation_success'), HTTP_REFERER);
		} else {
			showmessage(L('illegal_operation'), HTTP_REFERER);
		}
	}
	/**
	 * 排序
	 */
	public function listorder() {
		if (isset($_POST['listorders']) && is_array($_POST['listorders'])) {
			$listorder = $_POST['listorders'];
			foreach ($listorder as $k => $v) {
				$this->db->update(array('listorder'=>$v), array('id'=>$k));
			}
		}
		showmessage(L('operation_success'), HTTP_REFERER);
	}
	
	function lave() {
		$couponsid = trim($_GET[couponsid]) ? trim($_GET[couponsid]) : showmessage(L('优惠券不能为空'),HTTP_REFERER);
		if($_POST['dosubmit']) {
			$couponsdata = $this->db->get_one(array('id'=>$couponsid));
			
			$re_id = $this->db->update(array('lynum'=>'+=1'));
			$re_id = $this->db->update(array('lavenum'=>'-=1'));
			
			$content_db = pc_base::load_model('model_content_model');
			$modellist_t = getcache('modellist_t','commons');
			$modelid = $modellist_t['iappYp_company'][modelid];
		
			$content_db->set_model($modelid);
			$businessdata = $content_db->get_one(array('id'=>$couponsdata[businessid]));
			
			$info = array();
			$info[couponsid] = $couponsid;
			$info[mobile] = trim($_POST[mobile]) ? trim($_POST[mobile]) : showmessage(L('手机不能为空'),HTTP_REFERER);
			
			$info[pwd] = rand(10000000,99999999);
			$info[timeApply] = time();
			$info[msg] = "[$businessdata[title]]$couponsdata[ltitle]，优惠码：$info[pwd]，有效期至".date('Y-m-d', $couponsdata['end_time'])."，商户电话：$businessdata[r_tel]。【最黔端】";
			
			pc_base::load_app_class('smsapi', 'sms', 0); 
			$sms_setting = getcache('sms','sms');
			$sms_setting = $sms_setting[$this->siteid];
			$smsapi = new smsapi($sms_setting['userid'], $sms_setting['productid'], $sms_setting['sms_key']);
			$mobile = array($info[mobile]);
			$content = $info[msg];
			$sent_time = date('Y-m-d H:i:s',SYS_TIME);
			$id_code = random(6);//唯一吗，用于扩展验证
			$status = $smsapi->send_sms($mobile, $content, $sent_time,CHARSET,$id_code,16);
			
			$return_id = $this->db_iappCoupons_data->insert($info,'1');
			showmessage(L('add_success').$status, '?m=iappCoupons&c=manages&a=init&catid='.$info[catid].'&menuid='.$_GET['menuid']);
			
		} else {
			$page = max(intval($_GET['page']), 1);
			$data = $this->db_iappCoupons_data->listinfo(array('couponsid'=>$couponsid), '`id` DESC', $page);		
			include $this->admin_tpl('lave');			
		}
			
	}
	
	/**
	 * 显示栏目菜单列表
	 */
	public function public_categorys() {
		$show_header = '';

		$modellist_t = getcache('modellist_t','commons');
		$modelid = $modellist_t['iappYp_company'][modelid];
		$CATEGORY = getcache('category_'.$modelid,'commons');
		
		$cfg = getcache('common','commons');
		$ajax_show = intval($cfg['category_ajax']);
		$from = isset($_GET['from']) && in_array($_GET['from'],array('block')) ? $_GET['from'] : 'content';
		$tree = pc_base::load_sys_class('tree');
		if($_SESSION['roleid'] != 1) {
			$this->priv_db = pc_base::load_model('category_priv_model');
			$priv_result = $this->priv_db->select(array('action'=>'init','roleid'=>$_SESSION['roleid'],'siteid'=>$this->siteid,'is_admin'=>1));
			$priv_catids = array();
			foreach($priv_result as $_v) {
				$priv_catids[] = $_v['catid'];
			}
			if(empty($priv_catids)) return '';
		}
	

		$categorys = array();
		if(!empty($CATEGORY)) {
			foreach($CATEGORY as $r) {
				if($r['siteid']!=$this->siteid || ($r['type']==2 && $r['child']==0)) continue;
				if($_SESSION['roleid'] != 1 && !in_array($r['catid'],$priv_catids)) {
					$arrchildid = explode(',',$r['arrchildid']);
					$array_intersect = array_intersect($priv_catids,$arrchildid);
					if(empty($array_intersect)) continue;
				}
				
				$r['icon_type'] = $r['vs_show'] = '';
				$r['type'] = 'init';
				$r['add_icon'] = "<a target='right' href='?m=iappCoupons&c=manages&a=add&menuid=".$_GET['menuid']."&catid=".$r['catid']."' ><img src='".IMG_PATH."add_content.gif' alt='".L('add')."'></a> ";
				
				$categorys[$r['catid']] = $r;
			}
		}
		if(!empty($categorys)) {
			$tree->init($categorys);
			
			$strs = "<span class='\$icon_type'>\$add_icon<a href='?m=iappCoupons&c=manages&a=init&menuid=".$_GET['menuid']."&catid=\$catid' target='right' onclick='open_list(this)'>\$catname</a></span>[<a target='right' href='?m=iappCoupons&c=manages&a=add&menuid=".$_GET['menuid']."&catid=\$catid' >添加</a>]";
			
			$strs2 = "<span class='folder'>\$catname</span>";
			
			$categorys = $tree->get_treeview(0,'category_tree',$strs,$strs2,$ajax_show);
		} else {
			$categorys = L('please_add_category');
		}

        include $this->admin_tpl('tree');
		exit;
	}
	
	/**
	 * 快速进入搜索
	 */
	public function public_ajax_search() {
		$content_db = pc_base::load_model('model_content_model');
		$modellist_t = getcache('modellist_t','commons');
		$modelid = $modellist_t['iappYp_company'][modelid];
		$content_db->set_model($modelid);
		
		if($_GET['catname']) {
			$field = 'title';
			$catname = trim($_GET['catname']);
			if (CHARSET == 'gbk') $catname = iconv('utf-8','gbk',$catname);
			
			$result = $content_db->select("$field LIKE('$catname%') ",'id,catid,title',10);
			if (CHARSET == 'gbk') {
				$result = array_iconv($result, 'gbk', 'utf-8');
			}
			echo json_encode($result);
		}
	}
}
?>