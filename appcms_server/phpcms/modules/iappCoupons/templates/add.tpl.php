<?php 
defined('IN_ADMIN') or exit('No permission resources.');
include $this->admin_tpl('header', 'admin');?>

<div class="pad-lr-10">

<fieldset>
	<legend>选择商家</legend>
	<table width="100%"  class="table_form">
  <tr>
    <th width="100">商家查询：</th>
    <td class="y-bg">
	<input type="text" size="41" id="cat_search" value="<?php echo L('请输入商户名称查询！');?>" onfocus="if(this.value == this.defaultValue) this.value = ''" onblur="if(this.value.replace(' ','') == '') this.value = this.defaultValue;">

	
	</td>
  </tr>
  <tr>
    <th >选择商家：</th>
    <td class="y-bg">
	
	<select name="business" size="10" style="width:500px;" id="business_div">
		<?php 
		if(is_array($data)){
			foreach($data as $r){
			  echo '<option value="'.$r[id].'"  catid="'.$r[catid].'" >'.$r[title].'</option>';
			}
		}
		?>
	</select>
	</td>
  </tr>
</table>
</fieldset>
<div class="bk15"></div>

<fieldset>
	<legend>添加优惠券</legend>
<form method="post" action="?m=iappCoupons&c=manages&a=add">
<table width="100%" cellpadding="0" cellspacing="1" class="table_form">
	<tr>
		<th width="100"><?php echo L('商家名称')?>：</th>
		<td >
			<span id="businessname">请在上面查询并选择商家！</span>
			<input value="" type="hidden" name="info[businessid]" id="businessid_id">
			<input value="" type="hidden" name="info[catid]" id="catid_id">
		</td>
	</tr>
	<tr>
		<th width="100"><?php echo L('标题')?>：</th>
		<td>
		<input type="text" name="info[title]" id="name"
			size="30" class="input-text">
		</td>
	</tr>
	<tr>
		<th width="100">长标题：</th>
		<td>
		<input type="text" name="info[ltitle]" id="ltitle"
			size="60" class="input-text">
		</td>
	</tr>
	
	<tr id="image_id">
		<th width="100"><?php echo L('优惠券图片')?>：</th>
		<td><?php echo form::images('info[image]', 'image', '', 'iappCoupons')?></td>
	</tr>
	
	<tr>
		<th width="100"><?php echo L('有效期从')?>：</th>
		<td>
			
				<?php echo form::date('info[start_time]',0,0,0,'false');?>到 &nbsp;<?php echo form::date('info[end_time]',0,0,0,'false');?>
		</td>
	</tr>
	<tr>
		<th width="100"><?php echo L('总量')?>：</th>
		<td><input type="text" name="info[num]" id="num"
			size="15" class="input-text">已领取：<input type="text" name="info[lynum]" id="lynum"
			size="15" class="input-text">已使用：<input type="text" name="info[synum]" id="synum"
			size="15" class="input-text">剩余数：<input type="text" name="info[lavenum]" id="lavenum"
			size="15" class="input-text">
			
			
			</td>
	</tr>
	<tr>
		<th width="100"><?php echo L('优惠券详情')?>：</th>
		<td>
		<textarea name="info[content]" id="content" cols="100"
			rows="6"><?php echo $info[content]?></textarea>
			</td>
	</tr>
	<tr>
		<th width="100"><?php echo L('使用须知')?>：</th>
		<td>
		<textarea name="info[Precautions]" id="Precautions" cols="100"
			rows="6"><?php echo $info[Precautions]?></textarea>
			</td>
	</tr>
	
	
	
	
		
	
	


	<tr>
		<td>&nbsp;</td>
		<td>
		<input type="submit" name="dosubmit" id="dosubmit" value="<?php echo L('提交')?>" class="button">&nbsp;<input type="reset" value=" <?php echo L('重写')?> " class="button"></td>
	</tr>
	
</table>
</form>
</fieldset>
</div>
<script type="text/javascript">
$().ready(
function(){
	$('#cat_search').keyup(
		function(){
			var value = $("#cat_search").val();
			if (value.length > 0){
				$.getJSON('?m=iappCoupons&c=manages&a=public_ajax_search', {catname: value}, function(data){
					if (data != null) {
						var str = '';
						$.each(data, function(i,n){
							str += '<option value="'+n.id+'" catid="'+n.catid+'">'+n.title+'</option>';
						});
						$('#business_div').html(str);
					} else {
						$('#business_div').html("");
					}
				});
			}
		}
	);
}
)


$("#business_div").change(function(){
  $("#businessname").html($(this).find("option:selected").text());
  $("#businessid_id").val($(this).attr("value"));
  $("#catid_id").val($(this).find("option:selected").attr("catid"));
 
  
});
</script>
</body>
</html>