/**
 * 获取系统
 */
function getDevice(){
	var device = localStorage['device'];
	if (typeof(device) == "undefined") {
		device = uexWidgetOne.getPlatform();
		if (typeof(device) != "undefined") {
			localStorage['device'] = device;
		}else{
			localStorage['device'] = 999;
		}
	}
	return parseInt(device);
}

/**
 * 用户
 */
function checkLogin(){
	var uid = getLocVal('u_id');
	if (parseInt(uid)>0) {
		return true;
	} else {
		uexWindow.cbConfirm = function(opId, dataType, data){
			if (data == 0) {
				openWin('login', 'login.html', '12');
			}
		}
		var mycars = ['确定', '取消'];
		uexWindow.confirm('提示', '请先登录', mycars);
		return false;
	}
}

function c_l(){
	var uid = getLocVal('u_id');
	return isDefine(uid);
}

function gologin()
{
	uexWindow.cbConfirm = function(opId, dataType, data){
		if (data == 0) {
			openWin('login', 'login.html', '12');
		}
	}
	var mycars = ['确定', '取消'];
	uexWindow.confirm('提示', '请先登录', mycars);
}

function logout(){
	localStorage.removeItem("u_id");
	localStorage.removeItem("u_nick");
	localStorage.removeItem("u_acctoken");
	localStorage.removeItem("u_mobile");
	localStorage.removeItem("u_integral");
	
	localStorage.removeItem['sina_connectid'];
	localStorage.removeItem['qzone_connectid'];
	localStorage.removeItem['qqweibo_connectid'];
}




/**
 * @param String inWndName 新窗口名称
 * @param String html		新窗口路径
 * @param String inAniID	打开动画
 * @param String f
 */
function openWin(inWndName,html,inAniID,f){
	if(inAniID)
		uexWindow.open(inWndName,'0',html,inAniID,'','',(f)?f:0);
	else
		uexWindow.open(inWndName,'0',html,0,'','',(f)?f:0);
}

function openUser(name,url,inAniID){
	if (c_l()) {
		uexWindow.open(name, '0', url, (inAniID)?inAniID:0, '', '', 0);
	}else{
		gologin();
	}
}

/**
 * 关闭窗口
 * @param string n 关闭窗口动画，默认-1
 */
function winClose(n){
	if(n){
		uexWindow.close(n);
		return;
	}
	if(parseInt(n)==0){
		uexWindow.close(n);
		return;
	}
	uexWindow.close(-1);
}

/**
 * 给DOM对象赋值innerHTML
 * @param String id 对象id或者对象
 * @param String html html字符串
 * @param String showstr 当html不存在时的提示语
 */
function setHtml(id, html,showstr) {
	var showval = isDefine(showstr)? showstr : "";
	if ("string" == typeof(id)) {
		var ele = $$(id);
		if (ele != null) {
			ele.innerHTML = isDefine(html) ? html : showval;
		}else{
			alert("没有id为"+id+"的对象");
		}
	} else if (id != null) {
		id.innerHTML = isDefine(html) ? html : showval;
	}
}

/**
 * 判断是否是空
 * @param value
 */
function isDefine(value){
    if(value == null || value == "" || value == "undefined" || value == undefined || value == "null" || value == "(null)" || value == 'NULL' || typeof(value) == 'undefined'){
        return false;
    }
    else{
		value = value+"";
        value = value.replace(/\s/g,"");
        if(value == ""){
            return false;
        }
        return true;
    }
}

/**
 * localStorage保存数据
 * @param String key  保存数据的key值
 * @param String value  保存的数据
 */
function setLocVal(key,value){
	window.localStorage[key] = value;
}

/**
 * 根据key取localStorage的值
 * @param Stirng key 保存的key值
 */
function getLocVal(key){
	if(window.localStorage[key])
		return window.localStorage[key];
	else
		return "";
}

/**
 * 清除缓存
 * @param Striong key  保存数据的key，如果不传清空所有缓存数据
 */
function clearLocVal(key){
	if(key)
		window.localStorage.removeItem(key);
	else
		window.localStorage.clear();
}

function setStorJson(objName, json){
	if(json) setLocVal(objName,JSON.stringify(json));
}

function getStorJson(objName){
	var ret = {};
	var str = getLocVal(objName);
	if(str) ret=JSON.parse(str);
	return ret;
}

/**
 * 获取经纬度
 */
function getLocation(){
    uexLocation.onChange=function (inLat,inLog){
		localStorage['lng']=inLog;
		localStorage['lat']=inLat;
        uexLocation.closeLocation();
    }
    uexLocation.openLocation();
}

/**
 * 去除字符串中的空格
 * @param String s
 */
function trim(str){ //删除左右两端的空格
	return str.replace(/(^\s*)|(\s*$)/g, "");
}
function ltrim(str){ //删除左边的空格
	return str.replace(/(^\s*)/g,"");
}
function rtrim(str){ //删除右边的空格
	return str.replace(/(\s*$)/g,"");
}

/**
 * json对象转为string
 * @param {Object} j
 */
function json2str(j){
	return JSON.stringify(j);
}
/**
 * string转为json对象
 * @param String s
 */
function str2json(s){
	return JSON.parse(s);
}

/**
 * 取input数据
 * @id: input标签id
 * 返回值：数据内容
 */
function getValue(id){
	var e = $$(id);
	if(e) return e.value;
}

/**
 * 设置input数据
 * @id: input标签id
 * @val: 要设置的数据
 */
function setValue(id, val){
	var e = $$(id);
	if(e) e.value = val;
}

/**
 * 创建DOM节点
 * @param String t
 */
function createEle(t){
	return document.createElement(t);
}
/**
 * 删除DOM节点
 * @param String id
 */
function removeNode(id){
	var e = $$(id);
	if(e) e.parentElement.removeChild(e);
}

/**
 * 调用本地浏览器打开网址
 * @param String url
 */
function loadLink(url){
	var appInfo = ''; 
	var filter = '';
	var dataInfo = url.toLowerCase();
	var pf = uexWidgetOne.platformName;
	if(pf=='android'){
		appInfo = 'android.intent.action.VIEW';
		filter = 'text/html';
	}
	if(dataInfo.indexOf('http://')<0 && dataInfo.indexOf('https://')<0){
		dataInfo = 'http://'+dataInfo;
	}
	uexWidget.loadApp(appInfo, filter, dataInfo);
}

/**
 * 下载文件
 *@String url_update 下载路径 
 *@String name   保存的文件名及格式 例：file.jpg
 *@funciton cb   下载成功后的回调函数
 **/
var did = 1000;
function down_file(url_update,name,cb){
	did++;
	if(getDevice()==1)
		var saveUpdate = "/sdcard/download/"+name;
	else
		var saveUpdate = "wgt://data/"+name;
	$toast("正在加载中...");
	uexDownloaderMgr.onStatus = function(opCode,fileSize,percent,status){
		switch(status) {
			case 0:
				logs(opCode+"=="+percent);
				var str = '下载进度：'+percent+'%';
				if(fileSize==-1) str = '下载中，请稍候...';
				uexWindow.toast('1','5', str,'');
			break;
			case 1:
				uexDownloaderMgr.closeDownloader(opCode);
				uexWindow.closeToast();
				cb(saveUpdate);
			break;
			case 2:
				uexDownloaderMgr.closeDownloader(opCode);
			break;;
		}
	}
	uexDownloaderMgr.cbCreateDownloader = function(opCode,dataType,data){
		if(data == 0){
			uexDownloaderMgr.download(opCode,url_update,saveUpdate,'0');
		}else{
			alert("创建失败");
		}
	}
	
	uexFileMgr.cbIsFileExistByPath=function(opId,dataType,data){
		if(isDefine(data)){
			cb(saveUpdate);
		}else{
			did++;
			uexDownloaderMgr.createDownloader(did);	
		}
	}
	uexFileMgr.isFileExistByPath(did,saveUpdate);
}

function gut(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
	var r = window.location.search.substr(1).match(reg);
	if (r != null) return unescape(r[2]); return null;
}

/**
*检查网络
*返回值：-1=网络不可用  0=WIFI网络  1=3G网络  2=2G网络
*/

function checkNet(cb){
	uexDevice.cbGetInfo=function (opCode,dataType,data){
        var device = eval('('+data+')');
		var connectStatus=device.connectStatus;
		if(isDefine(connectStatus)){
			cb(connectStatus);
			//if(connectStatus==-1){
			//	console.log('网络状态：网络不可用');
			//}else if(connectStatus==0){
			//	console.log('网络状态：WIFI网络'); 
			//}else if(connectStatus==1){
			//	console.log('网络状态：3G网络'); 
			//}else if(connectStatus==2){
			//	console.log('网络状态：2G网络');
			//}
		}
	}
	uexDevice.getInfo('13');
}


/**
*缓存图片
*/
var imgmod = '';
function dis_imgcache(sel,key,url,cb,err,dest,ext,head){
	var g_uid = getstorage('UID');
	if(g_uid){
		var simgid = 'showimage'+g_uid;
		imgmod = getstorage(simgid);
		if(imgmod!='true' && head){
			url = 'skin/images/noimg.png';
			key = url;
		}
	}
	zy_imgcache(sel,key,url,cb,err,dest,ext);
}

function imgLoadErr(id){
	var e = $$(id);
	if(e && e.style) e.style.cssText = "background-image: url(skin/images/noimg.png)";

}

function imgLoadSuc(id, src){
	var e = $$(id);
	if(e && e.style) e.style.cssText = "background-image: url("+src+")";
}

function imgLoadErrSrc(id){
	var e = $$(id);
	if(e) e.src = "skin/images/noimg.png";
}

function imgLoadSucSrc(id, src){
	var e = $$(id);
	if(e) e.src = src;
}


/*
 * 幻灯片
 */
var em_focus=1;
function zy_Switch(t,i){
	var Switch=$$('switch'),Em=Switch.getElementsByTagName('em');
	if(typeof(i)!='undefined'){
		em_focus=i;
	}
	Switch.querySelector('.focus').className ='';
	if(em_focus==Em.length){
		em_focus=0;
	}
	Em[em_focus].className='focus';
	t.moveToPoint(em_focus);
	em_focus++;
}

function zy_slide(){
    var switchTime;
	$$('slider').slide=new zySlide(
		'slider',
		'H',
		function(){
			window.clearInterval(switchTime);
			zy_Switch(this,this.currentPoint);
			var t=this;
			switchTime=window.setInterval(function(){zy_Switch(t);},5000);
		},
		false,
		function(e){
		}
	);
	switchTime=window.setInterval(function(){zy_Switch($$('slider').slide);},5000);
}


function set_stab(model, l_data){
	var _html = '';
	
	
	
	
	/*
	 var ln = 0;
	 for (var k in l_data) {
	 	ln++;
	 }
	 if (ln > 0) {
		 var page = Math.ceil(ln/4);
		 var i = 1;
		 var j = 0;
		 for (k in l_data) {
		 
			 if (i % 4 == 1) {
			 	_html += '<div class="item">';
			 }
			 
			 var cur_css = l_tab_default[model] == k ? 'class="cur"' : '';
			 _html += '<div id="' + k + '" ' + cur_css + ' ontouchstart="zy_touch(\'down\')" onclick="stab(\'' + k + '\',\'' + l_data[k]['model'] + '\',\'' + l_data[k]['catid'] + '\');" >' + l_data[k]['title'] + '</div>';
			 
			 if (i % 4 == 0) {
				 _html += '</div>';
				 j++;
			 }
		 
		 	i++;
		 }
		 
		 //alert(j+'|'+page)
		 if(j<page){
		 	_html += '</div>';
		 }
		 _html = '<div class="items" id="stab">'+_html+'</div>';
	 
	 }
	 
	 
	 
	 
	 */
	 
	 
	
	var ln = 0;
	for (var k in l_data) {
		ln++;
	}
	
	if (ln > 0) {
		for (k in l_data) {
			var cur_css = l_tab_default[model] == k ? 'class="cur"' : '';
			_html += '<div id="' + k + '" ' + cur_css + ' ontouchstart="zy_touch(\'down\')" onclick="stab(\'' + k + '\',\'' + l_data[k]['model'] + '\',\'' + l_data[k]['catid'] + '\');" ><span>' + l_data[k]['title'] + '</span></div>';
		}
		 _html = '<div class="item" id="item" style="left: 0px;">'+_html+'</div>';
	}
	_html = '<div class="items" id="items">'+_html+'</div>';
	
	
	/*
		
	_html = '<div id="filter_new" class="cur" ontouchstart="zy_touch(\'cur\')" onclick="filter_order(\''+model+'\',\'new\',\'filter_new\')">最新</div>'+
				'<div id="filter_hot" ontouchstart="zy_touch(\'cur\')" onclick="filter_order(\''+model+'\',\'hot\',\'filter_hot\')">人气</div>'+
				'<div id="filter_catid" ontouchstart="zy_touch(\'cur\')" onclick="filter_open(\'' + model + '\',\''+model+'\');">筛选</div>';
	
	*/
	
	return _html;
}



/*
 * tab切换，带catid
 * 当前id
 * 分类
 */
function mtab(k,model,filter){
	var divs=$$('mtab').getElementsByTagName("div");
	for(var i=0;i<divs.length;i++)
	{
		if(divs[i].id ==k)
		{
			$$(divs[i].id).className='cur';
		}else{
		 	$$(divs[i].id).className='';
		}
		
		if(filter==1){
			var _s_model =  eval('l_'+k);
			$$("stab").innerHTML = set_stab(k,_s_model);
		}else{
			$$("stab").innerHTML = '';
		}
	}
	zy_con("content", k+"_content.html", 0, $$("header").offsetHeight);
}

function stab(k,model,catid){
	var divs=$$('item').getElementsByTagName("div");
	for(var i=0;i<divs.length;i++)
	{
		if(catid>0){
			localStorage[model+'_catid'] = catid;
		}else{
			localStorage.removeItem(model+'_catid');
		}		
		
		if(divs[i].id ==k)
		{
			$$(divs[i].id).className='cur';
		}else{
		 	$$(divs[i].id).className='';
		}
	}
	zy_con("content", model+"_content.html", 0, $$("header").offsetHeight);
}

function filter(model){
	localStorage['filter_p_url'] = model;
	
	var data = eval('l_'+model);
	var html='';
	for(var k in data){
		if (url_tab(model) == k) {
			if (data[k]['filter'] == 1) {
				html = setfilter(data[k]['model'],model);
				
				/*
				'<div id="filter_new" class="cur" ontouchstart="zy_touch(\'cur\')" onclick="filter_order(\''+data[k]['model']+'\',\'new\',\'filter_new\')">最新</div>'+
				'<div id="filter_hot" ontouchstart="zy_touch(\'cur\')" onclick="filter_order(\''+data[k]['model']+'\',\'hot\',\'filter_hot\')">人气</div>'+
				'<div id="filter_catid" ontouchstart="zy_touch(\'cur\')" onclick="filter_open(\'' + data[k]['model'] + '\',\''+model+'\');">筛选</div>';
				*/
				
			}
		}
	}
	$$("filter").innerHTML = html;
}



/**
 * 头部
 * type 【default默认(返回按钮加标题)、content文章(tab)、life生活(tab)、talk说说、news新闻(tab)、coupons优惠券、activity活动、comment评论分享、index首页、title只有标题】
 * title 标题
 */
function setHead(type,title){
	var headhtml ='<div id="header">';
	
	var lhtml = '';
	if (typeof(l_footer[type])!= "undefined") {
		if (pattern_type == "winphone") {
			lhtml += '<span class="l icon back" ontouchstart="zy_touch(\'act\')" onclick="uexWindow.close(-1);"></span>';
		}else if(pattern_type == "simple") {
			lhtml += '<span class="l icon flt" ontouchstart="zy_touch(\'act\')" onclick="toggleMenu();"></span>';
		}
	}else{
		lhtml += '<span class="l icon back" ontouchstart="zy_touch(\'act\')" onclick="uexWindow.close(-1);"></span>';
	}
	
	switch (type) {
		case 'content':
			headhtml += '<div class="title">' + lhtml;
			headhtml += '<div id="mtab" class="tab">';
			var _sh = '';
			for (var k in l_content) {
				var css = '';
				if (url_tab('content') == k) {
					css = 'class="cur"';
					if (l_content[k]['filter'] == 1) {
						var _m = eval('l_'+l_content[k]['model']);
						_sh = set_stab(l_content[k]['model'],_m);
					}
				}
				headhtml += '<div id="' + k + '" '+css+' ><span ontouchstart="zy_touch(\'down\')" onclick="mtab(\'' + k + '\',\'' + type + '\',\'' + l_content[k]['filter'] + '\');">' + l_content[k]['title'] + '</span></div>';	
			}
			
			headhtml += '</div>';
			headhtml += '</div>';
			headhtml += '<div class="stab" id="stab">' + _sh + '</div>';
			break;
		case 'life':
			headhtml += '<div class="title">' + lhtml;
			headhtml += '<div id="mtab" class="tab">';
			var _sh = '';
			for (var k in l_life) {
				var cur_css = '';
				if (url_tab('life') == k) {
					cur_css = 'class="cur"';
					if (l_life[k]['filter'] == 1) {
						var _m = eval('l_'+l_life[k]['model']);
						_sh = set_stab(l_life[k]['model'], _m);
					}
				}
				
				headhtml += '<div id="' + k + '" '+cur_css+' ><span ontouchstart="zy_touch(\'down\')" onclick="mtab(\'' + k + '\',\'' + type + '\',\'' + l_life[k]['filter'] + '\');">' + l_life[k]['title'] + '</span></div>';

			}
			headhtml += '</div>';
			headhtml += '</div>';
			headhtml += '<div class="stab" id="stab">' + _sh + '</div>';
			break;
		case 'talk':
			headhtml += '<div class="title">' + lhtml;
			headhtml += '<span id="talkup" class="up"></span>';
			headhtml += '<span class="txt">' + title + '</span>';
			headhtml += '<span class="r icon talk" ontouchstart="zy_touch(\'act\')" onclick="openUser(\'talk_ask\',\'talk_ask.html\',2);"></span>';
			headhtml += '</div>';
			break;
		
		case 'news':
			headhtml += '<div class="title">' + lhtml;
			headhtml += '<span class="txt">' + title + '</span>';
			headhtml += '</div>';
			
			headhtml += '<div class="stab" id="stab">';
			headhtml += set_stab('news', l_news);
			headhtml += '</div>';
			break;

		case 'picture':
			headhtml += '<div class="title">' + lhtml;
			headhtml += '<span class="txt">' + title + '</span>';
			headhtml += '</div>';
			
			headhtml += '<div class="stab" id="stab">';
			headhtml += set_stab('picture', l_picture);
			headhtml += '</div>';
			break;
		case 'video':
			headhtml += '<div class="title">' + lhtml;
			headhtml += '<span class="txt">' + title + '</span>';
			headhtml += '</div>';
			
			headhtml += '<div class="stab" id="stab">';
			headhtml += set_stab('video', l_video);
			headhtml += '</div>';
			break;
		case 'jifen':
			headhtml += '<div class="title">' + lhtml;
			headhtml += '<span class="txt">' + title + '</span>';
			headhtml += '</div>';
			
			headhtml += '<div class="stab" id="stab">';
			headhtml += set_stab('jifen', l_jifen);
			headhtml += '</div>';
			
			break;
		case 'more':
			var _lh = '';
			if (pattern_type == "winphone") {
				_lh += '<span class="l icon back" ontouchstart="zy_touch(\'act\')" onclick="uexWindow.close(-1);"></span>';
			}else if(pattern_type == "simple") {
				_lh += '<span class="l icon flt" ontouchstart="zy_touch(\'act\')" onclick="toggleMenu();"></span>';
			}
			
			if (_lh != '') {
				headhtml += '<div class="title">' + _lh;
				headhtml += '<span class="txt">' + title + '</span>';
				headhtml += '</div>';
			}
			break;
		case 'main':
			headhtml += '<div class="title">' + lhtml;
			headhtml += '<span class="txt">' + title + '</span>';
			headhtml += '</div>';
			break;
		default:
			headhtml += '<div class="title">' + lhtml;
			headhtml += '<span class="txt">' + title + '</span>';
			headhtml += '</div>';
			break;
	}
	
	headhtml += '</div>';
	document.write(headhtml);
}

/**
 * url处理，如果设置了model_tab 则用model_tab，否则用默认
 */
function url_tab(model){
	var data = eval('l_'+model);
	var url = data[l_tab_default[model]]['model'];
	if (typeof(localStorage[model+'_tab']) != "undefined") {
		url = localStorage[model+'_tab'];
	}
	return url;
}

/**
 * 默认栏目处理
 */
function catid_cur(model){
	var catid = l_catid_default[model];
	if (typeof(localStorage[model+'_catid']) != "undefined") {
		catid = localStorage[model+'_catid'];
	}
	return catid;
}

function setContent(){
	var html ='<div id="content"></div>';
	document.write(html);
}

/*
 * 底部菜单
 * 当前id
 */
function setFooter(id){
	if (typeof(pattern_type) != "undefined" && pattern_type == "portal" && typeof(l_footer[id])!= "undefined") {
		var footerhtml = '<div id="footer"><ul>';
		for (k in l_footer) {
			if (id == l_footer[k]['model']) {
				footerhtml += '<li><div class="cur ' + l_footer[k]['model'] + ' "></div><span class="cur">' + l_footer[k]['title'] + '</span></li>';
			}
			else {
				var url = l_footer[k]['model'];
				footerhtml += '<li ontouchstart="zy_touch(\'down\')" onclick="open_fw(\'' + k + '\');"><div class="' + l_footer[k]['model'] + '"></div><span>' + l_footer[k]['title'] + '</span></li>';
			}
		}
		footerhtml += '</ul></div>';
		document.write(footerhtml);
	}
}

function open_fw(m){
	if(l_footer[m]['islogin']==1){
		if (!c_l()) {
			gologin();
			return false;
		}
	}
	var url = l_footer[m]['model'];
	openWin(url,url+'.html');
}



function filter_open(model,filter_p_url){
	localStorage['filter_p_url'] = filter_p_url;
	var divs=$$('filter').getElementsByTagName("div");
	for(var i=0;i<divs.length;i++)
	{
		if(divs[i].id !='filter_catid')
		{
			$$(divs[i].id).className='';
		}else{
		 	$$(divs[i].id).className='cur';
		}
	}
	
	var e = $$("header");
	var y = e.offsetHeight;
	var s = window.getComputedStyle($$("content"),null);
	localStorage['l_filter_cur'] = 'l_'+model;
	uexWindow.openPopover('filter','0','filter.html','',0,int(y),int(s.width),int(s.height),int(s.fontSize),'1');
}

function filter_catid(model,catid){
	localStorage[model+'_catid'] = catid;
	uexWindow.closePopover('filter');
	zy_con("content", model+"_content.html", 0, $$("header").offsetHeight);
}

function filter_order(model, order,divid){
	uexWindow.closePopover('filter');
	var divs=$$('filter').getElementsByTagName("div");
	for(var i=0;i<divs.length;i++)
	{
		if(divs[i].id !=divid)
		{
			$$(divs[i].id).className='';
		}else{
		 	$$(divs[i].id).className='cur';
		}
	}
	
    localStorage[model+'_order'] = order;
	zy_con("content", model+"_content.html", 0, $$("header").offsetHeight);
}


function filter_order_pro(model){
	var order = 'new';
	if (typeof(localStorage[model+'_order']) != "undefined") {
		order = localStorage[model+'_order'];
	}
	return order;
}

/*
 * 版本比较
 * 版本号
 */
function k_v(v){
	var a = version.replace(".","");
	var b = v.replace(".","");
	return a<b? true:false;
}

/*
 * 抽屉处理
 */
var showStatus =0;
function toggleMenu(){
	var w = parseInt(window.innerWidth);
	var h = parseInt(window.innerHeight);
	
	var wl = (w*80)/100;
	var wr = (w*20)/100;

	if(showStatus==0){
		showStatus=1;
		uexWindow.setWindowFrame(wl, 0, 300);
		//uexWindow.open('menu','0','menu.html',0,'','',0);
		
		//uexWindow.openPopover('menu',0,'menu.html','',-wl,0,w,h,'',0);
		//uexWindow.bringPopoverToFront('menu');
		
		
		uexWindow.openPopover('menu_pop',0,'menu_pop.html','',0,0,wr,h,'',0);
	}else{
		showStatus=0;
		uexWindow.setWindowFrame(0, 0, 300);
		uexWindow.closePopover('menu_pop');
		//uexWindow.closePopover('menu');
	}
}


function showclass(){
		var pn =0;
		
		$$('stab').gesture=new zyEvent('stab',
			function(me,dx,dy){//移动过程中事件

				var _item = $$('item').getElementsByTagName('div');
	
				var mw = 0;
				for (k in _item) {
					if (typeof(_item[k]) == "object") {
						var w = parseInt(_item[k].offsetWidth);
						mw += w;
					}
				}
				
				var lw = $$("item").style.left;
				    lw=(lw).substr(0,(lw.length-2)); 
					lw=parseInt(lw);
					lw= lw + dx;

				var wa = parseInt($$("items").offsetWidth);
				var wb = parseInt($$("item").offsetWidth);
				
				if(mw > wa){
					if(lw >0){
						lw = 0;
					}else if(lw < -(mw-wa)){
						lw = -(mw-wa);
					}
					$$("item").style.left = lw+'px';
				}
			},
			function(me,dx,dy,t){//手指松开后事件
				
				//alert('dx:'+dx+' dy:'+dy+' me:'+me+' t:'+t);
			},
			false,
			function(e){//对象动画结束事件
				alert('root');
			}
		);
	}
	

