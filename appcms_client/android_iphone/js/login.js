var Users = function(data){}
Users.prototype={
	//检查是否登录
	checkUser: function(){
		if(!localStorage['u_id']||localStorage['u_id']==0||!localStorage['u_acctoken']||!localStorage['u_nick']||localStorage['u_nick']==''){
			return false;
		}else{
			return true;
		}
	},
	//退出登录
	logout : function(){
		localStorage.removeItem("u_id");
		localStorage.removeItem("u_nick");
		localStorage.removeItem("u_acctoken");
		localStorage.removeItem("u_mobile");
		localStorage.removeItem("u_integral");

		this.removesina();
		this.removeqzone();
		this.removeqqweibo();
	},
	
	removesina: function(){
		localStorage.removeItem['sina_connectid'];
	},
	removeqzone: function(){
		localStorage.removeItem['qzone_connectid'];
	},
	removeqqweibo: function(){
		localStorage.removeItem['qqweibo_connectid'];
	},
};