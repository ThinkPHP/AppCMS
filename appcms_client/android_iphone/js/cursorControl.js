
function cursorControl(a){
    this.element=a;
    this.range=!1;
    this.start=0;
    this.init();
};
cursorControl.prototype={
    init:function(){
        var _that=this;
        this.element.onkeyup=this.element.onmouseup=function(){
            this.focus();
            if(document.all){
                _that.range=document.selection.createRange();
            }else{
                _that.start=_that.getStart();
            }
        }
    },
    getType:function(){
        return Object.prototype.toString.call(this.element).match(/^\[object\s(.*)\]$/)[1];
    },
    getStart:function(){
        if (this.element.selectionStart || this.element.selectionStart == '0'){  
            return this.element.selectionStart; 
        }
    },
    insertText:function(text){
        this.element.focus();  
        if(document.all){
            document.selection.empty();  
            this.range.text = text;  
            this.range.collapse();  
            this.range.select();
        }  
        else{
            if(this.getType()=='HTMLDivElement'){
				var sel = window.getSelection();
				var rang = sel.rangeCount > 0 ? sel.getRangeAt(0) : null;
				if (rang == undefined 
					|| rang == null
					|| (rang.commonAncestorContainer.id !="editdiv"
						&& rang.commonAncestorContainer.parentNode.id !="editdiv")){
					this.element.focus();
					rang = document.createRange();
					rang = selectNode(this.element);
					rang.setStart(range.getEndContainer, rang.endOffset);
				}
				rang.deleteContents();
				rang.insertNode(rang.createContextualFragment(text));
				var tempRange = document.createRange();
				var a = document.getElementById("editdiv")
				tempRange.selectNodeContents(a);
				if(rang.commonAncestorContainer.id == "editdiv"){
					tempRange.setStart(rang.endContainer, rang.endOffset+1);
					tempRange.setEnd(rang.endContainer, rang.endOffset+1);
				} else {
					tempRange.setStartAfter(rang.endContainer.nextSibling);
					tempRange.setEndAfter(rang.endContainer.nextSibling);
				}
				sel.removeAllRanges();
				sel.addRange(tempRange);
				this.element.focus();

            }else{
                this.element.value=this.element.value.substr(0,this.start)+text+this.element.value.substr(this.start);
            };
        } 
    },
    getText:function(){
        if (document.all){  
            var r = document.selection.createRange();  
            document.selection.empty();  
            return r.text;  
        }  
        else{  
            if (this.element.selectionStart || this.element.selectionStart == '0'){
                var text=this.getType()=='HTMLDivElement'?this.element.innerHTML:this.element.value;
                return text.substring(this.element.selectionStart,this.element.selectionEnd); 
            } 
            else if (window.getSelection){  
                return window.getSelection().toString()
            };  
        }  
    }
};


function phiz(div){
	var html = '';
	for (i = 1; i < 36; i++) {
		html +='<div onClick="fn2(\'<img src=skin/phiz/'+i+'.gif >\')" ><img src="skin/phiz/'+i+'.gif"></div>';
	}
	$$(div).innerHTML = html;
}

 