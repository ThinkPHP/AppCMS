# AppCMS说明 #


### 服务端 appcms_server

appcms_server为appcms服务端程序，依赖于phpcms，属于phpcms独立插件；
把appcms_server目录下所有文件复制到phpcms根目录，访问 http://域名/iappInstall/ 根据提示操作即可安装。


### 客户端 appcms_client

appcms_client为appcms客户端程序，使用appcan开发模式，app打包流程如下：
 
1. 到appcan官方网站 [注册账号](http://dashboard.appcan.cn/register) ，进入开发管理创建应用，获取应用id、应用key备用；
2. 到appcan官网下载appcan ide并默认安装；
3. 用ide新建appcan项目，需要填写第一步获取到的应用id、应用key；
4. 把appcms_client/android_iphone下除config.xml外的所有文件复制到刚新建项目的phone目录下，客户端部署完成;
5. 绑定网站,修改 /appcms_client/android_iphone/js/config.js 文件第一行 网址为自己的网址；
6. ide上找到【发行>>app打包】即可对客户端进行打包及二次开发；


### 帮助文档 readme


[APPCMS程序官网](http://www.appcms.org) 


```
PS：由于官方宣布“appcms从v1.1.3起开放所有商业版功能，同时开放所有源码，全面支持二次开发，以后版本不再有个人版、商业版的区别，此后只有一个版本的appcms，免费用户与商业用户软件功能完全一致！”，所以我只是个搬运工。
```